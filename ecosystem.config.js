module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    // First application
    {
      name: "middleware-video",
      script: "middleware/video/dist/server.js",
      watch: false,
      ignore_watch: ["node_modules", "newrelic_agent.log", ".git"],
      env: {
        NODE_ENV: "development",
        PORT: 4000
      },
      env_test: {
        NODE_ENV: "test",
        PORT: 4000
      },
      env_pre: {
        NODE_ENV: "pre",
        PORT: 4000
      },
      env_uat: {
        NODE_ENV: "uat",
        PORT: 4000
      },
      env_production: {
        NODE_ENV: "production",
        PORT: 4000
      }
    },
    /* -- no se usa porque no ha music
    {
      name      : 'middleware-music',
      script    : 'middleware/music/src/server/server.js',
      watch     : false,
      ignore_watch : ["node_modules","newrelic_agent.log", ".git"],
      env: {
        NODE_ENV: 'development',
        PORT: 3000,
      },
      env_production : {
        NODE_ENV: 'production',
        PORT: 3000,
      }
    },
    */
    {
      name: "stvapp",
      script: "server.js",
      watch: false,
      ignore_watch: ["node_modules", "newrelic_agent.log", ".git"],
      env: {
        NODE_ENV: "development",
        PORT: 8080
      },
      env_test: {
        NODE_ENV: "test",
        PORT: 8080
      },
      env_pre: {
        NODE_ENV: "pre",
        PORT: 8080
      },
      env_uat: {
        NODE_ENV: "uat",
        PORT: 8080
      },
      env_production: {
        NODE_ENV: "production",
        PORT: 8080
      }
    }
  ]
};
