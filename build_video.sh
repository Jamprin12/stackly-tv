#!/usr/bin/env bash

VIDEO_PATH=./middleware/video
cd ${VIDEO_PATH} && npm install && npm run build
