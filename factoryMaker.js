/**
 * Explora un directorio recursivamente para leer los archivos y generar la factoría para importación dinámica.
 *
 * @author Erik Dávila
 * @email edgar.davila@ironbit.com.mx
 * @param {String} dir
 * @param {Function} done
 */
const fs = require('fs');
const path = require('path');
console.log('Generando para dispositivo:', process.argv[2] || 'todos')
let pack = process.argv[2] || '';
function fileBrowser(dir, done) {
    let results = [];
    fs.readdir(dir, function(err, list) {
        if (err) return done(err);
        var pending = list.length;
        if (!pending) return done(null, results);
        list.forEach(function(file){
            file = path.resolve(dir, file);
            fs.stat(file, function(err, stat){
                // si es directorio, ejecuta una llamada recursiva para recorrerlo
                if (stat && stat.isDirectory()) {
                    fileBrowser(file, function(err, res){
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    //lectura de archivos que que conformarán la factoría
                    fs.readFile(file, 'utf-8', (err, data) => {
                        if(err) {
                            console.log('error: ', err);
                        } else {
                            //detectamos que el archivo contenga las lineas "class nombreClase extends"
                            let module =/[^]class([^]*)extends/g.exec(data);
                            let moduleDir = '';
                            if (module) {
                                //sustituimos las diagonales invertidas por diagonales normales
                                if(file.indexOf("\\"  ) != -1) {
                                  //de la ruta completa, obtenemos la relativa que será a partir del directorio devices.
                                  moduleDir = /\\devices.*/g.exec(file);
                                  moduleDir = moduleDir[0].replace(/\\/gi, "/");
                                }
                                else {
                                  moduleDir = /\/devices.*/g.exec(file);
                                  moduleDir = moduleDir;
                                }
                                //generamos el arreglo de resultados con las rutas y el nombre de el módulo a importar
                                results.push([`import ${module[1]} from '..${moduleDir}'`, module[1]]);
                            }
                        }
                        if (!--pending) done(null, results);
                    });
                }
            });
        });
    });
};
fileBrowser("./src/devices/"+ pack, function(err, data){
    if (err){
        throw err;
    }
    let routes='',
        modules='';
    //ruta de nuestra factoría
    let factoryDir = './src/utils/dynamicClass.js';
    for (var i = 0; i < data.length; i++) {
        //extraemos de nuestro arreglo las cadenas de texto y damos el formato para guardarlas
        routes = `${routes}${data[i][0]};\n`;
        modules = `${modules}${data[i][1]},`;
    }
    //generamos el objeto de clases que servirá para dinamizar la factoría
    modules = `const classes = {${modules}};`;
    //leyendo la factoría para insertar datos en ella adelante.
    fs.readFile(factoryDir, 'utf-8', (err, data) => {
        if (err) {
            data = "import  basic  from 'basic';\nconst classes = { };\n\rexport default function dynamicClass (className, platform) {\n    platform = platform.replace(\/\\b\\w/g, l => l.toUpperCase());\n    return new classes[`${platform}${className}`];\n}"
            fs.writeFile(factoryDir, data, function(error) {
                if (error) {
                    console.error("Error de escritura:  " + error.message);
                } else {
                    console.log("se ha creado el archivo: " + factoryDir);
                }
            });
        }
        //detectamos con la regex la sección que se va a sobreescribir en nuestra factoría.
        data = data.replace(/import.([^]*).};/g, `${routes}\n${modules}`);
        //guardando factoría con los datos obtenidos
        fs.writeFile(factoryDir, data, function(error) {
            if (error) {
                console.error("Error de escritura:  " + error.message);
            } else {
                console.log("Factoría generada en: " + factoryDir);
            }
        });
    });
});
