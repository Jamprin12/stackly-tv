#
#
# DOCUM
#
#
import json
import logging
import logging.config
import argparse
import os  # file system
import shutil  # dirs
from collections import OrderedDict  # order
from yattag import Doc  # report

# ---------------------------------------------------------------
config = {
    'title'     : 'FAAdocs',
    'file_index': {
        'index.js',
        'index.jsx'
    },
    'file_exts' : {
        '.js',
        '.jsx'
    }
}
todos = {}
todos_used = {}


#
# MAIN
# ---------------------------------------------------------------
def main():
    global config
    global todos

    #
    # CONFIG
    #
    logging.config.fileConfig('logging.conf')
    logging.info('INIT')
    #
    parser = argparse.ArgumentParser(description='Documentador')
    parser.add_argument('--config_file', type=argparse.FileType('r', encoding='UTF-8'), help='Archivo de configuración')
    parser.add_argument('source', help='Code source')
    parser.add_argument('target', help='Report folder (must exist)')
    parser.add_argument('--dry-run', action="store_true", default=False, dest='dry_run', help='Usado para simulación')
    parameters = parser.parse_args()
    #
    if parameters.config_file is not None:
        logging.info("Config file = [%s]" % parameters.config_file.name)
        config = json.load(parameters.config_file)
    config['dirs'] = {
        'source'     : parameters.source,
        'source_from': len(parameters.source) + 1,
        'target'     : parameters.target,
        'target_from': len(parameters.target) + 1
    }
    print('-' * 40)
    print('CONFIG: %s' % config)
    print('SOURCE: %s' % parameters.source)
    print('-' * 40)

    #
    # REAL
    #
    discovery(parameters.source, 1)
    todos = OrderedDict(sorted(todos.items()))
    print('-' * 40)
    print("Discovered %s modules." % len(todos))
    build_used_by()  # USED BY
    print("Used by %s modules." % len(todos_used))
    print('-' * 40)

    build_report(parameters.source, parameters.target)
    # #####
    logging.info('END')


#
# DISCOVERLAND
# ---------------------------------------------------------------
def discovery(path, contador):
    global todos
    current = discover(path)
    todos[path] = current
    for p in current:
        if p['check']:
            if p['file_path'] not in todos:
                nuevocont = contador + 1
                discovery(p['file_path'], nuevocont)
            p['checked'] = True
    return None


def discover(path):
    result = []
    index = get_index_from_dir(path)
    if index:
        imports = file_get_imports(index)
        for im in imports:
            result.append(format_import(os.path.relpath(index.name), im))
    return result


#
# USED BY
# ---------------------------------------------------------------
def build_used_by():
    global todos
    global todos_used
    for f in todos:
        for i in todos[f]:
            if i['checked']:
                if not i['file_path'] in todos_used:
                    todos_used[i['file_path']] = {}
                todos_used[i['file_path']][f] = todos[f]


#
# FILE SYSTEM
# ---------------------------------------------------------------
def get_index_from_dir(source):
    if os.path.isfile(source):
        # print('FOUND> %s' % source)
        extension = os.path.splitext(source)[1]
        if extension == '.js' or extension == '.jsx':
            return open(source, 'r')
        else:
            print('NOT LOADING --[', extension, '] from', source)
            return None
    for ext in config['file_exts']:
        propose = os.path.abspath(source + ext)
        if os.path.isfile(propose):
            # print('FOUND> %s' % propose)
            return open(propose, 'r')
    for file in config['file_index']:
        propose = os.path.abspath(os.path.join(source, file))
        if os.path.isfile(propose):
            # print('FOUND> %s' % propose)
            return open(propose, 'r')
    print('PANIC! FILE NOT FOUND', source)
    return None


#
# PARSER
# ---------------------------------------------------------------
def file_get_imports(in_file):
    imports = []
    buffer = ""
    in_buffer = False
    try:
        for line in in_file:
            words = line_to_array(line)
            # manejo de multiline --
            if not in_buffer:
                if has_import(words) and exists_open_brace(words) and not exists_close_brace(words):
                    in_buffer = True
                    buffer = clear_crlf(line)
                    continue  # al proximo
            else:
                buffer += clear_crlf(line)
                if exists_close_brace(words):
                    in_buffer = False
                    words = line_to_array(buffer)
                    buffer = ""
                else:
                    continue  # al proximo
            # manejo de la linea --
            if has_import(words):
                imports.append((line, words))
        return imports
    except Exception as error:
        print('ERROR -- not loading ', in_file.name, error)
        return imports


def line_to_array(line):
    words = line.strip() \
        .replace(",", "") \
        .replace(";", "") \
        .replace("\t", ' ') \
        .replace('  ', ' ') \
        .split(" ")  # parsing
    words = [w for w in words if not w == '']  # - palabras vacias
    return words


def clear_crlf(line):
    return line.replace("\n", "").replace("\r", "")


def has_import(words):
    return len(words) > 0 and words[0] == 'import'


def exists_open_brace(ar):
    return ar.count('{') > 0


def exists_close_brace(ar):
    return ar.count('}') > 0


def format_import(file_name, im):
    global config
    global conf_to
    conf_to = os.path.abspath(config['dirs']['target'])
    resultado = {
        "source"   : os.path.normpath(file_name),
        "modulos"  : [],
        "file"     : "",
        "file_path": "",
        "check"    : False,
        "checked"  : False,
        "code_line": im[0]
    }
    words = im[1]
    if not words[0] == 'import':
        return resultado

    for w in words:
        if w == 'import' or w == '{' or w == '}':  # no el import
            continue
        if w == '//' or w == '/*':  # no el comentario
            break
        if w == 'from':  # no el from
            continue
        if w[0] == '"' or w[0] == "'":  # el from del import
            resultado['file'] = w
            continue
        resultado['modulos'].append(w)  # soy modulo

    resultado['file'] = resultado['file'].replace('"', '').replace("'", "")
    if resultado['file'][0] == ".":
        file_path = os.path.normpath(os.path.join(os.path.dirname(file_name), resultado['file']))

        source_from = config['dirs']['source_from']
        component = get_module_dir(file_path);

        resultado['check'] = True
        resultado['file_path'] = file_path
        resultado['path_to'] = os.path.join("..", component, path_to_filename(file_path[source_from:]))
    return resultado


#
# REPORT
# ---------------------------------------------------------------
def build_report(src_dir, target_dir):
    """
    Construye todos los reportes
    :param src_dir: directorio del codigo
    :param target_dir: directorio de la salida html
    :return: None
    """
    print("Building reports")
    # parameters
    modulos = ['containers', 'components', 'requests', 'utils', 'otros']
    if not os.path.isdir(target_dir):
        print('ERROR -- Target not found ', target_dir)
        return
    # Directories
    dirs = {'base': target_dir}
    for d in modulos:
        dirs[d] = os.path.join(target_dir, d)
        make_dir(dirs[d])
        if not os.path.isdir(dirs[d]):
            print('ERROR -- Cannot create directory [%s].' % dirs[d])
            return
    for d in dirs:
        print('  dir:%s = %s' % (d, dirs[d]))

    #
    # reports
    report_index(modulos, src_dir, dirs['base'])
    report_modulos(modulos, dirs)


#
# REPORT INDEX
def report_index(modulos, src_dir, dir_base):
    print("- Report Index")
    links = "\n\n\n".join([report_links_index(modulos, d, src_dir) for d in modulos])
    menu = "&nbsp;&nbsp;" + " | ".join(
        ["<a href='#%s'>%s</a>" % (d, d) for d in modulos])
    index_html = html_html(html_head('Inicio'), html_body(html_header("FAA-Docs", menu) + links))
    save_to(os.path.join(dir_base, "index.html"), index_html)


def report_links_index(modulos, objecttype, base_path):
    doc, tag, text = Doc().tagtext()
    path_from = len(base_path) + 1
    containers = [t for t in todos if
                  is_a_module(t, objecttype, modulos) and len(todos[t]) > 0]
    doc.asis('<a name="%s"></a><br/>' % objecttype)
    with tag('h3'):
        text(objecttype.capitalize())
    with tag('ul'):
        for t in containers:
            with tag('li'):
                # print(path_from, t[path_from:], "|", path_to_title(t), "|")
                doc.asis(html_link(
                    os.path.join(objecttype, path_to_filename(t[path_from:])),
                    path_to_title(t, ": ")
                ))
    return doc.getvalue()


#
# REPORT FILES
def report_modulos(modulos, dirs):
    for modulo in modulos:
        print("- Report Modulo %s" % modulo.capitalize())
        items = [t for t in todos if is_a_module(t, modulo, modulos) and len(todos[t]) > 0]
        report_files(items, dirs[modulo])


def is_a_module(t, modulo, all_modulos):
    if modulo != 'otros':
        return t.find(modulo) > -1
    else:
        encontrado = False
        for m in all_modulos:
            if (not m == 'otros') and t.find(m) > -1:
                encontrado = True
        # if not encontrado:
        # 	print(modulo, encontrado, t)
        return not encontrado


def report_files(data, dir_modulo):
    global config
    path_from = config['dirs']['source_from']
    for ct in data:
        file_title = path_to_title(ct)
        file_header = html_header(file_title)
        file_modulos = report_imports_table(ct)
        file_usedby = report_used_in(ct)
        file_cont = html_html(html_head(file_title, 1), html_body(file_header + file_modulos + file_usedby))
        file_name = os.path.join(dir_modulo, path_to_filename(ct[path_from:]))
        save_to(file_name, file_cont)  # save


def report_used_in(key):
    global config
    global todos_used
    doc, tag, text = Doc().tagtext()
    used_by = todos_used[key] if key in todos_used else {}
    with tag('h3'):
        text('Used in %s files' % len(used_by))
    if len(used_by) > 0:
        doc.asis(report_usedby_table(key, used_by))
    return doc.getvalue()


def report_usedby_table(key, rows):
    global config
    doc, tag, text = Doc().tagtext()
    with tag('table', klass="tabla"):
        with tag('thead'):
            with tag('tr'):
                with tag('th'):
                    text('Used In')
                with tag('th'):
                    text('As')
        with tag('tbody'):
            for c in rows:
                with tag('tr'):
                    with tag('td'):
                        doc.asis(html_link(get_relative_path(c), report_files_path_to_title(c, "|")))
                    with tag('td'):
                        for i in rows[c]:
                            if i['file_path'] == key:
                                text(", ".join(i['modulos']))
                                doc.stag('br')
                                with tag('div', klass="code"):
                                    text(i['code_line'])
    return doc.getvalue()


def report_imports_table(key):
    global config
    doc, tag, text = Doc().tagtext()
    with tag('h3'):
        text('Imports')
    with tag('table', klass="tabla"):
        with tag('thead'):
            with tag('tr'):
                with tag('th'):
                    text('Modulo')
                with tag('th'):
                    text('Tipo')
                with tag('th'):
                    text('Desde')
        with tag('tbody'):
            for c in todos[key]:
                with tag('tr'):
                    with tag('td'):
                        text(", ".join(c['modulos']))
                    with tag('td'):
                        text(get_module_dir(c['file_path']))
                    with tag('td'):
                        if not c['check']:
                            text(c['file'])
                        else:
                            doc.asis(html_link(c['path_to'], report_files_path_to_title(c['file_path'], "|")))
                            with tag('div', klass="code"):
                                text(c['code_line'])
    return doc.getvalue()


def report_files_path_to_title(path, char=" > "):
    global config
    path_from = config['dirs']['target_from']
    words = path_clean(path[path_from:], "--").split("--")
    return char.join(words)


#
# REPORT UTILS
def path_to_filename(path):
    return "%s.html" % path_clean(path)


def path_to_title(path, char=" > "):
    global config
    path_from = config['dirs']['target_from']
    words = path_clean(path[path_from:], "--").split("--")
    return char.join(words[1:])


def path_clean(path, char="_"):
    return path.replace("/", char).replace("\\", char)


def get_module_dir(path):
    if path.find('containers') > -1:
        return 'containers'
    elif path.find('components') > -1:
        return 'components'
    elif path.find('requests') > -1:
        return 'requests'
    elif path.find('utils') > -1:
        return 'utils'
    else:
        return 'otros'


def get_relative_path(path):
    global config
    path_from = config['dirs']['source_from']
    return os.path.join('..', get_module_dir(path), path_to_filename(path[path_from:]))


#
# HTML
def html_html(header, body):
    doc, tag, text = Doc().tagtext()
    doc.asis('<!DOCTYPE html>\n')
    with tag('html'):
        doc.asis('\n', header)
        doc.asis('\n', body, '\n')
    return doc.getvalue()


def html_head(title, up_dirs=0):
    doc, tag, text = Doc().tagtext()
    css_file = "../" * up_dirs + "index.css"
    font_file = "https://fonts.googleapis.com/css2?family=Share+Tech+Mono&display=swap"
    with tag('head'):
        with tag('title'):
            text(title)
    doc.stag('link', href=css_file, rel="stylesheet")
    doc.stag('link', href=font_file, rel="stylesheet")
    return doc.getvalue()


def html_header(title, extra=""):
    global config
    doc, tag, text = Doc().tagtext()
    sys_title = config['title']
    root_file = os.path.join(config['dirs']['target'], "index.html")
    with tag('header'):
        with tag('a', href=root_file):
            text(sys_title)
        text(' [' + title + ']')
        doc.asis(extra)
    return doc.getvalue()


def html_body(content):
    doc, tag, text = Doc().tagtext()
    with tag('body'):
        doc.asis(content)
    return doc.getvalue()


def html_link(src, label):
    doc, tag, text = Doc().tagtext()
    with tag('a', href=src):
        text(label)
    return doc.getvalue()


#
# FS
def save_to(path, data):
    with open(path, "w+") as file:
        file.write(data)
        file.close()


def make_dir(path):
    if os.path.isdir(path):
        shutil.rmtree(path)
    os.mkdir(path)


#
# BOOT
# ---------------------------------------------------------------
if __name__ == '__main__':
    main()  # sys.argv
