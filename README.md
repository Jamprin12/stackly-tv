# Stackly TV

## Install

3 tabs diferentes de terminal

```sh
cd middleware/video && npm run watch
```

```sh
cd middleware/video && npm run develop
```

Directorio raiz del proyecto y abrir en el navegador localhost:3000
```sh
npm start
```

## Middleware

```bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```