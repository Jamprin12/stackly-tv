const express = require('express');
const path = require('path');

const server = express();
const port = process.env.PORT || 8080;

server.use(express.static(path.resolve(__dirname, 'build')));

server.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'stvreact/../build','index.html'));
});

server.listen(port);
console.log(`Stv App listen on port:${port}`);
