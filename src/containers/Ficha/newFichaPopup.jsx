import React, {
  useState,
  useEffect,
  useRef,
  useCallback,
  useContext,
} from "react";
import { makeStyles } from "@material-ui/core/styles";
import { CircularProgress } from "@material-ui/core";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import Snackbar from "@material-ui/core/Snackbar";
import get from "lodash/get";
import { useTranslation } from "react-i18next";

import Device from "../../devices/device";

import * as api from "../../requests/loaderNew";
import {
  getBookmark,
  fetchFavoriteLoader,
  addFavoriteLoader,
  delFavoriteLoader,
  fetchFavoriteSerieLoader,
  addFavoriteSerieLoader,
  delFavoriteSerieLoader,
} from "../../requests/loaderNew";

import ErrorApi from "../../components/2020/Messages/Errors/ErrorApi";
import Ribbons from "../../components/2020/Ribbon/RibbonsVirtualized";
import { Modal } from "../../components/2020/Modals/Modal";
import Seasons from "../../components/2020/Seasons/Seasons";
import LoadingComponent from "../../components/2020/Loading/LoadingComponent";
import ModalVcard from "../../components/2020/Modals/planos/container";
import ModalBuy from "../../components/2020/Modals/planos/Buy";
import Button from "../../components/2020/Buttons/ButtonGeneric";
import ResumeNew from "../../components/2020/Resume/ResumeNewScroll";

import logoPlanos from "./images/Planos.svg";
import episodesIcon from "./images/Episodes.svg";
import iconPlay from "./images/net_vcard_play.svg";

import FullVideo from "../../containers/Video/Video";
import { Context } from "../../containers/App/Layout";
import { ModalContext } from "../../containers/App/ModalContext";

import useContentData from "./useContentData";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    width: "100%",
    height: "100%",
  },
  channelsContainer: () => ({
    position: "fixed",
    boxSizing: "border-box",
    height: theme.sizeBody.hd.height,
    width: theme.sizeBody.hd.width,
    zIndex: 999,
  }),
  collection: {
    overflow: "hidden!important",
  },
  ribbongridWrapper: {
    // height: 345,
    height: 320,
    width: "100%",
    paddingLeft: 20,
    boxSizing: "border-box",
    zIndex: 1,
    bottom: 0,
    position: "absolute",
  },
}));

const VCard = ({
  user,
  groupId,
  onClose,
  setFocus,
  isSerie,
  visible = true,
}) => {
  const { t, i18n } = useTranslation();
  const { setContext } = useContext(Context);
  const { setContext: setContextModal } = useContext(ModalContext);

  const classes = useStyles();

  const refContainer = useRef();

  const keys = Device.getDevice().getKeys();

  const [favouriteLoading, setFavouriteLoading] = useState(false);
  const [isFavourite, setIsFavourite] = useState(false);

  const [snackMessage, setSnackMessage] = useState("");
  const [snackOpen, setSnackOpen] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpen2, setIsModalOpen2] = useState(false);
  const [isModalPlans, setIsModalPlans] = useState(false);

  const [infoBuy, setInfoBuy] = useState({});

  const [isPreview, setIsPreview] = useState(false);
  const [showPlayer, setShowPlayer] = useState(!visible);

  const [currentChannel, setCurrentChannel] = useState();
  const [loadingPlayer, setLoadingPlayer] = useState(true);
  const [isSVOD, setIsSVOD] = useState(false);

  const [changing, setChanging] = useState(false);

  const [
    {
      ribbons = [],
      info,
      canPlay,
      episodes,
      offers,
      offersZup,
      loading,
      error,
      languages,
    },
    refresh,
  ] = useContentData({
    groupId,
    user,
    isSerie,
    searchLastEpisode: true,
  });

  useEffect(() => {
    if (changing) {
      setChanging(false);

      setCurrentChannel(changing);

      refresh({
        groupId: changing,
        user,
        isSerie,
        searchLastEpisode: false,
      });

      setTimeout(() => {
        setShowPlayer(true);
      }, 200);
    }
  }, [changing]);

  useEffect(() => {
    if (!showPlayer) {
      // si viene de home continuar viendo, no hay q mostrar la vcard
      // entonces al cerrar el player cierro la vcard
      if (!visible && !loading) {
        onClose();
      } else {
        setTimeout(() => {
          setFocus();
        }, 100);
      }
    }
  }, [showPlayer]);

  useEffect(() => {
    if (!loading) {
      setCurrentChannel(info.group_id);
      setTimeout(() => {
        setFocus();
      }, 200);
    }
  }, [loading]);

  useEffect(() => {
    refContainer.current.addEventListener("keydown", handleKeyPress);

    return () => {
      refContainer.current.removeEventListener("keydown", handleKeyPress);
    };
  }, [groupId]);

  const handleKeyPress = (e) => {
    const currentKey = keys.getPressKey(e.keyCode) || null;

    switch (currentKey) {
      case "BLUE":
        e.preventDefault();
        e.stopPropagation();
        break;
      case "SUB_AUD":
      case "GREEN":
        e.preventDefault();
        e.stopPropagation();
        break;
      case "YELLOW":
        e.preventDefault();
        e.stopPropagation();
        break;
      case "UP":
        break;
      case "DOWN":
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    const getDataFavorite = async () => {
      const favorites = await fetchFavoriteLoader(user);
      const resultSerie = await fetchFavoriteSerieLoader(user);
      let newResult = [];
      newResult = [
        ...(get(favorites, "response.groups") || []),
        ...(get(resultSerie, "response.series") || []),
      ];
      newResult =
        newResult.filter((it) =>
          it.episode ? it.id == info.serie_id : it.id == groupId
        ).length > 0
          ? true
          : false;
      setIsFavourite(newResult);
    };
    if (!loading) {
      getDataFavorite();
    }
  }, [loading]);

  const changeFavouriteState = async (isFavourite) => {
    let result;
    if (info.episode) {
      if (isFavourite == true) {
        result = await delFavoriteSerieLoader(info.serie_id);
      } else {
        result = await addFavoriteSerieLoader(info.serie_id);
      }
    } else {
      if (isFavourite == true) {
        result = await delFavoriteLoader(groupId);
      } else {
        result = await addFavoriteLoader(groupId);
      }
    }

    let stateResult = !isFavourite;
    if (result && result.msg !== "OK") {
      stateResult = isFavourite;
      const textError = (
        <div style={{ display: "flex" }}>
          <div
            style={{
              display: "center",
              alignItems: "center",
              marginRight: "10px",
              alignSelf: "center",
            }}
          >
            <ErrorOutlineIcon />
          </div>
          <div>
            <h4 style={{ margin: 0 }}>
              {t("net_error_agregar_minha_lista1", "Error")}
            </h4>
            {t("net_error_agregar_minha_lista2", "Error")}
          </div>
        </div>
      );
      setSnackMessage(textError);
      setSnackOpen(true);
    }

    setFavouriteLoading(false);
    setIsFavourite(stateResult);
  };

  const getFavoriteButton = () => {
    if (favouriteLoading) {
      return (
        <div
          style={{
            borderRadius: "4px",
            margin: "5px",
            background: "rgba(255, 255, 255, 0.2)",
            width: "120px",
            height: "47px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CircularProgress style={{ color: "white", height: 20, width: 20 }} />
        </div>
      );
    }

    return (
      <Button
        margin={5}
        width={120}
        heightFocoDisable={true}
        description={
          !isFavourite
            ? t("net_agregar_minha_lista", "Mi lista")
            : t("net_agregado_minha_lista", "adicionado")
        }
        onClick={() => {
          setFavouriteLoading(true);
          changeFavouriteState(isFavourite);
          setTimeout(() => {
            setFocus();
          }, 300);
        }}
        snRight=" "
      >
        {!isFavourite ? (
          <div style={{ display: "flex" }}>
            <AddCircleOutlineIcon style={{ fontSize: 30 }} />
          </div>
        ) : (
          <div style={{ display: "flex" }}>
            <CheckCircleOutlineIcon style={{ fontSize: 30 }} />
          </div>
        )}
      </Button>
    );
  };

  const getButtons = ({
    hasLanguages,
    hasPreview,
    canPlay,
    episodes,
    offersZup = {},
  }) => {
    const buttons = [];

    if (hasLanguages) {
      if (hasPreview) {
        buttons.push(
          <Button
            description={t("btn_trailer", "Trailer")}
            onClick={() => {
              setIsPreview(true);
              setShowPlayer(true);
            }}
            margin={5}
            width={120}
            heightFocoDisable={true}
          >
            <img
              style={{ height: 30 }}
              src={`${t("asset.net_vcard_trailer")}`}
            />
          </Button>
        );
      }
      if (canPlay) {
        buttons.push(
          <Button
            margin={5}
            width={120}
            heightFocoDisable={true}
            description={t("npvr_confirm_play", "Ver ahora")}
            onClick={() => {
              setIsPreview(false);
              setShowPlayer(true);
            }}
          >
            <img style={{ height: 30 }} src={`${iconPlay}`} />
          </Button>
        );
      } else {
        if (
          !offersZup.alugar &&
          (!get(offersZup, "assinar") ||
            (!get(offersZup, "assinar.addons.length") &&
              !get(offersZup, "assinar.plans.length")))
        ) {
          // buttons.push(
          //   <Button
          //     onClick={() => {}}
          //     margin={5}
          //     width={120}
          //     heightFocoDisable={true}
          //   >
          //     NoOffer
          //   </Button>
          // );
        } else {
          if (offersZup.alugar) {
            const e = offersZup.alugar;

            let currency = t("net_planos_precio", "R$");
            let data_price = e.totalPrice;
            let Price_show = parseFloat(
              e.totalPrice.amount / 10 ** e.totalPrice.scale
            ).toFixed(2);

            const itemName = e.items[0].composition.name;
            buttons.push(
              <Button
                margin={5}
                width={120}
                heightFocoDisable={true}
                key={itemName}
                description={t("net_alugar_por", "alugar por")}
                title={`${currency} ${Price_show.replace(".", ",")}`}
                onClick={() => {
                  setIsSVOD(false);
                  setInfoBuy({ price: data_price, item: e });
                  setIsModalOpen(true);
                }}
              />
            );
          }
          if (
            offersZup.assinar.addons.length === 1 &&
            offersZup.assinar.plans.length === 0
          ) {
            offersZup.assinar.addons.map((e, i) => {
              let currency = t("net_planos_precio", "R$");
              let data_price = e.totalPrice;
              let Price_show = parseFloat(
                e.totalPrice.amount / 10 ** e.totalPrice.scale
              ).toFixed(2);

              const itemName = e.items[0].composition.name;
              buttons.push(
                <Button
                  margin={5}
                  width={120}
                  heightFocoDisable={true}
                  key={itemName}
                  description={t("net_assinar_boton_vcard", "assinar por")}
                  title={`${currency} ${Price_show.replace(".", ",")}`}
                  onClick={() => {
                    setIsSVOD(true);
                    setInfoBuy({ price: data_price, item: e });
                    setIsModalOpen(true);
                  }}
                />
              );
            });
          } else {
            buttons.push(
              <Button
                margin={5}
                width={120}
                heightFocoDisable={true}
                key={"assinarPopup"}
                description={t("net_alugar_por123", "ver planos")}
                onClick={() => {
                  setIsModalPlans(true);
                  setIsModalOpen(true);
                }}
              >
                <img style={{ height: 30 }} src={`${logoPlanos}`} />
              </Button>
            );
          }
        }
      }
    }

    if (episodes) {
      buttons.push(
        <Button
          margin={5}
          width={120}
          heightFocoDisable={true}
          description={"episódios"}
          onClick={() => {
            onShowEpisodes();
          }}
        >
          <img style={{ height: 40, display: "block" }} src={episodesIcon} />
        </Button>
      );
    }

    return <div style={{ display: "flex" }}>{buttons}</div>;
  };

  const onShowEpisodes = () => {
    setIsModalOpen2(true);
    setContextModal({
      isOpen: true,
      component: Seasons,
      disableAutoFocus: true,
      props: {
        getBookmark: getBookmark,
        season: info.season,
        episode: info.episode,
        data: episodes,
        info: info,
        onClick: (groupId) => {
          setChanging(groupId);
          setContextModal({ isOpen: false });
          setIsModalOpen2(false);
        },
      },
      onClose: closeModal,
    });
  };

  const closeModal = () => {
    setIsModalPlans(false);
    setIsModalOpen(false);
    setIsModalOpen2(false);
    setTimeout(() => {
      setFocus();
    }, 100);
  };

  const handleVcard = useCallback((item) => {
    refresh({
      groupId: item.group_id || item.id,
      user,
      isSerie: Boolean(item.episode || item.episode_number),
    });
  }, []);

  if (error) {
    return (
      <ErrorApi getData={() => refresh({ groupId: currentChannel, user })} />
    );
  }

  return (
    <div ref={refContainer} className={classes.container}>
      {showPlayer && (
        <div
          className={`${classes.channelsContainer}`}
          style={{ display: showPlayer ? "block" : "none" }}
        >
          {loading ? (
            <LoadingComponent />
          ) : (
            <FullVideo
              isModalOpenVcard={isModalOpen2}
              onShowEpisodes={onShowEpisodes}
              setLoadingPlayer={setLoadingPlayer}
              currentChannel={info.group_id}
              optLanguages={languages}
              setShowPlayer={setShowPlayer}
              paywayToken={canPlay}
              isPreview={isPreview}
              isLive={false}
              currentContentId={
                isPreview ? null : get(languages, "selectLang.content_id", null)
              }
              setCurrentChannel={setChanging}
              info={info}
              episodes={episodes}
            ></FullVideo>
          )}
        </div>
      )}

      {loading ? (
        <LoadingComponent />
      ) : (
        <ResumeNew
          onClose={onClose}
          hasLanguages={Boolean(get(languages, "options", []).length)}
          item={info}
          favoriteButton={getFavoriteButton()}
          buttons={getButtons({
            hasLanguages: Boolean(get(languages, "options", []).length),
            hasPreview: get(info, "hasPreview"),
            offers,
            offersZup,
            canPlay,
            episodes: Boolean(get(info, "season")),
          })}
        >
          <div className={`${classes.ribbongridWrapper}`}>
            <Ribbons
              api={api}
              setFocus={setFocus}
              prefixId="vcard"
              list={ribbons}
              handleVcard={handleVcard}
              snUp={".resume-action-buttons .focusable:first-child"}
              snLeft=" "
            />
          </div>
        </ResumeNew>
      )}
      {isModalOpen && (
        <Modal onClose={closeModal}>
          {isModalPlans ? (
            <ModalVcard
              api={api}
              isPopup={true}
              onClose={closeModal}
              listAddons={get(offersZup, "assinar.addons")}
              listPlans={get(offersZup, "assinar.plans")}
              refresh={() => refresh({ groupId: currentChannel, user })}
              playFullMedia={() => {
                refresh({ groupId: currentChannel, user });
                setShowPlayer(true);
              }}
              info={info}
            />
          ) : (
            <ModalBuy
              api={api}
              isPopup={true}
              isSVOD={isSVOD}
              onClose={() => {
                closeModal();
              }}
              refresh={() => refresh({ groupId: currentChannel, user })}
              item={infoBuy.item}
              price={infoBuy.price}
              playFullMedia={() => {
                refresh({ groupId: currentChannel, user });
                setShowPlayer(true);
              }}
              info={info}
            />
          )}
        </Modal>
      )}
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        open={snackOpen}
        onClose={() => {
          setSnackOpen(false);
        }}
        message={snackMessage}
      />
    </div>
  );
};

export default VCard;
