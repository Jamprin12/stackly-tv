import React, { useState, useEffect } from "react";
import get from "lodash/get";
import { useTranslation } from "react-i18next";

import Device from "../../devices/device";
import * as api from "../../requests/loaderNew";
import { UserSeenLast, getRentedByGroupId } from "../../requests/loaderNew";

import {
  getItemProperties,
  getItemPropertiesTalent,
} from "../../components/2020/resizeTmp";

const getLanguages = async ({ data }) => {
  const options = get(
    data,
    "common.extendedcommon.media.language.options.option",
    []
  );

  let supportedStreams = getApaSupportedStream();
  supportedStreams = supportedStreams["vod"];

  // xq saca los _ma ?
  supportedStreams = await Promise.all(
    supportedStreams.filter((stream) => !stream.includes("_ma"))
  );
  const streamTypeDevice = supportedStreams[0];

  const aOptions = await Promise.all(
    options.filter((item) => item.encodes.includes(streamTypeDevice))
  );

  const finalOptions = [];
  await Promise.all(
    aOptions.map((item) => {
      finalOptions[item.option_id] = item;
    })
  );

  // options por default
  const langDefault =
    finalOptions["D-PT"] || finalOptions["S-PT"] || aOptions[0];

  return { options: aOptions, selectLang: langDefault };
};

const getApaSupportedStream = () => {
  let device =
    Device.getDevice().getSubplatform() || Device.getDevice().getPlatform();

  const streams = {
    arris: {
      vod: ["dashwv_ma", "dashwv"],
      live: ["dashwv_ma", "dashwv", "ip_multicast", "hls_kr"],
      timeshift: ["dashwv"],
    },
    android: {
      vod: ["smooth_streaming_ma", "smooth_streaming"],
      live: ["ip_multicast", "hls_kr"],
      timeshift: ["hls_kr"],
    },
    hisense: {
      vod: ["smooth_streaming_ma", "smooth_streaming"],
      live: ["smooth_streaming"],
      timeshift: ["smooth_streaming"],
    },
    lg: {
      vod: ["smooth_streaming_ma", "smooth_streaming", "hls"],
      live: ["hls_kr"],
      timeshift: ["hls_kr"],
    },
    nagra: {
      vod: ["hlsprm_ma", "hlsprm"],
      live: ["dvbc_ma", "dvbc"],
      timeshift: ["hlsprm"],
    },
    opera: {
      vod: ["smooth_streaming", "hls"],
      live: ["hls_kr", "hls"],
      timeshift: ["hls_kr"],
    },
    polaroid: {
      vod: ["dashwv"],
      live: ["hls_kr"],
      timeshift: ["hls_kr"],
    },
    ps4: {
      vod: ["smooth_streaming_ma", "smooth_streaming"],
      live: ["smooth_streaming"],
      timeshift: ["smooth_streaming"],
    },
    samsung: {
      vod: [
        "smooth_streaming_ma",
        "smooth_streaming",
        "hls",
        "widevine_classic",
      ],
      live: ["smooth_streaming", "hls_kr", "hls"],
      timeshift: ["smooth_streaming"],
    },
    sony: {
      vod: ["smooth_streaming_ma", "smooth_streaming"],
      live: ["smooth_streaming", "smooth_streaming_ma"],
      timeshift: ["hls_kr", "hls"],
    },
    stbcoship: {
      vod: ["smooth_streaming_ma", "smooth_streaming"],
      live: ["hls_kr"],
      timeshift: ["hls_kr"],
    },
    stbhuawei: {
      vod: ["smooth_streaming_ma", "smooth_streaming"],
      live: ["ip_multicast_lms", "ip_multicast_udp", "ip_multicast", "hls_kr"],
      timeshift: ["hls_kr"],
    },
    stbkaon: {
      vod: ["smooth_streaming_ma", "smooth_streaming"],
      live: ["ip_multicast_lms", "ip_multicast", "hls_kr"],
      timeshift: ["hls_kr"],
    },
    tizen: {
      vod: ["smooth_streaming", "smooth_streaming_ma"],
      live: ["smooth_streaming_ma", "smooth_streaming"],
      timeshift: ["smooth_streaming"],
    },
    web0s: {
      vod: ["smooth_streaming_ma", "smooth_streaming"],
      live: ["smooth_streaming_ma", "smooth_streaming"],
      timeshift: ["smooth_streaming"],
    },
    workstation: {
      vod: ["smooth_streaming_ma", "smooth_streaming", "hls"],
      live: ["hls_kr", "hls"],
      timeshift: ["hls_kr", "hls"],
    },
    workstationChafari: {
      vod: ["hls_ma", "hls"],
      live: ["hls_kr", "hls"],
      timeshift: ["hls_kr", "hls"],
    },
  };

  return streams[device];
};

const getContent = async ({ data, groupId }) => {
  const isSerie = Boolean(
    get(data, "common.extendedcommon.media.serieseason.id")
  );

  const renderItem = await getRentedByGroupId(groupId);

  const item = getItemProperties({
    item: {
      ...data.common,
      external: data.external,
      is_series: isSerie,
      vcardSerie: isSerie,
    },
  });

  return {
    ...item,
    serie_id: get(data, "common.extendedcommon.media.serie.id"),
    id: get(data, "common.id"), // se usa para comprar
    imageSmall: get(data, "common.image_small"), // se usa para comprar
    infoRented: get(renderItem, "cardEndDate"),
    title: isSerie
      ? get(data, "common.extendedcommon.media.serie.title")
      : get(data, "common.title"),
    episodeTitle: isSerie ? get(data, "common.title") : null,
    episode: get(data, "common.extendedcommon.media.episode.number"),
    season: get(data, "common.extendedcommon.media.episode.season"),
    duration: get(data, "common.duration"),
    // category: get(data, "common.extendedcommon.genres.genre.0.desc"),
    category: get(data, "external.gracenote.genres", []).slice(0, 2).join(", "), // from gracenote
    year: get(data, "common.extendedcommon.media.publishyear"),
    rating: get(data, "external.gracenote.rating_classind"),
    description: get(data, "common.large_description"),
    leg: get(data, "common.extendedcommon.media.language.subbed") == "true",
    language:
      get(data, "common.extendedcommon.media.language.dubbed") == "true",
    resolution:
      get(data, "common.extendedcommon.media.profile.hd.enabled") == "true",
    hasPreview: get(data, "common.extendedcommon.media.haspreview") == "true", // para los botones
  };
};

const getEpisodes = async ({ data, groupId, user }) => {
  const isSerie = Boolean(
    get(data, "common.extendedcommon.media.serieseason.id")
  );

  if (!isSerie) {
    return [];
  }

  let contentSerie = await api.contentSerie(
    get(data, "common.extendedcommon.media.serie.id")
  );

  return get(contentSerie, "serie");
};

const getPaywayToken = async ({ offers }) => {
  const result = offers.filter((e) => e.purchase_data !== "");

  return get(result, "0.purchase_data.payway_token_play") || null;
};

const getContentData = async ({ groupId }) => {
  return api.contentData(groupId);
};

const getOffersAndPaywayToken = async ({ groupId }) => {
  const offers = await api.GetOffersZUP({ groupId });

  // payway para poder reproducir
  const canPlay = await getPaywayToken({
    offers: get(offers, "response.offers", []),
  });
  return canPlay;
};

const getData = ({
  groupId: prueba,
  user,
  isSerie,
  searchLastEpisode = false,
}) => {
  const { t, i18n } = useTranslation();
  const [result, setResult] = useState(null);
  const [loading, setLoading] = useState(true);

  const [groupIdLocal, setGroupIdLocal] = useState(prueba);
  const [userLocal, setUserLocal] = useState(user);
  const [isSerieLocal, setIsSerieLocal] = useState(isSerie);
  const [searchLastEpisodeLocal, setSearchLastEpisodeLocal] = useState(
    searchLastEpisode
  );

  const getOffersZup = async ({ offers, title, canPlay }) => {
    if (!offers.length || canPlay) {
      return {};
    }

    // filtrar para traer tovd y sovd (alquiler y subscription)
    // el filtro ya estaba, agrego subcripcion (= A) por que no se que mas puede traer
    let keys = await Promise.all(
      offers
        .filter((e) => e.product_id == "M" || e.product_id == "A")
        .map((e) => e.key)
    );

    const offersZup = await api.getOffers(title, keys, keys);

    const addons = get(offersZup, "data.addons");
    const plans = get(offersZup, "data.plans");
    const alugar = addons.find((item) => item.name.includes("TVOD"));
    const addonsAsinar = addons.filter((item) => !item.name.includes("TVOD"));

    return { alugar, assinar: { addons: addonsAsinar, plans } };
  };

  const getTalents = ({ data, t }) => {
    const talents =
      get(data, "external.gracenote.cast") ||
      get(data, "common.extendedcommon.roles.role") ||
      [];

    const atmp = [];
    talents
      .filter(
        (role) => parseInt(role.role_id) === 1 || parseInt(role.role_id) === 16
      )
      .map((role) => {
        role.talents.map((t) => {
          atmp.push({
            rolName: role.role_name || role.name,
            ...t,
          });
        });
      });

    const finalTalents = atmp.sort((a, b) =>
      parseInt(a.order) > parseInt(b.order)
        ? 1
        : parseInt(b.order) > parseInt(a.order)
        ? -1
        : 0
    );

    return {
      title: t("ficha.talent", "Elenco"),
      type: "talent",
      items: finalTalents.map((item) => {
        return {
          type: "talent",
          ...getItemPropertiesTalent({ item }),
        };
      }),
    };
  };

  const getContentAllData = async ({
    groupId: id,
    user,
    isSerie,
    searchLastEpisode = false,
    t,
  }) => {
    try {
      let groupId = id;
      if (isSerie && searchLastEpisode) {
        const seenLastInfo = await UserSeenLast(groupId, user);

        groupId = get(seenLastInfo, "episode.id");
      }

      const [offers, contentData, recommendations] = await Promise.all([
        api.GetOffersZUP({ groupId }),
        api.contentData(groupId),
        api.contentRecomendations(groupId),
      ]);
      const aOffers = get(offers, "response.offers") || [];

      // payway para poder reproducir
      const canPlay = await getPaywayToken({ offers: aOffers });

      const [languages, info, episodes, talents, offersZup] = await Promise.all(
        [
          getLanguages({ data: contentData.group }),
          getContent({ data: contentData.group, groupId }),
          getEpisodes({ data: contentData.group, groupId, user }),
          getTalents({ data: contentData.group, t }),
          getOffersZup({
            offers: aOffers,
            title: get(contentData, "group.common.title"),
            canPlay,
          }),
        ]
      );

      const ribbons = [];
      // ribbons.push(episodes);
      if (talents.items.length) {
        ribbons.push(talents);
      }
      if (recommendations) {
        ribbons.push({
          title: t("ficha.SeeOther", "conteúdos similares"),
          items: recommendations.map((item) => {
            return getItemProperties({
              item,
              version: "v5.86",
            });
          }),
        });
      }

      return {
        ribbons,
        info,
        canPlay,
        offers: aOffers,
        offersZup,
        episodes,
        languages,
      };
    } catch (e) {
      return { error: "Error" };
    }
  };

  const getData = async (groupId, user, isSerie, searchLastEpisode) => {
    const {
      ribbons,
      info,
      canPlay,
      offers,
      offersZup,
      error,
      episodes,
      languages,
    } = await getContentAllData({
      t,
      groupId,
      user,
      isSerie,
      searchLastEpisode,
    });

    setResult({
      info,
      ribbons,
      canPlay,
      offers,
      offersZup,
      episodes,
      languages,
      error,
    });
    setLoading(false);
  };

  useEffect(() => {
    if (loading) {
      getData(groupIdLocal, userLocal, isSerieLocal, searchLastEpisodeLocal);
    }
  }, [loading]);

  const refresh = ({
    groupId,
    user,
    isSerie = false,
    searchLastEpisode = false,
  }) => {
    setGroupIdLocal(groupId);
    setUserLocal(user);
    setIsSerieLocal(isSerie);
    setSearchLastEpisodeLocal(searchLastEpisode);
    setLoading(true);
  };

  return [{ ...result, loading }, refresh];
};

export default getData;
export {
  getContent,
  getEpisodes,
  getContentData,
  getOffersAndPaywayToken,
  getLanguages,
};
