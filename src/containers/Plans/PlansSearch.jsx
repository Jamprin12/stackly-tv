/**
 * Buscador de canales
 */
import get from "lodash/get";

const lHead = "[PSearch] > ";
const SVOD = "svod";
const LIVE = "live";

class PlanSearch {
  constructor(data) {
    if (data) this.setData(data);
  }

  //
  // BUILD
  //
  setData(data) {
    this.plans = this.getIndexed(get(data, "data.plans", {}));
    this.addons = this.getIndexed(get(data, "data.addons", {}));
    this.channels = [];
    this.getChannelsAndAddons();
    this.dump();
  }
  getIndexed(data) {
    const result = [];
    data.forEach((p) => {
      result[p.id] = p;
    });
    return result;
  }
  //
  // CHANNELS
  //
  getChannelsAndAddons() {
    this.channels = []; // de cero
    const initTime = Date.now();
    for (var key in this.plans) {
      this.plans[key].items.forEach((item) =>
        this.upsertChannel(this.plans[key].id, "plans", item)
      );
    }
    for (var key in this.addons) {
      this.addons[key].items.forEach((item) =>
        this.upsertChannel(this.addons[key].id, "addons", item)
      );
    }
    const endTime = Date.now();
    console.log(
      lHead + "getChannels - channels builded.",
      endTime - initTime,
      "ms"
    );
    // console.log(lHead + " CHANNELS ", this.channels);
  }
  upsertChannel(planId, type, comp) {
    // const id = get(comp, "composition.componentId");
    const channelKey = get(comp, "composition.name"); // get(comp, "composition.componentId");
    // si no existe, lo creo
    if (!this.channels[channelKey]) {
      this.channels[channelKey] = {
        id: get(comp, "composition.componentId"),
        name: get(comp, "composition.name"),
        systerParameters: get(comp, "composition.systemParameters"),
        plans: [],
        addons: [],
      };
    }
    this.channels[channelKey][type].push(planId); // agrego el plan/addon
  }
  getChannelName(channel) {
    const result = get(
      this.channels,
      channel + ".systerParameters.DISPLAY_NAME",
      channel // no existe!
    );
    // console.log(lHead + "getChannelName-", channel, result);
    return result;
  }

  //
  // LISTADO CANALES
  //
  getChannels() {
    return this.channels;
  }
  //
  // SEARCH
  //
  searchOneChannelPackages(channel) {
    if (this.channels[channel]) {
      return {
        channel: channel,
        title: [channel],
        plans: this.channels[channel].plans,
        addons: this.channels[channel].addons,
      };
    } else {
      return { plans: [], addons: [], title: [channel] };
    }
  }
  searchChannelPackages(channels) {
    // por las dudas que llegue un string
    if (typeof channels !== "object") {
      channels = [channels];
    }
    // console.log(lHead + "searchChannelPackages -- 0 channels", channels);

    let results = { plans: [], addons: [], title: [] };

    if (channels.length == 0) {
      return results;
    } else if (channels.length == 1) {
      // console.log(lHead + "searchChannelPackages -- 1ch", channels);
      results = this.searchOneChannelPackages(channels[0]);
    } else {
      // console.log(lHead + "searchChannelPackages- var.ch", channels);
      const curEl = this.searchOneChannelPackages(channels.pop());
      const prevEl = this.searchChannelPackages(channels);
      results = this.filterPackages(curEl, prevEl);
      // console.log(lHead + "searchChannelPackages- var.res", {
      //   curEl,
      //   prevEl,
      //   results,
      // });
    }
    // console.log(lHead + "searchChannelPackages- ", {channels,results});
    return results;
  }
  search(channels) {
    const tin = Date.now();
    const search = this.searchChannelPackages(channels);
    // console.log(lHead + "SEARCH", search);
    let results = {
      plans: [],
      plans_cant: search.plans.length,
      addons: [],
      addons_cant: search.addons.length,
      filterTitles: "",
    };
    results.filterTitles = search.title.map((t) => this.getChannelName(t));
    for (var p in search.plans) results.plans.push(this.plans[search.plans[p]]);
    for (var a in search.addons)
      results.addons.push(this.addons[search.addons[a]]);
    // results.plans_cant=results.plans.length;
    // results.addons_cant=results.addons.length

    const tou = Date.now();
    console.log(lHead + "search TIME :", tou - tin, channels);
    return results;
  }
  //
  // FILTROS INTERNOS
  //
  filterPackages(pack1, pack2) {
    // console.log(lHead + "filterPackages", pack1, pack2);
    let results = {
      channel: [pack1.channel, pack2.channel],
      title: pack1.title.concat(pack2.title),
      plans: [],
      addons: [],
    };
    // PLANS
    const p1plans = get(pack1, "plans", []); // convierto en array
    const p2plans = get(pack2, "plans", []);
    if (p1plans.length > 0 && p2plans.length > 0) {
      // console.log(lHead + "filterPackages--", p1plans, p2plans);
      p1plans.forEach((pl) => {
        const incluir = this.filterObject(pl, p2plans).length;
        if (incluir) {
          results.plans.push(pl);
          // console.log(lHead + "filterPackages--plans--incluir", pl);
        } else {
          // console.log(lHead + "filterPackages--plans--NOincluir", pl);
        }
      });
    }
    // ADDONS
    const p1addons = get(pack1, "addons", []); // convierto en array
    const p2addons = get(pack2, "addons", []);
    // console.log(lHead + "filterPackages--", p1addons, p2addons);
    if (p1addons.length > 0 && p2addons.length > 0) {
      p1addons.forEach((pl) => {
        const incluir = this.filterObject(pl, p2addons).length;
        if (incluir) {
          results.addons.push(pl);
          // console.log(lHead + "filterPackages--addons--incluir", pl);
        } else {
          // console.log(lHead + "filterPackages--addons--NOincluir", pl);
        }
      });
    }
    // SALIDA
    // console.log(lHead + "filterPackages--out", results);
    return results;
  }
  filterObject(key, object) {
    // console.log(lHead+'filterPackages--filterObject',object.filter((l)=> l===key));
    return object.filter((l) => l === key);
  }

  //
  // RESULTS
  //
  getPackage(id) {
    // if(this.plans[id])
    return "por ahora nada";
  }

  //
  // UTILS
  //
  dump() {
    // console.log(lHead + "dump - plans=", this.plans);
    // console.log(lHead + "dump - addons=", this.addons);
    console.log(lHead + "dump - CHANNELS", this.getChannels());
    console.log(
      // no existe
      lHead + "dump - search NX_LIVE_GOOSE_HD (no existe) >>",
      this.search("NX_LIVE_GOOSE_HD")
    );
    console.log(
      // no existe
      lHead + "dump - search 3 can 1 no existe >>",
      this.search([
        "NX_LIVE_ESPN_HD",
        "NX_LIVE_GOOSE_HD",
        "NX_SVOD_CLARO_VIDEO",
      ])
    );
    console.log(
      // dos
      lHead + "dump - dos canales >>",
      this.search(["NX_LIVE_ESPN_HD", "NX_SVOD_CLARO_VIDEO"])
    );
    console.log(
      // tres
      lHead + "dump - 3 canales >>",
      this.search(["NX_LIVE_ESPN_HD", "NX_SVOD_CLARO_VIDEO", "NX_LIVE_ESPN_BR"])
    );
    console.log(
      // cuatro
      lHead + "dump - 4 canales >>",
      this.search([
        "NX_LIVE_ESPN_HD",
        "NX_SVOD_CLARO_VIDEO",
        "NX_LIVE_ESPN_BR",
        "NX_LIVE_ESPN_EXTRA",
      ])
    );

    // "e6717f2b-2c99-4d11-86bd-79634c1a953a" "NX_LIVE_TNT_HD"
    // "3e8abea2-ef90-49df-85d9-772244801330" "NX_SVOD_CLARO_VIDEO"

    // "3e8abea2-ef90-49df-85d9-772244801330" "NX_SVOD_CLARO_VIDEO"
    // "31d5cfd7-8f8e-4181-8d30-5db7fb6ad042" "NX_LIVE_ESPN_HD"
  }
}

export default PlanSearch;
