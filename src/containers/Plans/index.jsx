import React, { useState, useEffect } from "react";
import get from "lodash/get";

import * as api from "../../requests/loaderNew";

import PlanSearch from "./PlansSearch";

import LoadingComponent from "../../components/2020/Loading/LoadingComponent";
import ErrorApi from "../../components/2020/Messages/Errors/ErrorApi";
import ModalVcard from "../../components/2020/Modals/planos/container";

const PlansPage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [listPlans, setListPlans] = useState([]);
  const [listAddons, setListAddons] = useState([]);
  const [error, setError] = useState(false);

  const search = new PlanSearch();

  const getData = async () => {
    try {
      const listOffers = await api.getListOffers();
      search.setData(listOffers);
      setListPlans(get(listOffers, "data.plans") || []);
      setListAddons(get(listOffers, "data.addons") || []);
      setError(false);
      setIsLoading(false);
    } catch (e) {
      console.log("jose error:", e);
      setError(true);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const closeModal = () => {
    setTimeout(() => {
      window.SpatialNavigation.focus("@container");
    }, 100);
  };

  setTimeout(() => {
    window.SpatialNavigation.focus("@container");
  }, 500);

  // error api
  if (error) {
    return <ErrorApi getData={getData} />;
  }

  if (isLoading) {
    return <LoadingComponent />;
  }

  return (
    <ModalVcard
      api={api}
      listAddons={listAddons}
      listPlans={listPlans}
      onClose={closeModal}
    />
  );
};

export default PlansPage;
