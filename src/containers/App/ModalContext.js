import React, { useState, createContext } from "react";

import { Modal } from "../../components/2020/Modals/Modal";

const ModalContext = createContext({
  component: null,
});

const ModalProvider = ({ children, user }) => {
  const [context, setContext] = useState({});

  return (
    <ModalContext.Provider
      value={{
        context,
        setContext: (data) =>
          setContext({
            ...context,
            style: {},
            cancelBack: false,
            disableAutoFocus: false,
            user: user,
            ...data,
          }),
      }}
    >
      {children}
    </ModalContext.Provider>
  );
};

const ModalConsumer = React.memo(() => {
  return (
    <ModalContext.Consumer>
      {({ context, setContext }) => {
        const {
          component: Component,
          props,
          isOpen,
          onClose,
          user,
          style,
          cancelBack,
          disableAutoFocus = false,
        } = context;

        const handleClose = () => {
          setContext({ isOpen: false });
          if (onClose) {
            onClose();
          } else {
            setTimeout(() => {
              window.SpatialNavigation.focus("@container");
            }, 200);
          }
        };

        return (
          isOpen && (
            <Modal
              onClose={cancelBack ? () => {} : handleClose}
              style={style}
              disableAutoFocus={disableAutoFocus}
            >
              {({ setFocus }) => (
                <Component
                  {...props}
                  setFocus={setFocus}
                  user={user}
                  onClose={handleClose}
                />
              )}
            </Modal>
          )
        );
      }}
    </ModalContext.Consumer>
  );
});

export { ModalContext, ModalProvider, ModalConsumer };
