import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory, Route, Switch } from "react-router-dom";
import get from "lodash/get";

import MaterialNav from "../../components/2020/Header/newNav";
import LoadingComponent from "../../components/2020/Loading/LoadingComponent";

import Search from "../../containers/Search/search";
import PlansPage from "../../containers/Plans";
import Profile from "../../containers/Social/newSocialContainer";
import MyList from "../../containers/MyList";
import Tv from "../../containers/Tv/tv";
import HomeNew from "../../containers/Home/HomeNew";
import VCardNewPopup from "../../containers/Ficha/newFichaPopup";

import { Modal } from "../../components/2020/Modals/Modal";
import { ModalProvider, ModalConsumer } from "./ModalContext";

import * as api from "../../requests/loaderNew";
import { Logout } from "../../requests/new/login/auth";

const Context = React.createContext(); // para abrir la vcard popup en toda la app

const useStyles = makeStyles((theme) => ({
  content: () => ({
    display: "flex",
    width: "100%",
    height: "100%",
    overflow: "hidden",
    position: "relative",
  }),
}));

const PrivateRoute = React.memo(
  ({ component: Component, user, path, location, match, ...options }) => {
    const prueba = React.useCallback(
      (props) => {
        return <Component {...props} user={user} />;
      },
      [location]
    );

    return (
      <Route
        path={path}
        component={prueba}
        location={location}
        match={match}
        {...options}
      />
    );
  }
);

const Layout = ({ location }) => {
  const classes = useStyles();
  const history = useHistory();

  const [user, setUser] = useState({});
  const [context, setContext] = useState({});

  React.useEffect(() => {
    const Login = async () => {
      try {
        const resultUser = await api.isLoggedIn();

        const result = get(resultUser, "response");

        if (result && result.user_id) {
          setUser(result);
          return true;
        }
      } catch (e) {
        console.log("error", e.message);
      }
      history.push("/landing");
    };

    Login();
  }, []);

  if (!user.user_id) {
    return <LoadingComponent image visible={true} />;
  }

  return (
    <div className={classes.content}>
      <MaterialNav location={location} user={user} logout={Logout} />

      <div id="privateContent" className={classes.content}>
        <ModalProvider user={user}>
          <Context.Provider
            value={{
              context,
              setContext: (data) =>
                setContext({ ...context, user: user, ...data }),
            }}
          >
            <Switch>
              <PrivateRoute path="/profile" component={Profile} user={user} />
              <PrivateRoute path="/search" component={Search} user={user} />
              <PrivateRoute path="/mylist" component={MyList} user={user} />
              <PrivateRoute path="/plans" component={PlansPage} user={user} />

              <PrivateRoute path="/node/tv" component={Tv} user={user} />

              <PrivateRoute
                path="/node/:code"
                component={HomeNew}
                user={user}
              />
            </Switch>

            <VcardConsumer />
            <ModalConsumer />
          </Context.Provider>
        </ModalProvider>
      </div>
    </div>
  );
};

const VcardConsumer = React.memo(() => {
  return (
    <Context.Consumer>
      {({ context, setContext }) => {
        const {
          isOpen,
          user,
          gropupId,
          onClose,
          isSerie = false,
          visible,
        } = context;
        const handleClose = () => {
          setContext({ isOpen: false });
          if (onClose) {
            onClose();
          } else {
            setTimeout(() => {
              window.SpatialNavigation.focus("@container");
            }, 200);
          }
        };

        return (
          isOpen && (
            <Modal style={{ zIndex: 999 }} onClose={handleClose}>
              {({ setFocus }) => (
                <VCardNewPopup
                  visible={visible}
                  isSerie={isSerie}
                  setFocus={setFocus}
                  user={user}
                  groupId={gropupId}
                  onClose={handleClose}
                />
              )}
            </Modal>
          )
        );
      }}
    </Context.Consumer>
  );
});

export { Context };
export default Layout;
