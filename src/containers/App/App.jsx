import React, { Component } from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { createBrowserHistory } from "history";

import Device from "../../devices/device";

import DeviceNetworkStatus from "./DeviceNetworkStatus";

import Focus from "./../../components/Focus";

import LandingPage from "../../containers/Landing";
import Login from "./../../containers/Login/LoginNew";
import ChannelsModals from "./../../containers/Templates/ChannelsModals";
import Status from "../../components/2020/Modals/network/status";
import { ModalProvider, Modal } from "../../components/2020/Modals/Modal";
import ModalContext from "../../components/2020/Modals/ModalContext";

import Layout from "./Layout";

import "./i18n";

import "./app.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showNetworkStatus: false,
      channelCharged: false,
      user: {},
    };

    this.history = createBrowserHistory();

    this.keys = Device.getDevice().getKeys();

    this.lastKey = null;
    this.delayKeyTime = 90; //MS
    this.delayTimerId = null;
    this.isKeysBloqued = false;

    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyPress);
    document.removeEventListener("keyup", this.handleKeyUp);
    document.removeEventListener("AndroidKeyEvent", this.onAndroidKeyEvent);
  }

  componentDidMount = async () => {
    document.addEventListener("keyup", this.handleKeyUp);
    document.addEventListener("keydown", this.handleKeyPress);
    document.addEventListener("AndroidKeyEvent", this.onAndroidKeyEvent);

    new DeviceNetworkStatus({ showModal: this.showModal }).startCheckNetwork();
  };

  showModal = ({ modalType }) => {
    if (modalType === "DESCONEXION") {
      this.setState({ showNetworkStatus: true });
    } else {
      this.setState({ showNetworkStatus: false });
    }
  };

  handleDelayKey(e, key) {
    if (this.lastKey !== key && this.delayTimerId) {
      clearTimeout(this.delayTimerId);
    }

    this.isKeysBloqued = this.lastKey === key && this.delayTimerId;
    this.lastKey = key;

    if (this.isKeysBloqued) {
      e.stopPropagation();
      e.preventDefault();
    } else {
      const _this = this;
      this.delayTimerId = setTimeout(() => {
        clearTimeout(_this.delayTimerId);
        _this.delayTimerId = null;
      }, this.delayKeyTime);
    }
  }

  onAndroidKeyEvent(e) {
    const event_data = e.detail;
    const param = event_data.param;

    let keyCode = Number.parseInt(param, 10); // 10 warning
    let el = document.activeElement || document.body;

    // Create the event
    var eventObj = document.createEventObject
      ? document.createEventObject()
      : document.createEvent("Events");

    if (eventObj.initEvent) {
      eventObj.initEvent("keydown", true, true);
    }

    eventObj.keyCode = keyCode;
    eventObj.which = keyCode;

    el && el.dispatchEvent
      ? el.dispatchEvent(eventObj)
      : el.fireEvent("onkeydown", eventObj);
  }

  handleKeyPress(e) {
    const currentKey = this.keys ? this.keys.getPressKey(e.keyCode) : null;

    this.handleDelayKey(e, currentKey);

    if (currentKey === "BACK") {
      e.preventDefault();
    }

    if (currentKey === "HD") {
      e.preventDefault();
      this.props.history.push("/node/tv");
    }

    if (currentKey === "PPV") {
      this.props.history.push("/node/home");
    }

    if (currentKey === "INFO") {
    }

    if (currentKey === "MSG") {
    }

    if (currentKey === "PREV") {
    }

    if (currentKey === "FAV") {
    }

    if (currentKey === "VOD") {
    }

    if (currentKey === "MUSIC") {
    }
  }

  handleKeyUp(e) {
    if (this.delayTimerId) {
      clearTimeout(this.delayTimerId);
      this.lastKey = null;
      this.delayTimerId = null;
    }
  }

  closeModal = () => {
    this.setState({ showNetworkStatus: false });
  };

  render() {
    return (
      <ModalContext.Provider value={{ keys: this.keys }}>
        <Router history={this.history}>
          <div
            style={{
              width: "1280px",
              height: "720px",
              fontFamily: "Roboto, sans-serif",
              fontSize: "16px",
              fontWeight: "300",
              color: "#fff",
              background: "#000000",
              margin: 0,
              padding: 0,
              overflow: "hidden",
              display: "flex",
            }}
          >
            <Focus />

            <div
              id="container"
              style={{
                display: "flex",
                width: "100%",
                height: "100%",
                boxSizing: "border-box",
                zIndex: 1,
              }}
            >
              <ModalProvider>
                <Switch>
                  <Route exact path="/">
                    <Redirect to="/node/home" />
                  </Route>
                  <Route path="/landing" component={LandingPage} />
                  <Route path="/login" component={Login} />
                  <Route path="/modalchannel" component={ChannelsModals} />
                  <Layout />
                </Switch>

                {this.state && this.state.showNetworkStatus && (
                  <Modal onClose={this.closeModal}>
                    <Status onClose={this.closeModal} />
                  </Modal>
                )}
              </ModalProvider>
            </div>
          </div>
        </Router>
      </ModalContext.Provider>
    );
  }
}

export default App;
