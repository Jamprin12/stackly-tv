import jwtDecode from "jwt-decode";

import { liveChannels } from "../../requests/loaderNew";

const getChannels = (userId) => {
  return liveChannels(userId)
    .then((result) => {
      const response = result.response ? result.response : false;
      const paqs = (response && response.user_entitlements) || [];

      const notRepeatChannels = [];
      const channels = [];

      paqs.map((item) => {
        let payway_token = jwtDecode(item.payway_token_live);
        if (Array.isArray(payway_token.pgs)) {
        } else {
          payway_token &&
            payway_token.pgs &&
            payway_token.pgs.groups && /// Agregado GOOSE a veces viene null [ENDPOINT]
            payway_token.pgs.groups.map((id) => {
              if (notRepeatChannels[id]) {
                return false;
              }
              notRepeatChannels[id] = id;

              const result = {
                id: id,
                code: item.bss_code,
                offerid: payway_token.pgs.offerId,
                purchaseid: payway_token.pgs.purchaseId,
                npvrstorage: !isNaN(parseInt(payway_token.pgs.npvrStorage)) ? parseInt(payway_token.pgs.npvrStorage) : 0,
                timeshift: !isNaN(parseInt(payway_token.pgs.timeshift)) ? parseInt(payway_token.pgs.timeshift) : 0,
                npvr_token: item.payway_token_live,
                play: payway_token.pgs.play,
                key: item.key,
              };
              channels.push(result);
            });
        }
      });
      return channels;
    })
    .catch((error) => {
      console.error("[ENDPOINT] [fetchLinealChannels] ERROR ", error);
    });
};

export default getChannels;
