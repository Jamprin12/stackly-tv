import React, { useState, useEffect, useContext, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";
import { useHistory } from "react-router-dom";
import { List, Set } from "immutable";
import { useTranslation } from "react-i18next";

import DeviceStorage from "../../components/DeviceStorage/DeviceStorage";
import Device from "../../devices/device";

import { epgLoader } from "../../requests/loaderNew";

import getChannels from "./getChannels";

import Epg from "../../components/2020/newEpg/Epg";
import LoadingComponent from "../../components/2020/Loading/LoadingComponent";
import { Modal } from "../../components/2020/Modals/Modal";
import Program from "../../components/2020/Modals/epg/epgProgram";
import NotChannel from "../../components/2020/Modals/epg/notChannel";
import Languages from "../../components/2020/Modals/idiomas/idiomasNew";
import ListChannels from "../../components/2020/ListChannels";
import MessageGeneric from "../../components/2020/Messages/MessageGenericOneButton";

import { ModalContext } from "../../containers/App/ModalContext";
import FullVideo from "../../containers/Video/Video";

// ======================
// pasar a context
let MenuReference = null;

const getFocusActive = () => {
  const modal = document.getElementsByClassName("modal-overlay");
  if (modal.length === 0) {
    const elementoPredeterminadoFocus = window.SpatialNavigation.focus(".elementopredeterminado");
    if (!elementoPredeterminadoFocus) {
      const ribbonFocus = window.SpatialNavigation.focus(".scroller-container .focusable");
      if (!ribbonFocus) {
        window.SpatialNavigation.focus(".active");
      }
    }
  }
};

const setHidden = (element = null) => {
  if (element && element.style) {
    element.style.display = "none";
  }
};

const checkVisibility = (element = null) => {
  if (element && element.style) {
    switch (element.style.display) {
      case "none":
        return false;
        break;
      case "":
      case "block":
      default:
        return true;
    }
  } else return false;
};

const setVisible = (element = null) => {
  if (element && element.style) {
    element.style.display = "block";
  }
};

const isMenuVisible = () => {
  return checkVisibility(MenuReference);
};

const showMenu = () => {
  if (!isMenuVisible()) {
    setVisible(MenuReference);
    setTimeout(getFocusActive, 300);
  }
};

const hideMenu = () => {
  if (isMenuVisible()) {
    setHidden(MenuReference);
  }
};
// =========================

const getCurrentEvent = (list, currentChannel) => {
  const eventFocus = list.find((item) => {
    if (item.channel.group_id === currentChannel.group_id) {
      const current = moment().isBetween(moment(item.date_begin, "YYYY/MM/DD HH:mm:ss"), moment(item.date_end, "YYYY/MM/DD HH:mm:ss"), null, "[)");
      return current;
    }
  });
  return eventFocus;
};

const requestEpg = async () => {
  const query = {
    from: 0,
    quantity: 100,
    date_from: moment().format("YYYYMMDD") + "000000",
    date_to: moment().add("days", 1).format("YYYYMMDD") + "000000",
  };

  const result = await epgLoader(query);

  return result;
};

const getChannelSaved = (listChannels) => {
  let selectChannel = false;

  const channelToPlay = DeviceStorage.getItem("lastChannel");

  for (let i = 0; i < listChannels.size; i++) {
    if (!selectChannel && listChannels.get(i).canPlay) {
      selectChannel = listChannels.get(i);
    }

    if (channelToPlay === listChannels.get(i).code && listChannels.get(i).canPlay) {
      selectChannel = listChannels.get(i);
    } else if (channelToPlay === listChannels.get(i).group_id && listChannels.get(i).canPlay) {
      selectChannel = listChannels.get(i);
    }
  }

  DeviceStorage.setItem("lastChannel", selectChannel);

  return selectChannel;
};

const useStyles = makeStyles((theme) => ({
  channelsContainer: () => ({
    boxSizing: "border-box",
    height: theme.sizeBody.hd.height,
    width: theme.sizeBody.hd.width,
    padding: "50px 40px 0px 40px",
    position: "fixed",
  }),

  opacityBody: {
    background: "rgba(0,0,0,.5)",
    position: "absolute",
    left: 0,
    top: 0,
    width: "100%",
    height: "100%",
  },
  flex: () => ({
    width: "100%",
    display: "flex",
    alignItems: "center",
    height: "100%",
  }),
}));

const Channels = ({ user }) => {
  const { t, i18n } = useTranslation();
  const history = useHistory();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [showPopupNotChannel, setShowPopupNotChannel] = useState(false);

  const [event, setEvent] = useState({});

  const [listEvents, setListEvents] = useState(List([]));
  const [listChannels, setListChannels] = useState(List([]));

  const [loading, setLoading] = useState(true);
  const [showChannels, setShowChannels] = useState(false);
  const [currentChannel, setCurrentChannel] = useState(false);

  const [loadingPlayer, setLoadingPlayer] = useState(true);

  const [showLanguages, setShowLanguages] = useState(false);
  const [optLanguages, setOptLanguages] = useState([]);
  const [languageSel, setLanguageSel] = useState(null);

  const { setContext: setContextModal } = useContext(ModalContext);

  const classes = useStyles();

  const keys = Device.getDevice().getKeys();

  const setFocus = () => {
    setTimeout(() => {
      const foco = showChannels ? "@listChannels" : "@epgFinal";
      window.SpatialNavigation.focus(foco);
    }, 200);
  };

  useEffect(() => {
    onShowChannels();
  }, [showChannels]);

  useEffect(() => {
    // si hay lista, play al canal
    if (listChannels.size !== 0) {
      changeChannel({
        channel: getChannelSaved(listChannels),
        firstLoad: true,
      });
    }
  }, [listChannels]);

  useEffect(() => {
    // ocultar menu si el usuario tiene canales
    if (!loading && listChannels.size !== 0) {
      hideMenu();
    } else {
      window.SpatialNavigation.focus("@container");
    }
  }, [loading]);

  useEffect(() => {
    document.addEventListener("keydown", handleKeyPress);

    // si el usuario no tiene canales, no hace falta cargar la lista ( mustro mens sin canales)
    // si tiene cargo lista, y cargo canPlay a cada uno
    const channels = async () => {
      MenuReference = document.getElementById("header");

      const userChannels = await getChannels(user.user_id);

      if (userChannels && userChannels.length) {
        const channels = await requestEpg();

        const aChannels = [];
        const aEvents = [];

        let pos = 0;
        channels.map((ch, i) => {
          const find = userChannels.find((uch) => uch.id === ch.group_id);
          aChannels.push({
            ...ch,
            code: find && find.code,
            canPlay: Boolean(find),
          });

          ch.events.map((e) => {
            aEvents.push({
              ...e,
              index: pos++,
              channel: {
                group_id: ch.group_id,
                image: ch.image,
                name: ch.name,
                number: ch.number,
                index: i,
                canPlay: Boolean(find),
              },
            });
          });
        });

        setListChannels(List(aChannels));
        setListEvents(List(aEvents));
      }
      setLoading(false);
    };

    channels();

    return () => {
      document.removeEventListener("keydown", handleKeyPress);
      document.removeEventListener("sn:focused", onFocus);

      showMenu();
    };
  }, []);

  useEffect(() => {
    if (listEvents.size && currentChannel) {
      const event = getCurrentEvent(listEvents, { group_id: currentChannel });
      setEvent(event);
    }
  }, [listEvents, currentChannel]);

  const handleKeyPress = (e) => {
    const currentKey = keys ? keys.getPressKey(e.keyCode) : null;
    switch (currentKey) {
      case "BACK":
        e.preventDefault();
        e.stopPropagation();

        let modal = document.getElementsByClassName(`modalNew`);
        if (modal && modal[0]) {
          closeModal();
        } else {
          history.goBack();
        }

        break;
      case "PREV":
        const prev = DeviceStorage.getItem("previousChannel");
        if (parseInt(prev)) {
          const channel = listChannels.filter((item) => item.group_id === prev);
          changeChannel({ channel: channel.length && channel[0] });
        }
        break;
      case "YELLOW":
        e.preventDefault();
        e.stopPropagation();

        yellowHandler();
        break;
      case "BLUE":
        e.preventDefault();
        e.stopPropagation();

        // retraso porque esta el foco en containerEvent que lleva el foco al evento
        setTimeout(() => {
          blueHandler();
        }, 400);
        break;
      case "GREEN":
        e.preventDefault();
        e.stopPropagation();

        setShowLanguages(true);
        setIsModalOpen(true);
        break;
    }
  };

  const blueHandler = useCallback(() => {
    document.addEventListener("sn:focused", onFocus);
    showMenu();
  }, []);

  const onFocus = (e) => {
    if (e.detail.sectionId === "container") {
      const ele = document.getElementById("tvContainer");
      if (ele) {
        hideMenu();
        document.removeEventListener("sn:focused", onFocus);
      }
    }
  };

  const changeChannel = ({ channel, firstLoad = false, event }) => {
    if (event) {
      channel = event.channel;
    }

    if (!channel || !channel.group_id) {
      return;
    }

    if ((event && event.current) || !event) {
      if (!channel.canPlay && !firstLoad) {
        setIsModalOpen(true);
        setShowPopupNotChannel(channel);
        return;
      }
      const last = DeviceStorage.getItem("lastChannel");

      if (channel.group_id === last) {
        return;
      }

      if (parseInt(last)) {
        DeviceStorage.setItem("previousChannel", last);
      }
      DeviceStorage.setItem("lastChannel", channel.group_id);

      setShowChannels(false);
      setLanguageSel(null);
      setLoadingPlayer(true);
      setCurrentChannel(channel.group_id);
    } else {
      setEvent(event);
      setIsModalOpen(true);
    }
  };

  const closeModal = () => {
    setShowLanguages(false);
    setIsModalOpen(false);
    setShowPopupNotChannel(false);
    setFocus();
  };

  const handleLanguage = ({ item }) => {
    setLanguageSel(item);
    closeModal();
  };

  const greenHandler = useCallback(() => {
    setShowLanguages(true);
    setIsModalOpen(true);
  }, []);

  const yellowHandler = useCallback(() => setShowChannels((prevState) => !prevState), []);

  const onShowChannels = () => {
    if (showChannels) {
      setContextModal({
        isOpen: true,
        component: ListChannels,
        disableAutoFocus: true,
        props: {
          currentChannel,
          getCurrentEvent,
          listEvents,
          listChannels,
          changeChannel,
        },
        onClose: () => {
          setShowChannels(false);
          closeModal();
        },
      });
    } else {
      setContextModal({
        isOpen: false,
      });
    }
  };

  if (loading) {
    return <LoadingComponent visible={true} />;
  }

  if (listChannels.size === 0) {
    return <MessageGeneric title={t("net_sin_plan_wooow", "Wooo!")} msg={t("net_sin_plan_pantalla", "Parece que você ainda não possui nenhuma assinatura. Para assinar, acesse a opção Planos, no aplicativo do seu celular.")} onClick={(e) => history.push("/plans")} textButton={"ver planos"} />;
  }

  return (
    <div id="tvContainer" style={{ position: "fixed", top: 0, left: 0 }}>
      {isModalOpen && (
        <Modal onClose={closeModal}>
          {showPopupNotChannel ? (
            <NotChannel channel={showPopupNotChannel} onClose={closeModal} />
          ) : showLanguages ? (
            <Languages isLive={true} title={"Áudio e Legenda"} options={optLanguages} selected={languageSel} onClick={handleLanguage} onClose={closeModal} />
          ) : (
            <Program event={event} channel={event.channel} onClose={closeModal} />
          )}
        </Modal>
      )}

      <div className={`${classes.channelsContainer} `}>
        <FullVideo setLoadingPlayer={setLoadingPlayer} currentChannel={currentChannel} setOptLanguages={setOptLanguages} languageSel={languageSel}>
          {!showChannels && (
            <Epg keys={keys} setEvent={setEvent} loading={loadingPlayer} channels={listChannels} events={listEvents} currentChannel={currentChannel} changeChannel={changeChannel} yellowHandler={yellowHandler} blueHandler={blueHandler} notHide={isModalOpen} greenHandler={greenHandler} />
          )}
        </FullVideo>
      </div>
    </div>
  );
};

export default Channels;
