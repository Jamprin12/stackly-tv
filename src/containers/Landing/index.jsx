import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
//Storybook
import SimpleImage from "../../components/2020/Image/SimpleImage";
import GradientLanding from "../../components/2020/Gradients/GradientLanding";
import ButtonGeneric from "../../components/2020/Buttons/ButtonGeneric";

import imageLogo from "../../assets/net_launch_logo_claro.svg";
import imageBackground from "../../assets/net_profile_background.png";

const useStyles = makeStyles((theme) => ({
  global: {
    backgroundColor: "black",
  },
  contatText: {
    marginLeft: 127,
  },
  contentButton: {
    display: "flex",
    justifyContent: "center",
    paddingTop: 60,
  },
  title: {
    fontSize: 43,
  },
  logo: {
    position: "absolute",
    top: 40,
    left: 127,
  },
  descriptionLanding: {
    fontSize: 27,
    marginTop: 20,
    fontWeight: 300,
  },
  buttonLanding: {
    marginTop: 40,
    width: "100%",
    textTransform: "none",
  },
  introInfo: {
    maxWidth: 615,
    height: theme.sizeBody.hd.height,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },
  introBackground: {
    minWidth: 665,
    height: theme.sizeBody.hd.height,
    background: theme.palette.primary.main,
    backgroundImage: `url(${imageBackground})`,
    backgroundSize: "cover",
    backgroundPosition: "right center",
    position: "relative",
  },
}));

const LandingPage = () => {
  const { t, i18n } = useTranslation();
  const classes = useStyles();
  const history = useHistory();

  useEffect(() => {
    setTimeout(() => {
      window.SpatialNavigation.focus(".landing .focusable:first-child");
    }, 200);
  }, []);

  const metadataTitle = t("net_launch_bienvenido", "Bem-vindo");
  const metadataDescrip = t(
    "net_launch_mensaje",
    "Reunimos o maior acervo de conteúdo, programas de TV, filmes e series. Tudo o que você gosta em um só lugar!"
  );
  const metadataButton = t("net_launch_boton_conectar", "conectar");

  return (
    <React.Fragment>
      <Grid container spacing={0} className={`${classes.global} landing`}>
        <Grid item xs={6} className={`${classes.introInfo}`}>
          <div className={classes.contatText}>
            <div className={`${classes.logo}`}>
              <SimpleImage width={183} image={imageLogo} alt="Netflex" />
            </div>
            <Typography component="h3" variant="h3" className={classes.title}>
              {metadataTitle}
            </Typography>
            <Typography variant="h5" className={classes.descriptionLanding}>
              {metadataDescrip}
            </Typography>
            <div className={classes.contentButton}>
              <ButtonGeneric
                href="#"
                onClick={(e) => {
                  e.preventDefault();
                  history.push("/login");
                }}
                title={metadataButton}
              />
            </div>
          </div>
        </Grid>
        <Grid
          item
          xs={6}
          className={classes.introBackground}
        >
          <GradientLanding width={"50%"} />
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export default LandingPage;
