import React, { useState, useCallback, useEffect, useContext } from "react";
import get from "lodash/get";
import { makeStyles } from "@material-ui/core/styles";
import { Collection, AutoSizer } from "react-virtualized";

import * as api from "../../requests/loaderNew";
import DeviceStorage from "../../components/DeviceStorage/DeviceStorage";

import ResumeNew from "../../components/2020/Resume/ResumeNew";
import CardLandscape from "../../components/2020/Cards/CardLandscape";
import TitleList from "../../components/2020/Typography/TitleList";
import LoadingComponent from "../../components/2020/Loading/LoadingComponent";
import Ribbons from "../../components/2020/Ribbon/RibbonsVirtualized";

import Popup from "../../containers/Templates/ChannelsModals";
import { ModalContext } from "../../containers/App/ModalContext";

import useDataFetch from "./getData";

import { Context } from "../../containers/App/Layout";

const useStyles = makeStyles((theme) => ({
  channelsContainer: () => ({
    boxSizing: "border-box",
    height: "720px!important",
    width: "1280px!important",
    padding: "75px 45px 0px 45px",
    zIndex: 999,
    //overflow: "hidden",
  }),
  ribbongridWrapper: () => ({
    bottom: 0,
    height: 360,
    width: theme.sizeBody.hd.width - 95,
    overflow: "hidden",
    position: "absolute",
  }),
  landscape: () => ({
    padding: 0,
    margin: 0,
    height: 655,
    bottom: 10,
    overflow: "hidden",
  }),
  contentLi: () => ({
    position: "relative",
    listStyle: "none",
    float: "left",
    minHeight: 165,
  }),
}));

const Home = ({ match, user, history, code: codeProp }) => {
  const { setContext } = useContext(Context);
  const { setContext: setContextModal } = useContext(ModalContext);

  const code = match.params.code || codeProp;

  const [dataOpenCannels, setDataOpenCannels] = useState(false);
  const [loadingOpenCannels, setLoadingOpenChannels] = useState(true);

  const [loadingFinal, setLoadingFinal] = useState(true);
  const [itemSelect, setItemSelect] = useState(false);
  const [{ result, type, loading }] = useDataFetch({ code, user });

  const classes = useStyles();

  useEffect(() => {
    const getDataOpenCannels = async () => {
      const data = await api.getOpenChannelData();
      if (data.length) {
        setDataOpenCannels(data);
      } else {
        setLoadingOpenChannels(false);
      }
    };

    getDataOpenCannels();
  }, []);

  useEffect(() => {
    if (!loadingFinal) {
      setTimeout(() => {
        setFocusContainer();
      }, 300);
    }
  }, [loadingFinal]);

  useEffect(() => {
    if (dataOpenCannels.length) {
      showModal();
    }
  }, [dataOpenCannels]);

  useEffect(() => {
    if (!loading && !loadingOpenCannels) {
      setLoadingFinal(false);
    }
  }, [loading, loadingOpenCannels]);

  const sendToPlay = useCallback(async (data, isTv = false) => {
    if (isTv) {
      DeviceStorage.setItem("lastChannel", typeof data === "string" ? data : data.group_id);
      history.push(`/node/tv`);
      return null;
    }

    setContext({
      visible: false,
      isSerie: Boolean(data.episode),
      isOpen: true,
      gropupId: data.group_id,
      onClose: setFocusContainer,
    });
  }, []);

  const focusHandler = useCallback(({ data }) => {
    setItemSelect(data);
  }, []);

  const cellSizeAndPositionGetter = ({ index }) => {
    const y = parseInt(index / 4);
    const x = index >= 4 ? index % 4 : index;

    return {
      height: 165,
      width: 280,
      x: x * 285,
      y: y * 165,
    };
  };

  const cellRenderer = ({ index, key, style, list }) => {
    const item = list[index];

    return (
      <div key={key} style={style}>
        <CardLandscape
          border={false}
          isFocusable={true}
          width={263}
          height={146}
          bgSize={"264px 146px"}
          bgSizeFocus={"280px 171px"}
          data={item}
          image={item.imageCard || item.cover}
          onClick={(e) => {
            e.preventDefault();
            handleVcard(item);
          }}
          focusHandler={() => {}}
        ></CardLandscape>
      </div>
    );
  };

  const setFocusContainer = useCallback(() => {
    window.SpatialNavigation.focus("@container");
  }, []);

  const handleVcard = useCallback((item) => {
    // 542015 group id 7 psicopatas con varios audios

    if (item.href) {
      history.push({
        pathname: item.href,
        state: { menuSelect: match.url },
      });
    } else {
      setContext({
        isSerie: Boolean(item.episode || item.episode_number),
        isOpen: true,
        // gropupId: item.group_id === "885866" ? "542015" : item.group_id || item.id,
        gropupId: item.group_id || item.id,
        onClose: setFocusContainer,
      });
    }
  }, []);

  const showModal = () => {
    setContextModal({
      isOpen: true,
      component: Popup,
      // disableAutoFocus: true,
      props: {
        data: dataOpenCannels,
        setItem: DeviceStorage.setItem,
      },
      onClose: () => {
        setLoadingOpenChannels(false);
        setFocusContainer();
      },
    });
  };

  if (loadingFinal) {
    return <LoadingComponent />;
  }

  if (type === "list") {
    const list = get(result, "0.items") || [];

    let title = "";
    switch (code) {
      case "movies":
        title = "Movies";
        break;
      case "filmes_acao":
        title = "Ação";
        break;
      case "filmes_teens":
        title = "Teens";
        break;
      case "filmes_comedia":
        title = "Comédia";
        break;
    }

    return (
      <div
        style={{
          display: "flex",
          width: "100%",
          height: "100%",
          flexFlow: "column",
          paddingLeft: "15px",
        }}
      >
        <TitleList title={title} />
        <AutoSizer>
          {({ height, width }) => (
            <Collection
              className={classes.collection}
              cellCount={list.length}
              cellRenderer={(data) => cellRenderer({ ...data, list })}
              cellSizeAndPositionGetter={cellSizeAndPositionGetter}
              verticalOverscanSize={50}
              height={height - 60} // el autoSizer detecta el height de toda la app, hay que restarle el titulo
              width={width}
            />
          )}
        </AutoSizer>
      </div>
    );
  }

  return (
    <ResumeNew code={code} item={itemSelect || get(result, "0.items.0") || false}>
      <div className={`${classes.ribbongridWrapper}`}>
        <Ribbons api={api} prefixId="home" list={result} focusHandler={focusHandler} handleVcard={handleVcard} sendToPlay={sendToPlay} />
      </div>
    </ResumeNew>
  );
};

export default Home;
