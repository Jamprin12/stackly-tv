import React, { useState, useEffect } from "react";
import get from "lodash/get";
import { useTranslation } from "react-i18next";

import { levelLoader } from "../../requests/loaderNew";

const getDataHome = ({ code, user }) => {
  const { t, i18n } = useTranslation();
  const [result, setResult] = useState([]);
  const [type, setType] = useState("list");
  const [loading, setLoading] = useState(true);

  const getDataApi = async ({ code = "home", user }) => {
    const userStatus =
      (user && user.superhighlight && user.superhighlight.join()) ||
      "anonymous";
    const userHash = get(user, "session_userhash");
    const lasttouch = get(user, "lasttouch.seen");

    try {
      const response = await levelLoader("level2", {
        user_token: get(user, "user_token"),
        node: code,
        user_status: userStatus,
        user_hash: userHash,
        lasttouch: lasttouch,
        first_page: true,
      });
      return response || [];
    } catch (e) {
      console.log("Error level", e);
      return [];
    }
  };

  useEffect(() => {
    const getData = async () => {
      const { components, gridType } = await getDataApi({ code, user });

      const result = await Promise.all(
        components
          .filter((ribbon) => ribbon.items && ribbon.items.length)
          .map((ribbon) => {
            const { id } = ribbon;

            if (
              id === "Continuar-Assistindo" ||
              id === "nx-user-seen" ||
              id === "nx-live-events" ||
              id === "nx-user-channels"
            ) {
              return {
                ...ribbon,
                items: ribbon.items.map((item, i) => {
                  return {
                    ...item,
                    sendToPlay: true,
                    subTitle: `${t(
                      "net_inicio_carrusel_ao_vivo",
                      "ao vivo - termina as"
                    )} ${item.date_end_new}`,
                  };
                }),
              };
            } else {
              return ribbon;
            }
          })
      );

      setType(gridType);
      setResult(result);
      setLoading(false);
    };

    setLoading(true);
    getData();
  }, []);

  return [{ result, type, loading }];
};

export default getDataHome;
