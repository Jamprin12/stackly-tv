import React, { useState, useEffect, useContext, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import get from "lodash/get";
import moment from "moment";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import DeviceStorage from "../../components/DeviceStorage/DeviceStorage";
import Device from "../../devices/device";

import * as api from "../../requests/loaderNew";
import { predictiveSearch } from "../../requests/loaderNew";

import Keyboard from "../../components/2020/Keyboard/keyboard";
import Input from "../../components/2020/Input/Input";
import Ribbon from "../../components/2020/Ribbon/RibbonsPrueba";
import RenderButton from "../../components/2020/Search/SearchButtons";
import RenderLoading from "../../components/2020/Search/SearchLoading";
import SearchContainer from "../../components/2020/Search/SearchContainer";
import SearchContentAutocomplete from "../../components/2020/Search/SearchContentAutocomplete";
import SearchContentRibbons from "../../components/2020/Search/SearchContentRibbons";
import {
  getItemProperties,
  getItemPropertiesTalent,
  getItemPropertiesTv,
} from "../../components/2020/resizeTmp";

import { Context } from "../../containers/App/Layout";

const keysDevice = Device.getDevice().getKeys();

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    width: "100%",
    height: "100%",
    flexFlow: "column",
    justifyContent: "space-between",
  },
  keyboard: {
    width: "100%",
    background: "#212224",
    alignSelf: "flex-end",
  },
  formContainer: {
    display: "flex",
    height: "100%",
    alignSelf: "center",
    alignItems: "center",
  },
  form: {
    display: "flex",
    flexFlow: "column",
    width: "500px",
  },
}));

const Search = ({ user }) => {
  const { t, i18n } = useTranslation();
  const { setContext } = useContext(Context);
  const history = useHistory();
  const classes = useStyles();

  const [values, setValues] = useState("");

  const [pagSelected, setPagSelected] = useState("all");
  const [data, setData] = useState({});
  const [result, setResult] = useState([]);
  const [loading, setLoading] = useState(false);

  let searchTimeOut;

  const setFocusKeyboar = useCallback(() => {
    setTimeout(() => {
      window.SpatialNavigation.focus("@keyboard");
    }, 100);
  }, []);

  const setFocusContainer = useCallback(() => {
    setTimeout(() => {
      window.SpatialNavigation.focus("@searchCont");
    }, 100);
  }, []);

  useEffect(() => {
    setFocusKeyboar();
  }, []);

  useEffect(() => {
    searchByText(values);
  }, [values]);

  useEffect(() => {
    if (get(data, pagSelected)) {
      setLoading(true);

      setTimeout(() => {
        setResult(get(data, pagSelected) || []);
        setLoading(false);
      }, 200);
    }
  }, [pagSelected, data]);

  const searchByText = async (text) => {
    if (searchTimeOut) {
      clearTimeout(searchTimeOut);
    }

    if (text.length < 3) {
      setResult([]);
      setLoading(false);
      setData({});
      return false;
    }

    setLoading(true);

    searchTimeOut = setTimeout(async () => {
      try {
        let data = await predictiveSearch(text);

        const res = get(data, "response");

        const movies01 = get(res, "prediction.movies.movie") || [];
        const series01 = get(res, "prediction.series.serie") || [];
        const movies02 = get(res, "prediction.now.movies.movie") || [];
        const series02 = get(res, "prediction.now.series.serie") || [];
        const tv01 = get(res, "prediction.epgs.epg.now") || [];
        const tv02 = get(res, "prediction.epgs.epg.future") || [];
     
        const talents01 =
          get(res, "prediction.external_talents.gracenote") || [];
        const talents02 = get(res, "prediction.talents.talent") || [];

        const talents = [
          ...talents01.map((item) => {
            return {
              ...item,
              type: "talent",
            };
          }),
          ...talents02.map((item) => {
            return {
              ...item,
              type: "talent",
            };
          }),
        ];

        const movies = [
          ...movies01.map((item) => {
            return {
              ...item,
              type: "search",
            };
          }),
          ...movies02.map((item) => {
            return {
              ...item,
              type: "search",
            };
          }),
        ];
        const series = [
          ...series01.map((item) => {
            return {
              ...item,
              type: "search",
            };
          }),
          ...series02.map((item) => {
            return {
              ...item,
              type: "search",
            };
          }),
        ];
        const tv = [
          ...tv01.map((item) => {
            return {
              ...item,
              type: "ao-vivo",
            };
          }),
          ...tv02.map((item) => {
            return {
              ...item,
              type: "ao-vivo",
            };
          }),
        ];

        const all = [...movies, ...series, ...tv, ...talents];

        setData({
          movies,
          series,
          talents,
          tv,
          all,
        });
      } catch (e) {
        console.log("info error", e);
        setData({});
      }
      setLoading(false);
    }, 300);
  };

  const handleVcard = useCallback((item) => {
    setContext({
      isOpen: true,
      user: user,
      gropupId: item.group_id || item.id,
      onClose: setFocusContainer,
      isSerie: Boolean(item.episode || item.episode_number),
    });
  }, []);

  const setFocusInput = useCallback(({ name }) => {
    setFocusKeyboar();
  }, []);

  return (
    <div className={`${classes.container}`}>
      <SearchContainer>
        <SearchContentAutocomplete>
          <Input
            currentFocus={"search"}
            icon
            name="search"
            placeholder={t("search_result_welcome", "Ingresa tu bÃºsqueda")}
            value={values}
            onClick={setFocusInput}
          />
        </SearchContentAutocomplete>
        <SearchContentRibbons height={365}>
          <RenderButton
            pagSelected={pagSelected}
            setPagSelected={setPagSelected}
            result={result}
          />
          {loading || !values || values.length < 3 || !result.length ? (
            <RenderLoading
              result={result}
              input={values.length > 3 ? values : ""}
              loading={loading}
            />
          ) : (
            <div
              className="fromVMenu"
              style={{ marginTop: 15, marginLeft: 25 }}
            >
              <Ribbon
                isFirst={true}
                isLast={true}
                api={api}
                visibleNumber={pagSelected === "talents" ? 8 : 5}
                scrollToTop={false}
                title={"resultados (" + result.length + ")"}
                //type={"search"}
                type={"ao-vivo"}
                sendToPlay={(group_id) => {
                  DeviceStorage.setItem("lastChannel", group_id);
                  history.push("/node/tv");
                }}
                snUp="@searchButtons"
                snDown="@keyboard"
                clickHandler={handleVcard}
                items={result.map((item) => {
                  let now = moment();
                  const time = now.toObject();
                  time.minutes -= time.minutes % 15;
                  now = now.format("YYYYMMDDHHmmss");
                  now =
                    now.substring(0, now.length - 4) +
                    (time.minutes == "0" ? "00" : time.minutes.toString()) +
                    "00";

                  if (item.type === "ao-vivo") {
                    let itemResult = getItemPropertiesTv({
                      item,
                      common: get(item, "channel_group.common"),
                      event: get(item, "channel_group.common"),
                      dateFrom: now,
                      dateBegin: item.begintime,
                      dateEnd: item.endtime,
                      isSearch: true,
                    });

                    const dateFormat = "YYYY/MM/DD HH:mm:ss";
                    const isCurrent = moment().isBetween(itemResult.date_begin, itemResult.date_end, null, '(]');

                    let momentBeginTime = moment(itemResult.date_begin, dateFormat);
                    let momentEndTime = moment(itemResult.date_end, dateFormat);

                    let newDateFormat = momentBeginTime.locale('pt-br').calendar(null, {
                      lastDay: "[ontem às] HH[h]",
                      nextDay: () => {
                        return momentBeginTime.format("mm") === "00" ? "[amanhã às] HH[h]" : "[amanhã às] HH[h]mm";
                      },
                      nextWeek: () => {
                        return momentBeginTime.format("mm") === "00" ? "[dia] DD/MM [às] HH[h]" :  "[dia] DD/MM [às] HH[h]mm";
                      },
                      sameElse: "L",
                      sameDay: () => {
                        if(isCurrent) {
                          return momentEndTime.format('mm') === '00' ? `[ao vivo · termina às] ${momentEndTime.format('HH')}[h]` : `[ao vivo · termina às] ${momentEndTime.format('HH')}[h]${momentEndTime.format('mm')}`
                        } else {
                          return momentBeginTime.format("mm") === "00" ? "[começa hoje às] HH[h]" : "[começa hoje às] HH[h]mm"
                        }
                      },
                    });

                    return {
                      type: "ao-vivo",
                      sendToPlay: true,
                      ...itemResult,
                      subTitle: newDateFormat,
                    };
                  } else if (item.type === "talent") {
                    return {
                      type: "talent",
                      ...getItemPropertiesTalent({ item }),
                    };
                  }
                  return {
                    ...getItemProperties({
                      item,
                      version: "v5.86",
                    }),
                    type: item.type,
                    clickHandler: () => handleVcard(item),
                  };
                })}
              />
            </div>
          )}
        </SearchContentRibbons>
      </SearchContainer>
      <div className={classes.keyboard}>
        <Keyboard
          region={DeviceStorage.getItem('region')}
          keysDevice={keysDevice}
          snUp={result.length ? "@searchCont" : "@searchButtons"}
          currentValue={values}
          onClick={(text) => {
            setValues(text);
          }}
        />
      </div>
    </div>
  );
};

export default Search;
