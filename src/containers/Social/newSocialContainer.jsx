import React, { useState, useEffect, useCallback, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import get from "lodash/get";
import { useTranslation } from "react-i18next";

import * as api from "../../requests/loaderNew";
import { seenLoader } from "../../requests/loaderNew";

import Ribbon from "../../components/2020/Ribbon/RibbonsPrueba";
import LoadingComponent from "../../components/2020/Loading/LoadingComponent";
import { getItemProperties } from "../../components/2020/resizeTmp";

import { Context } from "../../containers/App/Layout";

import imageBackground from "./images/net_profile_background.jpg";

const useStyles = makeStyles((theme) => ({
  fullContainer: {
    display: "flex",
    width: "100%",
    height: "100%",
    flexFlow: "column",
  },
  title: {
    color: "#c7c7c7",
    fontSize: 22,
  },
  message: {
    color: "#c7c7c7",
    fontSize: 18,
    fontWeight: "lighter",
    textDecoration: "none",
  },
  background: {
    backgroundImage: `url(${imageBackground})`,
    backgroundSize: "cover",
    width: "100%",
    height: 400,
    padding: "100px 0px 0 0px",
    boxSizing: "border-box",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minHeight: 350,
    paddingLeft: 20,
  },
  contentRibbonsProfile: {
    paddingLeft: 20,
    paddingTop: 60,
    width: "100%",
    height: 320,
    boxSizing: "border-box",
  },
  name: {
    width: "45%",
    fontSize: 48,
    textAlign: "left",
  },
  contentAvatar: {
    width: "55%",
  },
  avatar: {
    borderRadius: "50%",
    border: "4px solid #60676C",
    height: 183,
    width: 184,
    backgroundColor: "transparent",
    backgroundRepeat: "no-repeat",
    backgroundSize: 358,
    backgroundPosition: "center",
    backgroundImage: `url(${require("./images/android.png")})`
  },
}));

const SocialContainer = ({ user }) => {
  const { t, i18n } = useTranslation();
  const { setContext } = useContext(Context);
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [carrouseles, setCarrouseles] = useState([]);

  useEffect(() => {
    if (!loading) {
      setTimeout(() => {
        setFocusContainer();
      }, 500);
    }
  }, [loading]);

  useEffect(() => {
    const getData = async () => {
      try {
        const result = await seenLoader(user);
        const cars = get(result, "groups") || [];
        const final = cars.map((item) => {
          return getItemProperties({
            item,
            version: "v5.86",
          });
        });
        setCarrouseles(final);
        setLoading(false);
      } catch (e) {
        console.log("error: ", e);

        setCarrouseles([]);
        setLoading(false);
      }
    };
    getData();

    setTimeout(() => {
      setFocusContainer();
    }, 800);
  }, []);

  const handleVcard = useCallback((item) => {
    setContext({
      isOpen: true,
      gropupId: item.group_id || item.id,
      onClose: setFocusContainer,
    });
  }, []);

  const setFocusContainer = useCallback(() => {
    window.SpatialNavigation.focus("@container");
  }, []);

  if (loading) {
    return <LoadingComponent background="black" visible={true} />;
  }

  return (
    <div className={classes.fullContainer}>
      <div className={classes.background}>
        <h1 className={classes.name}>{`${user.firstname}`}</h1>
        <div className={classes.contentAvatar}>
          <div className={classes.avatar}></div>
        </div>
      </div>
      {carrouseles.length === 0 ? (
        <div style={{ padding: "20px" }}>
          <h2 className={classes.title}>
            {t("profile.social_profile_seen_title", "Recien Vistos")}
          </h2>
          <div style={{ width: "100%" }}>
            <a
              className={`focusable ${classes.message}`}
              href={"javascript:void(0)"}
              dangerouslySetInnerHTML={{
                __html: t("profile.own_social_profile_empty_seen_msg"),
              }}
            />
          </div>
        </div>
      ) : (
        <div className={classes.contentRibbonsProfile}>
          <Ribbon
            isFirst={true}
            isLast={true}
            api={api}
            clickHandler={handleVcard}
            title={t("profile.social_profile_seen_title", "Recien Vistos")}
            type="landscape"
            slidesToShow={4.2}
            items={carrouseles}
            carruselTitle={"Recien Vistos"}
            snUp={""} // para que el foco no vaya a ningun lado
          />
        </div>
      )}
    </div>
  );
};

export default SocialContainer;
