/**
 * Main AAF playing state
 */
export const AAFPLAYER_PLAY = 'PLAYING';
export const AAFPLAYER_STOP = 'STOPPING';

/**
 * Tipos de contenido que se playean
 * El "grabado", debería ser del tipo VOD?
 */
export const AAF_CONTENT_IS_NPVR = 'content_is_npvr';
export const AAF_CONTENT_IS_VOD = 'content_is_vod';
export const AAF_CONTENT_IS_CATCHUP = 'content_is_catchup';
export const AAF_CONTENT_IS_TIMESHIFT = 'content_is_timeshift';
export const AAF_CONTENT_IS_TV = 'content_is_tv';
export const AAF_CONTENT_IS_AUDIO = 'content_is_audio';
export const AAF_CONTENT_IS_RADIO = 'content_is_radio';

/**
 * TODO Este debería ser el único lugar donde se definen las constantes de abajo
 * Tipos de streaming
 */
export const SS = 'smooth_streaming';
export const DASHWV = 'dashwv';
export const DASHWV_MA = 'dashwv';
export const SS_MA = 'smooth_streaming_ma';
export const HLS = 'hls';
export const HLS_MA = 'hls_ma';
export const HLS_KR = 'hls_kr';
export const HLSPRM = 'hlsprm';
export const IP_MULTICAST = 'ip_multicast';
export const IP_MULTICAST_UDP = 'ip_multicast_udp';
export const DVBC = 'dvbc';
export const DVBS = 'dvbs';
export const WVC = 'widevine_classic';
export const AUDIO = 'audio';
export const RADIO = 'radio';
export const SPOT = 'spot';
export const PLAYERIMAGE = 'image';

/**
 * Tipos de logging messages
 */
export const LOG_BASIC = 0;
export const LOG_WARNING = 1;
export const LOG_ERROR = 2;
export const LOG_INFO = 3;

/**
 * Time retry playing
 */
export const retryTimeEvents = 15000;


/**
 * PGM errors para saber si reintentar llamada pgm hasta encontrar un streamType soportado por el device
 */
export const PLY_PLY_00009 = 'PLY_PLY_00009'; // Content is not available in the requested format
export const PLY_PLY_00001 = 'PLY_PLY_00001'; // Invalid stream type
