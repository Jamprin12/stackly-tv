import get from "lodash/get";

import getAppConfig from "../../../config/appConfig";
import Device from "../../../devices/device";

import { playerGetMedia } from "../../../requests/loaderNew";

import * as constant from "./constants";

class AAFPlayerMain {
  playerInstance = null;
  wrapperContainer = null;
  pgm = null;

  devicePlayerParams = {};
  callbacks = {};
  bindedevents = {};

  constructor(wrapperContainer, callbacks) {
    this.platform = Device.getDevice().getPlatform();
    this.playerInstance = Device.getDevice().getPlayer();

    this.wrapperContainer = wrapperContainer;
    this.callbacks = callbacks;
  }

  isFunction = (obj) => {
    return Object.prototype.toString.call(obj) === '[object Function]';
  }

  getAudioTracks = () => {
    const result = this.playerInstance.getAudioTracks();

    return result;
  };

  setAudioTrack = (option) => {
    this.playerInstance.setAudioTrack(option);
  };

  seek(seconds) {
    this.playerInstance && this.playerInstance.seek(seconds);

    if (
      this.callbacks.onPlayerControlSeek &&
      this.isFunction(this.callbacks.onPlayerControlSeek)
    ) {
      this.callbacks.onPlayerControlSeek(seconds);
    }
  }

  setPlayerState = ({
    playerstate,
    groupId,
    contentId = null,
    isLive = true,
    isPreview = false,
    paywayToken = null,
  }) => {
    this.changingLang = false;
    if (
      get(this.nextOptions, "groupId") === groupId &&
      get(this.nextOptions, "contentId") !== contentId
    ) {
      this.changingLang = true;
    }

    this.nextOptions = {
      playerstate: playerstate,
      groupId: groupId,
      contentId: contentId,
      isLive: isLive,
      isPreview: isPreview,
      paywayToken: paywayToken,
    };

    this.stopPlayer(); // en channels cuando cambias de canal, sin esto no cambia

    switch (playerstate) {
      case constant.AAFPLAYER_PLAY:
        this.sendLoading();
        this.__startPlayer();
        break;
      default:
      case constant.AAFPLAYER_STOP:
        this.stopPlayer(true);
        break;
    }
  };

  async __startPlayer() {
    try {
      this.pgm = await this.__getDataPlaying(this.nextOptions);

      if (this.callbacks.onResolveParams) {
        // mergeo el getMedia.languages ( que tiene el current ) , con el del extendedcommon que tiene la desc
        const languages = get(this.pgm, "language.options", []);
        const languagesExtras = get(
          this.pgm,
          "group.common.extendedcommon.media.language.options.option",
          []
        );
        const langFinal = languagesExtras.map((item) => {
          const sel = languages.find(
            (element) => element.content_id === item.content_id
          );
          return {
            ...sel,
            ...item,
          };
        });
        // -------------------

        this.callbacks.onResolveParams({
          initialTime: this.nextOptions.isPreview
            ? 0
            : get(this.pgm, "media.initial_playback_in_seconds", 0),
          languages: langFinal,
          changingLang: this.changingLang,
        });
      }
    } catch (e) {
      this.callbacks.onResolveError({
        message: e.message,
        code: e.code,
        replayOptions: "",
      });
    }
  }

  load = (time) => {
    try {
      const options = this.__getVideoParams(time);
      this.nextOptions = options;

      options.events = this.bindedevents;

      this.playerInstance.createMedia(options);
      this.playerInstance.loadMedia();

      this.play();
    } catch (e) {
      this.callbacks.onResolveError({
        message: e.message,
        code: e.code,
        replayOptions: "",
      });
    }
  };

  sendLoading = () => {
    this.callbacks.setLoading();
  };

  play = () => {
    this.playerInstance.play();
  };

  pause = () => {
    this.playerInstance.pause();
  };

  stopPlayer(sendCallbacks) {
    this.playerInstance.destroy(sendCallbacks);

    if (
      sendCallbacks &&
      this.callbacks.onPlayerStopMedia &&
      this.isFunction(this.callbacks.onPlayerStopMedia)
    ) {
      this.callbacks.onPlayerStopMedia(true);
    }
  }

  getApaSupportedStream() {
    let device = Device.getDevice().getSubplatform() || this.platform;

    const streams = {
      arris: {
        vod: ["dashwv_ma", "dashwv"],
        live: ["dashwv_ma", "dashwv", "ip_multicast", "hls_kr"],
        timeshift: ["dashwv"],
      },
      android: {
        vod: ["smooth_streaming_ma", "smooth_streaming"],
        live: ["ip_multicast", "hls_kr"],
        timeshift: ["hls_kr"],
      },
      hisense: {
        vod: ["smooth_streaming_ma", "smooth_streaming"],
        live: ["smooth_streaming"],
        timeshift: ["smooth_streaming"],
      },
      lg: {
        vod: ["smooth_streaming_ma", "smooth_streaming", "hls"],
        live: ["hls_kr"],
        timeshift: ["hls_kr"],
      },
      nagra: {
        vod: ["hlsprm_ma", "hlsprm"],
        live: ["dvbc_ma", "dvbc"],
        timeshift: ["hlsprm"],
      },
      opera: {
        vod: ["smooth_streaming", "hls"],
        live: ["hls_kr", "hls"],
        timeshift: ["hls_kr"],
      },
      polaroid: {
        vod: ["dashwv"],
        live: ["hls_kr"],
        timeshift: ["hls_kr"],
      },
      ps4: {
        vod: ["smooth_streaming_ma", "smooth_streaming"],
        live: ["smooth_streaming"],
        timeshift: ["smooth_streaming"],
      },
      samsung: {
        vod: [
          "smooth_streaming_ma",
          "smooth_streaming",
          "hls",
          "widevine_classic",
        ],
        live: ["smooth_streaming", "hls_kr", "hls"],
        timeshift: ["smooth_streaming"],
      },
      sony: {
        vod: ["smooth_streaming_ma", "smooth_streaming"],
        live: ["smooth_streaming", "smooth_streaming_ma"],
        timeshift: ["hls_kr", "hls"],
      },
      stbcoship: {
        vod: ["smooth_streaming_ma", "smooth_streaming"],
        live: ["hls_kr"],
        timeshift: ["hls_kr"],
      },
      stbhuawei: {
        vod: ["smooth_streaming_ma", "smooth_streaming"],
        live: [
          "ip_multicast_lms",
          "ip_multicast_udp",
          "ip_multicast",
          "hls_kr",
        ],
        timeshift: ["hls_kr"],
      },
      stbkaon: {
        vod: ["smooth_streaming_ma", "smooth_streaming"],
        live: ["ip_multicast_lms", "ip_multicast", "hls_kr"],
        timeshift: ["hls_kr"],
      },
      tizen: {
        vod: ["smooth_streaming", "smooth_streaming_ma"],
        live: ["smooth_streaming_ma", "smooth_streaming"],
        timeshift: ["smooth_streaming"],
      },
      web0s: {
        vod: ["smooth_streaming_ma", "smooth_streaming"],
        live: ["smooth_streaming_ma", "smooth_streaming"],
        timeshift: ["smooth_streaming"],
      },
      workstation: {
        vod: ["smooth_streaming_ma", "smooth_streaming", "hls"],
        live: ["hls_kr", "hls"],
        timeshift: ["hls_kr", "hls"],
      },
      workstationChafari: {
        vod: ["hls_ma", "hls"],
        live: ["hls_kr", "hls"],
        timeshift: ["hls_kr", "hls"],
      },
    };

    return streams[device];
  }

  async __getDataPlaying({
    groupId,
    contentId,
    isLive,
    isPreview,
    paywayToken,
  }) {
    let nextContentType = isLive
      ? constant.AAF_CONTENT_IS_TV
      : constant.AAF_CONTENT_IS_VOD;

    let supported_stream_types = this.getApaSupportedStream();

    let supportedStreams = [];
    switch (nextContentType) {
      case constant.AAF_CONTENT_IS_VOD:
        supportedStreams = supported_stream_types["vod"];
        break;
      case constant.AAF_CONTENT_IS_TV:
        supportedStreams = supported_stream_types["live"];
        break;
      default:
        throw new Error(
          "Error al intentar reproducir video, falló la resolución del contentType a reproducir"
        );
    }

    if (supportedStreams.length === 0) {
      throw new Error(
        "El device no tiene definidos los tipos de streaming que soporta"
      );
    }
    // --------------------------------------------------------------

    // saco los _ma ( ya estaba asi cuando llegue )
    supportedStreams = supportedStreams.filter(
      (stream) => !stream.includes("_ma")
    );

    let pgm = await playerGetMedia(
      groupId,
      contentId,
      isPreview,
      supportedStreams[0],
      null,
      null,
      paywayToken
    );

    if (pgm === null) {
      throw new Error(
        "Error, el contenido no se puede reproducir en este dispositivo"
      );
    } else if (pgm.errors) {
      let error = new Error();

      error = {
        message: get(pgm, "errors.0.message"),
        code: get(pgm, "errors.0.code"),
      };

      throw error;
    }
    pgm.streamType = supportedStreams[0]

    return pgm;
  }

  __getVideoParams(time) {
    const pgm = this.pgm;

    const extendedCommonLangOptions = get(
      pgm,
      "group.common.extendedcommon.media.language.options.option",
      []
    );

    // por ahora no vi que esto llegue
    let multipleLangOptions = {};
    if (pgm.media.subtitles) {
      multipleLangOptions.subtitles = pgm.media.subtitles;
    }
    if (pgm.media.audio) {
      multipleLangOptions.audio = pgm.media.audio;
    }
    // -------------------------------

    const config = getAppConfig();
    return {
      ...this.nextOptions,
      src: get(pgm, "media.video_url"),
      drmInfo: {
        device_id: config.device_id,
        server_url: get(pgm, "media.server_url"),
        challenge: get(pgm, "media.challenge"),
        certificate_url: get(pgm, "media.certificate_url"),
        resultCert: get(pgm, "media.resultCert"),
      },
      provider: get(pgm, "group.common.extendedcommon.media.proveedor.nombre"),
      resume: time,
      streamType: get(pgm, "streamType"),
      groupId:
        get(pgm, "media.groupId") ||
        get(pgm, "group.common.id") ||
        get(pgm, "groupId"),
      extendedCommonLangOptions,
      multipleLangOptions,
      totalTime: get(pgm, "media.duration.seconds", null),
      creditsTime: parseInt(
        get(
          pgm,
          "group.common.extendedcommon.media.rollingcreditstime",
          -30 // poner en un nuevo config ?
        )
      ),
      parentWrapper: this.wrapperContainer,
    };
  }

  isEndScreen(currentTime) {
    // para probar siguiente episodio
    // if (!this.prueba) {
    //   this.callbacks.onCreditsTime(this.pgm.next_group_id);
    //   this.prueba = true;
    // } else {
    //   return null;
    // }
    // -----------------------------

    if (
      this.nextOptions.isPreview ||
      this.nextOptions.isLive ||
      this.sendCreditsTime
    ) {
      return null;
    }

    const totalTime = this.totalDuration;
    const creditsTimeSeconds = this.nextOptions.creditsTime + totalTime;

    if (currentTime >= creditsTimeSeconds) {
      this.sendCreditsTime = true;

      if (
        this.callbacks.onCreditsTime &&
        this.isFunction(this.callbacks.onCreditsTime)
      ) {
        this.callbacks.onCreditsTime({
          item: get(this.pgm, "next_group.common", {}),
          groupId: get(this.pgm, "next_group_id"),
        });
      }
    }
  }
}

export default AAFPlayerMain;
