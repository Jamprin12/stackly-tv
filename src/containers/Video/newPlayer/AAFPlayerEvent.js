import AAFPlayerMain from "./AAFPlayerMain";
import * as playerConstant from "../../../devices/playerConstants";

class AAFPlayerEvent extends AAFPlayerMain {
  constructor(wrapperContainer, callbacks) {
    super(wrapperContainer, callbacks);
    this.bindEvents();
  }

  // retorna el tiempo total
  getDuration() {
    return this.playerInstance.getDuration();
  }

  /**
   * Bind de eventos que se envían durante el ciclo de playing
   */
  bindEvents() {
    // Eventos estándar
    this.bindedevents.onLoad = this.onLoad.bind(this);
    this.bindedevents.onError = this.onError.bind(this);
    this.bindedevents.onWaiting = this.onWaiting.bind(this);
    this.bindedevents.onTimeUpdate = this.onTimeUpdate.bind(this);
    this.bindedevents.onPlaying = this.onPlaying.bind(this);
    this.bindedevents.onFinished = this.onFinished.bind(this);
    this.bindedevents.onCanPlay = this.onCanplay.bind(this);
    this.bindedevents.onDurationChange = this.onDurationchange.bind(this);
    this.bindedevents.onBufferingStart = this.onBufferingStart.bind(this);
    this.bindedevents.onBufferingProgress = this.onBufferingProgress.bind(this);
    this.bindedevents.onBufferingFinish = this.onBufferingFinish.bind(this);
    //Evento de tizen para reiniciar el player
    this.bindedevents.onReplay = this.onReplay.bind(this);
    // Eventos particulares música
    this.bindedevents.onPlayingSong = this.onPlayingSong.bind(this);
    this.bindedevents.onPlayingRadio = this.onPlayingRadio.bind(this);
    //Evento para seek tracking
    this.bindedevents.onBookMark = this.onBookMark.bind(this);
    this.bindedevents.onBitrateChange = this.onBitrateChange.bind(this);
  }

  onLoad(evt) {
    if (this.callbacks.onLoad && typeof this.callbacks.onLoad === "function") {
      this.callbacks.onLoad(evt, this.devicePlayerParams);
    }
  }

  onError(errorMessage = null, errorCode = null) {
    console.log("playerEvent error", errorCode, errorMessage )
    if (this.callbacks.onResolveError) {
      this.callbacks.onResolveError(
        errorMessage,
        errorCode,
        this.devicePlayerParams,
        // this.getCurrentTime()
      );
    }
  }

  onWaiting(evt = null) {
    if (this.callbacks.onWaiting) {
      this.callbacks.onWaiting(evt);
    }
  }

  /**
   *
   * @param {*} currentTime en segundos
   */
  onTimeUpdate(currentTime) {
    const finalTime =
      !this.charged && currentTime < this.initialTime
        ? this.initialTime
        : currentTime;

    this.isEndScreen(currentTime); // calcular el final
    if (
      this.callbacks.onProgress &&
      typeof this.callbacks.onProgress === "function"
    ) {
      this.charged = true;
      this.callbacks.onProgress(finalTime);
    }
  }

  onPlaying(evt = null) {}

  onFinished(evt = null) {
    if (
      this.callbacks.onEnded &&
      typeof this.callbacks.onEnded === "function"
    ) {
      this.callbacks.onEnded(true);
    }
  }

  onCanplay(evt = null) {
    if (
      this.callbacks.onCanPlay &&
      typeof this.callbacks.onCanPlay === "function"
    ) {
      this.callbacks.onCanPlay(evt);
    }

    if (this.devicePlayerParams.streamType === playerConstant.AUDIO) {
      this.callbacks.onPlayingSong();
    } else if (this.devicePlayerParams.streamType === playerConstant.RADIO) {
      this.callbacks.onPlayingRadio();
    }
  }

  onDurationchange(evt = null) {
    this.totalDuration = this.playerInstance.getDuration(); // para no llamar todo el tiempo en isendScreen

    // If VOD, calculate finplayer
    this.creditsTime = null;
    if (
      this.callbacks.onDurationChange &&
      typeof this.callbacks.onDurationChange === "function"
    ) {
      this.callbacks.onDurationChange(this.getDuration());
    }
  }

  onBufferingStart(evt = null) {
    if (this.callbacks.onBufferingStart) {
      this.callbacks.onBufferingStart(evt, this.devicePlayerParams);
    }
  }

  onBufferingProgress(evt = null) {}

  onBufferingFinish(evt = null) {
    if (this.callbacks.onBufferingFinish) {
      this.callbacks.onBufferingFinish(evt, this.devicePlayerParams);
    }
  }

  // Custom - eventos música
  onPlayingSong(evt = null) {
    if (
      this.callbacks.onPlayingSong &&
      typeof this.callbacks.onPlayingSong === "function"
    ) {
      this.callbacks.onPlayingSong(evt);
    }
  }

  onPlayingRadio(evt = null) {
    if (
      this.callbacks.onPlayingRadio &&
      typeof this.callbacks.onPlayingRadio === "function"
    ) {
      this.callbacks.onPlayingRadio(evt);
    }
  }

  onReplay(evt = null) {
    if (
      this.callbacks.onReplay &&
      typeof this.callbacks.onReplay === "function"
    ) {
      this.callbacks.onReplay(evt);
    }
  }

  onBookMark(time) {
    if (
      this.callbacks.onBookMark &&
      typeof this.callbacks.onBookMark === "function"
    ) {
      this.callbacks.onBookMark(time);
    }
  }

  onBitrateChange(bitrate, bandwidth) {
    if (
      this.callbacks.onBitrateChange &&
      typeof this.callbacks.onBitrateChange === "function"
    ) {
      this.callbacks.onBitrateChange(bitrate, bandwidth);
    }
  }
}

export default AAFPlayerEvent;
