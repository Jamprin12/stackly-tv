import React, {
  useState,
  useEffect,
  useRef,
  useCallback,
  useContext,
} from "react";
import { useHistory } from "react-router-dom";
import get from "lodash/get";

import Device from "../../devices/device";
import { deviceAttach } from "../../requests/loaderNew";

import TrackerManager from "../../utils/trackers/TrackerManager";
import PlayerTracker from "../../utils/trackers/PlayerTracker";

import PlayerEvent from "./newPlayer/AAFPlayerEvent";

import ModalNextEpisode from "../../components/2020/Modals/NextEpisode";
import ErrorContenido from "../../components/2020/Modals/player/ErrorContenido";
import ErrorMaxDispositivos from "../../components/2020/Modals/player/ErrorMaxDispositivos";
import Resume from "../../components/2020/Modals/player/Resume";
import LoadingComponent from "../../components/2020/Loading/LoadingComponent";
import Languages from "../../components/2020/Modals/idiomas/idiomasNew";

import Controls from "../../components/2020/Player/newControls";

import { ModalContext } from "../../containers/App/ModalContext";

var sendTrack; // mandar posicion cuando muevo el slider-progress
const keys = Device.getDevice().getKeys();

const Video = ({
  isModalOpenVcard,
  onShowEpisodes = () => {},
  setShowPlayer,
  currentChannel,
  currentContentId,
  setLoadingPlayer,
  children,
  isPreview = false,
  isLive = true,
  paywayToken = null,
  setCurrentChannel,
  info,
  optLanguages,
  setOptLanguages, // ao-vivo
  languageSel = null,
}) => {
  const { setContext } = useContext(ModalContext);
  const history = useHistory();

  const [isModalOpen, setIsModalOpen] = useState(false);

  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);

  const VideoContainerTv = useRef(null);
  const Player = useRef();

  const [durationTotal, setDurationTotal] = useState(0);
  const [durationProgress, setDurationProgress] = useState(0);
  const [initialProgress, setInitialProgress] = useState(0);

  const [showNextEpisodes, setShowNextEpisodes] = useState(0);
  const [currentChannelLocal, setCurrentChannelLocal] = useState(
    currentChannel
  );
  const [currentContentIdLocal, setCurrentContentIdLocal] = useState(
    currentContentId
  );
  const [paywayTokenLocal, setPaywayTokenLocal] = useState(paywayToken);
  const [infoLocal, setInfoLocal] = useState(info);
  const [isPause, setIsPause] = useState(false);
  const [languages, setLanguages] = useState(get(optLanguages, "options", []));
  const progress = useRef();

  useEffect(() => {
    setLoading(true);
    setPaywayTokenLocal(paywayToken);
    setCurrentChannelLocal(currentChannel);
    setCurrentContentIdLocal(currentContentId);
  }, [currentChannel]);

  useEffect(() => {
    progress.current = durationProgress;
  }, [durationProgress]);

  useEffect(() => {
    if (languageSel) {
      Player.current.setAudioTrack(languageSel);
    }
  }, [languageSel]);

  useEffect(() => {
    setLoadingPlayer(loading);
    if (!loading) {
      window.SpatialNavigation.focus("@newPlayer");
    }
  }, [loading]);

  useEffect(() => {
    if (!isLive) {
      VideoContainerTv.current.addEventListener("keydown", handleKeyPress);
    }

    return () => {
      VideoContainerTv.current.removeEventListener("keydown", handleKeyPress);

      if (!isLive && !isPreview) {
        TrackerManager.stop(progress.current);
      }

      Player.current.setPlayerState({
        playerstate: "STOPPING",
      });
    };
  }, []);

  useEffect(() => {
    if (!Player.current) {
      Player.current = new PlayerEvent(VideoContainerTv.current, {
        onCreditsTime: onCreditsTime,
        onPlayerStopMedia: onEnded,
        onDurationChange: onDurationChange,
        onEnded: onEnded,
        onProgress: onProgress,
        onResolveParams: onResolveParams,
        onResolveError: onResolveError,
        onLoad: () => {
          setLoading(false);
          getLanguagesMultiple();
        },
        setLoading: () => {
          setLoading(true);
        },
        onPlayerControlSeek: onBookMark,
      });
    }

    if (currentChannelLocal || currentContentIdLocal || isPreview || isLive) {
      Player.current.setPlayerState({
        playerstate: "PLAYING",
        groupId: currentChannelLocal,
        contentId: currentContentIdLocal,
        isPreview: isPreview,
        isLive: isLive,
        paywayToken: paywayTokenLocal,
      });

      // enviar visualizacion actual, para recuperar al volver a iniciar la reproduccion
      if (!isLive && !isPreview) {
        const playerTracker = new PlayerTracker();
        new TrackerManager([playerTracker]);
      }
    }
  }, [currentChannelLocal, currentContentIdLocal]);

  const handleKeyPress = (e) => {
    const currentKey = keys ? keys.getPressKey(e.keyCode) : null;
    switch (currentKey) {
      case "BACK":
        e.preventDefault();
        e.stopPropagation();
        setShowPlayer(false);
        break;
    }
  };

  const getLanguagesMultiple = () => {
    const options = Player.current.getAudioTracks();
    if (options.length) {
      setOptLanguages && setOptLanguages(options);
    }
  };

  const onResolveParams = async ({
    initialTime = 0,
    languages = [],
    changingLang = false,
  }) => {
    if (isPreview) {
      setLanguages(languages);
    }

    const deviceAttachResult = await deviceAttach(currentChannelLocal);
    const deviceAttachId = get(deviceAttachResult, "attach.id", null);

    const extraTrackerParams = {
      isLive: isLive,
      groupId: currentChannelLocal,
      deviceAttachId,
    };
    if (!isLive && !isPreview) {
      TrackerManager.setupExtraParams(extraTrackerParams);
    }

    if (changingLang) {
      // si cambio idioma continuo sin mostrar resume
      onPlay(progress.current - 4);
    } else if (initialTime) {
      setIsModalOpen(true);
      setContext({
        isOpen: true,
        cancelBack: true,
        component: Resume,
        props: {
          cancel: () => {
            setShowPlayer(false);
            closeModal();
          },
          onInitial: () => {
            onPlay(0);
          },
          onResume: () => {
            onPlay(initialTime);
          },
        },
        onClose: closeModal,
      });
    } else {
      onPlay(0);
    }
  };

  const onPlay = (initialTime) => {
    setInitialProgress(initialTime);
    Player.current.load(initialTime);
  };

  const onResolveError = ({ code }) => {
    if (code == "PLY_DEV_00006") {
      setIsModalOpen(true);
      setContext({
        isOpen: true,
        component: ErrorMaxDispositivos,
        props: {},
        onClose: () => {
          if (isLive) {
            history.push("/node/home");
          } else {
            closeModal();
            setShowPlayer(false);
          }
        },
      });

      setLoading(false);
    } else {
      setError(true);
      setLoading(false);
    }
  };

  const onEnded = (isEnd) => {
    closeModal();

    clearTimeout(sendTrack);
    sendTrack = setTimeout(() => {
      TrackerManager.tick(durationTotal);
    }, 200);
    TrackerManager.end(progress.current);

    if (isEnd) {
      setShowPlayer && setShowPlayer(false);
    } else {
      if (!showNextEpisodes && !loading) {
        setShowPlayer && setShowPlayer(false);
      }
    }
  };

  const onDurationChange = (data) => {
    setDurationTotal(data);
  };

  const onProgress = (data) => {
    // setIDurationProgressCallback(data);
    setDurationProgress(data);
  };

  const onBookMark = (currentTime) => {
    if (!isLive && !isPreview) {
      clearTimeout(sendTrack);
      sendTrack = setTimeout(() => {
        TrackerManager.tick(currentTime);
      }, 200);
    }
  };

  const onCreditsTime = ({ item, groupId }) => {
    TrackerManager.end(progress.current);
    if (groupId && item) {
      setIsModalOpen(true);
      setShowNextEpisodes(true);
      setContext({
        style: { background: "transparent" },
        isOpen: true,
        component: ModalNextEpisode,
        props: {
          item: item,
          handlePlay: handlePlayNextEpisode,
          keys: keys,
        },
        onClose: closeModal,
      });
    }
  };

  const closeModal = useCallback(() => {
    setShowNextEpisodes(false);
    setIsModalOpen(false);
  }, []);

  const back = () => {
    setShowPlayer(false);
  };

  const handlePlayNextEpisode = (item) => {
    if (setCurrentChannel) {
      setCurrentChannel(item.id);
    }
    closeModal();
  };

  const handlePausePlay = () => {
    setIsPause((prevState) => {
      if (prevState) {
        Player.current.play();
      } else {
        Player.current.pause();
      }
      return !prevState;
    });
  };

  const handleBackwardForward = (isBack = false) => {
    if (isBack) {
      Player.current.seek(progress.current - 10);
    } else {
      Player.current.seek(progress.current + 10);
    }
  };

  const new_language = languages.map((e) => {
    return e.subtitle ? "legenda" : "audio";
  });

  const handleShowLanguage = useCallback(() => {
    setIsModalOpen(true);
    setContext({
      isOpen: true,
      component: Languages,
      props: {
        isPreview: isPreview,
        contentId: currentContentIdLocal,
        title: new_language.find((e) => e === "audio")
          ? "Áudio"
          : "Áudio e Legenda", // SE PUEDE MEJORAR
        titleAudioLegendas: new_language.find((e) => e === "legenda")
          ? "Legenda"
          : "", // SE PUEDE MEJORAR
        options: new_language.find((e) => e === "audio")
          ? languages.filter((e) => e.option_id.includes("S"))
          : languages, // SE PUEDE MEJORAR
        optionsLegendas: new_language.find((e) => e === "legenda")
          ? languages.filter((e) => e.option_id.includes("D"))
          : languages, // SE PUEDE MEJORAR
        onClick: ({ item }) => {
          if (!item.is_current) {
            setCurrentContentIdLocal(item.content_id);
          }
          setContext({ isOpen: false }); // cierra popup
          closeModal(); // setea params para ver controles
        },
      },
      onClose: closeModal,
    });
  }, [currentContentIdLocal, languages]);

  return (
    <div
      id="HTML5VideoWrapperTv"
      ref={VideoContainerTv}
      style={{
        position: "absolute",
        width: "100%",
        height: "100%",
        left: 0,
        top: 0,
        backgroundColor: "black",
        zIndex: 3,
      }}
    >
      {error && <ErrorContenido useButtom={!isLive} onClose={back} />}

      {loading && <LoadingComponent visible={true} />}

      {!isModalOpenVcard && !isModalOpen && !isLive && !loading && !error && (
        <Controls
          keys={keys}
          title={infoLocal.title}
          subTitle={
            infoLocal.episodeTitle &&
            `${infoLocal.episodeTitle} - Temporada ${infoLocal.season} Episodio ${infoLocal.episode}`
          }
          isSerie={Boolean(infoLocal.episodeTitle)}
          back={back}
          loading={loading}
          player={Player.current}
          initialProgress={initialProgress}
          durationProgress={durationProgress}
          durationTotal={durationTotal}
          onShowEpisodes={onShowEpisodes}
          isPause={isPause}
          handlePausePlay={handlePausePlay}
          useLanguages={Boolean(languages.length)}
          onShowLanguages={handleShowLanguage}
          handleBackwardForward={handleBackwardForward}
          // yellowHandler={() => setShowPlayer((prevState) => !prevState)}
          // blueHandler={blueHandler}
        />
      )}

      {children}
    </div>
  );
};

export default Video;
