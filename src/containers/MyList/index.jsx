import React, { useState, useEffect, useCallback, useContext } from "react";
import get from "lodash/get";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Collection, AutoSizer } from "react-virtualized";
import { useTranslation } from "react-i18next";

import {
  fetchFavoriteLoader,
  fetchFavoriteSerieLoader,
} from "../../requests/loaderNew";

import { getItemProperties } from "../../components/2020/resizeTmp";
import Message from "../../components/2020/Messages/message";
import TextMessages from "../../components/2020/Typography/TextMessages";
import CardLandscape from "../../components/2020/Cards/CardLandscapeNew";
import LoadingComponent from "../../components/2020/Loading/LoadingComponent";
import ErrorApi from "../../components/2020/Messages/Errors/ErrorApi";

import imagePopcorn from "./images/net_contenido_alquilado_sin_contenido.svg";

import { Context } from "../../containers/App/Layout";

const useStyles = makeStyles((theme) => ({
  collection: {
    overflow: "hidden!important",
  },
  ribbongridWrapper: () => ({
    bottom: 0,
    height: 340,
    width: theme.sizeBody.hd.width - 95,
    zIndex: 1,
    overflow: "hidden",
    position: "absolute",
  }),
  wrapList: {
    display: "flex",
    width: "100%",
    height: "100%",
    flexFlow: "column",
    paddingLeft: 15,
  },
  landscape: () => ({
    padding: 0,
    margin: 0,
    height: 655,
    bottom: 10,
    overflow: "hidden",
  }),
  contentLi: () => ({
    position: "relative",
    listStyle: "none",
    float: "left",
    minHeight: 165,
  }),
  title: () => ({
    fontSize: 25,
    paddingBottom: 5,
    paddingTop: 10,
    margin: 0,
    marginLeft: 7,
  }),
  globalError: {
    width: 1185,
    height: 720,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  contentError: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  image: {
    height: 140,
    width: 140,
  },
  itemsLength: {
    fontWeight: 400,
    fontSize: 25,
    color: "#848484",
  },
}));

const MyList = ({ user, history, match }) => {
  const { t, i18n } = useTranslation();
  const { setContext } = useContext(Context);
  const [isLoading, setIsLoading] = useState(true);
  const [items, setItems] = useState([]);
  const classes = useStyles();

  const getData = async () => {
    const result = await fetchFavoriteLoader(user);
    const resultSerie = await fetchFavoriteSerieLoader(user);
    let newResult = [];
    newResult = {
      groups: [
        ...(get(result, "response.groups") || []),
        ...(get(resultSerie, "response.series") || []),
      ],
    };

    setIsLoading(false);
    setItems(get(newResult, "groups"));
    // para probar error
    //setItems(false);
    // para probar sin items
    //setItems([]);
  };

  useEffect(() => {
    getData();
  }, []);

  const handleVcard = useCallback((item) => {
    setContext({
      isOpen: true,
      gropupId: item.original.first_episode
        ? item.original.episode.id
        : item.group_id,
      onClose: setFocusContainer,
    });
  }, []);

  const setFocusContainer = useCallback(() => {
    window.SpatialNavigation.focus("@container");
  }, []);

  const cellSizeAndPositionGetter = ({ index }) => {
    const y = parseInt(index / 4);
    const x = index >= 4 ? index % 4 : index;

    return {
      height: 160,
      width: 280,
      x: x * 285,
      y: y * 165,
    };
  };

  const cellRenderer = ({ index, key, style, list }) => {
    const item2 = list[index];

    const item = getItemProperties({ item: item2, version: "v5.86" });
    return (
      <div key={key} style={style}>
        <CardLandscape
          snUp={index === 0 ? " " : null}
          snDown={index >= list.length - 4 ? " " : null}
          //border={false}
          isFocusable={true}
          scrollToTop={false}
          width={260}
          height={144}
          bgSize={"260px 144px"}
          bgSizeFocus={"276px 163px"}
          data={item}
          image={item.imageCard}
          // image={
          //   (item &&
          //     item.image_large &&
          //     item.image_large.replace("http:", "https:")) ||
          //   (item &&
          //     item.image_small &&
          //     item.image_small.replace("http:", "https:"))
          // }
          clickHandler={() => {
            handleVcard(item);
          }}
          focusHandler={() => {}}
        ></CardLandscape>
      </div>
    );
  };

  setTimeout(() => {
    setFocusContainer();
  }, 800);

  if (isLoading) {
    return <LoadingComponent />;
  }

  // error api
  if (!items) {
    return <ErrorApi getData={getData} />;
  }

  // sin items
  if (items.length == 0) {
    return (
      <div className={classes.landscape}>
        <Grid
          container
          spacing={0}
          className={`fromVMenu rents-error ${classes.globalError}`}
        >
          <h2
            className={classes.title}
            style={{
              position: "absolute",
              top: 10,
              left: 10,
            }}
          >
            {t(
              "net_minha_lista_conteudos_adicionados",
              "conteudos adicionados"
            )}{" "}
            <span className={classes.itemsLength}>{` (0)`}</span>
          </h2>
          <Grid item xs={4} className={classes.contentError}>
            <Message image={imagePopcorn} height={300}>
              <TextMessages
                title={t("net_sin_plan_wooow", "woow!")}
                textContent={t(
                  "net_minha_lista_empty",
                  "Parece que voce ainda nao possui nenhum conteudo alugado"
                )}
              />
            </Message>
          </Grid>
        </Grid>
      </div>
    );
  }

  return (
    <div className={classes.wrapList}>
      <h1 className={classes.title}>
        {t("net_minha_lista_conteudos_adicionados", "conteudos adicionados")}
        <span className={classes.itemsLength}>{` (${items.length})`}</span>
      </h1>
      <AutoSizer>
        {({ height, width }) => {
          return (
            <Collection
              className={classes.collection}
              cellCount={items.length}
              cellRenderer={(data) => cellRenderer({ ...data, list: items })}
              cellSizeAndPositionGetter={cellSizeAndPositionGetter}
              verticalOverscanSize={50}
              height={height - 60} // el autoSizer detecta el height de toda la app, hay que restarle el titulo
              width={width}
            />
          );
        }}
      </AutoSizer>
    </div>
  );
};

export default MyList;
