import React, { useState, useEffect, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import get from "lodash/get";
import { useTranslation } from "react-i18next";

import { Modal } from "../../components/2020/Modals/Modal";
import Keyboard from "../../components/2020/Keyboard/keyboard";
import Error401 from "../../components/2020/Modals/login/Error401";
import Error477 from "../../components/2020/Modals/login/Error477";
import Error504 from "../../components/2020/Modals/login/Error504";
import Input from "../../components/2020/Input/Input";
import Button from "../../components/2020/Buttons/ButtonGeneric";
import HeaderLogin from "../../components/2020/HeaderLogin/HeaderLogin";
import LoadingComponent from "../../components/2020/Loading/LoadingComponent";

import DeviceStorage from "../../components/DeviceStorage/DeviceStorage";
import Device from "../../devices/device";

import { loginZup } from "../../requests/loaderNew"; // a traves de loaderNew para interceptar

const keysDevice = Device.getDevice().getKeys();

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    width: "100%",
    height: "100%",
    flexFlow: "column",
    justifyContent: "space-between",
  },
  keyboard: {
    width: "100%",
    background: "#212224",
    alignSelf: "flex-end",
  },
  formContainer: {
    display: "flex",
    height: "100%",
    alignSelf: "center",
    alignItems: "flex-start",
    marginTop: 42,
  },
  form: {
    display: "flex",
    flexFlow: "column",
    width: "500px",
  },
  flexFormRow: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  gridForm: {
    marginBottom: 20,
  },
  titleInput: {
    fontSize: 20,
    margin: 0,
    marginBottom: 20,
  },
}));

const Login = () => {
  // const { t, i18n } = useTranslation(); // no unussed parameters
  const { t } = useTranslation();
  const history = useHistory();
  const classes = useStyles();

  const [values, setValues] = useState({ email: "", password: "" });
  const [currentKey, setCurrentKey] = useState("email");
  const [typePassword, setTypePassword] = useState("password");

  const [firstLoading, setFirstLoading] = useState(true);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setFirstLoading(false);
      setFocus();
    }, 1200);
  }, []);

  const handleSubmit = async () => {
    setLoading(true);

    if (!values.email.length) {
      setLoading(false);
      showError({ code: 401 });

      return false;
    }

    const params = {
      password: encodeURIComponent(values.password),
      includpaywayprofile: true,
      username: values.email,
    };

    const { success, code, error } = await performLogin(params);
    console.log('[LOGIN]--loginNew.jsx -- handleSubmit',success,code,error);
    if (success) {
      history.push("/node/home");
    } else {
      showError({ code, error });
    }
    setLoading(false);
  };

  const performLogin = async (params = {}) => {
    let response = { success: false };
    try {
      const result = await loginZup(params);
      console.log('[LOGIN]--loginNew.jsx -- performLogin',result);
      if (result && result.status === "0") {
        response = { success: true, data: result.response };
      }
    } catch (error) {
      response = { success: false, ...error };
    }
    return response;
  };

  const showError = ({ code, error }) => {
    setIsModalOpen(code);
  };

  const closeModal = useCallback(() => {
    setIsModalOpen(false);
    setTimeout(() => {
      setFocus();
    }, 100);
  }, []);

  const changeTypePassword = useCallback((e) => {
    e.preventDefault();

    setFocus();
    setTypePassword((prevState) =>
      prevState === "text" ? "password" : "text"
    );
  }, []);

  const setFocus = useCallback(() => {
    setTimeout(() => {
      window.SpatialNavigation.focus("@keyboardGral");
    }, 100);
  }, []);

  const setFocusInput = useCallback(({ name }) => {
    setCurrentKey(name);
    setFocus();
  }, []);

  if (loading || firstLoading) {
    return <LoadingComponent image background="#030c14" />;
  }

  return (
    <div className={`login ${classes.container}`}>
      {isModalOpen && (
        <Modal onClose={closeModal}>
          {isModalOpen === 401 && <Error401 onClose={closeModal} />}
          {isModalOpen === 477 && <Error477 onClose={closeModal} />}
          {isModalOpen !== 401 && isModalOpen !== 477 && (
            <Error504
              onClose={closeModal}
              getData={() => {
                closeModal();
                handleSubmit();
              }}
            />
          )}
        </Modal>
      )}

      <div className={classes.formContainer}>
        <HeaderLogin />
        <div className={classes.form}>
          <div>
            <p className={classes.titleInput}>
              {t("login.net_login_user", "Usuário ou email")}
            </p>
          </div>
          <div className={classes.gridForm}>
            <Input
              currentFocus={currentKey}
              name="email"
              placeholder={t("login.email", "email")}
              value={values.email}
              onClick={setFocusInput}
            />
          </div>
          <div>
            <p className={classes.titleInput}>
              {t("login.net_login_pass", "digite sua senha")}
            </p>
          </div>
          <div className={`${classes.gridForm} ${classes.flexFormRow}`}>
            <Input
              isFocusable={get(values, "email.length") > 0}
              currentFocus={currentKey}
              type={typePassword}
              name="password"
              placeholder={t("login.ingresar_password", "Contraseña")}
              value={values.password}
              onClick={setFocusInput}
            />
            <Button
              heightFocoDisable
              width="130px"
              margin={"0 0 0 10px"}
              isFocusable={get(values, "email.length") > 0}
              type="button"
              title={
                typePassword === "text"
                  ? t("login.register_hide_pwd_button_value", "Ocultar")
                  : t("login.register_show_pwd_button_value", "Mostrar")
              }
              onClick={changeTypePassword}
            />
          </div>
          <div className={classes.gridForm}>
            <Button
              width="100%"
              margin={0}
              isFocusable={
                get(values, "email.length", 0) && get(values, "password.length")
              }
              title={t("login.login_btn_lbl", "Iniciar sesión")}
              onClick={handleSubmit}
            />
          </div>
        </div>
      </div>

      <div className={classes.keyboard}>
        <Keyboard
          region={DeviceStorage.getItem('region')}
          keysDevice={keysDevice}
          showMails={true}
          currentValue={values[currentKey]}
          onClick={(text) => {
            setValues((prevState) => {
              return {
                ...prevState,
                [currentKey]: text,
              };
            });
          }}
        />
      </div>
    </div>
  );
};

export default Login;
