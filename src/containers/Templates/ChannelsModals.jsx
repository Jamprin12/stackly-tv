import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Slider from "react-slick";

import ButtonGeneric from "../../components/2020/Buttons/ButtonGeneric";
import SimpleImage from "../../components/2020/Image/SimpleImage";
import Typography from "../../components/2020/Typography/Typography";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./slide.css";

const useStyles = makeStyles(() => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    flexDirection: "column",
    background: "black",
  },
  contentDataCenter: {
    width: 960,
    height: 720,
  },
  slider: {
    width: "100%",
  },
  logo: {
    marginTop: 60,
  },
  background: {
    position: "absolute",
    top: 0,
    width: 960,
    height: 450,
    boxShadow: "inset 0em -2em 8em 6em #000",
    backgroundSize: "cover",
  },
  contentSlideData: {
    position: "relative",
    padding: "0 200px",
    boxSizing: "border-box",
    display: "flex!important",
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  icon: {
    background: "#3D5AFE",
    borderRadius: 8,
    width: 40,
    height: 40,
    padding: 5,
    boxSizing: "border-box",
    color: "white",
    margin: "30px 0",
  },
}));

const ChannelsOpen = ({ data, onClose, setItem = () => {} }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const history = useHistory();

  const handleAoVivo = (code) => {
    setItem("lastChannel", code);
    history.push(`/node/tv`);
    return null;
  };

  const settings = {
    dots: true,
    arrows: true,
    infinite: false,
    initialSlide: 0,
    slidesToScroll: 1,
    slidesToShow: 1,
    // beforeChange: (currentSlide, nextSlide) => {
    //   setTimeout(() => {
    //     window.SpatialNavigation.focus(`#openChannelButton_${nextSlide} .focusable:first-child`);
    //   }, 300);
    // },
    afterChange: function (currentSlide) {
      setTimeout(() => {
        window.SpatialNavigation.focus(`#openChannelButton_${currentSlide} .focusable:first-child`);
      }, 200);
    },
  };

  return (
    <div className={classes.container}>
      <div className={classes.contentDataCenter}>
        <Slider {...settings} className={classes.slider}>
          {data.map((item, i) => {
            console.log('lucho', data)
            return (
              <div key={i}>
                <div className={classes.background} style={{ backgroundImage: `url(${item.image_16_9})` }} />
                <div key={i} className={classes.contentSlideData}>
                  <SimpleImage className={classes.logo} image={t("asset.logo")} height={71} />
                  <SimpleImage className={classes.icon} image={t("asset.wifi")} height={71} />
                  <Typography fontSize={32} fontWeight="bold" variant="h2">
                    {item.title}
                  </Typography>
                  <Typography fontSize={32} color="#B8B8B8" margin="20px 0 80px 0" variant="p">
                    {item.description}
                  </Typography>
                  <div id={`openChannelButton_${i}`}>
                    {item.codeVod && <ButtonGeneric minHeight={54} margin={5} title={"ver filmes e séries"} onClick={() => history.push(`/node/${item.codeVod}`)} />}
                    {item.codeLive && <ButtonGeneric minHeight={54} margin={5} title={"ver programas ao vivo"} onClick={() => handleAoVivo(item.codeLive)} />}
                    <ButtonGeneric minHeight={54} margin={5} title={"agora nao"} onClick={onClose} />
                  </div>
                </div>
              </div>
            );
          })}
        </Slider>
      </div>
    </div>
  );
};

export default React.memo(ChannelsOpen);
