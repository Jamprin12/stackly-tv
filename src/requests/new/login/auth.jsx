import storage from "../../../components/DeviceStorage/DeviceStorage";

import ZUPConfig from "../../../config/ZUPConfig";
import getAppConfig from "../../../config/appConfig";

import ZUPUtils from "../../../utils/ZUPUtils";

import { logoutLoader } from "../../loaderNew";

/**
 * Login de ZUP
 * user info from Cleber > username: 62038725470 password: @Nowauto email:user32603269692@gmail.com
 * user info from Martin Lucero > username nowmultiscreen1 password nowmulti1
 *
 * @param {object} params {email / username / password}
 */
const loginZup = async (params) => {
  var url = "";
  var body = {};
  var options = {};

  try {
    url = ZUPConfig.getUrl("login");
    var headers = ZUPConfig.getItem("headers");

    /**
     * HACK! Backdoor
     * para logearse facil
     */
    // 1

    if (params.username == "j@gmail.com") {
      params.email = "joseirrazabal@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "joseirrazabal@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "12345678";
    }
    if (params.username == "q@gmail.com") {
      params.email = "user84040197496@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user84040197496@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 2
    if (params.username == "a@gmail.com") {
      params.email = "user95838129400@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user95838129400@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 4
    if (params.username == "x@gmail.com") {
      params.email = "user65957599905@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user65957599905@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 5
    if (params.username == "c@gmail.com") {
      params.email = "user4257439420@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user4257439420@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 6
    if (params.username == "v@gmail.com") {
      params.email = "user84234599179@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user84234599179@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 7
    if (params.username == "s@gmail.com") {
      params.email = "user55013940448@gmail.com";
      params.username = "user55013940448@gmail.com";
      params.password = "@Nowauto";
    }
    // 11
    if (params.username == "f@gmail.com") {
      params.email = "user87586985983@gmail.com";
      params.username = "user87586985983@gmail.com";
      params.password = "@Nowauto";
    }
    // 12
    if (params.username == "m@gmail.com") {
      params.email = "amcopruebas.mm@gmail.com";
      params.username = "amcopruebas.mm@gmail.com";
      params.password = "Nowauto123";
    }
    // 13
    if (params.username == "r@gmail.com") {
      params.email = "amcopruebas.rr@gmail.com";
      params.username = "amcopruebas.rr@gmail.com";
      params.password = "Nowauto123";
    }
    // 14
    if (params.username == "j@gmail.com") {
      params.email = "julianbermolen@gmail.com";
      params.username = "julianbermolen@gmail.com";
      params.password = "Amco2020";
    }
    // 15
    if (params.username == "vi@gmail.com") {
      params.email = "vivibiw289@mailernam.com";
      params.username = "vivibiw289@mailernam.com";
      params.password = "12345678";
    }
    // 16
    if (params.username == "qa@gmail.com") {
      params.email = "qa23@lalala.fun";
      params.username = "qa23@lalala.fun";
      params.password = "nx123@123";
    }

    // ---------------------------- nuevos
    // 17
    if (params.username == "q1@gmail.com") {
      params.email = "stv-teste-prod1@grr.la";
      params.username = "stv-teste-prod1@grr.la";
      params.password = "12345678";
    }
    // 18
    if (params.username == "q2@gmail.com") {
      params.email = "stv-teste-prod2@grr.la";
      params.username = "stv-teste-prod2@grr.la";
      params.password = "12345678";
    }
    // 18
    if (params.username == "d1@gmail.com") {
      params.email = "dressa1@2go-mail.com";
      params.username = "dressa1@2go-mail.com";
      params.password = "12345678";
    }
    // 18
    if (params.username == "d2@gmail.com") {
      params.email = "dressa2@2go-mail.com";
      params.username = "dressa2@2go-mail.com";
      params.password = "12345678";
    }

    //-------------------------------------------

    /**
     * Construyo el url / body / options a mandar
     */
    // const url = url_base + url_login;
    body = {
      userType: params.username.indexOf("@") !== false ? "EMAIL" : "USERNAME", // autodetecto emails
      userValue: params.username,
      password: params.password,
      customFields: ZUPUtils.getCustomFields(),
    };
    options = {
      body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
      cache: "no-cache", // String	See the cache property for the valid values.
      headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
      method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
      credentials: "include", // include|omit String See the credentials property for the valid values.
      mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
    };

    var myRequest, response, data; // declaraciones

    /**
     * Llamado real
     */
    try {
      myRequest = new Request(url, options);
      response = await fetch(myRequest);
    } catch (err) {
      throw {
        code: 500,
      };
    }

    /**
     * Conversion a json
     */
    try {
      data = await response.json();
    } catch (err) {
      throw {
        code: response.status,
      };
    }

    // HTTP 401 -- Unauthorized
    if (response.status == 401) {
      if (data.customer && data.code === "user.only.claro") {
        throw {
          code: 477,
        };
      } else {
        throw {
          code: response.status,
        };
      }
    }
    // HTTP 477 -- Sanitized
    if (response.status == 477) {
      throw {
        code: response.status,
      };
    }
    // HTTP 504 -- Timeout
    if (response.status == 504) {
      throw {
        code: response.status,
      };
    }
    // HTTP 400 -- Bad Request
    if (response.status == 400) {
      throw {
        code: response.status,
      };
    }
    // HTTP 500 -- Internal Server Error
    if (response.status == 500) {
      throw {
        code: response.status,
      };
    }
    // Other HTTP errors
    if (response.status !== 200) {
      throw {
        code: response.status,
      };
    }

    /**
     * Levanto los headers de respuesta
     * para poder volver a llamar a ZUP
     */
    ZUPUtils.saveResponseHeaders(response.headers);

    var responseHeaders = {};
    var head = response.headers;
    for (var pair of head.entries()) {
      responseHeaders[pair[0]] = pair[1];
    }

    storage.setItem("zup-authtoken", responseHeaders.authtoken);

    /**
     * JWT
     * Decodifico el JWT y lo pego directamente en AMCO
     */
    try {
      data.chainResponse.AMCO = ZUPUtils.getJWT(data.chainResponse.AMCO.jwt);
    } catch (err) {
      throw new Error("Invalid data received.");
    }

    /**
     * STORAGE
     * Cambio en el storage los datos que hay que mantener
     */
    storage.setItem("HKS", data.chainResponse.AMCO.session_stringvalue); // con o sin parentesis es igual
    storage.setItem("user_hash", data.chainResponse.AMCO.session_userhash); // sino revienta el last_seen // el login lo vuelve a setear
    storage.setItem("zup-customer-id", data.customer.id);
    storage.setItem("zup-amco-user_token", data.chainResponse.AMCO.user_token);
    storage.setItem("zup-amco-user_name", data.customer.name);
    // storage.setItem('zup-session_stringvalue',data.chainResponse.AMCO.session_stringvalue) //.replace('(','').replace(')','')
    // storage.setItem('zup-session_parametername',data.chainResponse.AMCO.session_parametername)
    // storage.setItem('zup-session_userhash',data.chainResponse.AMCO.session_userhash)
    // storage.setItem('zup-idmSessionToken',data.chainResponse.AMCO.session_userhash)

    /**
     * Guardo la session de ZUP
     */
    storage.setItem("zup-x-customer-id", data.customer.id);
    ZUPUtils.saveSessionToken(data.sessionToken); // guardo datos para el refresh

    /**
     * variable de resultado
     * generar en base a lo se supone que tengo que devolver
     */
    const resultado = {
      msg: "OK",
      response: {
        email: data.customer.email,
        region: data.chainResponse.AMCO.region,
        status: "success",
        user_id: data.customer.id,
        user_token: data.chainResponse.AMCO.user_token,
        username: data.customer.email,
        session_userhash: data.chainResponse.AMCO.session_userhash,
      },
      status: "0",
    };

    /**
     * Installs refreshTokenZUP
     */
    ZUPUtils.checkSessionToken(); // GOOSE -- Installs refreshTokenZUP

    /**
     * I promised a promise... so...
     */
    return new Promise((resolve, reject) => {
      resolve(resultado);
    });
  } catch (err) {
    return new Promise((resolve, reject) => {
      reject(err);
    });
  }
};

/**
 * refreshes the ZUP token
 * SE USA ESTE!
 */
const refreshTokenZUP = async () => {
  try {
    /**
     * Configuracion basica
     */
    const refreshToken = ZUPUtils.getRefreshToken();

    if (refreshToken !== null) {
      const config = getAppConfig(); // config de la aplicacion
      const url = ZUPConfig.getUrl("refresh"); // url desde config de zup
      /**
       * Request headers
       */
      let headers = ZUPConfig.getItem("headers");
      headers["X-Customer-Id"] = storage.getItem("zup-x-customer-id");
      /**
       * Body
       */
      const body = {
        idmSessionToken: {
          refreshToken: refreshToken, //storage.getItem('zup-sessionToken-refreshToken')
        },
        customFields: {
          device_type: config.device_type,
          device_model: config.device_model,
          device_manufacturer: config.device_manufacturer,
          device_category: config.device_category,
          device_id: config.device_id,
          region: config.region,
        },
        tokens: {
          AMCO: {
            jwt: storage.getItem("zup-jwt"),
          },
        },
      };
      const options = {
        body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
        cache: "no-cache", // String	See the cache property for the valid values.
        headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
        method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
        credentials: "include", // include|omit String See the credentials property for the valid values.
        mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
      };
      /**
       * Llamado real
       */
      const myRequest = new Request(url, options);
      const response = await fetch(myRequest);
      var data = await response.json();

      if (data.code) {
        // ERROR
        let responseHeaders = {};
        for (var pair of response.headers.entries())
          responseHeaders[pair[0]] = pair[1];

        return false;
      }
      /**
       * Levanto los headers de respuesta
       * para poder volver a llamar a ZUP
       */
      // ZUPUtils.saveResponseHeaders(response.headers);
      /**
       * Guardo la session de ZUP
       */
      ZUPUtils.saveSessionToken(data.sessionToken); // guardo datos para el refresh

      // storage.setItem("zup-x-customer-id", data.customer.id); // por las dudas, teoricamente no cambia
      return true;
    }
  } catch (err) {
    return false;
  }
};

const Logout = async () => {
  const resp = logoutLoader();

  if (resp.response) {
    console.log("[LOGOUT] -- AMCO.logout!", resp.response);
  }
  let ui = storage.getItem("__UUID__");
  storage.clear();
  storage.setItem("__UUID__", ui);
  storage.setItem("IsLogOut", true);
  window.location.href = "/";
};

export { loginZup, refreshTokenZUP, Logout };
