/**
 * ZUP AUTH
 */
import storage from "../../../components/DeviceStorage/DeviceStorage";
import Log from "../../../utils/Log";
import ZUPUtils from "../../../utils/ZUPUtils";
import ZUPConfig from "../../../config/ZUPConfig";
import getAppConfig from "../../../config/appConfig";

/**
 * Login de ZUP
 * user info from Cleber > username: 62038725470 password: @Nowauto email:user32603269692@gmail.com
 * user info from Martin Lucero > username nowmultiscreen1 password nowmulti1
 *
 * @param {object} params {email / username / password}
 */
export async function loginZup(params) {
  var url = "";
  var body = {};
  var options = {};

  try {
    url = ZUPConfig.getUrl("login");
    var headers = ZUPConfig.getItem("headers");

    /**
     * HACK! Backdoor
     * para logearse facil
     */
    // 1

    if (params.username == "j@gmail.com") {
      params.email = "joseirrazabal@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "joseirrazabal@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "12345678";
    }
    if (params.username == "q@gmail.com") {
      params.email = "user84040197496@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user84040197496@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 2
    if (params.username == "a@gmail.com") {
      params.email = "user95838129400@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user95838129400@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 4
    if (params.username == "x@gmail.com") {
      params.email = "user65957599905@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user65957599905@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 5
    if (params.username == "c@gmail.com") {
      params.email = "user4257439420@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user4257439420@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 6
    if (params.username == "v@gmail.com") {
      params.email = "user84234599179@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.username = "user84234599179@gmail.com"; //"user96743253678@gmail.com"; //"user84040197496@gmail.com";
      params.password = "@Nowauto";
    }
    // 7
    if (params.username == "s@gmail.com") {
      params.email = "user55013940448@gmail.com";
      params.username = "user55013940448@gmail.com";
      params.password = "@Nowauto";
    }
    // 11
    if (params.username == "f@gmail.com") {
      params.email = "user87586985983@gmail.com";
      params.username = "user87586985983@gmail.com";
      params.password = "@Nowauto";
    }
    // 12
    if (params.username == "m@gmail.com") {
      params.email = "amcopruebas.mm@gmail.com";
      params.username = "amcopruebas.mm@gmail.com";
      params.password = "Nowauto123";
    }
    // 13
    if (params.username == "r@gmail.com") {
      params.email = "amcopruebas.rr@gmail.com";
      params.username = "amcopruebas.rr@gmail.com";
      params.password = "Nowauto123";
    }
    // 14
    if (params.username == "j@gmail.com") {
      params.email = "julianbermolen@gmail.com";
      params.username = "julianbermolen@gmail.com";
      params.password = "Amco2020";
    }
    // 15
    if (params.username == "vi@gmail.com") {
      params.email = "vivibiw289@mailernam.com";
      params.username = "vivibiw289@mailernam.com";
      params.password = "12345678";
    }
    // 16
    if (params.username == "qa@gmail.com") {
      params.email = "qa23@lalala.fun";
      params.username = "qa23@lalala.fun";
      params.password = "nx123@123";
    }

    // ---------------------------- nuevos
    // 17
    if (params.username == "q1@gmail.com") {
      params.email = "stv-teste-prod1@grr.la";
      params.username = "stv-teste-prod1@grr.la";
      params.password = "12345678";
    }
    // 18
    if (params.username == "q2@gmail.com") {
      params.email = "stv-teste-prod2@grr.la";
      params.username = "stv-teste-prod2@grr.la";
      params.password = "12345678";
    }
    // 18
    if (params.username == "d1@gmail.com") {
      params.email = "dressa1@2go-mail.com";
      params.username = "dressa1@2go-mail.com";
      params.password = "12345678";
    }
    // 18
    if (params.username == "d2@gmail.com") {
      params.email = "dressa2@2go-mail.com";
      params.username = "dressa2@2go-mail.com";
      params.password = "12345678";
    }

    //-------------------------------------------

    /**
     * Construyo el url / body / options a mandar
     */
    // const url = url_base + url_login;
    body = {
      userType: params.username.indexOf("@") !== false ? "EMAIL" : "USERNAME", // autodetecto emails
      userValue: params.username,
      password: params.password,
      customFields: ZUPUtils.getCustomFields(),
    };
    options = {
      body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
      cache: "no-cache", // String	See the cache property for the valid values.
      headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
      method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
      credentials: "include", // include|omit String See the credentials property for the valid values.
      mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
    };

    var myRequest, response, data; // declaraciones

    /**
     * Llamado real
     */
    try {
      console.log("[ZUP]", { url, options });
      myRequest = new Request(url, options);
      response = await fetch(myRequest);
    } catch (err) {
      throw {
        code: 500,
      };
    }

    /**
     * Conversion a json
     */
    try {
      data = await response.json();
    } catch (err) {
      throw {
        code: response.status,
      };
    }

    // HTTP 401 -- Unauthorized
    if (response.status == 401) {
      if (data.customer && data.code === "user.only.claro") {
        throw {
          code: 477,
        };
      } else {
        throw {
          code: response.status,
        };
      }
    }
    // HTTP 477 -- Sanitized
    if (response.status == 477) {
      throw {
        code: response.status,
      };
    }
    // HTTP 504 -- Timeout
    if (response.status == 504) {
      throw {
        code: response.status,
      };
    }
    // HTTP 400 -- Bad Request
    if (response.status == 400) {
      throw {
        code: response.status,
      };
    }
    // HTTP 500 -- Internal Server Error
    if (response.status == 500) {
      throw {
        code: response.status,
      };
    }
    // Other HTTP errors
    if (response.status !== 200) {
      throw {
        code: response.status,
      };
    }

    /**
     * Levanto los headers de respuesta
     * para poder volver a llamar a ZUP
     */
    ZUPUtils.saveResponseHeaders(response.headers);

    var responseHeaders = {};
    var head = response.headers;
    for (var pair of head.entries()) {
      responseHeaders[pair[0]] = pair[1];
    }

    storage.setItem("zup-authtoken", responseHeaders.authtoken);

    /**
     * JWT
     * Decodifico el JWT y lo pego directamente en AMCO
     */
    try {
      data.chainResponse.AMCO = ZUPUtils.getJWT(data.chainResponse.AMCO.jwt);
    } catch (err) {
      throw new Error("Invalid data received.");
    }

    /**
     * STORAGE
     * Cambio en el storage los datos que hay que mantener
     */
    storage.setItem("HKS", data.chainResponse.AMCO.session_stringvalue); // con o sin parentesis es igual
    storage.setItem("user_hash", data.chainResponse.AMCO.session_userhash); // sino revienta el last_seen // el login lo vuelve a setear
    storage.setItem("zup-customer-id", data.customer.id);
    storage.setItem("zup-amco-user_token", data.chainResponse.AMCO.user_token);
    storage.setItem("zup-amco-user_name", data.customer.name);
    // storage.setItem('zup-session_stringvalue',data.chainResponse.AMCO.session_stringvalue) //.replace('(','').replace(')','')
    // storage.setItem('zup-session_parametername',data.chainResponse.AMCO.session_parametername)
    // storage.setItem('zup-session_userhash',data.chainResponse.AMCO.session_userhash)
    // storage.setItem('zup-idmSessionToken',data.chainResponse.AMCO.session_userhash)

    /**
     * Guardo la session de ZUP
     */
    storage.setItem("zup-x-customer-id", data.customer.id);
    ZUPUtils.saveSessionToken(data.sessionToken); // guardo datos para el refresh

    /**
     * variable de resultado
     * generar en base a lo se supone que tengo que devolver
     */
    const resultado = {
      msg: "OK",
      response: {
        email: data.customer.email,
        region: data.chainResponse.AMCO.region,
        status: "success",
        user_id: data.customer.id,
        user_token: data.chainResponse.AMCO.user_token,
        username: data.customer.email,
        session_userhash: data.chainResponse.AMCO.session_userhash,
      },
      status: "0",
    };

    /**
     * Installs refreshTokenZUP
     */
    ZUPUtils.checkSessionToken(); // GOOSE -- Installs refreshTokenZUP

    /**
     * NO SE USA ! es para pruebas una vez que me logeo
     */
    // setTimeout(isLoggedIn, 5000); // goose -- hey! quede logueado en AMCO?
    // setTimeout(Logout, 10000);
    // setTimeout(refreshTokenZUP, 10000);
    // setInterval(refreshTokenZUP, 5000);
    // isLoggedIn(); // goose - PRUEBA!!!
    // setInterval(getClientData, 20000); // probando acceso
    // setTimeout(getClientData,5000);
    // setTimeout(getOffers,5000);
    // setTimeout(refreshTokenZUP,5000);
    // setInterval(refreshTokenZUP, 30000); // probando acceso
    // setTimeout(getRented,5000);
    // setTimeout(getRentedExpired,7000);
    // setTimeout(liveChannels ,7000);

    // const uno = await getRented()
    // const dos = await getRentedExpired()

    /**
     * I promised a promise... so...
     */
    return new Promise((resolve, reject) => {
      resolve(resultado);
    });
  } catch (err) {
    // GOOSE -- LOG
    Log.error(err, "[ZUP][loginZup]", {
      url: url,
      params: body,
      options: options,
    });

    return new Promise((resolve, reject) => {
      reject(err);
    });
  }
}
/*
  ZUP RESPONSE ##############

  chainResponse:{
    AMCO:{
      jwt: "eyJhbGciOiJIUzI1NiJ9.eyJlbnRyeSI6eyJkZXZpY2VfdHlwZSI6Imh0bWw1IiwiZGV2aWNlX21vZGVsIjoiaHRtbDUiLCJkZXZpY2VfbWFudWZhY3R1cmVyIjoid2luZG93cyIsImRldmljZV9jYXRlZ29yeSI6IndlYiIsImRldmljZV9pZCI6IjMxY2U5MjgwLWUxNTAtZjc3Mi1lZDc5LTA3OTBmMDJhNjg2NyIsInBsYXRmb3JtIjoiTkYiLCJyZWdpb24iOiJicmFzaWwiLCJmb3JtYXQiOiJqc29uIiwic291cmNlIjoibmV0ZmxleCIsImF1dGhfbWV0aG9kIjoiU09DIiwiYXV0aHBuIjoibmV0IiwiYXV0aHB0IjoiNWZhY2Q5ZDIzZDA1YmI4MyIsInVzZXJfaW5mbyI6IntcIm5ldF9jdXN0b21lcl90eXBlXCI6XCJORVQtY2xhcm8uY29tLmJyXCIsXCJuZXRfcGZVc2VybmFtZVwiOlwiMTQ1NTA3ODE3NDJcIixcIm5ldF91c2VyX3R5cGVcIjpcIk5FVFwiLFwibmV0X2xvZ2luX2VtYWlsXCI6XCJ1c2VyODA4NzczMzU5MjJAZ21haWwuY29tXCIsXCJuZXRfZW1haWxcIjpcInVzZXI4MDg3NzMzNTkyMkBnbWFpbC5jb21cIixcIm5ldF9iaXJ0aGRhdGVcIjpcIjAxLTAxLTIwMDFcIixcIm5ldF9kb2N1bWVudF90eXBlXCI6XCJDUEZcIixcIm5ldF9uYW1lXCI6XCJ1c2VyODA4NzczMzU5MjIgdXNlcjgwODc3MzM1OTIyXCIsXCJuZXRfZG9jdW1lbnRfaWRcIjpcIjE0NTUwNzgxNzQyXCIsXCJzdWJcIjpcIjE0NTUwNzgxNzQyXCJ9IiwicHJvZmlsZSI6ZmFsc2V9LCJyZXNwb25zZSI6eyJleHRyYV9kYXRhIjp7InNlc3Npb25fc3RyaW5ndmFsdWUiOiIoMWQybHQ5dXp4OXhucmhxbmE0YzM5czdud3kpIiwic2Vzc2lvbl9wYXJhbWV0ZXJuYW1lIjoiSEtTIiwic2Vzc2lvbl91c2VyaGFzaCI6Ik16YzVPVGt3T1RWOE1UVTJNamczTURjMk1ud3hPREE1T1dJMk5UVmlNMlE1TkdGbU5qQTFNV0kzTnpNek0yVmtZelExWW1WbVpETmtOakUyTURjM016TXhORGN4TXc9PSIsInVzZXJfdG9rZW4iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuZXlKcFlYUWlPakUxTmpJNE56QTNOaklzSW1WNGNDSTZNVFUyT0RBMU5EYzJNaXdpZFhOeUlqcDdJblZ6WlhKZmFXUWlPaUl6TnprNU9UQTVOU0lzSW5WelpYSnVZVzFsSWpvaU1UUTFOVEEzT0RFM05ESkFibVYwWm14bGVDNWpiMjBpTENKbGJXRnBiQ0k2SW5WelpYSTRNRGczTnpNek5Ua3lNa0JuYldGcGJDNWpiMjBpTENKbWFYSnpkRzVoYldVaU9pSjFjMlZ5T0RBNE56Y3pNelU1TWpJZ2RYTmxjamd3T0RjM016TTFPVEl5SWl3aWJHRnpkRzVoYldVaU9pSnNZWE4wYm1GdFpTSXNJbU52ZFc1MGNubGZZMjlrWlNJNklrSlNJaXdpY21WbmFXOXVJam9pWW5KaGMybHNJaXdpWVdOalpYQjBaV1JmZEdWeWJYTWlPakVzSW1kaGJXbG1hV05oZEdsdmJsOXBaQ0k2SWpWa01ERXhObVJsWXpJeU1XTXlOemMwTkRWaU1EZGhNeUlzSW1GalkyOTFiblFpT201MWJHeDlmUS4xcFFJWWJfVjdPNndwUF9BVGV0bEMzRVgyMlhIYzJqLWotZzBCQ0wyek9nIn19fQ.wDU_iH7DNaPO78r-t1iuH53lNw04eaDNB4gOZdzJaVk"
    }
  }
  customer:{
    cpf: "14550781742"
    createdAt: "2019-06-12T12:14:40"
    email: "user80877335922@gmail.com"
    id: "4a74a77d-d17d-4968-8431-3569bbd01fc3"
    name: "user80877335922"
    phone:{
      ddd: "34"
      number: "988883332"
    }
    status: "ACTIVE"
  }
  sessionToken:{
    accessToken: "e5bf5a1640de298a"
    expiresIn: "86400"
    refreshToken: "717511390dea54be"
    refreshTokenExpiresIn: "172800"
    tokenType: "bearer"
  }
  userInfo: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuZXRfY3VzdG9tZXJfdHlwZSI6ICJORVQtY2xhcm8uY29tLmJyIiwgIm5ldF9wZlVzZXJuYW1lIjogIjE0NTUwNzgxNzQyIiwgIm5ldF91c2VyX3R5cGUiOiAiTkVUIiwgIm5ldF9sb2dpbl9lbWFpbCI6ICJ1c2VyODA4NzczMzU5MjJAZ21haWwuY29tIiwgIm5ldF9lbWFpbCI6ICJ1c2VyODA4NzczMzU5MjJAZ21haWwuY29tIiwgIm5ldF9iaXJ0aGRhdGUiOiAiMDEtMDEtMjAwMSIsICJuZXRfZG9jdW1lbnRfdHlwZSI6ICJDUEYiLCAibmV0X25hbWUiOiAidXNlcjgwODc3MzM1OTIyIHVzZXI4MDg3NzMzNTkyMiIsICJuZXRfZG9jdW1lbnRfaWQiOiAiMTQ1NTA3ODE3NDIiLCAic3ViIjogIjE0NTUwNzgxNzQyIn0.Y87xHluxi5quxyKTA1b0CrJQlZHYrHxWxHFikj3VH5k"

  // -------------------------------------------
  CV RESPONSE #######################

  // version original del resultado
  var resultado = {
    msg: "OK",
    response: {
      accepted_terms: 1,
      city: null,
      counterValidEmail: 14,
      country_code: "BR",
      email: "net1@gmail.com",
      firstname: "nombre",
      gamification_id: "5d25e041a2ad1456a0414d33",
      hasSavedPayway: 1,
      hasUserSusc: 1,
      language: "PT",
      lastname: "apellido",
      lasttouch: {
        seen: "5d273e923ccaa",
        favorited: "5d25e99363d6c",
        purchased: "5d25e0418787f"
      },
      newWorkflow: 1,
      paymentMethods: {
        amcogate: true
      },
      paywayProfile: {
        subscriptions: [
          {
            key: "Telmexmexico_Subscription_SVOD_30d",
            name: "CV_MENSUAL",
            paymentmethod:{
              gateway: "amcogate",
              gateway_text: "TEXT_PGS_PAYMENT_BRASIL_GATEWAY_AMCOGATE"
            }
          }],
        paymentMethods: [
          {
            account: "AMCO_38413639",
            gateway: "amcogate",
            gateway_text: "TEXT_PGS_PAYMENT_BRASIL_GATEWAY_AMCOGATE",
            status: "VALID",
            user_category: null,
            user_category_text: null
          }]
      },
      region: "brasil",
      session_parametername: "HKS",
      session_servername: "10.20.25.9",
      session_stringvalue: "(pgr0nf7buvk32gh9vm0sradq62)",
      session_userhash: "Mzg0MTM2Mzl8MTU2Mjg1OTY3MHw3NmZiZDcyMWQ1YmY0MDQ5OTliMzUyZDU2NmFjZjZkOGExZmRiNWVjZTAyYTk3NTlkMQ==",
      socialNetworks: [{
        extra_data: null,
        id_usuario: "38413639",
        id_usuario_social: "5d25e041a2ad1456a0414d33",
        redsocial: "IMUSICA"
      }],
      status: "success",
      subregion: null,
      subscriptions: {
        AMCO: true
      },
      superhighlight: [
        "susc ",
        "no_suscripto_fox",
        "no_suscripto_fox_sports",
        "no_suscripto_hbo",
        "no_suscripto_crackle",
        "no_suscripto_noggin",
        "no_suscripto_picardia_nacional",
        "no_suscripto_indycar",
        "no_suscripto_edye"
      ],
      user_id: "38413639",
      user_session: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NjI4NTk2NzAsImV4cCI6MTU2ODA0MzY3MCwidXNyIjp7IldHIjp7IlBBSVMiOiJNWCIsIkNPVU5UUllDT0RFX0lQIjoiTVgiLCJSRUdJT04iOiI0NDEiLCJOT01CUkVfUkVHSU9OIjoibWV4aWNvIiwiSURJT01BIjoiRVNQIiwiSUQiOiIyNyIsImlwUmVtb3RlQWRkcmVzcyI6IjIwMC44MC4xNDkuNjgifSwidXN1YXJpbyI6eyJzdG9yYWdlIjp7IkFDVElWTyI6IkgiLCJJRF9VU1VBUklPIjoiMzg0MTM2MzkiLCJVU0VSTkFNRSI6Im5ldDFAZ21haWwuY29tIiwiRU1BSUwiOiJuZXQxQGdtYWlsLmNvbSIsIk5PTUJSRSI6Im5vbWJyZSIsIkFQRUxMSURPIjoiYXBlbGxpZG8iLCJQSU5fUEFSRU5UQUwiOm51bGwsIkNMQVNJRklDQUNJT05fUEFSRU5UQUwiOm51bGwsIlRFUk1JTk9TX0FDRVBUQURPUyI6IlMiLCJUSVBPX1VTVUFSSU8iOiJDQlJBTUNPIiwiUVVJRVJFX0FEVUxUTyI6Ik4iLCJJRF9TS0lOIjoiMSIsIklEX1NQQyI6bnVsbCwiTk9NQlJFX1NLSU4iOm51bGwsIkVTVEFETyI6bnVsbCwiRVNUQURPX0FDVElWTyI6IjEiLCJFWFRfSURfVVNVX1BBUlRORVIiOiJuZXQxQGdtYWlsLmNvbSIsIkVYVF9JRF9TRVNfUEFSVE5FUiI6bnVsbCwiRVhUX0lEX1VTVV9BQ0NPVU5UIjpudWxsLCJFWFRfSURfVVNVX1BST0RVQ1QiOm51bGwsIlVTRVJfUFJPRFVDVCI6Ijk4MCIsIlVTRVJfUFJPRFVDVF9UWVBFIjpudWxsLCJNRURJT19QQUdPX1NVU0NfR0FURVdBWV9OQU1FIjoiYW1jb2dhdGUiLCJQQUlTX1VTVUFSSU8iOiJCUiIsIlJFR0lPTl9VU1VBUklPIjoiYnJhc2lsIiwiU1VCUkVHSU9OX1VTVUFSSU8iOm51bGwsIkNJVURBRF9VU1VBUklPIjpudWxsLCJJRF9NRURJT19QQUdPX01PTkVEQSI6IjIzMCIsIklEX01FRElPX1BBR08iOiI2MiIsIk5PTUJSRV9NRURJT19QQUdPIjoiQW1jbyIsIk1PTkVEQSI6IlIkIiwiTUVESU9fUEFHT19HQVRFV0FZX05BTUUiOiJhbWNvZ2F0ZSIsIkZFQ0hBX05BQ0lNSUVOVE8iOm51bGwsIkZFQ0hBX0FMVEEiOiIxMC1KVUwtMTkiLCJMT0dJTl9GRUNIQSI6bnVsbCwiUkVEU09DSUFMTkVUV09SSyI6bnVsbCwiSURfVVNVQVJJT19TT0NJQUwiOm51bGwsIkVYVFJBX0RBVEEiOm51bGwsIlNPQ0lBTF9ORVRXT1JLUyI6W3siaWRfdXN1YXJpbyI6IjM4NDEzNjM5IiwiaWRfdXN1YXJpb19zb2NpYWwiOiI1ZDI1ZTA0MWEyYWQxNDU2YTA0MTRkMzMiLCJyZWRzb2NpYWwiOiJJTVVTSUNBIiwiZXh0cmFfZGF0YSI6bnVsbH1dLCJTRVNJT05fRkVDSEEiOjE1NjI4NTk2NzAsIkNPVU5URVJfVkFMSURfRU1BSUwiOjE0LCJHQU1JRklDQVRJT05fSUQiOiI1ZDI1ZTA0MWEyYWQxNDU2YTA0MTRkMzMiLCJBQ0NPVU5UIjoiQU1DT18zODQxMzYzOSJ9fX19.v2oObAelMjFpWU-SqwebZr3CRdLJ-LfknLuO8INj2QQ",
      user_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NjI4NTk2NzAsImV4cCI6MTU2ODA0MzY3MCwidXNyIjp7InVzZXJfaWQiOiIzODQxMzYzOSIsInVzZXJuYW1lIjoibmV0MUBnbWFpbC5jb20iLCJlbWFpbCI6Im5ldDFAZ21haWwuY29tIiwiZmlyc3RuYW1lIjoibm9tYnJlIiwibGFzdG5hbWUiOiJhcGVsbGlkbyIsImNvdW50cnlfY29kZSI6IkJSIiwicmVnaW9uIjoiYnJhc2lsIiwiYWNjZXB0ZWRfdGVybXMiOjEsImdhbWlmaWNhdGlvbl9pZCI6IjVkMjVlMDQxYTJhZDE0NTZhMDQxNGQzMyIsImFjY291bnQiOiJBTUNPXzM4NDEzNjM5In19.BTJObOlru3ScQocS7bcGQBcfewxp1ROi98Uq_ZnC6SI",
      username: "net1@gmail.com",
      validEmail: true
    },
    status: "0"
  }

*/

/**
 * refreshes the ZUP token
 * SE USA ESTE!
 */
export async function refreshTokenZUP() {
  try {
    /**
     * Configuracion basica
     */
    const refreshToken = ZUPUtils.getRefreshToken();

    if (refreshToken !== null) {
      const config = getAppConfig(); // config de la aplicacion
      const url = ZUPConfig.getUrl("refresh"); // url desde config de zup
      /**
       * Request headers
       */
      let headers = ZUPConfig.getItem("headers");
      headers["X-Customer-Id"] = storage.getItem("zup-x-customer-id");
      /**
       * Body
       */
      const body = {
        idmSessionToken: {
          refreshToken: refreshToken, //storage.getItem('zup-sessionToken-refreshToken')
        },
        customFields: {
          device_type: config.device_type,
          device_model: config.device_model,
          device_manufacturer: config.device_manufacturer,
          device_category: config.device_category,
          device_id: config.device_id,
          region: config.region,
        },
        tokens: {
          AMCO: {
            jwt: storage.getItem("zup-jwt"),
          },
        },
      };
      const options = {
        body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
        cache: "no-cache", // String	See the cache property for the valid values.
        headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
        method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
        credentials: "include", // include|omit String See the credentials property for the valid values.
        mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
      };
      /**
       * Llamado real
       */
      const myRequest = new Request(url, options);
      const response = await fetch(myRequest);
      var data = await response.json();

      if (data.code) {
        // ERROR
        let responseHeaders = {};
        for (var pair of response.headers.entries())
          responseHeaders[pair[0]] = pair[1];

        return false;
      }
      /**
       * Levanto los headers de respuesta
       * para poder volver a llamar a ZUP
       */
      // ZUPUtils.saveResponseHeaders(response.headers);
      /**
       * Guardo la session de ZUP
       */
      ZUPUtils.saveSessionToken(data.sessionToken); // guardo datos para el refresh

      // storage.setItem("zup-x-customer-id", data.customer.id); // por las dudas, teoricamente no cambia
      return true;
    }
  } catch (err) {
    return false;
  }
}

//
//
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
//
// DEPRECATED CODE
//
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
//
//

/**
 * refreshes the ZUP token
 * NO SE USA!!!!!
 */
export async function _old_refreshTokenZUP() {
  try {
    /**
     * Configuracion basica
     */
    const config = getAppConfig(); // config de la aplicacion
    const url = ZUPConfig.getUrl("old_refresh"); // url desde config de zup
    /**
     * Request headers
     */
    let headers = ZUPConfig.getItem("headers");
    // headers['Authorization']=ZUPUtils.getNewBearer();
    headers["X-Customer-Id"] = storage.getItem("zup-x-customer-id");
    /**
     * Body
     */
    const body = {
      idmSessionToken: {
        refreshToken: ZUPUtils.getRefreshToken(), //storage.getItem('zup-sessionToken-refreshToken')
      },
      customFields: {
        device_type: config.device_type,
        device_model: config.device_model,
        device_manufacturer: config.device_manufacturer,
        device_category: config.device_category,
        device_id: config.device_id,
        region: config.region,
      },
      tokens: {
        AMCO: {
          jwt: storage.getItem("zup-jwt"),
        },
      },
    };
    const options = {
      body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
      cache: "no-cache", // String	See the cache property for the valid values.
      headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
      method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
      credentials: "include", // include|omit String See the credentials property for the valid values.
      mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
    };
    /**
     * Llamado real
     */
    const myRequest = new Request(url, options);
    const response = await fetch(myRequest);
    var data = await response.json();
    /*
      code: "invalid.payload"
      message: "invalid.payload"
      originalError: "JSON parse error: Instantiation of [simple type, class br.com.zup.claro.net.digital.bss.api.user.model.IdmSessionToken] value failed for JSON property refreshToken due to missing (therefore NULL) value for creator parameter refreshToken which is a non-nullable type; nested exception is com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException: Instantiation of [simple type, class br.com.zup.claro.net.digital.bss.api.user.model.IdmSessionToken] value failed for JSON property refreshToken due to missing (therefore NULL) value for creator parameter refreshToken which is a non-nullable type↵ at [Source: (PushbackInputStream); line: 1, column: 40] (through reference chain: br.com.zup.claro.net.digital.bss.api.user.model.AutoLoginRequest["idmSessionToken"]->br.com.zup.claro.net.digital.bss.api.user.model.IdmSessionToken["refreshToken"])"
    */
    if (data.code) {
      let responseHeaders = {};
      for (var pair of response.headers.entries())
        responseHeaders[pair[0]] = pair[1];

      return false;
    }
    /**
     * Levanto los headers de respuesta
     * para poder volver a llamar a ZUP
     */
    // ZUPUtils.saveResponseHeaders(response.headers);
    /**
     * Guardo la session de ZUP
     */
    ZUPUtils.saveSessionToken(data.sessionToken); // guardo datos para el refresh
    storage.setItem("zup-x-customer-id", data.customer.id); // por las dudas, teoricamente no cambia
    /*
      code: "general.error"
      message: "General error."
      module: "IDM"
      originalError: "br.com.zup.claro.net.digital.infrastructure.exception.UnexpectedIntegrationException - IDM Integration Error"
    */
    // {
    //   "customer":{
    //     "id":"4a74a77d-d17d-4968-8431-3569bbd01fc3",
    //     "name":"user80877335922",
    //     "cpf":"14550781742",
    //     "email":"user80877335922@gmail.com",
    //     "phone":{
    //       "ddd":"34",
    //       "number":"988883332"
    //     },
    //     "status":"ACTIVE",
    //     "createdAt":"2019-06-12T12:14:40"
    //   },
    //   "sessionToken":{
    //     "accessToken":"f8a06683831312da",
    //     "expiresIn":"86400",
    //     "tokenType":"bearer",
    //     "refreshToken":"d2feaaea59910d22",
    //     "refreshTokenExpiresIn":"172800"
    //   },
    //   "userInfo":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuZXRfY3VzdG9tZXJfdHlwZSI6ICJORVQtY2xhcm8uY29tLmJyIiwgIm5ldF9wZlVzZXJuYW1lIjogIjE0NTUwNzgxNzQyIiwgIm5ldF91c2VyX3R5cGUiOiAiTkVUIiwgIm5ldF9sb2dpbl9lbWFpbCI6ICJ1c2VyODA4NzczMzU5MjJAZ21haWwuY29tIiwgIm5ldF9lbWFpbCI6ICJ1c2VyODA4NzczMzU5MjJAZ21haWwuY29tIiwgIm5ldF9iaXJ0aGRhdGUiOiAiMDEtMDEtMjAwMSIsICJuZXRfZG9jdW1lbnRfdHlwZSI6ICJDUEYiLCAibmV0X25hbWUiOiAidXNlcjgwODc3MzM1OTIyIHVzZXI4MDg3NzMzNTkyMiIsICJuZXRfZG9jdW1lbnRfaWQiOiAiMTQ1NTA3ODE3NDIiLCAic3ViIjogIjE0NTUwNzgxNzQyIn0.Y87xHluxi5quxyKTA1b0CrJQlZHYrHxWxHFikj3VH5k",
    //   "chainResponse":{
    //     "AMCO":{
    //       "jwt":"eyJhbGciOiJIUzI1NiJ9.eyJlbnRyeSI6eyJzb3VyY2UiOiJuZXRmbGV4IiwiZm9ybWF0IjoianNvbiIsInJlZ2lvbiI6ImJyYXNpbCIsInBsYXRmb3JtIjoiTkYiLCJkZXZpY2VfaWQiOiI1ZjY0OWJiNS1jMGViLTAzZTMtOTE2Ni1lNzg0Y2UyMmYxMDgiLCJkZXZpY2VfY2F0ZWdvcnkiOiJ0diIsImRldmljZV9tYW51ZmFjdHVyZXIiOiJzb255IiwiZGV2aWNlX21vZGVsIjoic29ueSIsImRldmljZV90eXBlIjoidHYiLCJhdXRocHQiOiI1ZmFjZDlkMjNkMDViYjgzIiwiYXV0aHBuIjoibmV0IiwiSEtTIjoiKDhsYTh4N2owdmw0bnRiNmZodzVxaTFpd3NiKSIsInVzZXJfaGFzaCI6Ik16YzVPVGt3T1RWOE1UVTJNemc0TmpnMk1Yd3dNV1JsTVdJeU5tVm1ZalF5T1dNeU1qTTNPRFkwTnpoaE56ZGpOVEEwTkRrek5qZzVNVE5pWTJNMU9EZ3dOV1F5WlE9PSIsInByb2ZpbGUiOmZhbHNlfSwicmVzcG9uc2UiOnsic2Vzc2lvbl9zdHJpbmd2YWx1ZSI6Iig4bGE4eDdqMHZsNG50YjZmaHc1cWkxaXdzYikiLCJzZXNzaW9uX3BhcmFtZXRlcm5hbWUiOiJIS1MiLCJzZXNzaW9uX3VzZXJoYXNoIjoiTXpjNU9Ua3dPVFY4TVRVMk16ZzROamczTm53ME56WTRaR1JtTlRFd1kyRTBNak15WVdZM1pUSTRObUk1WVdGaE1qRTVObUZrTjJVME1UTmpPVFk1WkRnek1XRTVaZz09IiwidXNlcl90b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5leUpwWVhRaU9qRTFOak00T0RZNE56WXNJbVY0Y0NJNk1UVTJPVEEzTURnM05pd2lkWE55SWpwN0luVnpaWEpmYVdRaU9pSXpOems1T1RBNU5TSXNJblZ6WlhKZmRIbHdaU0k2SWtOQ1VrNVBWa2xRSWl3aWRYTmxjbTVoYldVaU9pSXhORFUxTURjNE1UYzBNa0J1WlhSbWJHVjRMbU52YlNJc0ltVnRZV2xzSWpvaWRYTmxjamd3T0RjM016TTFPVEl5UUdkdFlXbHNMbU52YlNJc0ltWnBjbk4wYm1GdFpTSTZJblZ6WlhJNE1EZzNOek16TlRreU1pQjFjMlZ5T0RBNE56Y3pNelU1TWpJaUxDSnNZWE4wYm1GdFpTSTZJbXhoYzNSdVlXMWxJaXdpWTI5MWJuUnllVjlqYjJSbElqb2lRbElpTENKeVpXZHBiMjRpT2lKaWNtRnphV3dpTENKaFkyTmxjSFJsWkY5MFpYSnRjeUk2TVN3aVoyRnRhV1pwWTJGMGFXOXVYMmxrSWpvaU5XUXdNVEUyWkdWak1qSXhZekkzTnpRME5XSXdOMkV6SWl3aWNHRnlaVzUwWDJsa0lqb2lNemM1T1Rrd09UVWlMQ0poWTJOdmRXNTBJanB1ZFd4c0xDSmhaRzFwYmlJNmRISjFaWDE5LkRJMnA4SmxlT3NyVURuZi1JZ2tFVDdtS3pTamU2X08tSFdudkF4YXZCcXcifX0.mAfPUskqXGJzXZFRW8-OxSYO_zjv2BeH0eulRkG4cxs"
    //     }
    //   }
    // }
    return true;
  } catch (err) {
    return false;
    // return new Promise((resolve, reject) => {
    //   reject(err);
    // });
  }
}
