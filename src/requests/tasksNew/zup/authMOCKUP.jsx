/**
 * ZUP AUTH
 */
import storage from "../../../components/DeviceStorage/DeviceStorage";
// import ZUPUtils from "../../../utils/ZUPUtils";
// import ZUPConfig from "../../../config/ZUPConfig";
// import getAppConfig from "../../../config/appConfig";

import MOCKUP from "../../../config/loginMockup";

/**
 * Login de ZUP
 * user info from Cleber > username: 62038725470 password: @Nowauto email:user32603269692@gmail.com
 * user info from Martin Lucero > username nowmultiscreen1 password nowmulti1
 *
 * @param {object} params {email / username / password}
 */
export async function loginZup(params) {
  const fHead = "[LOGIN]{MOCKUP} --";

  console.log(fHead, "******************************");
  console.log(fHead, "*********** MOCKUP ***********");
  console.log(fHead, "******************************");
  console.log(fHead, "MOCKUP=", MOCKUP);
  console.log(fHead, "******************************");

  // resultado
  var resultado = {
    TYPE: "MOCKUP",
    status: "0", // must be string
    success: true,
    code: 200,
    error: "MOCKUP--no error",
  };

  // localStorage
  storage.setItem("zup-authtoken", MOCKUP["zup-authtoken"]);
  storage.setItem("HKS", MOCKUP["HKS"]); // con o sin parentesis es igual
  storage.setItem("user_hash", MOCKUP["user_hash"]); // sino revienta el last_seen // el login lo vuelve a setear
  storage.setItem("zup-customer-id", MOCKUP["zup-customer-id"]);
  storage.setItem("zup-amco-user_token", MOCKUP["zup-amco-user_token"]);
  storage.setItem("zup-amco-user_name", MOCKUP["zup-amco-user_name"]);
  storage.setItem("zup-x-customer-id", MOCKUP["zup-x-customer-id"]);
  storage.setItem("zup-MOCKUP", true);
  // --------------------

  console.log(fHead, "******************************");
  console.log(fHead, "result=", resultado);
  console.log(fHead, "******************************");

  return new Promise((resolve, reject) => {
    resolve(resultado);
  });
}

