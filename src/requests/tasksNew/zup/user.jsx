/**
 * ZUP USER
 */
import ZUPUtils from "../../../utils/ZUPUtils";
import ZUPConfig from "../../../config/ZUPConfig";
// import getAppConfig from "../../../config/appConfig";


/**
 * Obtiene datos del cliente
 * (bueno para mentener la session)
 */
export async function getClientData() {
  try {
    console.log("[ZUP][getClientData] -- init");

    /**
     * Configuracion basica
     */
    // const config = getAppConfig(); // config de la aplicacion
    const url = ZUPConfig.getUrl("getClientData"); // url desde config de zup

    /**
     * Request headers
     */
    let headers = ZUPConfig.getItem("headers");
    headers["Authorization"] = ZUPUtils.getNewBearer();

    /**
     * Body & Options
     */
    // const body =  {};
    const options = {
      // "method": "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
      // "body": JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
      cache: "no-cache", // String	See the cache property for the valid values.
      headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
      credentials: "include", // include|omit String See the credentials property for the valid values.
      mode: "cors" // cors|no-cors|same-origin String See the mode property for the valid values.
    };
    console.info(
      "[ZUP][getClientData] >>\nurl = ",
      url,
      "\noptions = ",
      options
    );

    /**
     * Llamado real
     */
    const myRequest = new Request(url, options);
    const response = await fetch(myRequest);
    const data = await ZUPUtils.readZupResponse(response, "getClientData");

    //
    // RESULT
    const result = {
      data
    };
    console.info("[ZUP][getClientData] >> resultado\n", result);

    return result;
  } catch (err) {
    console.error("[ZUP][getClientData] ERROR -- ", err);
    return {}; // Fake result

    // return new Promise((resolve, reject) => {
    //   reject(err);
    // });
  }
}
