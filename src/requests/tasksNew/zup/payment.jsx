/**
 * ZUP PAYMENT
 */
import ZUPUtils from "../../../utils/ZUPUtils";
import ZUPConfig from "../../../config/ZUPConfig";
import getAppConfig from "../../../config/appConfig";
import storage from '../../../components/DeviceStorage/DeviceStorage';
import Device from "../../../devices/device";

export async function getListOffers() {
  try {
    const url = ZUPConfig.getUrl("getOffers");

    const headers = ZUPConfig.getItem("headers");
    headers['X-Customer-Id'] = storage.getItem('zup-customer-id');
    headers['X-Channel-Id'] = ZUPConfig.getItem('channel_id');
    headers["Authorization"] = ZUPUtils.getNewBearer();

    const body = {
      "offerParameters" : {
        "addons" : {
          "TYPE" : "SVOD"
        },
        "plans" : {
          "TYPE" : "PLAN",
          "CATALOG_OFFER_TYPE": "ADDON"
        }
      }
    }

    const options = {
      method: "POST",
      body: JSON.stringify(body),
      cache: "no-cache",
      headers: headers,
      credentials: "include",
      mode: "cors"
    };

    const myRequest = new Request(url, options);
    const response = await fetch(myRequest);
    const data = await ZUPUtils.readZupPaymentResponse(response);

    if(data.plans) data.plans=getSvodLiveContents(data.plans)
    if(data.addons) data.addons=getSvodLiveContents(data.addons)
    console.log('[RENT] --------------------------------')
    console.log('[RENT] DATA',data);
    console.log('[RENT] --------------------------------')

    const result = {
      data
    };

    return result;
  } catch (err) {
    throw err
    return {};
  }
}

/**
 * Para los arrays de formato tipo PLAN y ADDONS del
 * getListOffers, genera array con los items separados
 * por SVOD y LIVE
 * Asignar el resultado del mismo dato
 * data = getSvodLiveContents(data)
 * @param array origins
 */
function getSvodLiveContents(origins){
  console.log('[RENT] getSvodLiveContents IN:',origins);
  origins.forEach((plan,nroplan)=>{ // para cada plan
    let live=[], svod=[];
    plan.items.forEach((i,nroitem)=>{ // por cada item del plan
      if(i.composition && i.composition.attributes && i.composition.attributes.length){ // si tengo el dato
        let attr=i.composition.attributes;
        for(var r=0; r<attr.length; r++){ // por cada attributo
          if(attr[r].name=="TYPE"){
            let type=i.composition.attributes[0].text
            switch(type){
              case 'LIVE': live.push(i); break;
              case 'SVOD': svod.push(i); break;
              default: console.log('[RENT] ERROR -- no lo entendi',type);
            }
          }
        }
      }
    })
    plan['SVOD']=svod;
    plan['LIVE']=live
  })
  console.log('[RENT] getSvodLiveContents OUT:',origins);
  return origins;
}


export async function CheckoutBuyPlan(catalogoOfferId, methodId, imageUrly, contentDescription, catalogItemId, content_id){
  console.log('[CHECKOUT] -- CheckoutBuyPlan', methodId);
  const url = ZUPConfig.getUrl("CheckoutBuy");

   // DEVICE
  let abstractId = Device.getDevice().getId();
  let device = Device.getDevice().getPlatform();

  const body =  {
    "catalogOfferId": catalogoOfferId,
    "catalogOfferType": "PLAN",
    "externalId": Date.now().toString(),
    "payment": {
      "methods": [
        {
          "methodId": methodId,
          "method": "CREDIT_CARD",
          "deviceInfo": {
            "brand":  "Apple",
            "deviceUniqueId": abstractId.getUniqueID(),
            "fingerPrint": storage.getItem("HKS") + new Date().toTimeString().split(' ')[0],
            "manufacturer": "Apple",
            "model":  "Apple",
            "userAgent": abstractId.getUserAgent(),
            "wifiMacAddress": "",
            "ip": abstractId.getIP()
          },
          "purchaseInfo": {
            // "imageUrl" : "http://image",
            "origin": "SMARTTV",
            "imageUrl": imageUrly,
            "contentDescription": contentDescription,
          }
        }
      ]
    },
    "externalId" : Date.now().toString(),
    "quantity" : 1,
    "userParameters" : [{
      "catalogItemId" : catalogItemId,
      "parameters" : {
        "CONTENT_ID" : content_id
      }
    }]
  }

  let headers = ZUPConfig.getItem("headers");
  headers['X-Customer-Id'] = storage.getItem('zup-customer-id');
  headers['X-Channel-Id'] = ZUPConfig.getItem('channel_id');
  headers["Authorization"] = ZUPUtils.getNewBearer();

  const options = {
    method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
    body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
    cache: "no-cache", // String	See the cache property for the valid values.
    headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
    credentials: "include", // include|omit String See the credentials property for the valid values.
    mode: "cors" // cors|no-cors|same-origin String See the mode property for the valid values.
  };

  const myRequest = new Request(url, options);
    const response = await fetch(myRequest);
    const data = await ZUPUtils.readZupPaymentResponse(response);
    const result = {
      data, status: response.status
    };

    return result
}

/**
 * ZUP Get the offers
 */
export async function getOffers(title, addons, plans) {
  try {
    console.log("getOffers");

    /**
     * Configuracion basica
     */
    //const config = getAppConfig(); // config de la aplicacion
    console.log("getOffers init")


    const url = ZUPConfig.getUrl("getOffers");
    console.log("getOffers url", url)
    /**
     * Request headers
     */
    let headers = ZUPConfig.getItem("headers");
    headers['X-Customer-Id'] = storage.getItem('zup-customer-id');
    headers['X-Channel-Id'] = ZUPConfig.getItem('channel_id');
    headers["Authorization"] = ZUPUtils.getNewBearer();

    /**
     * Body & Options
     */
    const body =  {
        "titleRequested" : title,
        "compositionsName" : {
            "addons" : addons, //"LANCAMENTOS"
            "plans" : plans // "LANCAMENTOS"
        }
    };
    const options = {
      method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
      body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
      cache: "no-cache", // String	See the cache property for the valid values.
      headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
      credentials: "include", // include|omit String See the credentials property for the valid values.
      mode: "cors" // cors|no-cors|same-origin String See the mode property for the valid values.
    };
    console.info(
      "[ZUP][getOffers] >>\nurl = ",
      url,
      "\noptions = ",
      options
    );

    /**
     * Llamado real
     */
    const myRequest = new Request(url, options);
    console.log("getOffers myRequest", myRequest)
    const response = await fetch(myRequest);
    console.log("getOffers response", response)
    const data = await ZUPUtils.readZupPaymentResponse(response);
    console.log("getOffers data", data)
    //
    // RESULT
    const result = {
      data
    };


    console.info("[ZUP][getOffers] >> resultado\n", JSON.stringify(result));
    console.info("[ZUP][getOffers] >> resultado\n", result);

    return result;
  } catch (err) {
    console.error("[ZUP][getOffers] ERROR -- ", err);
    return {}; // Fake result

  }
}

/**
 * CardCredit nuevo
 */


export async function CardCredit(){
  // URL
  const url = ZUPConfig.getUrl("cardCredit");

  // Header
  let headers = ZUPConfig.getItem("headers");
  headers['X-Customer-Id'] = storage.getItem('zup-customer-id');
  headers['X-Channel-Id'] = ZUPConfig.getItem('channel_id');
  headers["Authorization"] = ZUPUtils.getNewBearer();
 console.log("CardCredit headers", headers)

  const options = {
    method: "GET", // String	The http method such as 'GET', 'POST', 'DELETE'.
   // body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
    cache: "no-cache", // String	See the cache property for the valid values.
    headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
    credentials: "include", // include|omit String See the credentials property for the valid values.
    mode: "cors" // cors|no-cors|same-origin String See the mode property for the valid values.
  };
  const myRequest = new Request(url, options);
    console.log("CardCredit myRequest", myRequest)
    const response = await fetch(myRequest);
    console.log("CardCredit response", response)
    const data = await ZUPUtils.readZupPaymentResponse(response);
    console.log("CardCredit data", data)
    const result = {
      data
    };

    return result
}

export async function CheckoutBuy(catalogoOfferId, methodId, imageUrly, contentDescription){
  console.log('[CHECKOUT] -- CheckoutBuy', methodId);
  const url = ZUPConfig.getUrl("CheckoutBuy");

   // DEVICE
  let abstractId = Device.getDevice().getId();
  let device = Device.getDevice().getPlatform();

  //ARBOL CHECKOUT COMPRA
  const body =  {
    "catalogOfferId": catalogoOfferId,
    "catalogOfferType": "ADDON",
    "externalId": Date.now().toString(),
    "payment": {
      "methods": [
        {
          "methodId": methodId,
          "method": "CREDIT_CARD",
          "deviceInfo": {
            "brand":  "Apple",
            "deviceUniqueId": abstractId.getUniqueID(),
            "fingerPrint": storage.getItem("HKS") + new Date().toTimeString().split(' ')[0],
            "manufacturer": "Apple",
            "model":  "Apple",
            "userAgent": abstractId.getUserAgent(),
            "wifiMacAddress": "",
            "ip": abstractId.getIP()
          },
          "purchaseInfo": {
            "imageUrl": imageUrly,
            "contentDescription": contentDescription,
            "origin": "SMARTTV"
          }
        }
      ]
    }
  }

  let headers = ZUPConfig.getItem("headers");
  headers['X-Customer-Id'] = storage.getItem('zup-customer-id');
  headers['X-Channel-Id'] = ZUPConfig.getItem('channel_id');
  headers["Authorization"] = ZUPUtils.getNewBearer();
  console.log("CheckoutBuy headers", headers)

  const options = {
    method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
    body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
    cache: "no-cache", // String	See the cache property for the valid values.
    headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
    credentials: "include", // include|omit String See the credentials property for the valid values.
    mode: "cors" // cors|no-cors|same-origin String See the mode property for the valid values.
  };
  const myRequest = new Request(url, options);
    console.log("CheckoutBuy myRequest", myRequest)
    const response = await fetch(myRequest);
    console.log("CheckoutBuy response", response)
    const data = await ZUPUtils.readZupPaymentResponse(response);
    console.log("CheckoutBuy data", data)
    const result = {
      data, status: response.status
    };

    return result
}

export async function CheckoutRenta(catalogoOfferId, methodId, imageUrly, contentDescription, catalogItemId, content_id){
  console.log('[CHECKOUT] -- CheckoutRenta', methodId);
  const url = ZUPConfig.getUrl("CheckoutRenta");
  let abstractId = Device.getDevice().getId();
  let device = Device.getDevice().getPlatform();

  const body = {
    "catalogOfferId" : catalogoOfferId,
    "catalogOfferType" : "ADDON",
    "payment" : {
      "methods" : [ {
        "methodId" : methodId,
        "method" : "CREDIT_CARD",
        "deviceInfo" : {
          "deviceUniqueId" : abstractId.getUniqueID(),
          "userAgent" : abstractId.getUserAgent(),
          "wifiMacAddress" : "68:c4:4d:b9:cd:58",
          "ip" : abstractId.getIP(),
          "brand" : "tv",
          "fingerPrint" : storage.getItem("HKS") + new Date().toTimeString().split(' ')[0],
          "manufacturer" : "lg",
          "model" : abstractId.getModel()
        },
        "purchaseInfo" : {
          "imageUrl" : imageUrly,
          "contentDescription" : contentDescription,
          "origin" : "SMARTTV"
        },
        "installments" : 1
      } ]
    },
    "externalId" : Date.now().toString(),
    "quantity" : 1,
    "userParameters" : [ {
      "catalogItemId" : catalogItemId,
      "parameters" : {
        "CONTENT_ID" : content_id
      }
    } ]
  }

  console.log("test_body", body)



  let headers = ZUPConfig.getItem("headers");
  headers['X-Customer-Id'] = storage.getItem('zup-customer-id');
  headers['X-Channel-Id'] = ZUPConfig.getItem('channel_id');
  headers["Authorization"] = ZUPUtils.getNewBearer();
  console.log("CheckoutBuy headers", headers)

  const options = {
    method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
    body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
    cache: "no-cache", // String	See the cache property for the valid values.
    headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
    credentials: "include", // include|omit String See the credentials property for the valid values.
    mode: "cors" // cors|no-cors|same-origin String See the mode property for the valid values.
  };

  console.log("CheckoutRenta abstractId", abstractId)
  console.log("CheckoutRenta device", device)
  console.log("CheckoutRenta body", body)
  console.log("CheckoutRenta body json", JSON.stringify(body))
  console.log("CheckoutRenta options", options)
  const myRequest = new Request(url, options);
  console.log("CheckoutRenta myRequest", myRequest)
  const response = await fetch(myRequest);
  console.log("CheckoutRenta response", response)
  let data = await ZUPUtils.readZupPaymentResponse(response);
  console.log("CheckoutRenta data", data)
  const result = {
    data, status: response.status
  };

  return result
}


// Documentation
//
/*
    -- post to get payment method -----

    POST /bss/offers HTTP/1.1
    Content-Type: application/json
    Content-Type: application/json
    Accept: application/json
    **X-Application-Id: net_digital_test
    **X-Organization-Slug: claro
    **X-Customer-Id: 2002a4bd-1d04-4878-a382-bf7ada450d09
    **Authorization: Bearer eyJhbGciOiJSUzI1NiIsI...
    X-Channel-Id: eac1f766-5d34-4e4d-b0ed-5f86ced35466
    Host: localhost:8080
    Content-Length: 132

    {
    "titleRequested" : "Moonlight",
    "compositionsName" : {
        "addons" : [ "LANCAMENTOS" ],
        "plans" : [ "LANCAMENTOS" ]
    }
    }
*/
