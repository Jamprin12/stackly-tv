/**
 * ZUP AUTH
 */
// import Translator from "../../apa/Translator";
import moment from 'moment';
import storage from "../../../components/DeviceStorage/DeviceStorage";
import ZUPUtils from "../../../utils/ZUPUtils";
import ZUPConfig from "../../../config/ZUPConfig";
// import getAppConfig from "../../../config/appConfig";
// import { getClientData, getOffers } from "../../loader";

// const DAYS_IN_ADVANCE = 30;
// const DAYS_BEHIND = 30;
// const EXPIRED_NUMBER = 5;

export async function getRentedByGroupId(groupId){
  const result = await getRented()
  console.log('RESULT >>>>>>', result)
  console.log('GROUP ID >>>>>>', groupId)
  const filter = result && result.items.filter(item => item.contentId === groupId)
  console.log('filter rented items',filter)
  return filter[0] || false
}

export async function getRentedExpired(){

  const result = await getRented(true);
  return result;
}

/**
 * ZUP Get the offers
 */
export async function getRented(expired = false) {
  try {
    console.log(
      "[ZUP][RENTED] src/requests/tasks/zup/rented.jsx -- getRented : expired?",
      expired
    );

    /**
     * Configuracion basica
     */
    //const config = getAppConfig(); // config de la aplicacion

    const url = ZUPConfig.getUrl("getRented");
    /**
     * Request headers
     */
    let headers = ZUPConfig.getItem("headers");
    headers["X-Customer-Id"] = storage.getItem("zup-customer-id");
    headers["X-Channel-Id"] = ZUPConfig.getItem("channel_id");
    headers["Authorization"] = ZUPUtils.getNewBearer();

    /**
     * PARAMETROS FIJOS
     */
    const configParams = ZUPConfig.getItem("rented");
    const currencyParams = ZUPConfig.getItem('currency');

    /**
     * DATES
     */
    const today = new Date();
    let startDate = new Date();
    let endDate = new Date();
    if (expired) {
      // para atras..
      startDate.setDate(today.getDate() - configParams.days_behind);
      endDate = today;
    } else {
      // para adelante
      startDate = today;
      endDate.setDate(today.getDate() + configParams.days_in_advance);
    }
    const tsStartDate = startDate.toISOString().substring(0, 10);
    const tsEndDate = endDate.toISOString().substring(0, 10);
    const tsExpired = expired ? "EXPIRED" : "ACTIVE";
    // console.log("[ZUP][RENTED] dates -- ", expired, today, configParams.days_behind, tsStartDate, configParams.days_in_advance, tsEndDate );

    /**
     * Body & Options
     * showDuplicates=false should allways be false
     */
    var completeURL = url + `&startDate=${tsStartDate}&endDate=${tsEndDate}&status=${tsExpired}&expiredAmount=${configParams.expired_number}&showDuplicates=false`;
    const options = {
      method: "GET", // String	The http method such as 'GET', 'POST', 'DELETE'.
      cache: "no-cache", // String	See the cache property for the valid values.
      headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
      credentials: "include", // include|omit String See the credentials property for the valid values.
      mode: "cors" // cors|no-cors|same-origin String See the mode property for the valid values.
    };
    // console.info("[ZUP][RENTED][getRented] >>\nurl = ",url,"\noptions = ",options);

    /**
     * Llamado real
     */
    const myRequest = new Request(completeURL, options);
    // console.log("[ZUP][RENTED] myRequest", myRequest);
    const response = await fetch(myRequest);
    // console.log("[ZUP][RENTED] response", response);
    const data = await ZUPUtils.readZupPaymentResponse(response);
    console.log("[ZUP][RENTED] data", data);
    //
    // RESULT
    let result = {};
    // fill data
    if (data.length) {
      // hay datos
      result.params = {
        startDate: tsStartDate,
        endDate: tsEndDate,
        status: expired ? "EXPIRED" : "ACTIVE",
        expiredAmount: 5,
        showDuplicates: false
      };
      let el = {};
      let fecha = null
      let items = []
      for (var i = 0; i < data.length; i++) {
        el = data[i];
        // fecha = expired ? toTimeStamp(el.endDate) : toTimeStamp(el.startDate);
        // result[fecha]={
        items.push({
          id: el.contentId, //: "537121"
          contentDescription: el.contentDescription, //: "Quatro indicações ao Oscar® e baseado em uma história real. Um escândalo que arrasou uma nação."
          contentId: el.contentId, //: "537121"
          endDate: `${moment(el.endDate).format('DD/MM/YYYY')} às ${moment(el.endDate).format('HH:mm')}`, //.substr(0, 10), //: "2019-11-05T16:57:50"
          cardEndDate: `${moment(el.endDate).format('DD/MM/YYYY')} às ${moment(el.endDate).format('HH:mm')}`,
          entitlement: el.entitlement, //: "NX_TVOD_STANDARD_RENT_24_HRS"
          imageUrl: el.imageUrl + "?size&w=290", //: "http://clarovideocdn3.clarovideo.net/PELICULAS/QUIZSHOW/EXPORTACION_WEB/PT/QUIZSHOWWVERTICAL.jpg?size=200x300"
          price: {
            price:el.price.amount,
            currency: currencyParams[el.price.currency],
            scale:el.price.scale,
            realprice: parseFloat(el.price.amount / 10 ** el.price.scale).toFixed(2).replace('.', ',')
          }, //el.price, //: // amount: 900 // currency: "BRL" // scale: 2
          startDate:`${moment(el.startDate).format('DD/MM/YYYY')}`, //.substr(0, 10), //: "2019-11-04T16:57:50"
          cardStartDate:`${moment(el.startDate).format('DD/MM/YYYY')}`,
          validity: el.validity // // duration: 24 // period: "HOUR" // unlimited: false
        });
      }
      /**
       * Ordenamiento
       */
      if(!expired){
        // console.info("[ZUP][RENTED] >> ACTIVE resultado sort before \n", result);
        items.sort((a,b)=>{
          // console.log('[ZUP][RENTED][SORT]',a.endDate,b.endDate);
          return (a.endDate > b.endDate);
        })
        // console.info("[ZUP][RENTED] >> ACTIVE resultado sort after\n", result);
      }else{ //  ordenados pela data de expiração (ordem decrescente)
        // console.info("[ZUP][RENTED] >> EXPIRED resultado sort before \n", result);
        items.sort((a,b)=>{
          // console.log('[ZUP][RENTED][SORT]',a.endDate,b.endDate, a.endDate > b.endDate);
          return (a.endDate > b.endDate);
        })
        // console.info("[ZUP][RENTED] >> EXPIRED resultado sort after\n", result);
      }
      result.status = "OK";
      result.items = items;

    } else {
      // no hay datos
      result = {
        status : "ERROR",
        msg : "NODATA-ERROR",
        items:[]
      };
    }

    // console.info("[ZUP][RENTED] >> resultado\n", JSON.stringify(result));
    console.info("[ZUP][RENTED] >> resultado\n", result);

    // return result;

    return new Promise((resolve, reject) => {
      resolve(result);
    });

  } catch (err) {
    console.error("[ZUP][getOffers] ERROR -- ", err);
    return {
      status: "ERROR",
      msg: "NET-ERROR",
      items:[]
    }; // Fake result
  }

}


function toTimeStamp(myDate) {
  console.info("[ZUP][RENTED] >> toTimeStamp\n", myDate);
  myDate = myDate.split("-");
  console.info("[ZUP][RENTED] >> toTimeStamp\n", myDate);
  var newDate = new Date(myDate[1] + "/" + myDate[2] + "/" + myDate[0])
  console.info("[ZUP][RENTED] >> toTimeStamp\n", newDate);
  newDate = newDate.getTime()
  console.info("[ZUP][RENTED] >> toTimeStamp\n", newDate);
  return newDate; //will alert 1330210800000
}



