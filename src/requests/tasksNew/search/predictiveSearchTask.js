import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";
import DeviceStorage from "../../../components/DeviceStorage/DeviceStorage";
import filterList from "../filterList";

class predictiveSearchTask extends AbstractRequestTask {
  constructor(parameters) {
    super();
    this.customParameters = parameters;
  }

  getHeaders() {
    return {};
  }

  getParams() {
    const params = super.getParams();
    const epg = DeviceStorage.getItem("geo-epg");
    const geoEpg = JSON.parse(epg);

    return {
      ...params,
      ...this.customParameters,
      filterlist: filterList(),
      api_version: "v6",
      epg_version: geoEpg,
      suggest: "0",
      movies: "15",
      series: "15",
      live_channels: "15",
      events: "15",
      genres: "0",
      talents: "15",
      users: "0",
      unavailables: "0",
    };
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();

    return `${http}${url}/services/search/predictive`;
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default predictiveSearchTask;
