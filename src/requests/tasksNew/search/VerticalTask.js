import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";
import filterList from "../filterList";

class VerticalLookUp extends AbstractRequestTask {
  constructor(
    value = "",
    field = "",
    provider_id = "",
    from = 0,
    quantity = 20
  ) {
    super();

    this.value = value;
    this.field = field;
    this.from = from;
    this.quantity = quantity;
    this.provider_id = provider_id;
  }

  getHeaders() {
    return {};
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();
    return `${http}${url}/services/search/vertical`;
  }

  getParams() {
    const params = super.getParams();

    return {
      ...params,
      value: this.value,
      field: this.field,
      from: this.from,
      quantity: this.quantity,
      tenant_code: "netnow",
      api_version: "v6",
      provider_id: this.provider_id ? this.provider_id : "2", // default AMCO --> https://dlatvarg.atlassian.net/wiki/spaces/GCV/pages/204078641/search+vertical+V6
      filterlist: filterList(),
    };
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default VerticalLookUp;
