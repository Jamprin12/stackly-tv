//
// Utilidades para geolocalizacion
//
import RequestManager from "../../RequestManager";
import GeocodeTask from "./GeocodeTask";
import GeoEpgTask from "./GeoEpgTask";
import storage from "../../../components/DeviceStorage/DeviceStorage";

export async function getEpgVersionFromLatitudeLongitud(forced = false) {
  const lHead = "[LL] getEpgVersionFromLatitudeLongitud -- ";
  // forced = true; // for debugging purposes
  //
  // RETRY management
  //
  const doRetry = () => {
    incRetry();
    if (parseInt(storage.getItem("geo-retry") || "0", 10) < 5) {
      console.log(lHead + "set to retry :", storage.getItem("geo-retry"));
      setTimeout(getEpgVersionFromLatitudeLongitud, 10000);
    } else {
      resetRetry();
      setTimeout(getEpgVersionFromLatitudeLongitud, 1 * 60 * 1000); // treinta minutos?
    }
  };
  const incRetry = () =>
    storage.setItem(
      "geo-retry",
      parseInt(storage.getItem("geo-retry") || "0", 10) + 1
    );
  const resetRetry = () => storage.unsetItem("geo-retry");
  try {
    if (!forced) {
      if (
        storage.getItem("geo-coord") !== null &&
        storage.getItem("geo-location") !== null &&
        storage.getItem("geo-epg")
      ) {
        console.log(
          lHead + "cached location.",
          storage.getItem("geo-location")
        );
        return Promise.resolve(true);
      }
    }

    //
    // GOOGLE geolocation
    //
    const url =
      "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCyvEfTEH0tUh9MFv6-bcISK6BMHIAAFG4";
    const options = {
      method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
      cache: "no-cache", // String	See the cache property for the valid values.
    };
    const llRequest = new Request(url, options);
    const llResponse = await fetch(llRequest);
    if (llResponse.status !== 200) {
      doRetry();
      return Promise.resolve(null);
    }
    const llData = await llResponse.json();
    console.log(lHead + "Google response >", llData.location);
    storage.setItem("geo-coord", JSON.stringify(llData.location));

    //
    // GeocodeTask
    //
    // lat: -23.609344
    // lng: -46.7173376
    // longitud:-46.3322
    // latitud:-23.9618
    // const geoCode = new GeocodeTask(-46.3322,-23.9620);
    // llData.location.lng=-46.3322
    // llData.location.lat=-23.9618
    // console.log(lHead+'PORLAS',llData.location);

    const geoCode = new GeocodeTask(llData.location.lng, llData.location.lat);
    const regSub = await RequestManager.addRequest(geoCode);
    console.log(lHead + "AMCO response >", regSub.response);
    storage.setItem(
      "geo-location",
      JSON.stringify({
        region: regSub.response.region,
        subregion: regSub.response.subregion,
      })
    );

    //
    // GeoEpgTask
    //
    const geoEpg = new GeoEpgTask(
      regSub.response.region,
      regSub.response.subregion
    );
    const epgVer = await RequestManager.addRequest(geoEpg);
    console.log(lHead + "AMCO response >", epgVer.response);
    storage.setItem("geo-epg", JSON.stringify(epgVer.response.epg_version));

    //
    // SALIDA
    //
    resetRetry();
    return Promise.resolve(true);
  } catch (err) {
    doRetry();
    console.error(lHead + "GENERAL -- ERROR !", err);
    return Promise.resolve(null);
  }
}
