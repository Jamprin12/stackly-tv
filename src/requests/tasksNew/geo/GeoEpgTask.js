import AbstractRequestTask from "../AbstractRequestTask";
import { getApiEndpoint } from "../../../config/appConfig";

class GeoEpgTask extends AbstractRequestTask {
  constructor(region,subregion) {
    super();
    this.region=region;
    this.subregion=subregion;
  }

  getHeaders() {
    return {};
  }

  // getMethod() {
  //   return "POST";
  // }

  getParams() {
    const params = super.getParams();
    params.region=this.region.toLowerCase();;
    params.subregion=this.subregion.toLowerCase();;
    params.tenant_code='netnow'
    // console.log('[LL] -- getParams -- ',params);
    return params;
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();
    return `${http}${url}/services/epg/version`;
    // return `http://microfwk-web-test.clarovideo.net/services/user/geocode`;
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default GeoEpgTask;

/*
curl --location --request POST 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCyvEfTEH0tUh9MFv6-bcISK6BMHIAAFG4'

[Endpoint]/services/user/geocode?authpn=[authpn]&authpt=[authpt]&device_type=[device_type]&device_model=[device_model]&device_category=[device_category]&device_manufacturer=[device_manufacturer]&device_id=[device_id]&region=[region]&longitud=[longitud]&latitud=[latitud]

microfwk-web-test.clarovideo.net

curl --location --request GET 'http://microfwk-web-test.clarovideo.net/services/user/geocode?authpn=accedo&authpt=11s4e5l6a381e&region=brasil&format=json&device_type=generic&device_model=web&device_manufacturer=generic&device_category=web&api_version=v5.90&longitud=-46.3322&latitud=-23.9618' \
--header 'Cookie: PHPSESSID=uupkfg7jdqnv7714r8e2344fu0'

https://mfwkrestonx1-api.clarovideo.net/services/epg/version?format=json&api_version=5.86&authpn=net&authpt=5facd9d23d05bb83&device_category=tv&device_manufacturer=sony&device_model=generic&device_type=generic&region=brasil&subregion=sp_santos&tenant_code=netnow

http://mfwktvnx1-uat-api.clarovideo.net/services/epg/version?HKS=secmvkpye03q5portb2uee6lnn&api_version=v5.86&authpn=net&authpt=5facd9d23d05bb83&device_category=web&device_manufacturer=windows&device_model=html5&device_type=html5&device_id=abf95cc1-7acc-18bc-7b3e-dcac31586696&device_name=Chrome&device_so=Chrome&region=Brasil&format=json&subregion=default


*/
