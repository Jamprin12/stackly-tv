import AbstractRequestTask from "../AbstractRequestTask";

class GeocodeTask extends AbstractRequestTask {
  constructor(longitud,latitud) {
    super();
    this.longitud=longitud;
    this.latitud=latitud;
  }

  getHeaders() {
    return {};
  }

  // getMethod() {
  //   return "POST";
  // }

  getParams() {
    const params = super.getParams();
    params.longitud=this.longitud;
    params.latitud=this.latitud;
    // console.log('[LL] -- getParams -- ',params);
    return params;
  }

  getUrl() {
    // const http = "https://";
    // const url = Launcher.get("akamai_mfwk");
    // return `${http}${url}/services/user/geocode`;
    return `http://microfwk-web-test.clarovideo.net/services/user/geocode`;
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default GeocodeTask;

/*
curl --location --request POST 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCyvEfTEH0tUh9MFv6-bcISK6BMHIAAFG4'

[Endpoint]/services/user/geocode?authpn=[authpn]&authpt=[authpt]&device_type=[device_type]&device_model=[device_model]&device_category=[device_category]&device_manufacturer=[device_manufacturer]&device_id=[device_id]&region=[region]&longitud=[longitud]&latitud=[latitud]

microfwk-web-test.clarovideo.net

curl --location --request GET 'http://microfwk-web-test.clarovideo.net/services/user/geocode?authpn=accedo&authpt=11s4e5l6a381e&region=brasil&format=json&device_type=generic&device_model=web&device_manufacturer=generic&device_category=web&api_version=v5.90&longitud=-46.3322&latitud=-23.9618' \
--header 'Cookie: PHPSESSID=uupkfg7jdqnv7714r8e2344fu0'
*/
