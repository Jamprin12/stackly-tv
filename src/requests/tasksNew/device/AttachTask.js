import AbstractRequestTask from "../AbstractRequestTask";
import { getApiEndpoint } from "../../../config/appConfig";

class AttachTask extends AbstractRequestTask {
  constructor(groupId = "", css = 0) {
    super();
    this.groupId = groupId;
    this.css = css;
  }

  /*
   * This service must be call just with logged users
   */
  getUrl() {
    const protocol = "https://";
    const url = getApiEndpoint();
    return `${protocol}${url}/services/device/attach`;
  }

  getParams() {
    const params = super.getParams();
    const otherParams = {
      css: this.css,
      group_id: this.groupId,
    };

    return { ...params, ...otherParams };
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default AttachTask;
