import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";
import ZUPUtils from "../../../utils/ZUPUtils";
import getAppConfig from "../../../config/appConfig";
import DeviceStorage from "../../../components/DeviceStorage/DeviceStorage";

class LiveChannelsTask extends AbstractRequestTask {
  constructor(userId) {
    super();
    this.userId = userId;
  }

  forceHeaders() {
    return true;
  }

  getHeaders() {
    const { platform } = getAppConfig();

    const headers = {
      platform,
      Authorization: ZUPUtils.getAMCOBearer(),
    };

    return headers;
  }

  getParams() {
    const params = super.getParams();
    const epg = DeviceStorage.getItem("geo-epg");
    const geoEpg = JSON.parse(epg);

    return {
      ...params,
      api_version: "v2.0",
      authpn: "net",
      filter_id: geoEpg,
      authpt: "5facd9d23d05bb83",
    };
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();
    return `${http}${url}/services/payway/paymentservice/livechannels`;
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default LiveChannelsTask;
