import AbstractRequestTask from "../AbstractRequestTask";

import ZUPUtils from "../../../utils/ZUPUtils";
import ZUPConfig from "../../../config/ZUPConfig";
// import getAppConfig from "../../../config/appConfig";
// import storage from "../../../components/DeviceStorage/DeviceStorage";

class GetOffersZupTask extends AbstractRequestTask {
  constructor(params) {
    super();
    this.groupId = params.groupId;
    this.seasonId = params.seasonId;
    console.log("[PURCHASE] -- GetOffersZupTask -- constructor");
  }

  getMethod() {
    return "GET";
  }

  getUrl() {
    // const appConfig = getAppConfig();
    let url = ZUPConfig.getUrl("offers"); // url desde config de zup
    // url += `?api_version=v1.0`; //&content_id=${this.groupId} &authpn=${appConfig["authpn"]}&authpt=${appConfig["authpt"]}   &user_token=${ZUPUtils.getAMCOUserToken()}&platform=Netflex`;
    console.log("[PURCHASE][GetOffersZupTask] -- getUrl -- url=", url);
    // const http = "https://";
    // const url = Launcher.get("akamai_mfwk");
    // return `${http}${url}/services/payway/purchasebuttoninfo`;
    return url;
  }

  forceHeaders() {
    return true;
  }

  getHeaders() {
    // let headers = {};
    // headers["Authorization"] = ZUPUtils.getAMCOBearer();
    // headers["platform"] = 'Netflex';
    let headers = {
      platform: "Netflex",
      Authorization: ZUPUtils.getAMCOBearer(),
    };
    console.log(
      "[PURCHASE][GetOffersZupTask] -- getHeaders -- headers=",
      headers
    );
    return headers;
  }

  getParams() {
    const params = super.getParams();
    return {
      ...params,
      api_version: "v1.0",
      content_id: this.groupId,
      // season_id: this.seasonId,
      // user_token: ZUPUtils.getAMCOUserToken(),
      // platform: 'Netflex',
    };
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default GetOffersZupTask;
