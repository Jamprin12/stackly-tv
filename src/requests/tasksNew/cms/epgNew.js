import AbstractRequestTask from "../AbstractRequestTask";
import getAppConfig from "../../../config/appConfig";
import DeviceStorage from "../../../components/DeviceStorage/DeviceStorage";

class LevelUserTask extends AbstractRequestTask {
  constructor(uri, params = {}) {
    super();

    this.uri = uri;
    this.params = params;
  }

  getHeaders() {
    return {};
  }

  getUrl() {
    return getAppConfig().end_point_middleware + this.uri;
  }

  getParams() {
    let params = super.getParams();
    const epg = DeviceStorage.getItem("geo-epg");
    const geoEpg = JSON.parse(epg);
    this.params.filter_id = geoEpg;

    return Object.assign({}, params, this.params);
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default LevelUserTask;
