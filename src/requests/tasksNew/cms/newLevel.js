import moment from "moment";

import Device from "../../../devices/device";
import AbstractRequestTask from "../AbstractRequestTask";
import getAppConfig from "../../../config/appConfig";
import DeviceStorage from "../../../components/DeviceStorage/DeviceStorage";

class LevelUserTask extends AbstractRequestTask {
  constructor(uri, params = {}) {
    super();

    this.uri = uri;
    this.params = params;
  }

  getHeaders() {
    return {};
  }

  getUrl() {
    return getAppConfig().end_point_middleware + this.uri;
  }

  getParams() {
    let params = super.getParams();

    const epg = DeviceStorage.getItem("geo-epg");
    const geoEpg = JSON.parse(epg);

    const appKey = Device.getDevice().getConfig().appKey;
    const region = DeviceStorage.getItem("region");
    const sessionKey = `${appKey}-${region}`;

    return {
      ...params,
      ...this.params,
      sessionKey,
      app_now: moment().format("YYYYMMDDHHmmss"),
      filter_id: geoEpg,
      tenant_code: getAppConfig().tenant_code,
      api_version: "v6",
    };
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default LevelUserTask;
