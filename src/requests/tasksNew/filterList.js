import storage from "../../components/DeviceStorage/DeviceStorage";

const getAPAFilterList = () => {
  let filters = {
    mexico: {
      filterlist: "34429,34263,34450,34451,34469,35706,35707,32118",
    },
    argentina: { filterlist: "34429,34451" },
    chile: { filterlist: "34429,34450,34451,35706,32120" },
    colombia: { filterlist: "34429,34263,34450,34451,35706,32211" },
    guatemala: { filterlist: "34429,34450,34451,35706,32133" },
    honduras: { filterlist: "34429,34451,35706,32214" },
    peru: { filterlist: "34429,34263,34450,34451,35706,32191" },
    elsalvador: { filterlist: "34429,34450,34451,35706,32134" },
    dominicana: { filterlist: "34429,34451,32121" },
    nicaragua: { filterlist: "34429,34451,35706,32213" },
    costarica: { filterlist: "34429,34451,32023" },
    ecuador: { filterlist: "34429,34450,34451,35706,32119" },
    uruguay: { filterlist: "34429,34451,32022" },
    paraguay: { filterlist: "34429,34451,32212" },
    panama: { filterlist: "34429,34451,35706,32190" },
    brasil: { filterlist: "37937" },
    puertorico: { filterlist: "34429" },
    demoeng: { filterlist: "34429" },
  };

  let region = storage.getItem("region");

  try {
    if (filters) {
      filters = filters[region] || filters["default"];
    }

    if (filters && filters.filterlist) {
      return filters.filterlist;
    }
  } catch (e) {
    return null;
  }

  return null;
};

export default getAPAFilterList;
