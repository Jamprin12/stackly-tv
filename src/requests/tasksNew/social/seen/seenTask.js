import { getApiEndpoint } from "../../../../config/appConfig";
import AbstractRequestTask from "../../AbstractRequestTask";

class userTask extends AbstractRequestTask {
  constructor(params) {
    super();
    this.user_hash = params.user_hash;
  }

  getHeaders() {
    return {};
  }

  getParams() {
    const params = super.getParams();
    return Object.assign({}, params, { user_hash: this.user_hash });
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();

    return `${http}${url}/services/user/seen`;
  }

  success(data, b) {
    if (data.status == true) this.resolve(data);
    else this.reject(data);
  }
}

export default userTask;
