/**
 * Canal abierto
 * Manejo de datos de Canal Abierto
 */
import RequestManager from "../../RequestManager";
import moment from "moment";
import get from "lodash/get";
import storage from "../../../components/DeviceStorage/DeviceStorage";

import CanalAbiertoTask from "./CanalAbiertoTask";
import { has } from "immutable";

const lHead = "[CA] : ";
const OCCINTERVAL = "OCC_interval";
const OCCDATA = "OCC_data";
const OCCSEEN = "OCC_seen";
const OCCTTL = 60 * 60 * 1000; // in miliseconds

//
// SERVICIO
//
export function installOpenChannelCheck() {
  const fHead = lHead + "installOpenChannelCheck -- ";
  checkOpenChannelAPI(); // se dispara al inicio
  storage.setItem(OCCINTERVAL, setInterval(checkOpenChannelAPI, OCCTTL));
  console.log(fHead + "Installed!");
}
export function uninstallOpenChannelCheck() {
  clearInterval(storage.getItem(OCCINTERVAL));
}

//
// STORAGE
//
function saveMessagesToStorage(data) {
  storage.setItem(OCCDATA, JSON.stringify(data));
}
function loadMessagesFromStorage() {
  // const fHead = lHead + "loadMessagesFromStorage -- ";
  let data = storage.getItem(OCCDATA);
  data = data ? JSON.parse(data) : [];
  data = data.map((m) => {
    // codigos de ao-vivo y nodo
    m.codeLive = get(m, "code_live.0.bss_code");
    m.codeVod = get(m, "code_vod.0.node_name");

    // transforma fechas a moment
    m.start_date_formated = moment(m.end_date_formated);
    m.end_date_formated = moment(m.end_date_formated);
    return m;
  });
  // console.log(fHead + "data=", data);
  return data;
}
function saveSeenToStorage(data) {
  // const fHead = lHead + "saveSeenToStorage -- ";
  // console.log(fHead + "data", data, JSON.stringify(data));
  storage.setItem(OCCSEEN, JSON.stringify(data));
}
function loadSeenFromStorage() {
  // const fHead = lHead + "loadSeenFromStorage -- ";
  let data = storage.getItem(OCCSEEN);
  data = data ? JSON.parse(data) : [];
  return data;
}

//
// SEEN
//
function addSeenMessage(id) {
  // const fHead = lHead + "addSeenMessage -- ";
  let seen = loadSeenFromStorage();
  if (!hasBeenSeenMessage(id)) {
    seen.push({ id: id, when: moment() });
    saveSeenToStorage(seen);
  }
  return true;
}
function clearSeenMessage() {
  saveSeenToStorage([]);
}
function hasBeenSeenMessage(id) {
  // const fHead = lHead + "hasBeenSeenMessage -- ";
  let seen = loadSeenFromStorage();
  seen = seen.filter((m) => m.id == id);
  // console.log(fHead + "seen", id, "cant", seen);
  return seen.length > 0;
}
function purgeSeenMessages() {
  const fHead = lHead + "purgeSeenMessages -- ";
  const msgs = loadMessagesFromStorage();
  let seen = loadSeenFromStorage();
  seen = seen.filter((s) => msgs.filter((m) => m.id == s.id).length > 0);
  console.log(fHead + "seen before", loadSeenFromStorage(), "after", seen);
  saveSeenToStorage(seen);
}

//
// API
//
async function checkOpenChannelAPI() {
  const fHead = lHead + "checkCanalAbierto -- ";
  console.log(fHead + "TIC!");
  return new Promise((resolve, reject) => {
    const canalAbiertoTask = new CanalAbiertoTask();
    RequestManager.addRequest(canalAbiertoTask)
      .then((result) => {
        const data = get(result, "response.result.openstreams", []);
        console.log(fHead + "checkOpenChannelAPI -- TAC! -- GOT DATA!", [...data]); // para debug // muestro la copia original!!
        for (var i in data) {
          // sacamos los no activos!
          if (!isActive(data[i].start_date, data[i].end_date)) {
            data.splice(i, 1);
          } else {
            data[i]["start_date_formated"] = unixTimeStampToMoment(data[i]["start_date"]);
            data[i]["end_date_formated"] = unixTimeStampToMoment(data[i]["end_date"]);
            //
            //
            // clean up el seen
            //
          }
        }
        // data[0].code_live=[{bss_code:"NX_LIVE_Band_HD"}]; // para debug
        // data[0].code_vod=[{bss_code:"NX_SVOD_FOX"}]; // para debug
        console.log(fHead + "checkOpenChannelAPI -- TAC! -- SAVING DATA!", data); // para debug
        saveMessagesToStorage(data);
        purgeSeenMessages();

        console.log(fHead + "TEST shouldShowOpenChannelMessage?", shouldShowOpenChannelMessage()); // para debug
        console.log(fHead + "TEST getOpenChannelData?", getOpenChannelData()); // para debug
        console.log(fHead, "-------------------------");
        console.log(fHead + "TEST clearSeenMessage", clearSeenMessage()); // para debug
        // console.log(fHead + "TEST getOpenChannelData?", getOpenChannelData()); // para debug
        // console.log(fHead, "-------------------------");
        // console.log(fHead + "TEST messageShown [d04156a3-f02c-4362-9976-2ad54df39d57]", messageWasShown("d04156a3-f02c-4362-9976-2ad54df39d57")); // para debug
        // console.log(fHead + "TEST hasBeenSeenMessage? [d04156a3-f02c-4362-9976-2ad54df39d57]", hasBeenSeenMessage("d04156a3-f02c-4362-9976-2ad54df39d57")); // para debug
        // console.log(fHead + "TEST getOpenChannelData?", getOpenChannelData()); // para debug
        // console.log(fHead, "-------------------------");
        // console.log(fHead + "TEST allMessagesWereShown", allMessagesWereShown()); // para debug
        // console.log(fHead + "TEST shouldShowOpenChannelMessage?", shouldShowOpenChannelMessage()); // para debug
        // console.log(fHead + "TEST getOpenChannelData?", getOpenChannelData()); // para debug
        console.log(fHead + "-------------------------");
        console.log(fHead + "-tags--------------------");
        console.log(fHead + "-------------------------");
        console.log(fHead + "TEST isThisOpenChannel? [NX_LIVE_Band_HD]", isThisOpenChannel("NX_LIVE_Band_HD")); // para debug
        console.log(fHead + "TEST isThisOpenChannel? [NX_SVOD_FOX]", isThisOpenChannel("NX_SVOD_FOX")); // para debug
        console.log(fHead + "TEST isThisOpenChannel? [nx_fox]", isThisOpenChannel("nx_fox")); // para debug
        // console.log(fHead + "TEST messageShown [pepe]", messageWasShown("pepe")); // para debug
      })
      .catch((error) => {
        console.log(fHead + "ERROR", error);
        saveMessagesToStorage([]);
      });
  });
}

//
// UTILS
//
function unixTimeStampToMoment(uts) {
  const fHead = lHead + "unixTimeStampToDate -- ";
  let result = moment(uts * 1000);
  // console.log(fHead + " salida=", result);
  return result;
}
function isActive(dateFrom, dateTo) {
  const fHead = lHead + "isActive -- ";
  const result = moment().isBetween(moment(dateFrom * 1000), moment(dateTo * 1000), null, "[)");
  // console.log( fHead + " result=", moment().format(), result, moment(dateFrom * 1000).format(), moment(dateTo * 1000).format() );
  return result;
}
function getBssCodes(item) {
  // const fHead = lHead + "getBssCodes -- ";
  const code_live = get(item, "code_live").map((c) => get(c, "bss_code"));
  const code_vod = get(item, "code_vod").map((c) => get(c, "node_name"));
  // console.log(fHead + "code_live", code_live, "code_vod", code_vod);
  return code_live.concat(code_vod);
}
function hasBssCode(item, code) {
  // const fHead = lHead + "hasBssCode -- ";
  const bssCodes = getBssCodes(item);
  // console.log(fHead + "codes", bssCodes, bssCodes.includes(code));
  return bssCodes, bssCodes.includes(code);
}

// *********************************************************
// EXPORTS
// *********************************************************
//
// MENSAJES
/**
 * hay mensajes activos?
 */
export function shouldShowOpenChannelMessage() {
  // const fHead = lHead + "shouldShowOpenChannelMessage -- ";
  let cant = getOpenChannelData().length > 0;
  // console.log(fHead + "cantidad", cant);
  return cant;
}
/**
 * Levanta los mensajes a mostrar
 */
export function getOpenChannelData() {
  let msgs = loadMessagesFromStorage();
  msgs = msgs.filter((m) => !hasBeenSeenMessage(m.id)); // los que no se vieron
  return msgs;
}
/**
 * Tilda un mensaje como visto
 * @param {id} id del mensaje a tildar
 */
export function messageWasShown(id) {
  // const fHead = lHead + "messageShown -- ";
  // console.log(fHead + "mostre el mensaje", id, loadSeenFromStorage());
  return addSeenMessage(id);
}
/**
 * Tilda todos los mensajes existentes como vistos
 */
export function allMessagesWereShown() {
  let msgs = loadMessagesFromStorage();
  msgs.forEach((m) => messageWasShown(m.id)); // los que no se vieron
}

/**
 * Levanta todos los mensajes, incluso los que fueron vistos
 */
export function getAllOpenChannelData() {
  return loadMessagesFromStorage();
}
/**
 * Mi codigo es de canal abierto?
 */
export function isThisOpenChannel(code) {
  // const fHead = lHead + "isThisOpenChannel -- ";
  const msgs = loadMessagesFromStorage();
  const exists = msgs.filter((m) => hasBssCode(m, code));
  // console.log(fHead + "exists", exists);
  return exists.length > 0;
}
