import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";
import Device from "../../../devices/device";
// Sending info to the dashboard
import TrackerManager from "../../../utils/trackers/TrackerManager";

import ZUPUtils from "../../../utils/ZUPUtils";

const API_VERSION = "v5.87"; // "v5.85";

class PlayerGetMediaTask extends AbstractRequestTask {
  constructor(
    groupId = "",
    contentId = "",
    isTrailer = false,
    stream_type = "",
    startTime = "",
    endTime = "",
    timeshiftToken = "",
    showErrorModal
  ) {
    super();
    console.log("Veamos media1 ", groupId, timeshiftToken);

    let device_config = Device.getDevice().getConfig();
    this.player = Device.getDevice().getPlayer();
    this.groupId = groupId;
    this.contentId = contentId;
    this.isTrailer = isTrailer;
    this.stream_type = stream_type
      ? stream_type
      : device_config && device_config.stream_type
      ? device_config.stream_type
      : "smooth_streaming";
    console.log(
      "[PGM] --- PlayerGetMediaTask | constructor | this.stream_type -- 1 ",
      this.stream_type
    );

    this.startTime = startTime ? this.getClaroUnixEpochTime(startTime) : "";
    this.endTime = endTime ? this.getClaroUnixEpochTime(endTime) : "";
    this.timeshiftToken = timeshiftToken ? timeshiftToken : "";
    this.showErrorModal = showErrorModal;
    console.log("Veamos media2 ", groupId, this.timeshiftToken);
  }

  /**
   * Convierte un string con formato YYYY/MM/DD hh:mm:ss en Formato Unix Epoch UTC 64 bits para usar timeshift
   * @param {string} stringDate
   */
  getClaroUnixEpochTime(stringDate = 'YYYY/MM/DD hh:mm:ss') {
    const date = new Date(stringDate);
    const epoch = date.getTime() / 1000.0;
    const result = `${Math.round(epoch)}`;

    //return '15126610420000000'; // TIMESHIFTTESTING
    return result.length === 10 ? `${result}0000000` : result;
  }



  /* getTrackerManager() {
    const dashboardTracker = new DashboardTracker();

    return new TrackerManager([ dashboardTracker ]);
  }*/

  getEncType() {
    if (this.timeshiftToken) {
      return "application/x-www-form-urlencoded";
    } else {
      return super.getEncType();
    }
  }

  getShowErrorModal() {
    return this.showErrorModal;
  }

  getRetriesNumber() {
    if (this.showErrorModal) {
      return 2;
    } else {
      return 4;
    }
  }

  getShowModal() {
    if (this.timeshiftToken) {
      return false;
    } else {
      return super.getShowModal();
    }
  }

  // GOOSE -- HACK -- para pasar por header, pero no lo aceptan
  // getHeaders() {
  //   const headers = {
  //     'Content-Type': 'application/json',
  //     'Accept': 'application/json',
  //     'Authorization': ZupUtils.getAMCOBearer()
  //   };
  //   console.log('[PURCHASE] -- getHeaders',headers)
  //   return headers;
  // }

  // getUrl() {
  //   const http = "https://";
  //   let url = Launcher.get("akamai_mfwk");
  //   if(this.timeshiftToken) {
  //     let _url = `${http}${url}/services/player/getmedia?`;
  //     let p = this.getGETParams();
  //     for(let i in p) {
  //       _url += `${i}=${p[i]}&`;
  //     }
  //     return _url;
  //   }
  //   else {
  //     return `${http}${url}/services/player/getmedia`;
  //   }
  // }
  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();
    console.log(
      "[PURCHASE] -- getMedia:getUrl -- timeshiftToken",
      this.timeshiftToken
    );
    if (this.timeshiftToken) {
      let _url = `${http}${url}/services/player/getmedia?`;
      let p = this.getGETParams();
      for (let i in p) {
        if (i === "api_version") {
          _url += `${i}=${API_VERSION}&`;
        } else {
          _url += `${i}=${p[i]}&`;
        }
      }
      _url += "&user_token=" + ZUPUtils.getAMCOUserToken();
      console.log("[PURCHASE] -- getMedia:getUrl -- ", _url);
      return _url;
    } else {
      let _url = `${http}${url}/services/player/getmedia?`;
      _url += "&user_token=" + ZUPUtils.getAMCOUserToken();
      return _url;
      // return `${http}${url}/services/player/getmedia`;
    }
  }

  getMethod() {
    if (this.timeshiftToken) {
      return "POST";
    } else {
      return "GET";
    }
  }

  getParams() {
    // var par = []
    console.log("[PAYWAYTOKEN] -- timeshiftToken=", this.timeshiftToken);
    if (this.timeshiftToken) {
      let par = { payway_token: this.timeshiftToken };

      // HACK JWT
      par.payway_token = ZUPUtils.changeJWT(par.payway_token);
      console.log("[PAYWAYTOKEN] -- resultado=", par.payway_token);

      // // Se guarda en localStorage
      //localStorage.setItem('payway_token', par.payway_token);
      // console.log('test_localstorage', dell);
      return par;
    } else {
      // par = localStorage.getItem('payway_token', par);
      return this.getGETParams();
    }
    // HACK GOOSE -- auth pn pt
    // par.authpn = 'webclient'
    // par.authpt = 'tfg1h3j4k6fd7'
    // console.log('par hack', par);
    // return par;
  }

  getGETParams() {
    const params = super.getParams();
    let options = {
      ...params,
      group_id: this.groupId,
      content_id: this.contentId,
      stream_type: this.stream_type,
      preview: this.isTrailer ? "1" : "0",
      startTime: this.startTime,
      endTime: this.endTime,
      api_version: window.location.href.includes("/node/tv")
        ? "5.86"
        : API_VERSION,
    };
    // se saca este if para que no levante dash en LG Webos
    // if(options.stream_type!=='dashwv'){
    //   options.stream_type="dashwv";
    // }
    console.log(
      "[PURCHASE] -- playergetmedia -- options" + options.api_version
    );
    // console.trace()
    return options;
  }
  success(data, b) {
    this.resolve(data);
  }

  isValid(data) {
    console.log(
      "[GOO] -- src/requests/tasks/player/PlayerGetMediaTask.js -- isValid 1",
      data
    );
    TrackerManager.setup(data);
    data.url = this.getUrl();
    TrackerManager.playerGetMedia(data);

    //TODO Partial fix for SAMSUNG HIBRIDAS, WE NEED TO IMPROVE move after PAU fix
    console.log(
      "[GOO] -- src/requests/tasks/player/PlayerGetMediaTask.js -- isValid 2",
      data
    );
    if (
      data &&
      data.response &&
      data.response.media &&
      data.response.media.server_url
    ) {
      if (
        Device.getDevice().getPlatform() === "samsung" &&
        !this.isHBO(data) &&
        !this.isFOXV3(data)
      ) {
        data.response.media.server_url = data.response.media.server_url.replace(
          "https:",
          "http:"
        );
      }
    }
    //TODO Partial fix for SAMSUNG HIBRIDAS, WE NEED TO IMPROVE move after PAU fix
    return true;
  }

  isHBO(data) {
    if (
      data.response &&
      data.response.group &&
      data.response.group.common &&
      data.response.group.common.extendedcommon &&
      data.response.group.common.extendedcommon.media &&
      data.response.group.common.extendedcommon.media.proveedor &&
      data.response.group.common.extendedcommon.media.proveedor.nombre
    ) {
      return (
        data.response.group.common.extendedcommon.media.proveedor.nombre ===
        "HBO"
      );
    }
    return false;
  }

  isFOXV3(data) {
    if (
      data.response &&
      data.response.group &&
      data.response.group.common &&
      data.response.group.common.extendedcommon &&
      data.response.group.common.extendedcommon.media &&
      data.response.group.common.extendedcommon.media.proveedor &&
      data.response.group.common.extendedcommon.media.proveedor.codigo
    ) {
      return (
        data.response.group.common.extendedcommon.media.proveedor.codigo ===
        "foxv3"
      );
    }
    return false;
  }

  getHeaders() {
    if (!this.timeshiftToken) {
      return {
        "Access-Control-Allow-Origin": "*",
      };
    } else {
      return {
        "Content-Type": "application/x-www-form-urlencoded",
      };
    }
  }
}

export default PlayerGetMediaTask;
