import AbstractRequestTask from "../AbstractRequestTask";
import { getApiEndpoint } from "../../../config/appConfig";

class LogoutTask extends AbstractRequestTask {
  constructor(params = {}) {
    super();
    this.ownParams = params;
  }

  getParams() {
    const params = super.getParams();
    return Object.assign({}, params, this.ownParams);
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();
    return `${http}${url}/services/user/logout`;
  }
}

export default LogoutTask;
