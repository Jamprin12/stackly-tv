import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";

class UserFavoriteSerieTask extends AbstractRequestTask {
  constructor(parameters) {
    super();
    this.customParameters = parameters;
  }

  getHeaders() {
    return {};
  }

  getParams() {
    const params = super.getParams();
    console.log(params);
    return Object.assign({}, params, this.customParameters);
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();

    return `${http}${url}/services/user/favorite/listserie`;
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default UserFavoriteSerieTask;
