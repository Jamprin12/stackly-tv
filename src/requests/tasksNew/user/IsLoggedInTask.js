import AbstractRequestTask from "../AbstractRequestTask";
import { getApiEndpoint } from "../../../config/appConfig";

class IsLoggedIn extends AbstractRequestTask {
  constructor(includpaywayprofile = false) {
    super();
    this._includpaywayprofile = includpaywayprofile;
  }

  getHeaders() {
    return {};
  }

  getParams() {
    const params = super.getParams();
    const includpaywayprofile = this._includpaywayprofile;
    return Object.assign({}, params, { includpaywayprofile });
  }

  getUrl() {
    const http = "http://";
    const url = getApiEndpoint();
    return `${http}${url}/services/user/isloggedin`;
  }

  isValid(data) {
    if (!data.response || data.response.is_user_logged_in === undefined) {
      return false;
    }
    return true;
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default IsLoggedIn;
