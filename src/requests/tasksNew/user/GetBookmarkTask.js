import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";
import storage from "../../../components/DeviceStorage/DeviceStorage";
import filterList from "../filterList";

class GetBookmark extends AbstractRequestTask {
  constructor(group_id = "", user) {
    super();
    this._group_id = group_id;
    this.user = user;
  }

  getHeaders() {
    return {};
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();

    return `${http}${url}/services/user/getbookmark`;
  }

  getParams() {
    const params = super.getParams();
    const user_hash = storage.getItem("user_hash") || "";

    return {
      ...params,
      lasttouch: Math.random(),
      group_id: this._group_id,
      user_hash,
      filter_list: filterList(),
    };
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default GetBookmark;
