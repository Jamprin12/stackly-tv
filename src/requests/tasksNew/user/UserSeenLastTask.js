import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";
import DeviceStorage from "../../../components/DeviceStorage/DeviceStorage";

class UserSeenLastTask extends AbstractRequestTask {
  constructor(group_id = "", user) {
    super();
    this.group_id = group_id;
    this.user = user;
  }

  getHeaders() {
    return {};
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();

    // return `${http}${url}/services/user/seenlast`;
    return `${http}${url}/services/user/seencurrentepisode`;
  }

  getParams() {
    const params = super.getParams();
    const group_id = this.group_id;
    const user_hash = DeviceStorage.getItem("user_hash");

    return {
      ...params,
      lasttouch: Math.random(),
      group_id,
      user_hash,
    };
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default UserSeenLastTask;
