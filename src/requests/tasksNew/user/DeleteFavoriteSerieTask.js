import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";
import storage from "../../../components/DeviceStorage/DeviceStorage";

class DeleteFavoriteSerie extends AbstractRequestTask {
  constructor(object_id = "") {
    super();
    this._object_id = object_id;
  }

  getHeaders() {
    return {};
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();

    return `${http}${url}/services/user/favorite/deleteserie`;
  }

  getParams() {
    const params = super.getParams();
    const api_version = "5.91";
    const serie_id = this._object_id;
    const user_hash = storage.getItem("user_hash") || "";

    const final_params = Object.assign({}, params, {
      api_version,
      user_hash,
      serie_id,
    });
    console.log("[FAVORITE] DEL params", final_params);
    return final_params;
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default DeleteFavoriteSerie;
