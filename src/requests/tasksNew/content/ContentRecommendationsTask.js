﻿import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";
import filterList from "../filterList";

class ContentRecommendations extends AbstractRequestTask {
  constructor(groupId = {}, quantity = 10) {
    super();

    this.groupId = groupId;
    this.quantity = quantity;
  }

  getHeaders() {
    return {};
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();
    return `${http}${url}/services/content/recommendations`;
  }

  getParams() {
    const params = super.getParams();
    const newParams = {
      group_id: this.groupId,
      quantity: this.quantity,
      order_id: "ASC",
      order_way: "ASC",
      filterlist: filterList(),
    };

    return { ...params, ...newParams };
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default ContentRecommendations;
