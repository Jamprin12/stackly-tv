import AbstractRequestTask from "../AbstractRequestTask";
import getAppConfig from "../../../config/appConfig";

class ContentSerieTask extends AbstractRequestTask {
  constructor(groupId = "") {
    super();

    this.groupId = groupId;
  }

  getUrl() {
    return getAppConfig().end_point_middleware + "serie";
  }

  getParams() {
    const params = super.getParams();
    return {
      serie_id: this.groupId,
      ...params,
      tenant_code: "netnow",
      api_version: "v6",
    };
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default ContentSerieTask;
