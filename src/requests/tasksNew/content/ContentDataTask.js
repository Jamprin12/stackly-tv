import { getApiEndpoint } from "../../../config/appConfig";
import AbstractRequestTask from "../AbstractRequestTask";

class ContentDataTask extends AbstractRequestTask {
  constructor(groupId = "") {
    super();

    this.groupId = groupId;
  }

  getUrl() {
    const http = "https://";
    const url = getApiEndpoint();
    return `${http}${url}/services/content/data`;
  }

  getParams() {
    const params = super.getParams();
    return {
      group_id: this.groupId,
      ...params,
      tenant_code: "netnow",
      api_version: "v6",
    };
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default ContentDataTask;
