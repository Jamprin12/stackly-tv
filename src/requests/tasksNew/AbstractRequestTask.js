// import htmlDecode from '../../utils/htmlDecode';
import getAppConfig from "../../config/appConfig";
import storage from "../../components/DeviceStorage/DeviceStorage";
import Log from "../../utils/Log";

class AbstractRequestTask {
  success(data, b) {
    if (data.errors || !data.response) {
      return false;
    }

    return true;
  }

  isValid(data) {
    if (data.errors || !data.response || data.response === "error") {
      return false;
    }

    return true;
  }

  getResponseType() {
    return "";
  }

  replaceProtocol(url) {
    let newUrl = Object.assign(url);

    const signalKey = newUrl.indexOf("signalKey") === -1;
    const social = newUrl.indexOf("www.clarovideo.com") === -1;
    const cert = newUrl.indexOf("getcertificate") === -1;
    if (
      window.location &&
      window.location.protocol &&
      signalKey &&
      social &&
      cert
    ) {
      newUrl = newUrl.replace("https:", window.location.protocol);
      newUrl = newUrl.replace("http:", window.location.protocol);
    }
    return newUrl;
  }

  getShowErrorModal() {
    return false;
  }

  error(data, b) {
    const newError = this.parseErrors(data);
    this.reject(newError, b);
  }

  parseErrors(err) {
    // if (typeof window.newrelic === 'object') {
    //     console.log('Newrelic Error parseErrors', err);
    //     window.newrelic.noticeError(err);
    // }
    // GOOSE -- LOG
    Log.error(err, "[AbstractRequestTask][parseErrors]", {
      url: this.getUrl(),
      params: this.getParams(),
    });

    let message;
    let code = null;

    if (err.errors && err.errors["0"]) {
      code = err.errors["0"].code;
      message = err.errors["0"].message;
    } else if (err.errors) {
      if (err.errors.msg) {
        message = err.errors.msg;
      } else if (Array.isArray(err.errors.error)) {
        message = err.errors.error[0];
      } else {
        message = err.errors.error;
      }
      if (err.errors.code) code = err.errors.code;
    } else if (err.status == false) {
      message = err.message;
      code = err.code;
    } else {
      message = err.msg || null;
    }

    return {
      // message: htmlDecode(message),
      message: message,
      completeError: err,
      code,
    };
  }

  /*
        Para métodos POST, tipos:
        1. application/x-www-form-urlencoded - DEFAULT
        2. text/plain - TODO see @ src/requests/Ajax.js
        3. multipart/form-data
    */
  getEncType() {
    return "application/x-www-form-urlencoded";
  }

  getShowModal() {
    return true;
  }

  getRetriesNumber() {
    return 4;
  }

  getParams() {
    const config = getAppConfig();
    const HKS = storage.getItem("HKS") || "";
    const region = storage.getItem("region");

    let params = {
      HKS: HKS,

      api_version: config.api_version,
      authpn: config.authpn,
      authpt: config.authpt,

      device_category: config.device_category,
      device_manufacturer: config.device_manufacturer,
      device_model: config.device_model,
      device_type: config.device_type,

      device_id: config.device_id,
      device_name: config.device_name,
      device_so: config.device_so,

      region: region,
      format: config.format,
    };

    if (config.default_headers_enabled) {
      params.crDomain = encodeURI(
        document.location.protocol +
          "//" +
          document.location.hostname +
          ":" +
          document.location.port
      );
    }

    return params;
  }

  getMethod() {
    return "GET";
  }

  getUrl() {
    return this.url;
  }

  getHeaders() {
    const config = getAppConfig();

    if (
      config.default_headers_enabled &&
      this.getUrl().indexOf("webapi") === -1
    ) {
      console.log("******************* [HEADERS]");
      console.log(
        "******************* [HEADERS] -- url=",
        this.getUrl(),
        this.getUrl().indexOf("webapi")
      );
      console.log(
        "******************* [HEADERS] !! config.default_menu_node",
        config.default_headers
      );
      console.log("******************* [HEADERS]");
      return config.default_headers;
    }
    // return {
    //     'Access-Control-Allow-Origin': '*'
    // };
  }

  /**
   * Enviar forzosamente los headers
   * Para los casos en que hay headers para GET
   */
  forceHeaders() {
    const config = getAppConfig();
    return config.default_headers_enabled;
    // return true;
  }
}

export default AbstractRequestTask;
