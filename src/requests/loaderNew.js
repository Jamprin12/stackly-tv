import moment from "moment";
import get from "lodash/get";

import RequestManager from "./RequestManager";

import GetBookmarkTask from "./tasksNew/user/GetBookmarkTask";
import UserFavoriteTask from "./tasksNew/user/UserFavoriteTask";
import UserFavoriteSerieTask from "./tasksNew/user/UserFavoriteSerieTask";
import AddFavoriteTask from "./tasksNew/user/AddFavoriteTask";
import AddFavoriteSerieTask from "./tasksNew/user/AddFavoriteSerieTask";
import DeleteFavoriteTask from "./tasksNew/user/DeleteFavoriteTask";
import DeleteFavoriteSerieTask from "./tasksNew/user/DeleteFavoriteSerieTask";
import UserSeenLastTask from "./tasksNew/user/UserSeenLastTask";
import IsLoggedInTask from "./tasksNew/user/IsLoggedInTask";
import LogoutTask from "./tasksNew/user/LogoutTask";
import StartHeaderInfoTask from "./tasksNew/user/StartHeaderInfoTask";

import predictiveSearchTask from "./tasksNew/search/predictiveSearchTask";
import Vertical from "./tasksNew/search/VerticalTask";

import ContentSerieTask from "./tasksNew/content/ContentSerieTask";
import ContentDataTask from "./tasksNew/content/ContentDataTask";
import ContentRecommendationsTask from "./tasksNew/content/ContentRecommendationsTask";

import GetOffersZupTask from "./tasksNew/payway/GetOffersZupTask";
import LiveChannelsTask from "./tasksNew/payway/LiveChannelsTask";

import PlayerGetMediaTask from "./tasksNew/player/PlayerGetMediaTask";

import DeviceAttachTask from "./tasksNew/device/AttachTask";

import seenTask from "./tasksNew/social/seen/seenTask";

import EpgTask from "./tasksNew/cms/epgNew";
import NewLevel from "./tasksNew/cms/newLevel";

import PlayerTrackerTask from "./tasksNew/trackers/PlayerTrackerTask";

import CheckNetworkStatusTask from "./tasksNew/application/CheckNetworkStatusTask";

import { installOpenChannelCheck } from "./tasksNew/canalabierto/CanalAbierto"; // para usarla aca, no es el export.

import { getEpgVersionFromLatitudeLongitud } from "./tasksNew/geo/geoUtils"; // para usarla aca, no es el export.

import ZUPUtils from "../utils/ZUPUtils";
import ZUPConfig from "../config/ZUPConfig";
import getAppConfig from "../config/appConfig";
import storage from "../components/DeviceStorage/DeviceStorage";
import Device from "../devices/device";

// Canal abierto -- Open Channel
export { shouldShowOpenChannelMessage, getOpenChannelData, messageWasShown, allMessagesWereShown } from "./tasksNew/canalabierto/CanalAbierto"; // manejo de mensajes

export { loginZup, refreshTokenZUP } from "./tasksNew/zup/authMOCKUP";

export async function headerLoader() {
  let result;
  try {
    const startHeaderInfoTask = new StartHeaderInfoTask();
    result = await RequestManager.addRequest(startHeaderInfoTask);

    result.response.region = getAppConfig().region; // GOOSE -- HACK -- HARDCODE -- Region to Config // FAA-311

    getEpgVersionFromLatitudeLongitud();
    installOpenChannelCheck(); // get Canal Abierto

    let region = result.response.region;

    const regionLoggedStorage = storage.getItem("region");
    const HKS = result.response.session_stringvalue;
    const time = `${result.response.date} ${result.response.time}`;
    let server_time = null;

    if (regionLoggedStorage && storage.getItem("user_hash") && regionLoggedStorage !== region) {
      region = regionLoggedStorage;
      //TODO 3 hrs esta hardcode
      server_time = moment(time).add(0, "h");
    } else {
      server_time = moment(time);
    }
    storage.setItem("region", region);
    storage.setItem("HKS", HKS);
    storage.setItem("server_time", server_time);
    storage.setItem("local_time", moment());
  } catch (err) {
    console.error("[loader] starheaderinfo err", err);
  }

  return new Promise((resolve, reject) => {
    resolve(result);
  });
}

export async function contentRecomendations(groupId = "", quantity = 10) {
  let result;
  try {
    const contentRecommendationsTask = new ContentRecommendationsTask(groupId, quantity);
    result = await RequestManager.addRequest(contentRecommendationsTask);
  } catch (err) {
    console.error("Error calling contentRecomendations: ", err);
  }
  return new Promise((resolve, reject) => {
    if (result && result.status === "0") {
      resolve(result.response.groups || []);
    } else {
      reject(result);
    }
  });
}

export async function getOffers(title, addons, plans) {
  try {
    const url = ZUPConfig.getUrl("getOffers");
    let headers = ZUPConfig.getItem("headers");
    headers["X-Customer-Id"] = storage.getItem("zup-customer-id");
    headers["X-Channel-Id"] = ZUPConfig.getItem("channel_id");
    headers["Authorization"] = ZUPUtils.getNewBearer();

    const body = {
      titleRequested: title,
      compositionsName: {
        addons: addons, //"LANCAMENTOS"
        plans: plans, // "LANCAMENTOS"
      },
    };
    const options = {
      method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
      body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
      cache: "no-cache", // String	See the cache property for the valid values.
      headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
      credentials: "include", // include|omit String See the credentials property for the valid values.
      mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
    };

    const myRequest = new Request(url, options);
    const response = await fetch(myRequest);
    const data = await ZUPUtils.readZupPaymentResponse(response);

    const result = {
      data,
    };

    return result;
  } catch (err) {
    console.error("[ZUP][getOffers] ERROR -- ", err);
    return {}; // Fake result
  }
}

export async function getListOffers() {
  try {
    const url = ZUPConfig.getUrl("getOffers");

    const headers = ZUPConfig.getItem("headers");
    headers["X-Customer-Id"] = storage.getItem("zup-customer-id");
    headers["X-Channel-Id"] = ZUPConfig.getItem("channel_id");
    headers["Authorization"] = ZUPUtils.getNewBearer();

    const body = {
      offerParameters: {
        addons: {
          TYPE: "SVOD",
        },
        plans: {
          TYPE: "PLAN",
          CATALOG_OFFER_TYPE: "ADDON",
        },
      },
    };

    const options = {
      method: "POST",
      body: JSON.stringify(body),
      cache: "no-cache",
      headers: headers,
      credentials: "include",
      mode: "cors",
    };

    const myRequest = new Request(url, options);
    const response = await fetch(myRequest);
    const data = await ZUPUtils.readZupPaymentResponse(response);

    if (data.plans) data.plans = getSvodLiveContents(data.plans);
    if (data.addons) data.addons = getSvodLiveContents(data.addons);

    const result = {
      data,
    };

    return result;
  } catch (err) {
    throw err;
    // return {}; // unreachable code
  }
}

/**
 * Para los arrays de formato tipo PLAN y ADDONS del
 * getListOffers, genera array con los items separados
 * por SVOD y LIVE
 * Asignar el resultado del mismo dato
 * data = getSvodLiveContents(data)
 * @param array origins
 */
function getSvodLiveContents(origins) {
  origins.forEach((plan, nroplan) => {
    // para cada plan
    let live = [],
      svod = [];
    plan.items.forEach((i, nroitem) => {
      // por cada item del plan
      if (i.composition && i.composition.attributes && i.composition.attributes.length) {
        // si tengo el dato
        let attr = i.composition.attributes;
        for (var r = 0; r < attr.length; r++) {
          // por cada attributo
          if (attr[r].name == "TYPE") {
            let type = i.composition.attributes[0].text;
            switch (type) {
              case "LIVE":
                live.push(i);
                break;
              case "SVOD":
                svod.push(i);
                break;
              default:
                console.log("[RENT] ERROR -- no lo entendi", type);
            }
          }
        }
      }
    });
    plan["SVOD"] = svod;
    plan["LIVE"] = live;
  });
  console.log("[RENT] getSvodLiveContents OUT:", origins);
  return origins;
}

export async function isLoggedIn() {
  let result;

  try {
    const isLoggedInTask = new IsLoggedInTask(true);
    result = await RequestManager.addRequest(isLoggedInTask);

    if (storage.getItem("IsLogOut")) {
      storage.unsetItem("IsLogOut");
      result.response.is_user_logged_in = 0;
    }

    if (result && result.response && result.response.region) {
      const user_hash = result.response.session_userhash;
      const regionUser = result.response.region;
      const sub_region = result.response.subregion;
      const regionStorage = storage.getItem("region");
      user_hash && storage.setItem("user_hash", user_hash);
      sub_region && storage.setItem("sub_region", sub_region);

      if (regionUser !== regionStorage) {
        storage.setItem("region", regionUser);
        window.location.href = "/";
      }
    }
  } catch (err) {
    console.error("[ISLOGGEDIN] Error calling isLoggedInTask: ", err);
  }

  return new Promise((resolve, reject) => {
    resolve(result);
  });
}

// guardar lastouch en localstore para mandarlo en bookmark, actualizar lastouch cuando se hace tick
export async function playerGetMedia(groupId = "", contentId = "", isTrailer = false, streamType = "", startTime = "", endTime = "", timeshiftToken = "", showErrorModal) {
  let result;
  try {
    const playerGetMediaTask = new PlayerGetMediaTask(groupId, contentId, isTrailer, streamType, startTime, endTime, timeshiftToken, showErrorModal);
    result = await RequestManager.addRequest(playerGetMediaTask);
  } catch (err) {
    console.error("[PURCHASE] -- playerGetMedia -- Error calling playerGetMedia: ", err);
    result = err;
  }
  return new Promise((resolve, reject) => {
    if (result.status === 200 || result.status === 400) {
      if (result.status === 200) {
        result.response.entry = result.entry;
        resolve(result.response);
        if (result.response && result.response.user && result.response.user.lasttouch) {
          // store.dispatch(setLastTouchData(result.response.user.lasttouch));
        }
      }

      if (result.status === 400) {
        resolve(result);
      }
    } else {
      reject(result);
    }
  });
}

export async function getRentedByGroupId(groupId) {
  const result = await getRented();
  const filter = result && result.items.filter((item) => item.contentId === groupId);
  return filter[0] || false;
}

export async function getRented(expired = false) {
  try {
    const url = ZUPConfig.getUrl("getRented");

    let headers = ZUPConfig.getItem("headers");
    headers["X-Customer-Id"] = storage.getItem("zup-customer-id");
    headers["X-Channel-Id"] = ZUPConfig.getItem("channel_id");
    headers["Authorization"] = ZUPUtils.getNewBearer();

    const configParams = ZUPConfig.getItem("rented");
    const currencyParams = ZUPConfig.getItem("currency");

    const today = new Date();
    let startDate = new Date();
    let endDate = new Date();
    if (expired) {
      startDate.setDate(today.getDate() - configParams.days_behind);
      endDate = today;
    } else {
      startDate = today;
      endDate.setDate(today.getDate() + configParams.days_in_advance);
    }
    const tsStartDate = startDate.toISOString().substring(0, 10);
    const tsEndDate = endDate.toISOString().substring(0, 10);
    const tsExpired = expired ? "EXPIRED" : "ACTIVE";

    var completeURL = url + `&startDate=${tsStartDate}&endDate=${tsEndDate}&status=${tsExpired}&expiredAmount=${configParams.expired_number}&showDuplicates=false`;
    const options = {
      method: "GET", // String	The http method such as 'GET', 'POST', 'DELETE'.
      cache: "no-cache", // String	See the cache property for the valid values.
      headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
      credentials: "include", // include|omit String See the credentials property for the valid values.
      mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
    };

    const myRequest = new Request(completeURL, options);
    const response = await fetch(myRequest);
    const data = await ZUPUtils.readZupPaymentResponse(response);

    let result = {};
    if (data.length) {
      result.params = {
        startDate: tsStartDate,
        endDate: tsEndDate,
        status: expired ? "EXPIRED" : "ACTIVE",
        expiredAmount: 5,
        showDuplicates: false,
      };
      let el = {};
      let items = [];
      for (var i = 0; i < data.length; i++) {
        el = data[i];
        items.push({
          id: el.contentId, //: "537121"
          contentDescription: el.contentDescription, //: "Quatro indicações ao Oscar® e baseado em uma história real. Um escândalo que arrasou uma nação."
          contentId: el.contentId, //: "537121"
          endDate: `${moment(el.endDate).format("DD/MM/YYYY")} às ${moment(el.endDate).format("HH:mm")}`, //.substr(0, 10), //: "2019-11-05T16:57:50"
          cardEndDate: `${moment(el.endDate).format("DD/MM/YYYY")} às ${moment(el.endDate).format("HH:mm")}`,
          entitlement: el.entitlement, //: "NX_TVOD_STANDARD_RENT_24_HRS"
          imageUrl: el.imageUrl + "?size&w=290", //: "http://clarovideocdn3.clarovideo.net/PELICULAS/QUIZSHOW/EXPORTACION_WEB/PT/QUIZSHOWWVERTICAL.jpg?size=200x300"
          price: {
            price: el.price.amount,
            currency: currencyParams[el.price.currency],
            scale: el.price.scale,
            realprice: parseFloat(el.price.amount / 10 ** el.price.scale)
              .toFixed(2)
              .replace(".", ","),
          }, //el.price, //: // amount: 900 // currency: "BRL" // scale: 2
          startDate: `${moment(el.startDate).format("DD/MM/YYYY")}`, //.substr(0, 10), //: "2019-11-04T16:57:50"
          cardStartDate: `${moment(el.startDate).format("DD/MM/YYYY")}`,
          validity: el.validity, // // duration: 24 // period: "HOUR" // unlimited: false
        });
      }

      if (!expired) {
        items.sort((a, b) => {
          return a.endDate > b.endDate;
        });
      } else {
        items.sort((a, b) => {
          return a.endDate > b.endDate;
        });
      }
      result.status = "OK";
      result.items = items;
    } else {
      result = {
        status: "ERROR",
        msg: "NODATA-ERROR",
        items: [],
      };
    }

    return new Promise((resolve, reject) => {
      resolve(result);
    });
  } catch (err) {
    return {
      status: "ERROR",
      msg: "NET-ERROR",
      items: [],
    };
  }
}

export async function CardCredit() {
  const url = ZUPConfig.getUrl("cardCredit");

  let headers = ZUPConfig.getItem("headers");
  headers["X-Customer-Id"] = storage.getItem("zup-customer-id");
  headers["X-Channel-Id"] = ZUPConfig.getItem("channel_id");
  headers["Authorization"] = ZUPUtils.getNewBearer();

  const options = {
    method: "GET", // String	The http method such as 'GET', 'POST', 'DELETE'.
    cache: "no-cache", // String	See the cache property for the valid values.
    headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
    credentials: "include", // include|omit String See the credentials property for the valid values.
    mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
  };
  const myRequest = new Request(url, options);
  const response = await fetch(myRequest);
  const data = await ZUPUtils.readZupPaymentResponse(response);
  const result = {
    data,
  };

  return result;
}

export async function CheckoutBuyPlan(catalogoOfferId, methodId, imageUrly, contentDescription, catalogItemId, content_id) {
  const url = ZUPConfig.getUrl("CheckoutBuy");

  let abstractId = Device.getDevice().getId();

  const body = {
    catalogOfferId: catalogoOfferId,
    catalogOfferType: "PLAN",
    externalId: Date.now().toString(),
    payment: {
      methods: [
        {
          methodId: methodId,
          method: "CREDIT_CARD",
          deviceInfo: {
            brand: "Apple",
            deviceUniqueId: abstractId.getUniqueID(),
            fingerPrint: storage.getItem("HKS") + new Date().toTimeString().split(" ")[0],
            manufacturer: "Apple",
            model: "Apple",
            userAgent: abstractId.getUserAgent(),
            wifiMacAddress: "",
            ip: abstractId.getIP(),
          },
          purchaseInfo: {
            origin: "SMARTTV",
            imageUrl: imageUrly,
            contentDescription: contentDescription,
          },
        },
      ],
    },
    // externalId: Date.now().toString(), // no duplicate keys
    quantity: 1,
    userParameters: [
      {
        catalogItemId: catalogItemId,
        parameters: {
          CONTENT_ID: content_id,
        },
      },
    ],
  };

  let headers = ZUPConfig.getItem("headers");
  headers["X-Customer-Id"] = storage.getItem("zup-customer-id");
  headers["X-Channel-Id"] = ZUPConfig.getItem("channel_id");
  headers["Authorization"] = ZUPUtils.getNewBearer();

  const options = {
    method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
    body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
    cache: "no-cache", // String	See the cache property for the valid values.
    headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
    credentials: "include", // include|omit String See the credentials property for the valid values.
    mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
  };

  const myRequest = new Request(url, options);
  const response = await fetch(myRequest);
  const data = await ZUPUtils.readZupPaymentResponse(response);
  const result = {
    data,
    status: response.status,
  };

  return result;
}

export async function CheckoutBuy(catalogoOfferId, methodId, imageUrly, contentDescription) {
  const url = ZUPConfig.getUrl("CheckoutBuy");

  // DEVICE
  let abstractId = Device.getDevice().getId();
  // let device = Device.getDevice().getPlatform(); // no unused code

  //ARBOL CHECKOUT COMPRA
  const body = {
    catalogOfferId: catalogoOfferId,
    catalogOfferType: "ADDON",
    externalId: Date.now().toString(),
    payment: {
      methods: [
        {
          methodId: methodId,
          method: "CREDIT_CARD",
          deviceInfo: {
            brand: "Apple",
            deviceUniqueId: abstractId.getUniqueID(),
            fingerPrint: storage.getItem("HKS") + new Date().toTimeString().split(" ")[0],
            manufacturer: "Apple",
            model: "Apple",
            userAgent: abstractId.getUserAgent(),
            wifiMacAddress: "",
            ip: abstractId.getIP(),
          },
          purchaseInfo: {
            imageUrl: imageUrly,
            contentDescription: contentDescription,
            origin: "SMARTTV",
          },
        },
      ],
    },
  };

  let headers = ZUPConfig.getItem("headers");
  headers["X-Customer-Id"] = storage.getItem("zup-customer-id");
  headers["X-Channel-Id"] = ZUPConfig.getItem("channel_id");
  headers["Authorization"] = ZUPUtils.getNewBearer();

  const options = {
    method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
    body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
    cache: "no-cache", // String	See the cache property for the valid values.
    headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
    credentials: "include", // include|omit String See the credentials property for the valid values.
    mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
  };
  const myRequest = new Request(url, options);
  const response = await fetch(myRequest);
  const data = await ZUPUtils.readZupPaymentResponse(response);
  const result = {
    data,
    status: response.status,
  };

  return result;
}

export async function CheckoutRenta(catalogoOfferId, methodId, imageUrly, contentDescription, catalogItemId, content_id) {
  const url = ZUPConfig.getUrl("CheckoutRenta");
  let abstractId = Device.getDevice().getId();

  const body = {
    catalogOfferId: catalogoOfferId,
    catalogOfferType: "ADDON",
    payment: {
      methods: [
        {
          methodId: methodId,
          method: "CREDIT_CARD",
          deviceInfo: {
            deviceUniqueId: abstractId.getUniqueID(),
            userAgent: abstractId.getUserAgent(),
            wifiMacAddress: "68:c4:4d:b9:cd:58",
            ip: abstractId.getIP(),
            brand: "tv",
            fingerPrint: storage.getItem("HKS") + new Date().toTimeString().split(" ")[0],
            manufacturer: "lg",
            model: abstractId.getModel(),
          },
          purchaseInfo: {
            imageUrl: imageUrly,
            contentDescription: contentDescription,
            origin: "SMARTTV",
          },
          installments: 1,
        },
      ],
    },
    externalId: Date.now().toString(),
    quantity: 1,
    userParameters: [
      {
        catalogItemId: catalogItemId,
        parameters: {
          CONTENT_ID: content_id,
        },
      },
    ],
  };

  let headers = ZUPConfig.getItem("headers");
  headers["X-Customer-Id"] = storage.getItem("zup-customer-id");
  headers["X-Channel-Id"] = ZUPConfig.getItem("channel_id");
  headers["Authorization"] = ZUPUtils.getNewBearer();

  const options = {
    method: "POST", // String	The http method such as 'GET', 'POST', 'DELETE'.
    body: JSON.stringify(body), // Object	The data to send with the request.Must be a Blob, BufferSource, FormData, String, or URLSearchParams.
    cache: "no-cache", // String	See the cache property for the valid values.
    headers: headers, // Object	The http headers to send with the request.This will be passed to the Headers constructor.
    credentials: "include", // include|omit String See the credentials property for the valid values.
    mode: "cors", // cors|no-cors|same-origin String See the mode property for the valid values.
  };

  const myRequest = new Request(url, options);
  const response = await fetch(myRequest);
  let data = await ZUPUtils.readZupPaymentResponse(response);
  const result = {
    data,
    status: response.status,
  };

  return result;
}

export async function GetOffersZUP(params) {
  const task = new GetOffersZupTask(params);

  return generic(task, true);
}

export async function contentData(groupId = "") {
  const task = new ContentDataTask(groupId);

  return generic(task);
}

export async function contentSerie(groupId = "") {
  const task = new ContentSerieTask(groupId);

  return generic(task);
}

export async function trackerLoader(url, params) {
  const task = new PlayerTrackerTask(url, params, false);

  return generic(task);
}

export async function networkLoader() {
  const task = new CheckNetworkStatusTask();

  return generic(task, true);
}

export async function logoutLoader(url, options) {
  // const task = new LogoutTask();

  // return generic(task);
  return {};
}

export async function levelLoader(url, options) {
  const task = new NewLevel(url, options);

  return generic(task);
}

export async function seenLoader(user) {
  const task = new seenTask({ user_hash: user.session_userhash });

  return generic(task);
}

export async function liveChannels(userId = null) {
  const task = new LiveChannelsTask(userId);

  return generic(task, true);
}

export async function deviceAttach(groupId) {
  const task = new DeviceAttachTask(groupId);

  return generic(task);
}

export async function epgLoader(query) {
  const task = new EpgTask("epgNew", query);

  return generic(task);
}

export async function UserSeenLast(group_id, user) {
  const task = new UserSeenLastTask(group_id, user);

  return generic(task);
}

export function predictiveSearch(value = "") {
  const task = new predictiveSearchTask({ value });

  return generic(task, true);
}

export function getBookmark(groupId = "", user) {
  const task = new GetBookmarkTask(groupId, user);

  return generic(task, true);
}

export async function fetchFavoriteLoader(user) {
  const task = new UserFavoriteTask({ user_hash: user.session_userhash });

  return generic(task, true);
}

export async function fetchFavoriteSerieLoader(user) {
  const task = new UserFavoriteSerieTask({ user_hash: user.session_userhash });

  return generic(task, true);
}

export async function addFavoriteLoader(objectId) {
  const task = new AddFavoriteTask(objectId);

  return generic(task, true);
}

export async function addFavoriteSerieLoader(objectId) {
  const task = new AddFavoriteSerieTask(objectId);

  return generic(task, true);
}

export async function delFavoriteLoader(objectId) {
  const task = new DeleteFavoriteTask(objectId);

  return generic(task, true);
}

export async function delFavoriteSerieLoader(objectId) {
  const task = new DeleteFavoriteSerieTask(objectId);

  return generic(task, true);
}

export async function vertical(value, field, provider_id, from, quantity) {
  const task = new Vertical(value, field, provider_id, from, quantity);

  return generic(task);
}

const generic = async (task, returnAll = false) => {
  let result;
  try {
    result = await RequestManager.addRequest(task);

    return returnAll ? result : get(result, "response");
  } catch (err) {
    console.error("Error: ", err);
    throw new Error("Error conn");
  }
};
