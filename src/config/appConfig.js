import DeviceStorage from "../components/DeviceStorage/DeviceStorage";
import Device from "../devices/device";
import LOG from "../utils/Log";

const defaultAppKey = "531eed59e4b050ea818ae755";
const intVersion = "PI6S30";

/**
 * PRUEBA DE COOKIES EN CONFIG
 */
// function setCookie(name, value, extdays = 1) {
//   var d = new Date();
//   d.setTime(d.getTime() + extdays * 24 * 60 * 60 * 1000);
//   const expires = extdays ? "; expires=" + d.toUTCString() : "";
//   const cook = `${name}=${value}${expires}; path=/`;
//   console.log("[COOKIE] -- set", cook);
//   document.cookie = cook;
// }
// function delCookie(name) {
//   const cook = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/`;
//   console.log("[COOKIE] -- del", cook);
//   document.cookie = cook;
// }
// const getCookies = (key = null) => {
//   const cook = decodeURIComponent(document.cookie).split(";");
//   const cookies = {};
//   cook.forEach((c) => {
//     const coo = c.trim().split("=");
//     cookies[coo[0]] = coo[1];
//   });
//   if (key != null) {
//     console.log("[COOKIE] -- get", key, "= [", cookies[key] || "", "]");
//     return cookies[key] || "";
//   } else {
//     console.log("[COOKIE] -- all cookies", cookies);
//     return cookies;
//   }
//   return cookies;
// };
// TESTEO
// const acookie = getCookies();
// const bcookie = getCookies("pepe");
// const ccookie = getCookies("_ga");
// setCookie("nombre", "gustavo");
// const dcookie = getCookies();
// delCookie("nombre");
// const ecookie = getCookies();

const getDeviceID = () => {
  const device_config = Device.getDevice().getConfig();
  if (device_config.device_id) {
    DeviceStorage.setItem("__UUID__", device_config.device_id);
  }
  let Id = DeviceStorage.getItem("__UUID__");
  if (Id) {
    return Id;
  }
  let random = null;
  Id = "";
  for (let i = 0; i < 32; i++) {
    random = (Math.random() * 16) | 0;

    if (i === 8 || i === 12 || i === 16 || i === 20) {
      Id += "-";
    }
    Id += random.toString(16);
  }
  DeviceStorage.setItem("__UUID__", Id);
  return Id;
};

/**
 * Intenta adivinar el entorno por la url
 * con la que se esta ejecutando.
 */
export function guessEnvironment() {
  const href = document.location.href;
  let env = "desconocido";

  if (href.indexOf("localhost") !== -1) {
    env = "development";
  } else if (href.indexOf("-test") !== -1) {
    env = "test";
  } else if (href.indexOf("-pre") !== -1) {
    env = "pre";
  } else if (href.indexOf("-uat") !== -1) {
    env = "uat";
  } else if (href.indexOf("-prod") !== -1) {
    env = "production";
  } else {
    env = "production";
  }

  return env;
}

/**
 * Devuelve toda la configuracion
 */
function getAppConfig() {
  const device_config = Device.getDevice().getConfig();
  const dev_id = getDeviceID();

  let env = "[nada]";
  let end_point_middleware = "/webapi/"; // default por las dudas
  let default_headers_enabled = false;
  let default_headers = {
    // 'Access-Control-Allow-Origin': '*',
    // 'partition': 'netflexuat',
    // 'Pragma': 'akamai-x-cache-on, akamai-x-cache-remote-on, akamai-x-check-cacheable, akamai-x-get-cache-key, akamai-x-get-nonces, akamai-x-get-true-cache-key, akamai-x-get-client-ip',
    // 'X-Akamai-Debug': 'CDHE-56yrt4'
  };

  const guessedEnv = guessEnvironment();
  LOG.info("[CONFIG]{guessedEnv}", guessedEnv, document.location);
  switch (guessedEnv) {
    case "development":
      env = "development";
      end_point_middleware = "http://localhost:4000/webapi-video/";
      document.title = intVersion + " [DEV] Clarovideo";
      break;

    case "test":
      env = "test";
      end_point_middleware = "/webapi-video/";
      document.title = intVersion + " [TES] Clarovideo";
      break;

    case "pre":
      env = "pre";
      end_point_middleware = "/webapi-video/";
      document.title = intVersion + " [PRE] Clarovideo";
      break;

    case "uat":
      env = "uat";
      end_point_middleware = "/webapi-video/";
      document.title = intVersion + " [UAT] Clarovideo";
      break;

    case "production":
      env = "production";
      end_point_middleware = "/webapi-video/";
      document.title = intVersion + " [PROD] Clarovideo";
      break;

    default:
      env = "[default!]";
      end_point_middleware = "/webapi-video/";
      document.title = intVersion + " [???] Clarovideo";
      break;
  }

  const conf = {
    environment_in: env,
    environment_out: process.env.NODE_ENV,
    api_version: "v5.86",
    default_headers: default_headers,
    default_headers_enabled: default_headers_enabled,
    authpn: "net",
    authpt: "5facd9d23d05bb83",
    appKey: device_config.appKey || defaultAppKey,
    end_point_apa: device_config.end_point_apa,
    end_point_middleware,
    device_category: device_config.device_category,
    device_id: dev_id,
    device_manufacturer: device_config.device_manufacturer,
    device_model: device_config.device_model,
    device_name: device_config.device_name,
    device_so: device_config.device_so,
    device_type: device_config.device_type,
    tenant_code: "netnow",
    region: "brasil",
    format: "json",
    osversion: "2.0",
    akamai: {
      xml: 'var AKAMAI_MEDIA_ANALYTICS_CONFIG_FILE_PATH = "{URL}";',
      url: "//79423.analytics.edgesuite.net/js/csma.js",
    },
    api_errors: {
      format: {
        text: "api_error_format",
      },
    },
    UniqueID: getDeviceID(),
    // end_point_music,
    appversion: device_config.appversion,
    firmwareversion: device_config.firmwareversion,
    xdk_redirect: "/xdkredirect/",
    default_menu_node: "home",
    platform: "netflex",
  };
  LOG.info("[CONFIG]{conf}", conf);
  return conf;
}

/**
 * Devuelve el endpoint a utilizar
 */
export function getApiEndpoint() {
  const guessedEnv = guessEnvironment();
  let apiEndpoint = "mfwktvnx1-api.clarovideo.net";
  switch (guessedEnv) {
    case "development":
      apiEndpoint = "mfwktvnx1-uat-api.clarovideo.net";
      break;
    case "test":
      apiEndpoint = "mfwktvnx1-uat-api.clarovideo.net";
      break;
    case "pre":
      apiEndpoint = "mfwktvnx1-uat-api.clarovideo.net";
      break;
    case "uat":
      apiEndpoint = "mfwktvnx1-uat-api.clarovideo.net";
      break;
    case "production":
    default:
      break;
  }
  LOG.info("[CONFIG]{getApiEndpoint}", { guessedEnv, apiEndpoint });
  return apiEndpoint;
}

export default getAppConfig;
