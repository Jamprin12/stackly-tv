import { headerLoader } from "../requests/loaderNew";
import ZUPUtils from "../utils/ZUPUtils"; // refreshTokenZUP

export default async function loadInitApis() {
  ZUPUtils.checkSessionToken();

  await headerLoader();
}
