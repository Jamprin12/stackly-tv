/**
 * ZUP Config Data
 */
import LOG from "../utils/Log";
import {getApiEndpoint, guessEnvironment} from './appConfig';

//
// TO DELETE
//
// const app_key_old = "f5027fd05d460137590f000d3ac06d76"; // old key en el momento de login
// const new_app_key = "8f003090c6a601376298000d3ac06d76"; // no sabemos si se va a usar.
// const url_AMCO_general = "http://"+apiEndpoint+"/services";

// PRODUCTION
// ------
// host = "https://claro.gateway.zup.me",
// key = "8734f0b0c6a601376296000d3ac06d76",
// channelId = "2957d60f-e764-43f1-991d-fe74c250d6e3",
// slug = "claro",
// id = "net_digital_bsim"
// [13:43, 21/5/2020] Robinson: nx1@mailna.me
// [13:43, 21/5/2020] Robinson: 12345678
// ------
// host = "https://claro.gateway.zup.me";
// app_key = "8734f0b0c6a601376296000d3ac06d76"; // nueva key en el momento de alugados
// channel_id = "2957d60f-e764-43f1-991d-fe74c250d6e3";
// slug = "claro";
// id = "net_digital_bsim";
// ------

// https://claro.gateway.zup.me/net-digital-bss-sessionless/v1/bss/user/query/login?gw-app-key=8734f0b0c6a601376296000d3ac06d76

// UAT
// ------
// url_base_sessionless = "https://claropreprod.gateway.zup.me/net-digital-bss-sessionless/v1";
// url_base = "https://claropreprod.gateway.zup.me/net-digital-bss/v1";
// app_key = "bfcf1c405d4d01375911000d3ac06d76";
// channel_id = "b50abf87-8fc0-4d79-9d86-c62fdc4d820f";
// -------
// host = "https://claropreprod.gateway.zup.me";
// app_key = "bfcf1c405d4d01375911000d3ac06d76"; // nueva key en el momento de alugados
// channel_id = "b50abf87-8fc0-4d79-9d86-c62fdc4d820f";
// slug = "claro";
// id = "net_digital";
// ----------------------------------------------------------------------------------------------

// DEFAULT VALUES
let host = "https://claropreprod.gateway.zup.me";
let app_key = "bfcf1c405d4d01375911000d3ac06d76"; // nueva key en el momento de alugados
let channel_id = "b50abf87-8fc0-4d79-9d86-c62fdc4d820f";
let slug = "claro";
let id = "net_digital";

//
// Environments
//
const guessedEnv=guessEnvironment();
LOG.info('[CONFIG][ZUP]{guessedEnv}', guessedEnv, document.location)
switch(guessedEnv){
  case 'development':
  case 'test':
    host = "https://claro.gateway.zup.me";
    app_key = "bfcf1c405d4d01375911000d3ac06d76"; // parche, la de uat en prod
    // app_key = "8734f0b0c6a601376296000d3ac06d76"; // la correcta // nueva key en el momento de alugados
    channel_id = "2957d60f-e764-43f1-991d-fe74c250d6e3";
    slug = "claro";
    id = "net_digital_bsim";
    break;
  case 'pre':
    // host = "https://claropreprod.gateway.zup.me";
    // app_key = "bfcf1c405d4d01375911000d3ac06d76"; // nueva key en el momento de alugados
    // channel_id = "b50abf87-8fc0-4d79-9d86-c62fdc4d820f";
    // slug = "claro";
    // id = "net_digital";
    // break;
  case 'uat':
  case 'production':
  default:
    host = "https://claro.gateway.zup.me";
    app_key = "bfcf1c405d4d01375911000d3ac06d76"; // parche, la de uat en prod
    // app_key = "8734f0b0c6a601376296000d3ac06d76"; // la correcta // nueva key en el momento de alugados
    channel_id = "2957d60f-e764-43f1-991d-fe74c250d6e3";
    slug = "claro";
    id = "net_digital_bsim";
    break;
}
LOG.info("[CONFIG][ZUP]",{guessedEnv,host,app_key,channel_id,slug,id})

//
// VARS
//
let url_base_sessionless = `${host}/net-digital-bss-sessionless/v1`;
let url_base = `${host}/net-digital-bss/v1`;
// const apiEndpoint = getApiEndpoint();
const url_AMCO_base = "https://"+getApiEndpoint()+"/services"; // REAL

//
// CONFIG DB
//
const config = {
  version: 1,
  app_key: app_key,
  channel_id: channel_id,
  urls: {
    // Auth
    login: url_base_sessionless + "/bss/user/query/login?gw-app-key=" + app_key,
    old_refresh: url_base_sessionless + "/bss/user/query/refresh-token?gw-app-key=" + app_key,
    new_login: url_base_sessionless + "/bss/auth?gw-app-key=" + app_key, // GOOSE -- new_app_key no anda! avisar!!!
    refresh: url_base_sessionless + "/bss/auth-refresh?gw-app-key=" + app_key, // GOOSE -- new_app_key no anda! avisar!!!
    getClientData: url_base + "/bss/customer/query/customers?gw-app-key=" + app_key,
    // Rented
    getRented: url_base + "/bss/customer/query/customers/rented-content?gw-app-key=" + app_key,
    // Payment
    offers: url_AMCO_base+"/payway/paymentservice/offers", // estado del contenido en AMCO
    getOffers: url_base + "/bss/offers?gw-app-key=" + app_key, // obtener formas de pago en BSS
    cardCredit: url_base + "/bss/payment/query/credit-cards?gw-app-key=" + app_key,
    CheckoutBuy: url_base + "/bss/purchase-orders/checkout?gw-app-key=" + app_key,
    CheckoutRenta: url_base + "/bss/purchase-orders/checkout?gw-app-key=" + app_key
  },
  headers: {
    "Content-Type": "application/json",
    "X-Application-Id": id,
    "X-Organization-Slug": slug,
    "X-Application-Key": app_key
  },
  rented:{
    days_in_advance:30,
    days_behind:30,
    expired_number:5
  },
  currency:{
    BRL : 'R$'
  }
};

/**
 * ZUP config object
 * Static functions
 */
class ZUPConfig {
  /**
   * Returns the hole config object
   */
  static getAll() {
    return config;
  }
  /**
   * Gets an specific item
   * @param {string} keyName the key to retrieve
   * @param {string} defaultValue a default value
   */
  static getItem(keyName, defaultValue = "") {
    return config[keyName] || defaultValue;
  }
  /**
   * Gets a URL
   * @param {string} cmd comando a devolver
   */
  static getUrl(cmd) {
    return config["urls"][cmd];
  }
}

export default ZUPConfig;
