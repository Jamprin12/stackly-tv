import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles(() => ({
  containerBack: {
    width: 80,
  },
  back: ({ iconBack, iconBackFocus }) => ({
    position: "absolute",
    top: 30,
    left: 30,
    zIndex: 1,
    width: 50,
    height: 50,
    backgroundSize: 50,
    backgroundImage: `url(${iconBack})`,
    backgroundRepeat: "no-repeat",
    "&:focus": {
      backgroundImage: `url(${iconBackFocus})`,
      backgroundSize: 55,
      height: 55,
      width: 55,
    },
  }),
}));

const ButtonBack = ({ onClick, snDown = null }) => {
  const { t } = useTranslation();
  const classes = useStyles({ iconBack: t("asset.iconBack"), iconBackFocus: t("asset.iconBackFocus") });

  return (
    <div className={classes.containerBack}>
      <div
        className={`${classes.back} focusable`}
        tabIndex="0"
        data-sn-down={snDown}
        onClick={(e) => {
          e.preventDefault();
          onClick(e);
        }}
      />
    </div>
  );
};

export default React.memo(ButtonBack);
