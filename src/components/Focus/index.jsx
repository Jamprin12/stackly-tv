import { Component } from "react";

import "./spatial_navigation";

class Focus extends Component {
  componentDidMount() {
    window.SpatialNavigation.init();

    let areas = [
      {
        name: "newPlayer",
        config: {
          selector: "#HTML5VideoWrapperTv .focusable",
          restrict: "self-only",
        },
      },
      {
        name: "nav_down",
        config: {
          selector: "#header .focusable",
          leaveFor: {
            left: "",
            right: "@container",
          },
          enterTo: "default-element",
          defaultElement: ".active",
        },
      },
      // search ---
      {
        name: "keyboard",
        config: {
          selector: ".keyboardPad .focusable",
          leaveFor: {
            up: "@searchCont",
          },
          enterTo: "last-focused",
          defaultElement: "first-child",
        },
      },
      {
        name: "keyboardGral",
        config: {
          selector: ".keyboardGral .focusable",
          leaveFor: {
            up: "@searchCont",
          },
          enterTo: "last-focused",
          defaultElement: ".kbd-btn:first-child",
        },
      },
      {
        name: "searchButtons",
        config: {
          selector: "#searchButtoms .focusable",
          enterTo: ".active",
          defaultElement: ".active",
        },
      },
      {
        name: "searchCont",
        config: {
          selector: ".fromVMenu .focusable",
          leaveFor: {
            down: "@keyboardGral",
          },
          enterTo: "last-focused",
          defaultElement: ".ribbon .focusable:first-child",
        },
      },
      // --- ---
      {
        name: "container",
        config: {
          selector: "#privateContent .focusable",
          defaultElement: ".elementopredeterminado",
          rememberSource: true,
          enterTo: "last-focused",
        },
      },
      {
        name: "login",
        config: {
          selector: ".login .focusable",
          defaultElement: ".elementopredeterminado",
        },
      },
      {
        name: "landing",
        config: {
          selector: ".landing .focusable",
          defaultElement: ".elementopredeterminado",
        },
      },
      {
        name: "listChannels",
        config: {
          selector: "#listChannels .focusable",
          rememberSource: true,
          enterTo: "last-focused",
          defaultElement: "#listChannels .focusable.active",
        },
      },
      {
        name: "epgFinal",
        config: {
          selector: ".epgFinal .focusable",
          rememberSource: true,
          enterTo: "last-focused",
          restrict: "self-only",
          defaultElement: ".currentEvent",
        },
      },
      {
        name: "modal-area",
        config: {
          selector: ".modal-overlay .focusable",
          defaultElement: ".modal-default-item",
          restrict: "self-only",
        },
      },
      {
        name: "modal-new",
        config: {
          selector: ".modalNew .focusable",
          defaultElement: ".default-modal-item .focusable",
          restrict: "self-only",
        },
      },
    ];
    /*
    let areas = [
      {
        name: "header-top",
        config: {
          selector: ".header-top .focusable",
          // defaultElement: '.active',
          enterTo: "default-element",
          leaveFor: { down: ".header-down .focusable" }
        }
      },
      {
        name: "nav_down",
        config: {
          selector: ".header-down .focusable",
          leaveFor: {
            right: ".fromVMenu .focusable:first-child"
          },
          enterTo: "default-element",
          defaultElement: ".active"
        }
      },
      {
        name: "keyboard",
        config: {
          selector: ".keyboardGral .focusable",
          leaveFor: {
            right: ".results .focusable",
            up: ".ribbon-items .focusable"
            // up:'.button-filter .focusable'
          },
          //rememberSource: true,
          // enterTo: 'default-element',
          enterTo: "last-focused",
          defaultElement: ".kbd-btn:first-child"
        }
      },
      {
        name: "searchButtons",
        config: {
          selector: ".button-filter .focusable",
          leaveFor: {
            right: ".results .focusable",
            down: ".ribbon-items .focusable"
          },
          enterTo: "default-element",
          defaultElement: ".hack-active"
        }
      },
      {
        name: "keyboard_pin",
        config: {
          selector: ".keyboard .modal .focusable",
          leaveFor: { right: ".right .focusable" }
        }
      },
      {
        name: "main_container",
        config: {
          selector: ".container .focusable",
          defaultElement: ".elementopredeterminado",
          // leaveFor: { up:'.header-down .focusable',right:':focus' },
          // leaveFor: { left:'.header-down .focusable',right:':focus' },
          leaveFor: { left: ".header-down .active", right: ":focus" }
        }
      },
      {
        name: "player_controls",
        config: {
          selector: ".player-ui-controls .focusable"
        }
      },
      {
        name: "mini-epg",
        config: {
          selector: ".player-ui-epg .focusable",
          restrict: "self-only"
        }
      },
      {
        name: "epg_cover_flow",
        config: {
          selector: ".epg-cover-flow .focusable",
          restrict: "self-only"
        }
      },
      {
        name: "colors",
        config: {
          selector: ".color-codes .focusable"
        }
      },
      {
        name: "epg",
        config: {
          selector: ".full-epg .epg-events-container .focusable",
          restrict: "self-only"
        }
      },
      {
        name: "fin_player",
        config: {
          selector: ".end-player .focusable",
          defaultElement: ".focusable-default",
          enterTo: "default-element"
        }
      },
      {
        name: "modal-area-top",
        config: {
          selector: ".modal-overlay .vcard-seasons .focusable",
          // defaultElement: '.elementopredeterminado',
          // enterTo: 'default-element',
          leaveFor: { down: ".modal-overlay  .ribbon-container .focusable" },
          restrict: "self-only"
        }
      },
      {
        name: "modal-area",
        config: {
          selector: ".modal-overlay .focusable",
          defaultElement: ".modal-default-item",
          // defaultElement: '.elementopredeterminado',
          // enterTo: 'default-element',
          // leaveFor:{up:'.header-down .focusable'},
          restrict: "self-only"
        }
      },
      {
        name:'modal-new',
        config:{
          selector: '#modal-new .focusable',
          defaultElement: '.default-modal-item .focusable',
          restrict: 'self-only',
        }
      },


      {
        name: "epg-channels",
        config: {
          selector: ".channels-wrapper .focusable"
        }
      }
    ];
    */

    areas.map((x) => {
      return window.SpatialNavigation.add(x.name, x.config);
    });

    const enterUpHandler = function (e) {
      e.target.click();
    };

    const enterDownHandler = function (e) {
      e.preventDefault();
    };

    window.addEventListener("sn:enter-up", enterUpHandler);
    window.addEventListener("sn:enter-down", enterDownHandler);

    window.SpatialNavigation.makeFocusable();
    // window.SpatialNavigation.focus();
  }

  render() {
    return null;
  }
}

export default Focus;
