const className = 'focusable';

export default {
  id: 'root',
  main: 'root',
  className,
  selector: `.${className}`,
  filterout: 'filterout',
  nonfocusable: 'nonfocusable'
};
