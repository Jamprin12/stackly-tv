import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "@material-ui/core/styles";

import registerServiceWorker from "./registerServiceWorker";
import loadInitialApis from "./config/applicationLoader"; // GOOSE -- ZUP refreshTokenZUP in here
import Device from "./devices/device";

import App from "./containers/App/App";
import theme from "./theme";

import "font-awesome/css/font-awesome.css";

async function ready(loading) {
  const device = Device.getDevice().getPlatform();
  window.appReq = {
    url: [],
    data: {},
  };
  //TODO obtener mejor el HKS desde la cadena search
  if (window.location && window.location.search && window.location.search.indexOf && window.location.search.indexOf("HKS") > -1) {
    if (window.location.search.split("=") && window.location.search.split("=")[1]) localStorage.setItem("HKS", window.location.search.split("=")[1]);
  }

  await loadInitialApis();

  ReactDOM.render(
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>,

    document.getElementById("root"),
    () => {
      if (window.OldSiteTimer) {
        clearTimeout(window.OldSiteTimer);
      }
      console.log("[Application Start] 2", process.env.NODE_ENV, document.getElementById("root").classList);
      document.getElementById("root").classList.add(`root-${device}`);
    }
  );
}
ready();

registerServiceWorker();
