﻿import AbstractNetworkStatus from '../all/AbstractNetworkStatus';

class AndroidNetworkStatus extends AbstractNetworkStatus {
    constructor() {
        super();
    }
}

export default AndroidNetworkStatus;
