class exception {
    constructor() {
        this.ILLEGAL_ARGUMENT: "exception:illegal-argument",

        this.ILLEGAL_STATE: "exception:illegal-state",

        this.INTERNAL: "exception:internal",

        this.UNAUTHORIZED: "exception:unauthorized",

        this.UNSUPPORTED_OPERATION: "exception:unsupported-operation"
    }
}

export default new exception();
