﻿import AbstractNetworkStatus from '../all/AbstractNetworkStatus';

class PolaroidNetworkStatus extends AbstractNetworkStatus {
    constructor() {
        super();
    }
}

export default PolaroidNetworkStatus;
