import AbstractDeviceSetup from "../all/AbstractDeviceSetup";
import LGWeb0s from './LGWeb0s';

class Web0sDeviceSetup extends AbstractDeviceSetup {
  /* constructor() {
    super();
  } */

  setup() {
    return new Promise( (resolve) => {
      const tt = new LGWeb0s();
      tt.setup().then(() => { // no unuset saque let t=
        resolve();
      });
    });
  }
}

export default Web0sDeviceSetup;
