import AbstractHTML5Player from '../all/AbstractHTML5Player'

class WorkstationPlayer extends AbstractHTML5Player {
  constructor() {
    super();
  }
}

export default WorkstationPlayer;
