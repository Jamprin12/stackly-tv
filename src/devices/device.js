import DeviceDetection from "./DeviceDetection";

/*
function getDeviceConfig() {
  let device = null;
  const detection = new DeviceDetection;
  const deviceName = detection.getDevice("device");
  console.log('EDM: device', deviceName)
  try {
    const uri = `./${deviceName}/config`;
    device = require(`./${deviceName}/config`).default;
    console.log("Retrieving config from: ", uri);
  } catch(e) {
    const defaultDeviceName = "workstation";
    device = require(`./${defaultDeviceName}/config`).default;
    console.warn(`No config package found for device ${deviceName}, using default`, e);
  }
  return device;
}
*/

class Device {
  /**
   * Detect the current device and return an singleton instance
   * @return {AbstractDevice}
   */
  static getDevice() {
    if (Device.device) {
      return Device.device;
    }
    let y = new (new DeviceDetection().detect())(); // goose -- se agregan parentesis por warning
    console.log("[Device] getDevice", y);

    // Después del detect, lanzar el device setup si aplica
    // let defClass = require(`./${y.platform}/DeviceSetup`).default;
    let defClass = require(`./${y.platform}/DeviceSetup`).default;
    let objClass = new defClass(); // goose -- se agregan parentesis por warning
    //console.log('[Device] data..\ndetect=',y,'\nfile=',`./${y.platform}/DeviceSetup`,'\ndefClass=',defClass);
    //console.log('[DEVICE] objClass',objClass);

    // let rest = objClass.setup().catch((err)=>{
    objClass.setup(); //.catch(err => {
    //      console.error("[Device] Device.getDevice ERROR -- ", err);
    //   });

    // Importante: la carga adicional de scripts propios del device
    // es asíncrono (para no interrumpir la carga inicial de la app),
    // pero entonces considerar-doble check si se necesita hacer uso
    // de estos scripts justo cuando se carguen las demás utils del device
    // Al hacer el new abstractDevice abajo
    Device.device = y;

    return Device.device;
  }
}

export default Device;

/*
import DeviceDetection from '../utils/DeviceDetection';

/*
function getDeviceConfig() {
  let device = null;
  const detection = new DeviceDetection;
  const deviceName = detection.getDevice("device");
  console.log('EDM: device', deviceName)
  try {
    const uri = `./${deviceName}/config`;
    device = require(`./${deviceName}/config`).default;
    console.log("Retrieving config from: ", uri);
  } catch(e) {
    const defaultDeviceName = "workstation";
    device = require(`./${defaultDeviceName}/config`).default;
    console.warn(`No config package found for device ${deviceName}, using default`, e);
  }
  return device;
}
* /

class Device {

  /**
  * Detect the current device and return an singleton instance
  * @return {AbstractDevice}
  * /
  static getDevice () {
    if (Device.device) {
      return Device.device;
    }
    let y =  new (new DeviceDetection().detect());
    console.log('[Device] getDevice', y);

    // Después del detect, lanzar el device setup si aplica
    let defClass = require (`./${y.platform}/DeviceSetup`).default;
    let objClass = new (defClass);
    let rest = objClass.setup();

    // Importante: la carga adicional de scripts propios del device
    // es asíncrono (para no interrumpir la carga inicial de la app),
    // pero entonces considerar-doble check si se necesita hacer uso
    // de estos scripts justo cuando se carguen las demás utils del device
    // Al hacer el new abstractDevice abajo
    Device.device = y;

    return  Device.device;
  }
}

export default Device;
*/
