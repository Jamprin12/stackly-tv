/**
 * Class Log
 * Manejo de log de errores para NewRelic
 */
import LocalStorage from "../components/DeviceStorage/LocalStorage";

class Log {

    /**
     * Logs an error into NewRelic
     * @param {Error} err error object
     * @param {Object} data datos extra (tag,params,url,etc.)
     */
    static error(err,tag,data={}){
        // Into NewRelic
        if (typeof window.newrelic === 'object') {
            window.newrelic.noticeError(err,{tag:tag,data:data});
        }    
    }

    static info(...parameters){
        if(LocalStorage.getItem('debug')){
            console.log('[INFO]',parameters)
        }
    }
}

export default Log;
