var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var nodeExternals = require('webpack-node-externals');

const path = require('path');

var isProduction = process.env.NODE_ENV === 'production';
var productionPluginDefine = isProduction ? [
  new webpack.DefinePlugin({'process.env': {'NODE_ENV': JSON.stringify('production')}})
] : [];
var clientLoaders = isProduction ? productionPluginDefine.concat([
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false }, sourceMap: false })
]) : [];

module.exports = [
  {
    entry: './src/server.js',

    devtool: 'eval-source-map',

		output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'server.js',
      libraryTarget: 'commonjs2',
      publicPath: '/'
    },

		target: 'node',
    node: {
      console: false,
      global: false,
      process: false,
      Buffer: false,
      __filename: false,
      __dirname: false
		},

		module: {
			rules: [
				{
          test: /\.js$/,
          loader: 'babel-loader',
          options: {
            "plugins": [
              "transform-object-rest-spread",
              "transform-async-to-generator",
              [
                "babel-plugin-transform-runtime",
                {
                  "regenerator": true
                }
              ]
            ]
          }
				},
				{
					test: /\.json$/,
					loader: 'json-loader'
				}
			]
		},

    externals: nodeExternals(),
    plugins: productionPluginDefine,
  },
  {
		entry: './src/app/browser.js',
		resolve: {
      extensions: ['.js', '.jsx']
		},

		output: {
      path: path.resolve(__dirname, 'dist/assets'),
      publicPath: '/',
      filename: 'bundle.js'
		},

    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract('css!sass')
        }
      ]
		},

		plugins: clientLoaders.concat([
      new ExtractTextPlugin('index.css', {
        allChunks: true
      })
    ])
  }
];
