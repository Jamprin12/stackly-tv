import { Router }             from 'express';
import validate               from 'express-validation';
import axios                  from 'axios';
import Utils                  from '../requests/Utils';
import Minimize               from 'minimize';


import webInitiatorValidationRules from './validation/web-initiator';

const router = new Router();

router.get("/", validate(webInitiatorValidationRules), (req, res)  => {
  const {challenge, deviceid, manifest, laurl, provider, islive} = req.query;
  console.log('FERNANDO MIDDLEWARE >>>> ', req)
  axios.get(manifest)
    .then((result) => {
      const xml = result.data;
      const object = Utils.xmlToObject(xml);
      const protectionObject = Utils.getProtectionData(object);
      let header = {
        LA_URL: 'https://proxy.claro03.uat.verspective.net/amco-uat/playready',
        KID: protectionObject && protectionObject.DATA && protectionObject.DATA.KID || 'yCSMJ7F38PEUI6rD5McR4g==', // Prueba para tizen
        CHECKSUM: protectionObject && protectionObject.DATA && protectionObject.DATA.CHECKSUM || 'A6FHg3YfpDo=',  // Prueba para tizen
      }, customString;
      let challenge_bs = '';
      switch (provider) {
          case 'NOW':
            customString = {
              customdata: challenge ? JSON.parse(challenge) : '',
              device_id: deviceid
            };
            header.LA_URL = 'https://proxy.claro03.uat.verspective.net/amco-uat/playready?privatedata=' + `${customString.customdata.token}` + '&deviceuniqueid=' + `${customString.device_id}`;
          break;
          case 'AMCO':
            customString = JSON.stringify({
              customdata: challenge ? JSON.parse(challenge) : '',
              device_id: deviceid
            });
            header.LA_URL = laurl;
            challenge_bs = Buffer(customString).toString('base64');
          break;
      }
      const xmlResponse = Utils.createXMLFromObject(Utils.createPlayReadyInitiator(header, manifest, challenge_bs));
      const minimize = new Minimize({dom: {xmlMode: true, lowerCaseTags: false}, quotes: true});
      minimize.parse(xmlResponse, (error, data) => {
        res = Utils.setCacheHeader(res);
        res.set('Content-Type', 'application/vnd.ms-playready.initiator+xml');
        res.set('Access-Control-Allow-Origin', '*');
        res.send(data);
      });
    }).catch((e) => {
      res.json({
        mensaje: 'Ups No se puede reproducir',
        video_url: e.config.url,
        code: e.response.status,
        extra: e.response.statusText
      })
    })
});
export default router;
