import Joi from 'joi';

export default  {
  query: {
    challenge : Joi.any().optional(),
    deviceid  : Joi.string().required(),
    manifest  : Joi.string().required(),
    //TODO syntax below does not work, check Joi API
    //laurl     : Joi.string().when('provider', {is: Joi.exist(), then: Joi.string().optional(), otherwise: Joi.string().required() }),
    provider  : Joi.string().optional(),
    laurl     : Joi.any().optional(),
    islive    : Joi.any().optional(),
  }
};
