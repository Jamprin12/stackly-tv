exports.requiredParams = function(p) {
    let config =  {
        api_version:          p.api_version,
        authpn:               p.authpn,
        authpt:               p.authpt,
        format:               p.format,
        region:               p.region,
        device_id:            p.device_id,
        device_category:      p.device_category,
        device_model:         p.device_model,
        device_type:          p.device_type,
        device_manufacturer:  p.device_manufacturer,
        HKS:                  p.HKS,
        // filter_id:            p.filter_id || "",
    };
    return config;
}
