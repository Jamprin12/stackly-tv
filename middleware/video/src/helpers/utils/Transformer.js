class Transformer {

  transform(model) {
    throw new Error('You have to implement the method transform!');
  }

  collection(collection, params = {}) {
    if (!collection) {
      return [];
    }

    let newCollection = [];

    for (let i = 0; i < collection.length; i++) {
      const transform = this.transform(this.getModel(collection[i]), params);
      newCollection.push(transform);
    }

    return newCollection;
  }

  /**
   * Slot para override el formato
   * de cada item (para V6)
   * @param {object} item 
   */
  getModel(item){ // inicialmente, el que viene
    return item;
  }


  item(model) {
    return this.transform(model);
  }
}

export default Transformer;
