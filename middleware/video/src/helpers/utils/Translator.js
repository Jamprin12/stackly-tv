class Translator {
    constructor(data, country) {
        this.dictionary = data;
        this.country = country;
    }

    setCountry(country) {
        this.country = country;
    }

    setDictionary(dictionary) {
        this.dictionary = dictionary;
    }

    get(key) {
        return this.dictionary[this.country][key];
    }
}

export default Translator;