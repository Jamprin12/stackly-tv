function levelParser(data) {
    const defaultType = 'carrousel';

    for (let i = 0; i < data.length; i++) {
        const components = [];

        if (data[i].type === 'carrouseldoble') {
            const component = data[i];
            const subComponents = component.components.component;
            let newComponent = [];

            for (let e = 0; e < subComponents.length; e++) {
                const subComponent = subComponents[e];

                newComponent.push(subComponent);

                if (subComponent.type === 'Carrouselhorizontal'
                    || subComponent.type === 'Carrouselvertical'
                ) {
                    components.push({
                        name: data[i].name + '_' + subComponent.name,
                        type: defaultType,
                        components: {
                            component: newComponent
                        }
                    });

                    newComponent = [];
                }
            }

            for (let e = 0; e < components.length; e++) {
                let deleteItems = 0;
                if (e === 0) {
                    deleteItems = 1;
                }

                data.splice(i, deleteItems, components[e])
            }
        }
    }

    return data;
}

  function addParamsUrl (url, params) {
    if (url.indexOf("?") !== -1) {
      return url + "&" + toQueryStringWithOutEncode(params);
    }
    return url + "?" + toQueryStringWithOutEncode(params);
  }

function getFlags(data) {
    const flags = [];

    for (let i = 0; i < data.length; i++) {
        const ribbon = data[i];

        if (!ribbon.byUser && typeof ribbon.url === 'string') {
            flags.push({
                index: i,
                url: ribbon.url,
                flag: false,
                recommender: (ribbon.id.toLowerCase().indexOf('recomendador') >= 0)
            });
        }
    }

    return flags;
}

function getByUserTotal(data) {
    let byUser = false;

    for (let i = 0; i < data.length; i++) {
        if (data[i].byUser) {
            byUser = true;
            break;
        }
    }

    return byUser;
}

function launchResponse(flags) {
    let launch = true;

    for (let i = 0; i < flags.length; i++) {
        if (!flags[i].flag) {
            launch = false;
            break;
        }
    }

    return launch;
}

function getItems(data, configParams) {
    console.error('===============');
    for (let i = 0; i < flags.length; i++) {
        const itemTask = new ItemsTask(flags[i].url, configParams);

        new RequestManager.addRequest(itemTask).then((result) => {
            const data = result.data;

            console.error('edc:wwwwwwwww', i);
            console.error(flags[i].index);
            //console.error('edc::::::::::', data);
            console.error(flags[i].url);

            if (!data || data.errors) {
                console.error('edc:error getting items');
            }
        });
    }

    return data;
}

function accentDecode(tx) {
    let rp = String(tx);

    rp = rp.replace(/&aacute;/g, 'á');
    rp = rp.replace(/&eacute;/g, 'é');
    rp = rp.replace(/&iacute;/g, 'í');
    rp = rp.replace(/&oacute;/g, 'ó');
    rp = rp.replace(/&uacute;/g, 'ú');
    rp = rp.replace(/&ntilde;/g, 'ñ');
    rp = rp.replace(/&uuml;/g, 'ü');

    rp = rp.replace(/&Aacute;/g, 'Á');
    rp = rp.replace(/&Eacute;/g, 'É');
    rp = rp.replace(/&Iacute;/g, 'Í');
    rp = rp.replace(/&Oacute;/g, 'Ó');
    rp = rp.replace(/&Uacute;/g, 'Ú');
    rp = rp.replace(/&Ñtilde;/g, 'Ñ');
    rp = rp.replace(/&Üuml;/g, 'Ü');

    rp = rp.replace(/&nbsp;/g, ' ');

    return rp;
}

function parserGuideTv(data) {
    data.map((it, i) => {
        it.group_id = it.id;
        const key = `${it.code}_asset`;
        it.key = key;
        it.title = it.text;
    })

    return data;
}

function toQueryString(obj) {
  return Object.keys(obj).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(obj[key])).join('&');
} 

  function toQueryStringWithOutEncode(obj) {
    return Object.keys(obj).map(key => key + '=' + obj[key]).join('&');
  } 

  function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


export default {
    levelParser,
    getByUserTotal,
    getFlags,
    getItems,
    launchResponse,
    accentDecode,
    parserGuideTv,
    toQueryString,
    addParamsUrl,
    isEmpty
}
