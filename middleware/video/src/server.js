import newrelic from "newrelic";
import express from "express";
import React from "react";
import { renderToString } from "react-dom/server";
import App from "./app";
import template from "./template";

import epgRoutes from "./epg/routes";
import epgRoutesNew from "./epg/routes/index2";
import levelRoutes from "./level/routes";
import levelRoutes2 from "./level/routes/index2";
import levelUserRoutes from "./leveluser/routes";
import serie from "./serie/routes";
import badgesRoutes from "./badges/routes";
import compression from "compression";
import webinitiator from "./webinitiator/routes";
import drmurl from "./drmurl/routes";
import manifestlanguages from "./manifestlanguages/routes";
import pkg from "./../../../package.json";
import config from "./../src/config/default";
import {getApiEndpoint} from "./../src/config/middleware"; // MIDAPI config
import healthCheck from "./healthCheck/routes";
import channelsCheck from "./channelsCheck/routes";
import checkAppVersion from "./checkAppVersion/routes";
import successfulReset from "./successfulReset/routes";

const PORT = 4000;

const api = express();
const server = express();
server.use(compression());
server.use((req, res, next) => {
  // ----------------------------------
  // NO COMENTAR !!!!!! -- middleware para environments
  // ----------------------------------
  // INYECCION DE DEPENDENCIA: config
  req.config = config; // inyeccion de dependencia
  getApiEndpoint(req);
  console.log(
    "[INCOMMING]:",
    req.config.endPoints.guessedEnv, " | ",
    req.hostname, " | ",
    req.originalUrl, " | ",
    JSON.stringify(req.config)
  ); // resultado
  // ----------------------------------
  res.setHeader("MiddlewareVersion", pkg.version);
  res.setHeader("MiddlewareEntorno", req.config.endPoints.guessedEnv);
  res.setHeader("microfwk", config.endPoints.microfwk);
  res.setHeader("microfwkAPA", config.endPoints.microfwkAPA);
  next();
});

api.use("/assets", express.static("assets"));
api.use("/epg", epgRoutes);
api.use("/epgNew", epgRoutesNew);
api.use("/level", levelRoutes);
api.use("/level2", levelRoutes2);
api.use("/level-user", levelUserRoutes);
api.use("/serie",serie);
api.use("/badges", badgesRoutes);
api.use("/web-initiator", webinitiator);
api.use("/drm-url", drmurl);
api.use("/manifest-languages", manifestlanguages);
api.use("/healthCheck", healthCheck);
api.use("/channelsCheck", channelsCheck);
api.use("/check-app-version", checkAppVersion);
api.use("/successful-reset", successfulReset);

server.use("/webapi-video", api);

server.get("/", (req, res) => {
  const isMobile = false;
  const initialState = { isMobile };
  const appString = renderToString(<App {...initialState} />);

  res.send(
    template({
      body: appString,
      title: "Hello World from the server",
      initialState: JSON.stringify(initialState)
    })
  );
});

server.listen(PORT);
console.log("[middleware] Environment:", process.env.NODE_ENV);
console.log("[middleware] Listening on port", PORT, "..");
