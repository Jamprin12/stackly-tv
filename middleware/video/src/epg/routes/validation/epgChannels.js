var Joi = require('joi');

module.exports = {
  query: {
    // Config Required Params
    api_version:         Joi.string().required(),
    authpn:              Joi.string().required(),
    authpt:              Joi.string().required(),
    format:              Joi.string().required(),
    region:              Joi.string().required(),
    device_id:           Joi.string().required(),
    device_category:     Joi.string().required(),
    device_model:        Joi.string().required(),
    device_type:         Joi.string().required(),
    device_manufacturer: Joi.string().required(),
    HKS:                 Joi.string().required(),

    // EPG Component
    date_to:           Joi.string().required(),
    filter_inactive:   Joi.string().required(),
    date_from:         Joi.string().required(),
    quantity:          Joi.string().required(),
    node_id:           Joi.string().empty('').optional(),
    infinite_fix:      Joi.string().empty('').optional(),
    container_width:   Joi.string().required(),
    visible_time:      Joi.string().required(),
    interval_size:     Joi.string().required(),
    horizontal_blocks: Joi.string().required(),
    now:               Joi.string().required(),
    event_image:       Joi.boolean().optional(),
    renderForceUpdate: Joi.boolean().optional(),
    epg_version:       Joi.string().optional(),
    soa_version:       Joi.string().optional(),
  }
};
