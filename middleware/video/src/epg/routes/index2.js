import { Router } from "express";
import NodeCache from "node-cache";

import EpgChannels from "../requests/EpgChannels";
import RequestManager from "../../requests/RequestManager";
import moment from "moment";

var momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);

const router = Router();

const resizeImage = (src, width, height) => {
  if (!src) return null;

  if (src.indexOf("?") > 0) {
    src = src.substring(0, src.indexOf("?"));
  }

  let result = src.includes("assetsnx")
    ? `${src}?size&w=${width}`
    : src.includes("clarovideocdn")
    ? width < 500
      ? `${src}?size&imwidth=${width}`
      : `${src}?size=${width}x${height}`
    : src;

  return result;
};

const myCache = new NodeCache({ stdTTL: 100, checkperiod: 600 });

const getEvents = async (req, configParams, initial = false) => {
  const dateFrom = moment(configParams.date_from, "YYYYMMDDHHmmss");
  const dateTo = moment(configParams.date_to, "YYYYMMDDHHmmss");

  const channelsTask = new EpgChannels(req, configParams);
  const result = await new RequestManager.addRequest(channelsTask);

  const final =
    result &&
    result.data &&
    result.data.response.channels.map((item, indexCh) => {
      return {
        id: item.id,
        group_id: item.group_id,
        image: item.image,
        name: item.name,
        number: item.number,
        index: indexCh,
        events: [].concat(
          ...item.events.map((event, i) => {
            let ignore = false;
            let diference = 0;
            let dateEvent = moment(event.date_begin, "YYYY/MM/DD HH:mm:ss");
            let dateEventEnd = moment(event.date_end, "YYYY/MM/DD HH:mm:ss");

            if (i === 0 && dateEvent.isBefore(dateFrom, "seconds")) {
              diference = dateEvent.diff(dateFrom, "minutes");
            }

            let duration = moment
              .duration(event.duration, "HH:mm:ss")
              .asMinutes();

            if (initial) {
              duration = duration + diference;
            }

            // if (!initial && dateEventEnd.isAfter(dateTo, "seconds")) {
            if (dateEventEnd.isAfter(dateTo, "seconds")) {
              ignore = true;
            }

            const maxDuration = 180;

            // divido en varios eventos los que duran mas de maxDuration minutos
            let prueba = Math.ceil(duration / maxDuration);

            var result = [];
            let duration2 = duration;
            for (let y = 0; y <= prueba - 1; y++) {
              duration2 = duration - y * maxDuration;
              const prueba2 = duration2 > maxDuration ? maxDuration : duration2;

              const dateBegin = moment(
                event.date_begin,
                "YYYY/MM/DD HH:mm:ss"
              ).add(y * maxDuration, "minutes");

              const dateEnd = dateBegin.clone();
              dateEnd.add(prueba2, "minutes");

              result.push({
                id: event.id,
                ignore,
                title: event.name,
                description: event.description,

                // time: `${moment(event.date_begin, "YYYY/MM/DD HH:mm:ss").format(
                //   "HH:mm"
                // )} - ${moment(event.date_end, "YYYY/MM/DD HH:mm:ss").format(
                //   "HH:mm"
                // )}`,
                // date_begin: event.date_begin,
                // date_end: event.date_end,
                // date_end: dateBegin.format("YYYY/MM/DD HH:mm:ss"),
                // duration: duration,
                // width: duration * 6,

                time: `${dateBegin.format("HH:mm")} - ${dateEnd.format(
                  "HH:mm"
                )}`,
                date_begin: dateBegin.format("YYYY/MM/DD HH:mm:ss"),
                date_end: dateEnd.format("YYYY/MM/DD HH:mm:ss"),
                duration: prueba2,
                width: prueba2 * 6,
                image: resizeImage(
                  event.ext_eventimage_name_horizontal,
                  1280,
                  720
                ),
                image2: resizeImage(
                  event.ext_eventimage && event.ext_eventimage.iconic_16_9,
                  1280,
                  720
                ),
                language: event.language,
                genre: event.genres && event.genres.genre_name,
                year: event.ext_year,
                rating: event.parental_rating,
              });
            }
            return result;
          })
        ),
      };
    });

  return final;
};

router.get("/", async (req, res) => {
  const configParams = req.query;

  const dateFrom = moment(configParams.date_from, "YYYYMMDDHHmmss").add(
    1,
    "days"
  );
  const dateTo = moment(configParams.date_to, "YYYYMMDDHHmmss").add(1, "days");

  const params02 = {
    ...configParams,
    date_from: dateFrom.format("YYYYMMDDHHmmss"),
    date_to: dateTo.format("YYYYMMDDHHmmss"),
  };

  const dateFrom03 = moment(configParams.date_from, "YYYYMMDDHHmmss").add(
    2,
    "days"
  );
  const dateTo03 = moment(configParams.date_to, "YYYYMMDDHHmmss").add(
    2,
    "days"
  );

  const params03 = {
    ...configParams,
    date_from: dateFrom03.format("YYYYMMDDHHmmss"),
    date_to: dateTo03.format("YYYYMMDDHHmmss"),
  };

  // let final = myCache.get("myKey");
  let final = undefined;

  if (final == undefined) {
    // final = await getEvents(configParams, true);

    const [r1, r2, r3] = await Promise.all([
      getEvents(req, configParams, true),
      getEvents(req, params02),
      getEvents(req, params03),
    ]);

    let posEvent = 0;
    final = r1.map((ch, i) => {
      const events01 = ch.events;
      const events02 = r2[i].events;
      const events03 = r3[i].events;

      return {
        ...ch,
        events: [...events01, ...events02, ...events03]
          .map((e, i2, arr) => {
            let last = false;
            if (arr.length - 1 == i2) {
              last = true;
            }
            return {
              ...e,
              last,
              channel: {
                id: ch.id,
                group_id: ch.group_id,
                image: ch.image,
                name: ch.name,
                number: ch.number,
                index: ch.index,
              },
            };
          })
          .filter((item) => item.ignore == false),
      };
    });

    // final = result;
    myCache.set("myKey", final, 600);
  }

  res.header("Access-Control-Allow-Origin", "*");
  res.status(200).json({
    response: final || [],
  });
});

module.exports = router;
