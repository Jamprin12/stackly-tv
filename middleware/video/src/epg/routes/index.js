import { Router }            from 'express';
import validate              from 'express-validation';


import epgChannelsValidation from './validation/epgChannels';
import EpgChannels           from '../requests/EpgChannels';
import epgParser             from '../requests/parsers/epg';
import EpgResponseFactory    from '../requests/EpgResponseFactory';
import confParser            from '../../helpers/parsers/requiredParams';
import RequestManager        from '../../requests/RequestManager';

const router = Router();

router.get('/', validate(epgChannelsValidation), (req, res) => {

    const configParams = confParser.requiredParams(req.query);
    const epgParams    = epgParser.epgParams(req.query);
    if(epgParams.renderForceUpdate == undefined){
      delete epgParams.renderForceUpdate;
    }
    if(epgParams.epg_version == 'false'){
      delete epgParams.epg_version;
    }
    if(epgParams.soa_version == 'false'){
        delete epgParams.soa_version;
    }
    epgParams.metadata = 'full'; // Force common largo
    const channelsTask = new EpgChannels(epgParams, configParams);

    new RequestManager.addRequest(channelsTask).then((result) => {
        const data = result.data;
        let counter = parseInt(epgParams.from);
        res.header("Access-Control-Allow-Origin", "*");
        if ( !data ||  data.errors ) {
          res.status(400).json(data);
          return;
        }
        data.response.channels.map((channel) => {
            let tmp = [];
            channel.key     = counter;
            counter++;

            channel.encodes = [];
                      if(channel.group
                        && channel.group.common
                        && channel.group.common.extendedcommon
                        && channel.group.common.extendedcommon.media
                        && channel.group.common.extendedcommon.media.language
                        && channel.group.common.extendedcommon.media.language.options
                        && channel.group.common.extendedcommon.media.language.options.option
                        && channel.group.common.extendedcommon.media.language.options.option[0]
                      ) {
                        channel.encodes = channel.group.common.extendedcommon.media.language.options.option[0].encodes;
                        delete channel.group.common.extendedcommon;
                      }




            /*channel.events  = channel.events.filter((event) => {
                const key = event.date_begin + event.date_end;
                const indexTmp = tmp.indexOf(key);
                if (indexTmp !== -1) {
                    return false;
                }
                tmp.push(key);
                return true;
            });*/
            return channel;
        });

        if(epgParams.infinite_fix && epgParams.infinite_fix != "0") {
            /*epgParams.from = counter;
            //counter = parseInt(epgParams.from);*/
            epgParams.from = 0;
            epgParams.metadata = 'full';
            let counter = parseInt(epgParams.from);
            epgParams.quantity = epgParams.infinite_fix;
            new RequestManager.addRequest(new EpgChannels(epgParams, configParams)).then((result) => {
                const fixData = result.data;
                if (!fixData || fixData.errors) {
                    res.status(400).json(fixData);
                    return;
                }
                fixData.response.channels.map((channel) => {
                    let tmp = [];
                    channel.key     = counter;
                    channel.encodes = [];
                      if(channel.group
                        && channel.group.common
                        && channel.group.common.extendedcommon
                        && channel.group.common.extendedcommon.media
                        && channel.group.common.extendedcommon.media.language
                        && channel.group.common.extendedcommon.media.language.options
                        && channel.group.common.extendedcommon.media.language.options.option
                        && channel.group.common.extendedcommon.media.language.options.option[0]
                      ) {
                        channel.encodes = channel.group.common.extendedcommon.media.language.options.option[0].encodes;
                        delete channel.group.common.extendedcommon;
                      }
                    counter++;
                    /*channel.events = channel.events.filter((event) => {
                        const key = event.date_begin + event.date_end;
                        const indexTmp = tmp.indexOf(key);
                        if (indexTmp !== -1) {
                            return false;
                        }
                        tmp.push(key);
                        return true;
                    });*/
                    return channel;
                });

                data.response.channels = [...data.response.channels, ...fixData.response.channels];
                const response = new EpgResponseFactory(data, epgParams);
                res.status(200).json(response.make());
            });
        } else {
            const response = new EpgResponseFactory(data, epgParams);
            res.status(200).json(response.make());
        }
    });
});

module.exports = router;
