/**
 * Dependencies
 */
import React from 'react';

/**
 * Components
 */
import Channel from "./channel";

const ChannelList = (props) => {
    return (
        <div className='epg-channel-item-wrapper'>
            {
                props.channels.map((channel, index) => {
                    return <Channel key={index} channel={channel}/>
                })
            }
        </div>
    );
};

export default ChannelList;