import React from 'react';
import Event from "./event";
import {calcMinuteSize, calcWidth} from "./utils";
import m from 'moment';

const EventRow = (props) => {
    const {
        channel,
        date_from,
        date_to,
        container_width,
        interval_size,
        visible_time,
    } = props;

    const minuteSize = calcMinuteSize(container_width, visible_time, interval_size);
    const start = m(date_from, 'YYYYMMDDHHmmss');
    const end   = m(date_to, 'YYYYMMDDHHmmss');
    const difference = end.diff(start, 'minutes');
    const width = calcWidth(difference, minuteSize);

    const styles = {
        width,
    };

    const lastIndex = channel.events.length - 1;
    return (
        <div className='epg-events-row' style={styles}>
            {
                channel.events.map((event, index) => {
                    let first= false, last = false;
                    if (index === 0) {
                        first = true;
                    }
                    if (index === lastIndex) {
                        last = true;
                    }
                    const props = {
                        event,
                        date_from,
                        date_to,
                        container_width,
                        interval_size,
                        visible_time,
                        first,
                        last
                    };
                    return <Event key={index} {...props}/>;
                })
            }
        </div>
    );
};

export default  EventRow;