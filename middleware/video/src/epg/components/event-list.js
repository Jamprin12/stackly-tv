
import React from 'react';
import m from 'moment';
import classNames from 'classnames';

const percentOf = (duration, total) => duration / total;

const EventList = (props) => {
    const startTime = m(props.start, props.format);
    const endTime = m(props.end, props.format);
    const visibleMinutes = endTime.diff(startTime, 'minutes');
    const rowWidth = props.width;
    const imageSize = 110;
    const paddingLeft = 12;
    const rowStyles = {
        width: `${rowWidth}px`,
    };
    return (
        <div className="horarios xdk-sh scroller-handler">
            {
                props.channels.map((channel, indexChannel) => {
                    let totalWidth = 0;
                    return <div id={channel.id} key={indexChannel} className='epg-events-row xdk-c xdk-grid' style={rowStyles}>
                        {
                            channel.events.map((event, index, arr) => {

                               /*  const eventDurationParsed = `${
                                    m(event.date_begin, 'YYYY/MM/DD HH:mm:ss').format('HH.mm')}hs. a ${
                                    m(event.date_end, 'YYYY/MM/DD HH:mm:ss').format('HH.mm')}hs.`; */
                                const eventDurationParsed = `${
                                    m(event.date_begin, 'YYYY/MM/DD HH:mm:ss').format('HH.mm')} - ${
                                    m(event.date_end, 'YYYY/MM/DD HH:mm:ss').format('HH.mm')}`;
                                // Calculates widht in minutes (1 minute = 1 pixel)
                                let marginLeft = 0;
                                let duration = m.duration(event.duration).asMinutes();
                                const percent = percentOf(duration, visibleMinutes);
                                let diffWidth = 0;

                                /**
                                 *
                                 * When is the firs event on row, and it was began before the initial time to fetch
                                 * Calculate negative difference and substract it from event width.
                                 *
                                 */
                                if (index === 0) {
                                    const eventInit = m(event.date_begin, 'YYYY/MM/DD HH:mm:ss');
                                    const diff = eventInit.diff(startTime, 'minutes');
                                    if (diff < 0) {
                                        diffWidth += percentOf(diff, visibleMinutes) * rowWidth;
                                    }
                                }

                                if (index === arr.length - 1) {
                                    const eventFinish = m(event.date_end, 'YYYY/MM/DD HH:mm:ss');
                                    const diff = endTime.diff(eventFinish, 'minutes');
                                    if (diff < 0) {
                                        diffWidth += percentOf(diff, visibleMinutes) * rowWidth;
                                    }
                                }

                                const width = (rowWidth * percent) + diffWidth;
                                totalWidth += width;

                                const eventClass = classNames({
                                    'xdk-e': true,
                                    'epg-event-item-container': true
                                });

                                const image = (typeof event.ext_eventimage_name === 'undefined')
                                    ? '/images/epg_thumb_default.png'
                                    : event.ext_eventimage_name;
                                let offsetLeft = imageSize + paddingLeft;
                                let background = `url("${image.replace('https', 'http')}"), url("/images/epg_thumb_default.png")`;
                                if (imageSize > width/2) {
                                    offsetLeft = (paddingLeft > width/2) ? 0 : paddingLeft;
                                    background = 'none';
                                }
                                const containerStyles = {
                                    width: width,
                                    backgroundImage: background,
                                    backgroundSize: `${imageSize}px 100%`,
                                    backgroundRepeat:'no-repeat',
                                    backgroundPosition:'0',
                                    paddingLeft: `${offsetLeft}px`,
                                    marginLeft: `${marginLeft}px`,
                                };

                                // Items row
                                return (
                                    <div data={index} key={index} className={eventClass} style={containerStyles}>
                                        <div className='epg-event-item-label'>
                                            <div className='epg-event-item-title'>
                                                <span title={event.name}>
                                                    {event.name}
                                                </span>
                                            </div>
                                            <div className='epg-event-item-info'>
                                                <span title={eventDurationParsed}>
                                                    {eventDurationParsed}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                })
            }
        </div>
    );
};

export default EventList;
