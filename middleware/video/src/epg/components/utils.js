const percentOf = (duration, total) => duration / total;
/**
 * Placeholder image
 * @type {string}
 */
const defaultImage  = '/images/epg_thumb_default.png';

/**
 *
 * @param duration {number}
 * @param minuteSize {number}
 * @return {number}
 */
const calcWidth     = (duration, minuteSize = 10) => duration * minuteSize;

/**
 * @param duration {number} Event Duration in hours
 * @param visibleTime {number} Visible time in hours
 * @param minuteSize {number} Minute size in pixels
 */
const calcSizes     = (duration, visibleTime = 2, minuteSize = 10) => {
    const sizes     = [];
    if (duration > visibleTime) {
        const sections = Math.floor(duration / visibleTime);
        const partial = 60 * (duration % visibleTime);
        let i = 0;
        do {
            sizes.push(calcWidth(visibleTime * 60, minuteSize));
            i++;
        } while (i < sections);

        if (partial) {
            sizes.push(calcWidth(partial, minuteSize));
        }
    } else {
        sizes.push(calcWidth(duration * 60, minuteSize));
    }
    return sizes;
};

/**
 * @param containerWidth
 * @param visibleTime
 * @param intervalSize
 * @returns {number}
 */
const calcMinuteSize = (containerWidth = 1126, visibleTime = 120 ,intervalSize = 30) => {
    return (containerWidth * percentOf(intervalSize, visibleTime))/ intervalSize;
};

export {
    percentOf,
    defaultImage,
    calcWidth,
    calcSizes,
    calcMinuteSize
};
