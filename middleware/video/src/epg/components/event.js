/**
 * Dependencies
 */
import React from 'react';
import m from 'moment';
import classNames from 'classnames';

import {
    defaultImage,
    calcSizes,
    calcMinuteSize
} from './utils';

const Event = (props) => {
    const { event, now, date_from, date_to, container_width, interval_size, visible_time, first, last, event_image, group_id } = props;
    const from              = m(date_from, 'YYYYMMDDHHmmss');
    const to                = m(date_to, 'YYYYMMDDHHmmss');
    const start             = m(event.date_begin, 'YYYY/MM/DD HH:mm:ss');
    const end               = m(event.date_end, 'YYYY/MM/DD HH:mm:ss');
    let isBefore            = false;
    const isCurrent         = now.isBetween(start, end, null, '[)');
    if (isCurrent) {
      isBefore              = false;
    } else {
      isBefore              = now.isAfter(start);
    }
    const image             = event.ext_eventimage_name.replace('https', 'http');
    let duration            = m.duration({minutes: end.diff(start, 'minutes')}).asHours();
    // console.log('[EPG] EVENT', event)
    if (first && start.isBefore(from)) {
        duration -= from.diff(start, 'minutes') / 60;
    }
    if (last && end.isAfter(to)) {
        duration -= end.diff(to, 'minutes') / 60;
    }
    /* const durationParsed    = `${start.format('HH.mm')}hs. a ${end.format('HH.mm')}hs.`; */
    const durationParsed    = `${start.format('HH:mm')} - ${end.format('HH:mm')}`;
    const imageSize         = 125;
    const paddingLeft       = 12;
    const minuteSize        = calcMinuteSize(container_width, visible_time, interval_size);
    const sections          = calcSizes(duration, visible_time / 60, minuteSize);
    const eventClass        = classNames({
        'focusable': !isBefore && true,
        'epg-event-current': isCurrent,
        'epg-event-before': isBefore,
        'epg-event-item-container': true,
        // 'goose': isCurrent
    });

    let background          = event_image? image? `url(${image})` : `none` : `none`;
    let offsetLeft          = imageSize + paddingLeft;

    let idSpecial = isCurrent ? `currprog-${group_id}` : "";
    // console.log('\n\n\n-------------------------------------------\n[EVENT] -- \n',idSpecial, group_id);

    return (
        <div className="event" data-channel-group-id={group_id} style={{ display:'inline-block' }} id={idSpecial} data-image={event.ext_eventimage_name}>
            {
                sections.map((width, index) => {
                    if (!width) {
                        return null;
                    }
                    if (imageSize > width/2) {
                        offsetLeft = (paddingLeft > (width/2)) ? 0 : paddingLeft;
                        background = 'none';
                    }
                    const containerStyles = {
                        width: width,
                        //backgroundImage: background,
                        //backgroundSize: `${imageSize}px 100%`,
                        //backgroundRepeat:'no-repeat',
                        //backgroundPosition:'0',
                        //paddingLeft: background=='none'?'25px':offsetLeft,
                        paddingLeft: '15px',
                    };
                    return (
                        <a href="javascript:void(0)" key={index} id={`event-${event.id}-${index}`}
                             data-event-id={event.id}
                             data-channel-id={event.channel_id}
                             data-channel-group-id={group_id}
                             data-date={m(event.date_begin, 'YYYY/MM/DD').format('YYYY/MM/DD')}
                             className={eventClass}
                             style={containerStyles}>
                            <div className='epg-event-item-label'>
                                <div className='epg-event-item-title'>
                                    <span>{event.name}</span>
                                </div>
                                <div className='epg-event-item-info'>
                                    <span>{durationParsed}</span>
                                </div>
                            </div>
                        </a>
                    )
                })
            }
        </div>
    );
};


export default Event;
