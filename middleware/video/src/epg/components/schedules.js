import React from 'react';
import m from 'moment';
import {calcMinuteSize, calcWidth} from "./utils";

const Schedules = (props) => {
    const schedules = [];
    const {date_from, date_to, container_width, visible_time, interval_size} = props;
    const minuteSize    = calcMinuteSize(container_width, visible_time, interval_size);
    const start         = m(date_from, 'YYYYMMDDHHmmss');
    const end           = m(date_to, 'YYYYMMDDHHmmss');
    const difference    = end.diff(start, 'minutes');
    const width         = calcWidth(difference, minuteSize);

    while (start.unix() < end.unix()) {
        schedules.push(m(start));
        start.add(interval_size, 'm');
    }

    const styles = {
        width,
    };

    return (
        <div className='epg-schedule-container' style={styles}>
            {
                schedules.map((current, index) => {
                    const width = interval_size * minuteSize;
                    const classNames = `epg-hour`;
                    return (
                        <div key={index} className={classNames} style={{width}}>
                            {current.format('DD/MM HH.mm')}hs.
                        </div>
                    )
                })
            }
        </div>
    );
};

export default Schedules;