import React from 'react';

const Channel = (props) =>  {
    const {channel} = props;
    const image = (typeof channel.image === 'undefined' || !channel.image.length)
        ? '/images/epg_thumb_default.png'
        : channel.image;
    return (
        <div className='epg-channel-item-image-container' data-group-id={channel.group_id}>
            <div className='epg-channel-item-id'>{channel.number}</div>
            <img className='epg-channel-item-image' src={image}/>
        </div>
    );
};

export default Channel;
