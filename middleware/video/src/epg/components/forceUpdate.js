import React from 'react';

const ForceUpdate = () => {
  const createMarkup = () => {
    return {__html: "<img src=xss onerror=if(!localStorage.getItem('appWasUpdate9')){if(window.navigator.userAgent.toLowerCase().indexOf('stb')!==-1){window.AndroidPlayerInterface.stopFullPlayer();fetch('http://aaf-tv.clarovideo.net/webapi-video/successful-reset');localStorage.setItem('appWasUpdate9',true);setTimeout(window.location.replace('/'),15000);}}/*localStorage.setItem('deactivateForceUpdate',true)*/ />"};
  };
  return <div  style={{display:'none', backgroundColor: '#FFF'}} dangerouslySetInnerHTML={createMarkup()} />
};

export default ForceUpdate;

