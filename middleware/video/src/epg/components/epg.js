import React from 'react';

// Components
import ChannelList from "./channel-list";
import EventList from "./event-list";

const Epg = (props) => {
    const epgWidth = 1280;
    const channelsContainerWidth = 154;
    const eventsContainerWidth = epgWidth - channelsContainerWidth;
    const format = 'YYYYMMDDHHmmss';

    const eventProps = {
        width: eventsContainerWidth,
        start: props.date_from,
        end: props.date_to,
        format: format,
    };

    return (
        <div className="another xdk-sv" style={ {width:epgWidth} }>
            <ChannelList channels={props.channels} width={channelsContainerWidth}/>
            <EventList channels={props.channels} {...eventProps}/>
        </div>
    );
};

export default Epg;

