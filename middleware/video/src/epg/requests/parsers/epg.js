exports.epgParams = function(p) {
    let params =  {
      date_from:         p.date_from,
      date_to:           p.date_to,
      filter_inactive:   p.filter_inactive,
      from:              p.from,
      quantity:          p.quantity,
      node_id:           p.node_id,
      infinite_fix:    p.infinite_fix,
      container_width:   p.container_width,
      visible_time:      p.visible_time,
      interval_size:     p.interval_size,
      horizontal_blocks: p.horizontal_blocks,
      now:               p.now,
      event_image:       p.event_image,
      renderForceUpdate: p.renderForceUpdate,
      epg_version:       p.epg_version,
      full_epg:          p.full_epg,
      soa_version:       p.soa_version
    };
    return params;
}
