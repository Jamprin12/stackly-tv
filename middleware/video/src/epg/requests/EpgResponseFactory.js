/**
 * Dependencies
 */
import React                from 'react';
import { renderToString }   from 'react-dom/server';
import m                    from 'moment';
import {calcMinuteSize, calcWidth} from "../components/utils";
/**
 * Components
 */
import Channel from "../components/channel";
import ForceUpdate from "../components/forceUpdate";
import Schedules from "../components/schedules";
import Event from "../components/event";
import pkg from "../../../../../package.json";
class EpgResponseFactory {

    constructor(data, config = {}) {
        this.data = data;
        this.config = config;
    }

    make() {
        const { channels, total }   = this.data.response;
        const { entry }             = this.data;
        const {date_from, date_to, container_width, interval_size, visible_time} = this.config;
        const minuteSize = calcMinuteSize(container_width, visible_time, interval_size);
        const start = m(date_from, 'YYYYMMDDHHmmss');
        const end   = m(date_to, 'YYYYMMDDHHmmss');
        const difference = end.diff(start, 'minutes');

        // fix size of response
        const total_size_requested = entry.quantity;
        this.fixSize(channels, total_size_requested);
        const lastChannel = channels[channels.length - 1].number;

        return {
            entry,
            total,
            lastChannel,
            constants: {
                minute_size: minuteSize,
                row_width: calcWidth(difference, minuteSize),
            },
            schedules: this.buildSchedulesHTML(),
            channelsRender : this.buildChannelsHTML(channels),
            body: this.buildEventsHTML(channels),
            channels,
            appVersion: pkg.version,
        }
    }

    buildChannelsHTML(channels) {
        const data = [];
        channels.map((channel) => {
            data.push(renderToString(<Channel channel={channel}/>))
        });
        return data;
    }

    buildEventsHTML(channels) {
        let events = [];
        const {date_from, date_to, container_width, interval_size, visible_time, horizontal_blocks} = this.config;

        const minuteSize = calcMinuteSize(container_width, visible_time, interval_size);
        const start = m(date_from, 'YYYYMMDDHHmmss');
        const end   = m(date_to, 'YYYYMMDDHHmmss');
        const difference = end.diff(start, 'minutes');
        const size_blocks = difference/horizontal_blocks;

        channels.map((channel, channelIndex) => {
            const tmpEvents = [];
            events[channelIndex] = [];
            const init      = m(start);
            const finish    = m(init).add(size_blocks, 'm');
            while (finish.unix() <= end.unix()) {
                const filtered = this.filterEvents(channel.events, init, finish);
                const lastIndex = filtered.length - 1;
                const difference = finish.diff(init, 'minutes');
                const width = calcWidth(difference, minuteSize);
                const styles = {
                    width,
                };
                const block = (
                    <div className='epg-events-block' data-group-id={channel.group_id} id={`block-${init.format('YYYYMMDDHHmmss')}-${finish.format('YYYYMMDDHHmmss')}`} style={styles}>
                        {
                            filtered.map((event, index) => {
                                let first= false, last = false;
                                if (index === 0) {
                                    first = true;
                                }
                                if (index === lastIndex) {
                                    last = true;
                                }
                                const props = {
                                    event,
                                    now: m(this.config.now,'YYYYMMDDHHmmss'),
                                    date_from: init.format('YYYYMMDDHHmmss'),
                                    date_to: finish.format('YYYYMMDDHHmmss'),
                                    container_width,
                                    interval_size,
                                    visible_time,
                                    first,
                                    last,
                                    event_image:this.config.event_image,
                                    group_id:channel.group_id
                                };
                                return <Event key={index} {...props}/>;
                            })
                        }
                    </div>
                );
                //const eventsHtml = (this.config.renderForceUpdate || this.config.renderForceUpdate === undefined) ? renderToString(block)+renderToString(<ForceUpdate/>) : renderToString(block)
                //events[channelIndex].push(renderToString(block)+renderToString(<ForceUpdate/>));
                events[channelIndex].push(renderToString(block));
                tmpEvents.push(filtered);
                init.add(size_blocks, 'm');
                finish.add(size_blocks, 'm');
            }
            channel.events = tmpEvents;
            return channel;
        });
        return events;
    }

    buildSchedulesHTML() {
        const {date_from, date_to, container_width, interval_size, visible_time} = this.config;
        const props = {
            date_from,
            date_to,
            container_width,
            interval_size,
            visible_time,
        };
        return renderToString(<Schedules {...props}/>);
    }

  filterEvents(events, startTime, endTime) {
    if (events !== undefined) {
      return events.filter((event) => {
        const begin = m(event.date_begin, 'YYYY/MM/DD HH:mm:ss');
        const end = m(event.date_end, 'YYYY/MM/DD HH:mm:ss');
        const finished = end.isSameOrBefore(startTime);
        const toStart = begin.isSameOrAfter(endTime);
        return !finished && !toStart;
      });
    } else {
      return [];
    }
  };

  fixSize(array, targetSize) {
    if (array.length > 0) {
      let adding_index = 0;
      while (targetSize > array.length) {
        var lastNumber = array[array.length - 1];
       for(var i = 0; i < array.length; i++){
           if(array[i].number === lastNumber.number && (i+1) < array.length){
               var obj = array[i+1];
               break;
           }else if((i+1) >= array.length){
                obj = array[0];
                break;
           }
       }
        if (obj !== undefined) {
          const cloneObj = JSON.parse(JSON.stringify(obj));
          array.push(cloneObj);
        }
      }
    }
  }
}

export default EpgResponseFactory;
