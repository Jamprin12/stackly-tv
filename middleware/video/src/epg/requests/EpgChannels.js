import AbstractRequestTask from '../../requests/AbstractRequestTask';
import config from '../../config';

class epgChannels extends AbstractRequestTask {

  constructor(req, params, config) {
    super(req);
    this.config = config;
    this.params = params;
  }

  getUrl() {
    var url=`${this.microfwk}/services/epg/channel`;
    console.log('[REQUEST] -- epgChannels --',url)
    return url         
    // return `${config.endPoints.microfwk}/services/epg/channel`
  }

  getParams() {
    let params = super.getParams(this.config);
    let result = Object.assign(params, this.params);
    result.tenant_code='netnow'; // GOOSE -- HACK -- tenant_code
    return result;
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default epgChannels;