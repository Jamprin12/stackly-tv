import defaultConfig from '../config';

class AbstractRequestTask {

    constructor(req) {
        // console.log('**|' + this.constructor.name + '.constructor --')
        this.req=req || null
        this.microfwk= (req && req.config) ? req.config.endPoints.microfwk : defaultConfig.endPoints.microfwk
        this.microfwkAPA = (req && req.config) ? req.config.endPoints.microfwkAPA : defaultConfig.endPoints.microfwkAPA
        // console.log('**|' + this.constructor.name + '.microfwk',this.microfwk)
    }

    success(data, b) {
        return true;
    }

    error(data, b) {
        this.reject(data, b);
    }

    parseErrors(err) {
      return err;
    }

    getParams(config = {}) {
        return {
            api_version: config.api_version,
            authpn: config.authpn,
            authpt: config.authpt,
            format: config.format,
            region: config.region,
            device_id: config.device_id,
            device_category: config.device_category,
            device_model: config.device_model,
            device_type: config.device_type,
            device_manufacturer: config.device_manufacturer,
            HKS: config.HKS || config.hks
        };
    }

    getMethod() {
        return 'GET';
    }

    getUrl() {
        return this.url;
    }

    getRequestHeaders(){
        // console.log(defaultConfig)
        if(defaultConfig.defaultHeaders){
            return defaultConfig.defaultHeaders
        }else{
            return {}
        }
        // return {
        //     'partition':'clarobrasil', // 'clarobrasil' 'clarobrasiluat'
        //     'Pragma':'akamai-x-cache-on, akamai-x-cache-remote-on, akamai-x-check-cacheable, akamai-x-get-cache-key, akamai-x-get-nonces, akamai-x-get-true-cache-key, akamai-x-get-client-ip',
        //     'X-Akamai-Debug':'CDHE-56yrt4'
        // };
    }

    getHeaders() {
        return {
            'Access-Control-Allow-Origin': '*'
        };
    }
}

export default AbstractRequestTask;
