import AbstractRequestTask from "./AbstractRequestTask";
import config from "../config/index";

class ItemsTask extends AbstractRequestTask {
  constructor(req, uri, config, params = {}) {
    super(req);

    this.uri = uri;
    this.config = config;
    this.params = params;
  }

  getUrl() {
    var url = `${this.microfwk}${
      this.uri.substr(0, 2) == "//" ? this.uri.slice(1) : this.uri
    }`; // hey, no todos vienen mall... ufa.

    return url;
    // let temp=`${config.endPoints.microfwk}${this.uri.slice(1)}`;
    // return temp;
  }

  // agregado para mycontents, por ahora no rompe nada :|
  getRequestHeaders() {
    return {
      "Access-Control-Allow-Origin": "*",
      Authorization: `bearer ${this.params.user_token}`,
      platform: "netflex",
      partition: "netflexuat",
    };
  }

  getParams() {
    let params = super.getParams(this.config);
    this.params = Object.assign(params, this.params);

    console.log("")
    console.log(
      `(OK)[ITEMSTASK:getParams]{OK}|${this.getUrl()}|${JSON.stringify(params)}`
    );
    console.log("")
    return params;
  }

  success(data, b) {
    this.resolve(data);
  }
}

export default ItemsTask;
