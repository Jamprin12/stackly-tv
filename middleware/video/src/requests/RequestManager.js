import AbstractRequestTask from "./AbstractRequestTask";
import axios from "axios";
import config from "../config";

class RequestManager {
  /**
   * [addRequest send requests]
   * @type AbstractRequestTask requests
   * @return Promise
   */
  // TODO: CHECK HKS param
  static addRequest(request) {
    let params = request.getParams();
    if (typeof params.api_version != "undefined") {
      if (params.region == null) {
        return false;
      }
    }

    let data = request.getParams();
    if (data.isCacheable) {
      delete data.HKS;
      delete data.user_hash;
    }

    delete data.isCacheable;

    const url = request.getUrl();
    const headers = request.getRequestHeaders();

    const options = {
      url: url,
      params: data,
      headers: headers,
      method: request.getMethod(),
      withCredentials: true,
      timeout: 8 * 1000,
      crossDomain: true
    };
    if (config.proxy.enabled) {
      options["proxy"] = {
        host: config.proxy.host,
        port: config.proxy.port
      };
    }

    const axiosRequest = axios.create(options);

    // for console logs
    const keys = Object.keys(data);
    const parameters = keys.map(key => {return `${key}=${data[key]}`;}).join("&");
    const lUrl = url + (url.indexOf("?") === -1 ? "?" : "&") + parameters;

    return new Promise((resolve, reject) => {
      return (
        axiosRequest
          .request()
          // DEBUG *******************
          .then(data => {
            // **************************************************
            console.info(`(OK)[REQUEST]{OK}|${lUrl}|${JSON.stringify(options)}`);
            // **************************************************
            return data;
          })
          .then(request.success.bind({ request, resolve, reject })) // return
          .catch(error => {
            // **************************************************
            console.info(`(ERROR)[REQUEST]{ERROR}|${error.errno}|${error.code}|${error.message}|${lUrl}|${JSON.stringify(options)}`);
            // **************************************************
            reject(error);
            request.error.bind({ request, resolve, reject });
          })
      );
    });
  }
}

export default RequestManager;
