var _ = require("lodash");
var defaults = require("./default.js");
console.log(`[middleware] Config file [./default.js]`)

var config = {}
if(process.env.NODE_ENV){
    var extra="./" + (process.env.NODE_ENV.trim() || "development") + ".js";
    console.log(`[middleware] Config file extra [${extra}]`)
    config=require("./" + (process.env.NODE_ENV.trim() || "development") + ".js");
}
var conf=_.merge({}, defaults, config);
console.log(`[middleware] Config `,conf)
module.exports = conf;