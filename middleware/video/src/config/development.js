module.exports = {
  mode: "DEV",
  endPoints: {
    microfwk: 'http://mfwktvnx1-api.clarovideo.net',    // microfwk: "http://mfwkstbhuawei-api.clarovideo.net",
    microfwkAPA: "http://mfwktvnx1-api.clarovideo.net"  // microfwkAPA: "http://apa-api-huawei.clarovideo.net"
  },
  proxy: {
    enabled: false,
    host: "proxuprodbrnet.dlatv.local",
    port: "3128"
  }
};
