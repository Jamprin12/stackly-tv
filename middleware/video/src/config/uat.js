module.exports = {
    mode: "UAT",
    endPoints: {
      microfwk: "http://mfwktvnx1-api.clarovideo.net", // "http://cache.local.mfw:8000", // "http://microfwk-varnish-preprod:8000",
      microfwkAPA: "http://mfwktvnx1-api.clarovideo.net" // "http://apa-api-tv2sony.clarovideo.net"
    },
    proxy: {
      enabled: false,
      host: "",
      port: ""
    }
  };
  