/**
 * Funciones para inyectar cambios en la configuracion
 * al momento de ejecucion de la ruta.
 * Manejan cambios dados por la URL con la que se
 * invoca al middleware
 */

/**
 * Intenta adivinar el entorno por la url
 * con la que se esta ejecutando.
 */
const guessEnvironment = req => {
  const href = req.hostname;
  let env = "desconocido";

  if (href.indexOf("localhost") !== -1) {
    env = "development";
  } else if (href.indexOf("-test") !== -1) {
    env = "test";
  } else if (href.indexOf("-pre") !== -1) {
    env = "pre";
  } else if (href.indexOf("-uat") !== -1) {
    env = "uat";
  } else if (href.indexOf("-prod") !== -1) {
    env = "production";
  } else {
    env = "production";
  }
  return env;
};

/**
 * Devuelve el endpoint a utilizar
 * http://mfwktvnx1-api.clarovideo.net   PROD
 * http://mfwktvnx1-uat-api.clarovideo.net  UAT
 */
export function getApiEndpoint(req) {
  const guessedEnv = guessEnvironment(req);
  req.config.endPoints.guessedEnv = guessedEnv;
  switch (guessedEnv) {
    case "development":
      req.config.endPoints.microfwk = "http://mfwktvnx1-uat-api.clarovideo.net";
      req.config.endPoints.microfwkAPA = "http://mfwktvnx1-uat-api.clarovideo.net";
      break;
    case "test":
      req.config.endPoints.microfwk = "http://mfwktvnx1-uat-api.clarovideo.net";
      req.config.endPoints.microfwkAPA = "http://mfwktvnx1-uat-api.clarovideo.net";
      break;
    case "pre":
      req.config.endPoints.microfwk = "http://mfwktvnx1-uat-api.clarovideo.net";
      req.config.endPoints.microfwkAPA = "http://mfwktvnx1-uat-api.clarovideo.net";
      break;
    case "uat":
      req.config.endPoints.microfwk = "http://mfwktvnx1-uat-api.clarovideo.net";
      req.config.endPoints.microfwkAPA = "http://mfwktvnx1-uat-api.clarovideo.net";
      break;
    case "production":
      req.config.endPoints.microfwk = "http://mfwktvnx1-api.clarovideo.net";
      req.config.endPoints.microfwkAPA = "http://mfwktvnx1-api.clarovideo.net";
      break;
  }
  return guessedEnv;
}
