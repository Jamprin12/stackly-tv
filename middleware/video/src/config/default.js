module.exports = {
  mode: "DEFAULT",
  endPoints: {
    microfwk: "http://mfwktvnx1-api.clarovideo.net", // "http://cache.local.mfw:8000",
    microfwkAPA: "http://mfwktvnx1-api.clarovideo.net"
    // "http://apa-api-tv2sony.clarovideo.net" // microfwk: "http://mfwkstbhuawei-api.clarovideo.net", // microfwkAPA: "http://apa-api-huawei.clarovideo.net"
  },
  proxy: {
    enabled: false,
    host: "",
    port: ""
  },
  defaultHeaders: {
    // partition: "netflexuat", // 'clarobrasil' 'clarobrasiluat'
    // Pragma:
    //   "akamai-x-cache-on, akamai-x-cache-remote-on, akamai-x-check-cacheable, akamai-x-get-cache-key, akamai-x-get-nonces, akamai-x-get-true-cache-key, akamai-x-get-client-ip",
    // "X-Akamai-Debug": "CDHE-56yrt4"
  }
};
