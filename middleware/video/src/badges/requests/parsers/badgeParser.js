function badgeParams(p) {
    return {
        sessionKey: p.sessionKey
    };
}

export default {
    badgeParams
}
