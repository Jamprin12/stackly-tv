import utils from '../../../utils/helpers';

function parseError(error) {
  try {
    let status = null;
    let statusText = null;
    let url = null;
    let completeUrl = null;
    
    if (error.response) {
      status = error.response.status;
      statusText = error.response.statusText;
    }

    url = error.config.url;
    completeUrl = `${url}?${utils.toQueryString(error.config.params)}`;

    return {
      entry: error.config.params,
      errors: [
        {
          error: error.message,
          code: error.code,
          status,
          statusText,
          url,
          completeUrl,
        }],
    };

  } catch (e) {
    const errorData = {
      errors: [
        {
          error: e.message,
          status: 400
        }],
    };
    return errorData;
  }
}

export default {
  parseError
}
