import AbstractRequestTask from '../../requests/AbstractRequestTask';
import config from '../../config';

class AssetsTask extends AbstractRequestTask {
    constructor(req, params, config) {
        super(req);

        this.config = config;
        this.params = params;
    }

    getUrl() {
        var url=`${this.microfwk}/services/apa/asset`;
        console.log('[REQUEST] -- AssetsTask --',url)
        return url        
        //return `${config.endPoints.microfwkAPA}/services/apa/asset`;
        // return `${config.endPoints.microfwk}/services/apa/asset`;
    }

    getParams() {
        let params = super.getParams(this.config);
        this.params = Object.assign(params, this.params);

        return params;
    }

    success(data, b) {
        this.resolve(data);
    }
}

export default AssetsTask;
