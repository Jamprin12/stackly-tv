import AbstractRequestTask from '../../requests/AbstractRequestTask';
import config from '../../config';

class BadgeTask extends AbstractRequestTask {
    constructor(req, params, config) {
        super(req);

        this.config = config;
        this.params = params;
    }

    getUrl() {
        var url=`${this.microfwk}/services/apa/metadata`;
        console.log('[REQUEST] -- BadgeTask --',url)
        return url             
        //return `${config.endPoints.microfwkAPA}/services/apa/metadata`;
        // return `${config.endPoints.microfwk}/services/apa/metadata`;
    }

    getParams() {
        let params = super.getParams(this.config);
        this.params = Object.assign(params, this.params);

        return params;
    }

    success(data, b) {
        this.resolve(data);
    }
}

export default BadgeTask;
