import React from "react";
import { Router } from "express";
import validate from "express-validation";
import { renderToString } from "react-dom/server";

import AssetsTask from "./../requests/AssetsTask";
import Badge from "./../components/badge";
import badgeParser from "./../requests/parsers/badgeParser";
import BadgeTask from "./../requests/BadgeTask";
import errorParser from "./../requests/parsers/errorParser";
import confParser from "./../../helpers/parsers/requiredParams";
import badgeValidation from "./validation/badge";
import RequestManager from "./../../requests/RequestManager";
import Translator from "./../../helpers/utils/Translator";

let assets = null;
let translator = null;
const router = Router(badgeValidation);

router.get("/", validate(badgeValidation), (req, res) => {
  const configParams = confParser.requiredParams(req.query);
  const levelParams = badgeParser.badgeParams(req.query);

  const assetTask = new AssetsTask(req, levelParams, configParams);
  const AssetsRequest = new RequestManager.addRequest(assetTask);

  const badgeTask = new BadgeTask(req, levelParams, configParams);
  const BadgeRequest = new RequestManager.addRequest(badgeTask);

  Promise.all([AssetsRequest, BadgeRequest])
    .then(results => {
      // console.log('\n\n**************************************************\n',result[0])
      // console.log('\n\n**************************************************\n',result[1])
      // console.log('\n\n**************************************************\n')
      const data = results[1].data;

      if (!data || data.errors) {
        res.status(400).json(data);
        return;
      }

      assets = results[0].data;
      translator = new Translator(
        JSON.parse(data.translations).language,
        configParams.region
      );

      const badgesConfig = JSON.parse(data.providers_label_configuration);
      const badgesConfigParsed = parse(badgesConfig);

      res.header("Access-Control-Allow-Origin", "*");
      res.status(200).json(badgesConfigParsed);
    })
    .catch(error => {
      const requestError = errorParser.parseError(error);
      const statusCode = requestError.errors[0].status
        ? requestError.errors[0].status
        : 400;
      res.status(statusCode).json(requestError);
    });
});

function parse(value) {
  if (typeof value === "object") {
    for (const k in value) {
      if (k === "vod" || k === "live") {
        for (let i = 0; i < value[k].length; i++) {
          value[k][i].render = renderToString(
            <Badge
              type={value[k][i].type}
              label={translator.get(value[k][i].text)}
              src={assets[value[k][i].url]}
              style={getStyles(value[k][i])}
            />
          );
        }
      }
      value[k] = parse(value[k]);
    }
  }
  return value;
}

function getStyles(badge) {
  const ignore = ["type", "text", "url"];
  const style = {};

  for (let key in badge) {
    if (ignore.indexOf(key) === -1) {
      if (key === "backgroundColor") {
        if (badge[key].length > 6) {
          let alpha = parseFloat(
            parseInt((parseInt(badge[key].substring(1, 3), 16) / 255) * 1000) /
              1000
          );
          style.opacity = alpha;
          style.backgroundColor = `rgb(${parseInt(
            badge[key].substring(3, 5),
            16
          )}, ${parseInt(badge[key].substring(5, 7), 16)}, ${parseInt(
            badge[key].substring(7, 9),
            16
          )})`;
        }
      } else {
        style[parserBadgesCssProperties(key)] = badge[key];
      }
    }
  }
  return style;
}

function parserBadgesCssProperties(prop) {
  switch (prop) {
    case "gravity":
      return "float";
    case "textColor":
      return "color";
    case "textSize":
      return "fontSize";
    default:
      return prop;
  }
}

module.exports = router;

// NOTE requests
// metadata: https://apa-api-tv2sony.clarovideo.net/services/apa/metadata?HKS=c5bjgkc8u3tb89dmdg6ct31ku2&api_version=v5.6&authpn=amco&authpt=12e4i8l6a581a&device_category=tv&device_manufacturer=sony&device_model=sony&device_type=Workstation&device_id=b36c9915-8528-b31e-804a-f55eddab1e7b&device_name=sony&device_so=2.4.2&region=mexico&format=json&sessionKey=531eed59e4b050ea818ae755-mexico
// assets: https://apa-api-tv2sony.clarovideo.net/services/apa/asset?HKS=c5bjgkc8u3tb89dmdg6ct31ku2&api_version=v5.6&authpn=amco&authpt=12e4i8l6a581a&device_category=tv&device_manufacturer=sony&device_model=sony&device_type=Workstation&device_id=b36c9915-8528-b31e-804a-f55eddab1e7b&device_name=sony&device_so=2.4.2&region=mexico&format=json&sessionKey=531eed59e4b050ea818ae755-mexico
