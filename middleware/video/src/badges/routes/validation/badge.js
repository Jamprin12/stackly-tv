const Joi = require('joi');

module.exports = {
    query: {
        api_version:         Joi.string().required(),
        authpn:              Joi.string().required(),
        authpt:              Joi.string().required(),
        format:              Joi.string().required(),
        region:              Joi.string().required(),
        device_id:           Joi.string().required(),
        device_category:     Joi.string().required(),
        device_model:        Joi.string().required(),
        device_type:         Joi.string().required(),
        device_manufacturer: Joi.string().required(),
        HKS:                 Joi.string().required(),

        sessionKey:          Joi.string().required(),
    }
};
