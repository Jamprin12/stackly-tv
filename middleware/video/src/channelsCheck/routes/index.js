import { Router } from 'express';
import validate from 'express-validation';

import utils from './../../utils/helpers';
import RequestManager from './../../requests/RequestManager';
import levelValidation from './validation/level';
import LevelTask from '../../level/requests/LevelTask';
import ItemsTask from '../../requests/ItemsTask';
import levelParser from '../../level/requests/parsers/levelParser';
import confParser from './../../helpers/parsers/requiredParams';
import levelTransformer from '../../transformers/levelTransformer';
import itemTransformer from '../../transformers/itemTransformer';

import guideTask from '../../level/requests/guideTask';
import EpgChannels from '../../epg/requests/EpgChannels';

const router = Router(levelValidation);

router.get('/', validate(levelValidation), (req, res) => {

  req.query=Object.assign({},{
    HKS: 'MiddlewareAAF',
      api_version: 'v5.8',
      authpn: 'amco',
      authpt: '12e4i8l6a581a',
      device_category: 'stb',
      device_manufacturer: 'huawei',
      device_model: 'EC6108V9',
      device_type: 'ptv',
      device_id: 'MiddlewareAAF',
      device_name: 'MiddlewareAAF',
      device_so: 'Android 4.4.2',
      region: 'argentina',
      format: 'json',
      node: 'nagratv'}
    ,req.query)

  console.log('object',req.query)

  const configParams = confParser.requiredParams(req.query);
  const levelParams = levelParser.levelParams(req.query);

  const guide = new guideTask(req, levelParams, configParams);

  new RequestManager.addRequest(guide).then((result) => {
    {
      if(result.data.response && result.data.response.nodes && result.data.response.nodes[0] && result.data.response.nodes[0].id ) {
        levelParams.node_id =result.data.response.nodes[0].id;
      }
      else
      {
        console.log('result',result.data)
        res.status(400).json({response:{error:'Unable To get NodeID 0 from epg menu',resultFromAPI:result.data}});
        return;
      }
      const levelTask = new LevelTask(req, levelParams, configParams);

      new RequestManager.addRequest(levelTask).then((result) => {
        const data = result.data;
        if (!data || data.errors) {
          res.status(400).json({response:{error:'Error Getting LevelTv',resultFromAPI:data}});
          return;
        }

        let gridType = 'ribbons';
        if (data.response.modules.module.length > 0
          && (data.response.modules.module[0].type !== 'carrousel' && data.response.modules.module[0].type !== 'carrouseldoble')
        ) {
          gridType = 'list';
        }


        const responseData = ( levelData) => {
          let ribbons = data.response.modules.module;

          let components = utils.levelParser(ribbons);
          components = levelTransformer.collection(components);

          const total = components.length;
          const byUser = utils.getByUserTotal(components);

          const flags = utils.getFlags(components);
          const isTvNode = levelParams.isCurrentNodoTV;

          for (let i = 0; i < flags.length; i++) {
            if (isTvNode) {
              levelParams.quantity = 1000;
            }
            const itemsTask = new ItemsTask(req, flags[i].url, configParams, levelParams);

            new RequestManager.addRequest(itemsTask).then((result) => {
              flags[i].flag = true;
              const data = result.data;

              if (!data || data.errors) {
                data.itemTask = itemsTask;
                res.status(400).json(data);
                return;
              }

              const response = data.response.groups || data.response.highlight;

              let params = {};
              if (components[flags[i].index] && components[flags[i].index].type === 'highlight') {
                params.highlight = components[flags[i].index].type === 'highlight';
              }

              components[flags[i].index].items = itemTransformer.collection(response, params);

              const launchResponse = utils.launchResponse(flags);

              if (launchResponse) {
                for (let e = 0; e < components.length; e++) {
                  if (levelParams.user_status === 'anonymous' && components[e].byUser && components[e].items.length === 0) {
                    components.splice(e, 1);
                    e -= 1;
                  }
                }
                //TODO: Revisar como hacer que level indique el número de canal en tv
                if (isTvNode) {
                  let date=(new Date()).toJSON().replace('-','').replace('-','').substring(0,8);
                  const epgParams = {
                    date_from: date+'000000',
                    date_to:  date+'000000',
                    from: '0',
                    quantity: '1000',
                    node_id: levelParams.node_id
                  }

                  const channelsTask = new EpgChannels(epgParams, configParams);

                  new RequestManager.addRequest(channelsTask).then((resultEpg) => {
                    const epgData = resultEpg && resultEpg.data;
                    if (epgData && epgData.status == 0 && epgData.response && epgData.response.channels) {
                      const channels = epgData.response.channels;
                      const sortedItems = [];
                      const items = components[flags[i].index].items;


                      let channelsMap=channels.map(x=>x.group_id);
                      let itemsMap=items.map(x=>x.group_id);
                      let notInEPG=items.filter(x=>channelsMap.indexOf(x.group_id)==-1);
                      let notInLevel=channels.filter(x=>itemsMap.indexOf(x.group_id)==-1);
                      if(notInEPG.length==0 && notInLevel.length==0)
                      {
                        res.status(200).json({
                          response: {status:200, msg:'Exact Match' , numberOfChannels:itemsMap.length}
                        });
                      }
                      else {
                        res.status(500).json({
                          response: {status:500,msg:'Does not match', notInEPG, notInLevel}
                        });
                      }
                    }
                  });
                } else {
                  res.header("Access-Control-Allow-Origin", "*");
                  res.status(200).json({
                    response: { gridType, total, byUser, components }
                  });
                }
              }
            });
          }
        }
          responseData( data);

      });}
  }).catch((err) => {
    res.status(400).json(err);
  })


});

module.exports = router;
