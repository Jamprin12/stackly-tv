import { Router }             from 'express';
import validate               from 'express-validation';
import axios                  from 'axios';
import ManifestUtils                  from '../../utils/ManifestUtils';
import Minimize               from 'minimize';
import manifestLanguagesValidationRules from './validation/manifest-languages';

const router = new Router();

router.get("/", validate(manifestLanguagesValidationRules), (req, res)  => {
  const {manifest} = req.query;
  axios.get(manifest).then((result) => {
    const xml = result.data;
    const object = ManifestUtils.xmlToObject(xml);
    let langs = ManifestUtils.getLanguages(object);
    res = ManifestUtils.setCacheHeader(res);
    res.set('Content-Type', 'text/json');
    res.set('Access-Control-Allow-Origin', '*');
    res.json({languages: langs});
  });
  
});

export default router;