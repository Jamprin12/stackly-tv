import Joi from 'joi';

export default  {
  query: {
    manifest  : Joi.string().required()
  }
};
