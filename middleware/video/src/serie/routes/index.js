/**
 * Series
 */
import { Router } from "express";
import validate from "express-validation";
import get from "lodash/get";
import map from "lodash/map";

import routeValidation from "./routeValidation";

import RequestManager from "./../../requests/RequestManager";
import SerieTask from "./../requests/serieTask";
import SeasonTask from "./../requests/seasonTask";

const logHead = "[SERIE]";

const getSerie = async (req, query) => {
  const logFunc = logHead + "[getSerie] -- ";
  console.log(logFunc + "serie_id=", query.serie_id);
  const serieTask = new SerieTask(req, query);
  const result = await new RequestManager.addRequest(serieTask);
  const data = result && result.data;
  if (!data || data.errors) {
    console.log("(WARN)" + logFunc + "ERROR", data.errors);
    return [];
  }
  // console.log(logFunc + "data=", data);
  return get(data, "response.serie", []);
};

const getSeason = async (req, season_id, number) => {
  const logFunc = logHead + "[getSeason] -- ";

  let resultados = {};
  try {
    const params = Object.assign(req.query, {
      from: "0",
      quantity: "100",
      season_id: season_id,
      paging_type: "episode",
    });

    // console.log(logFunc + "params[", number, "]", params);
    const seasonTask = new SeasonTask(req, req.query);
    const result = await new RequestManager.addRequest(seasonTask);

    if (!result.data || result.data.errors) {
      console.log("(WARN)" + logFunc + "ERROR", result.data.errors);
      return [];
    }

    const data = get(result, "data.response.episodes", []);

    Object.keys(data).forEach((ep) => {
      const key = ep;
      const value = data[ep][0]; // viene como un array, si hay mas de uno, uso el primero
      // console.log(logFunc + "*/*", key, typeof value);
      resultados[key] = {
        ...value,
        id: value.id,
        title: value.title_episode,
        original: value,
      };
    });
  } catch (e) {
    resultados = [];
  }
  return resultados;
};

const router = Router(routeValidation);
router.get("/", validate(routeValidation), async (req, res) => {
  const logFunc = logHead + "[/] -- ";
  const initTime = Date.now();
  // const { components, gridType } = await getData({ req, query: req.query });
  const serie = await getSerie(req, req.query);
  // console.log(logHead + "[get] -- serie", serie);

  const midTime = Date.now();

  // console.log(logHead + "[get] -- serie", serie.seasons[0]);
  // serie.seasons = [serie.seasons[0]];

  const llamados = await Promise.all(
    serie.seasons.map((season) => getSeason(req, season.id, season.number))
  );
  // console.log(logFunc + "llamados", llamados);

  serie.seasons.map((s, key) => {
    let episodes = llamados[key];
    if (typeof llamados[key] === "object") {
      episodes = map(llamados[key], (val, id) => {
        return { ...val };
      });
    }

    const result = s;
    result.episodes = episodes;
    return result;
  });

  const final = {
    route: "/serie",
    entry: req.query,
    serie: serie,
  };

  const endTime = Date.now();
  console.log(
    "\n----------------------------\n" + logFunc + ":SPENT -- serie:",
    midTime - initTime,
    " seasons:",
    endTime - midTime,
    "\n--------------------------------"
  );

  res.header("Access-Control-Allow-Origin", "*");
  res.status(200).json({ response: final });
});

export default router;
