const Joi = require('joi');

module.exports = {
    query: {
        api_version:         Joi.string().required(),
        format:              Joi.string().required(),
        region:              Joi.string().required(),
        authpn:              Joi.string().required(),
        authpt:              Joi.string().required(),
        device_type:         Joi.string().required(),
        device_model:        Joi.string().required(),
        device_category:     Joi.string().required(),
        device_manufacturer: Joi.string().required(),
        tenant_code:         Joi.string().required(),
        serie_id:            Joi.string().required(),

        // device_id:           Joi.string().required(),
        // HKS:                 Joi.string().required(),
        // node:                Joi.string().required(),
        // user_status:         Joi.string().required(),
        // first_page:          Joi.bool(),
        // node_id:             Joi.empty('').optional(),
        // show_epg_ribbon:     Joi.bool(),
        // epg_version:         Joi.string().optional(),
        // soa_version:         Joi.string().optional()
    }
};

/*
api_version=v6&
format=json&
region=brasil&
authpn=net&
authpt=5facd9d23d05bb83&
device_type=net&
device_model=android&
device_category=mobile&
device_manufacturer=generic&
tenant_code=netnow&
serie_id=11181
*/
