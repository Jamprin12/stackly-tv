import AbstractRequestTask from '../../requests/AbstractRequestTask';
// import config from '../../config';

class SeasonTask extends AbstractRequestTask {
    constructor(req, params, config) {
        super(req);

        this.config = config;
        this.params = params;
    }

    getUrl() {
        var url=`${this.microfwk}/services/season/external/data`;
        console.log('[REQUEST] -- SeasonTask --',url)
        return url
        // return `${config.endPoints.microfwk}/services/cms/level`;
    }

    getParams() {
        let params = super.getParams(this.config);
        this.params = Object.assign(params, this.params);

        return params;
    }

    success(data, b) {
        this.resolve(data);
    }
}

export default SeasonTask;