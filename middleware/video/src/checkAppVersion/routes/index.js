import { Router } from 'express';
import validate from 'express-validation';
import pkg from '../../../../../package'
import versionValidation from './validation/version';
const router = Router(versionValidation);

router.get('/', validate(versionValidation), (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.status(200).json({
    response: { appVersion:pkg.version}
  });
});

module.exports = router;
