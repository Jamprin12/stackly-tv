import { Router } from "express";
import axios from "axios";
import config from "../../config";

const redis = require("redis");
const router = new Router();

const PORT_REDIS = 8102;

function getApiEndPoint(req) {
  let microfwk = "***";
  try {
    microfwk =
      req && req.config
        ? req.config.endPoints.microfwk
        : config.endPoints.microfwk;
  } catch (error) {
    console.log(
      "[CHECKHEALTH]--getApiEndPoint-->",
      error.code,
      "|",
      error.name,
      "|",
      error.message
    );
  }
  console.log("[CHECKHEALTH][getApiEndPoint] = ", microfwk);
  return microfwk;
}

function validateEnviroment(req, res, next) {
  console.log("[CHECKHEALTH][validateEnviroment]");
  try {
    if (process.env.NODE_ENV === "develop") {
      res.locals.node1 = "10.10.0.87";
      res.locals.node2 = "10.10.0.88";
    } else {
      res.locals.node1 = process.env.DYNOMITE_ENDPOINT1
        ? process.env.DYNOMITE_ENDPOINT1
        : "redis-fe-tvstb-01";
      res.locals.node2 = process.env.DYNOMITE_ENDPOINT2
        ? process.env.DYNOMITE_ENDPOINT2
        : "redis-fe-tvstb-02";
    }
  } catch (error) {
    console.error("[CHECKHEALTH][validateEnviroment] ERROR\n", error);
  }
  next();
}

function statusNodeOne(req, res, next) {
  console.log("[CHECKHEALTH][statusNodeOne]");
  try {
    const client_one = redis.createClient(PORT_REDIS, res.locals.node1);
    client_one.on("connect", function() {
      res.locals.node_one_status = true;
      client_one.quit();
      next();
    });
    client_one.on("error", function(error) {
      console.error(
        "[CHECKHEALTH][statusNodeOne] ERROR axios |",
        error.code,
        " | ",
        error.name,
        " | ",
        error.message
      );
      res.locals.node_one_status = false;
      client_one.quit();
      next();
    });
  } catch (error) {
    console.error("[CHECKHEALTH][statusNodeOne] ERROR\n", error);
  }
}

function statusNodeTwo(req, res, next) {
  console.log("[CHECKHEALTH][statusNodeTwo]");
  try {
    const client_two = redis.createClient(PORT_REDIS, res.locals.node2);
    client_two.on("connect", function() {
      res.locals.node_two_status = true;
      client_two.quit();
      next();
    });
    client_two.on("error", function(error) {
      console.error(
        "[CHECKHEALTH][statusNodeTwo] ERROR axios |",
        error.code,
        " | ",
        error.name,
        " | ",
        error.message
      );
      res.locals.node_two_status = false;
      client_two.quit();
      next();
    });
  } catch (error) {
    console.error("[CHECKHEALTH][statusNodeTwo] ERROR\n", error);
  }
}

function checkDynomite(req, res, next) {
  console.log("[CHECKHEALTH][checkDynomite]");
  try {
    let status_general_redis = false;
    if (res.locals.node_one_status || res.locals.node_two_status) {
      status_general_redis = true;
      res.locals.isDynomiteAvailable = true;
    } else {
      res.locals.isDynomiteAvailable = false;
    }
    const dynomite_redis = {
      details: {
        node1: {
          host: res.locals.node1,
          connected: res.locals.node_one_status
        },
        node2: {
          host: res.locals.node2,
          connected: res.locals.node_two_status
        }
      },
      status: status_general_redis
    };
    res.locals.dynomite_redis = dynomite_redis;
    next();
  } catch (error) {
    console.error("[CHECKHEALTH][checkDynomite] ERROR\n", error);
  }
}

function checkMicrofwkAPA(req, res, next) {
  console.log("[CHECKHEALTH][checkMicrofwkAPA]");
  try {
    const url = getApiEndPoint(req) + "/check_health.php"; // + "/services/apa/metadata";
    axios
      .get(url)
      .then(response => {
        res.locals.isMicrofwkAPAAvailable = true;
        next();
      }) 
      // .then(resp => {
      //   if (resp.data.error.code === "401")
      //     res.locals.isMicrofwkAPAAvailable = true;
      //   next();
      // })
      .catch(error => {
        console.error(
          `[CHECKHEALTH][checkMicrofwkAPA] ERROR axios | url=${url} > ${error.code} | ${error.name} | ${error.message} `
        );
        res.locals.isMicrofwkAPAAvailable = false;
        next();
      });
  } catch (error) {
    console.error(
      `[CHECKHEALTH][checkMicrofwkAPA] ERROR | url=${url} > ${error.code} | ${error.name} | ${error.message} `
    );
  }
}

function checkMicrofwk(req, res, next) {
  console.log("[CHECKHEALTH][checkMicrofwk]");
  try {
    const url = getApiEndPoint(req) + "/check_health.php"; // + "/services/apa/metadata";
    axios
      .get(url)
      .then(response => {
        res.locals.isMicrofwkAvailable = true;
        next();
      })
      .catch(error => {
        console.error(
          `[CHECKHEALTH][checkMicrofwk] ERROR axios | url=${url} > ${error.code} | ${error.name} | ${error.message} `
        );
        res.locals.isMicrofwkAvailable = false;
        next();
      });
  } catch (error) {
    console.error(
      `[CHECKHEALTH][checkMicrofwk] ERROR | url=${url} > ${error.code} | ${error.name} | ${error.message} `
    );
  }
}

function responseData(req, res, next) {
  // let dynomite_redis = res.locals.dynomite_redis;
  let details = {
    microfwkAPA: {
      service: getApiEndPoint(req) + "/check_health.php", // `${config.endPoints.microfwkAPA}/services/apa/metadata`,
      status: res.locals.isMicrofwkAPAAvailable
    },
    microfwk: {
      service: getApiEndPoint(req) + "/check_health.php", // `${config.endPoints.microfwk}/services/apa/metadata`,
      status: res.locals.isMicrofwkAvailable
    }
    // dynomite_redis
  };

  if (
    // !res.locals.isDynomiteAvailable ||
    !res.locals.isMicrofwkAPAAvailable ||
    !res.locals.isMicrofwkAvailable
  ) {
    res.locals.code = 500;
    res.locals.status = false;
  } else {
    res.locals.code = 200;
    res.locals.status = true;
  }

  res.locals.details = details;
  next();
}

const healthMiddleware = [
  // validateEnviroment,
  // statusNodeOne,
  // statusNodeTwo,
  // checkDynomite,
  checkMicrofwkAPA,
  checkMicrofwk,
  responseData
];

router.get("/", healthMiddleware, (req, res) => {
  res.set("Access-Control-Allow-Origin", "*");
  const details = res.locals.details;
  res.status(res.locals.code).json({ status: res.locals.status, details });
});

router.head("/", healthMiddleware, (req, res) => {
  const details = res.locals.details;
  res.status(res.locals.code).json({ status: res.locals.status, details });
});

export default router;
