function levelUserParams(p) {
  return {
    node: p.node,
    user_status: p.user_status,
    user_hash: p.user_hash,
    lasttouch: p.lasttouch,
    user_token: p.user_token,
    tenant_code: p.tenant_code // goose: agrego tenant code
  };
}

export default {
  levelUserParams
};
