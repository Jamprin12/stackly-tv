function levelUserFilterListParams(p) {
  if(p.filterlist)
    return {
      filterlist: p.filterlist,
    };
  else
    return null;
}

export default {
  levelUserFilterListParams
}
