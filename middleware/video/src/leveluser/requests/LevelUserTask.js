import AbstractRequestTask from '../../requests/AbstractRequestTask';
import config from '../../config';

class LevelUserTask extends AbstractRequestTask {
    constructor(req, params, config) {
        super(req);

        this.config = config;
        this.params = params;
    }

    getUrl() {
        var url=`${this.microfwk}/services/cms/leveluser`;
        console.log('[REQUEST] -- LevelUserTask --',url)
        return url
        // return `${config.endPoints.microfwk}/services/cms/leveluser`;
    }

    getParams() {
      let params = super.getParams(this.config);
      let timestamp = (new Date()).getTime();
      this.params = Object.assign(params, this.params, { "timestamp": timestamp });
      
      return params;
    }

    success(data, b) {
        this.resolve(data);
    }
}

export default LevelUserTask;
