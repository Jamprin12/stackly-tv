﻿import { Router } from "express";
import validate from "express-validation";

import confParser from "./../../helpers/parsers/requiredParams";
import ItemsTask from "../../requests/ItemsTask";
import itemTransformer from "../../transformers/itemTransformer";
import levelTransformer from "./../../transformers/levelTransformer";
import LevelUserTask from "./../requests/LevelUserTask";
import levelUserParser from "./../requests/parsers/levelUserParser";
import levelUserFilterListParser from "./../requests/parsers/levelUserFilterListParser";
import levelValidation from "./validation/level";
import RequestManager from "./../../requests/RequestManager";
import utils from "./../../utils/helpers";

import { getItemProperties } from "../../level/routes/index2";

const router = Router(levelValidation);

const getComponents = async (req, configParams, levelUserParams, carrouseles) => {
  let components = utils.levelParser(carrouseles);
  components = levelTransformer.collection(components);

  // filtro por si hay mas de uno, pero solo se va a utilizar el primero
  const noRecommenders = components.filter(
    (item) => item.id.toLowerCase().indexOf("recomendador") === -1
  );
  const itemsTask = new ItemsTask(
    req,
    noRecommenders[0].url,
    configParams,
    levelUserParams
  );
  const contAssistindo = await new RequestManager.addRequest(itemsTask);
  if (!contAssistindo || !contAssistindo.data || contAssistindo.data.errors) {
    return [];
  }
  // ---

  const resultFinal = [];
  resultFinal.push({
    id: noRecommenders[0].id,
    title: noRecommenders[0].title,
    items: contAssistindo.data.response.groups,
  });

  // los recomendados, busco la info reccoriendo los items de assistindo
  const recommenders = components.filter(
    (item) => item.id.toLowerCase().indexOf("recomendador") > -1
  );
  await Promise.all(
    recommenders.map(async (category, i) => {
      const group_id =
        resultFinal[0].items[i].group_id || resultFinal[0].items[i].id;
      const siblingTitle = resultFinal[0].items[i].title;

      const itemsTask = new ItemsTask(req, category.url, configParams, {
        ...levelUserParams,
        group_id,
      });

      const result = await new RequestManager.addRequest(itemsTask);
      if (!result || !result.data || result.data.errors) {
        return;
      }

      const response =
        result.data.response.groups || result.data.response.highlight;

      resultFinal.push({
        id: category.id,
        title: `Por que você assistiu ${siblingTitle}, nós recomendamos você`,
        items: response.map((item) => {
          return getItemProperties({
            item,
            version: configParams.api_version,
            category,
          });
        }),
      });
    })
  );

  return resultFinal;
};

router.get("/", validate(levelValidation), async (req, res) => {
  const configParams = confParser.requiredParams(req.query);
  let levelUserParams = levelUserParser.levelUserParams(req.query);

  const levelUserFilterList = levelUserFilterListParser.levelUserFilterListParams(
    req.query
  );

  if (levelUserFilterList) {
    levelUserParams = Object.assign(levelUserFilterList, levelUserParams);
  }

  const levelUserTask = new LevelUserTask(req, levelUserParams, configParams);
  const result = await RequestManager.addRequest(levelUserTask);

  const data = result && result.data;

  if (!data || data.errors) {
    return;
  }

  const components = await getComponents(
    req, 
    configParams,
    levelUserParams,
    data.response.modules.module
  );

  res.header("Access-Control-Allow-Origin", "*");
  res.status(200).json({
    response: {
      byUser: true,
      components: components,
      gridType: "ribbons",
    },
  });
});

module.exports = router;
