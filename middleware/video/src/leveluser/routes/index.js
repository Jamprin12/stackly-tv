﻿import { Router }       from 'express';
import validate         from 'express-validation';

import confParser       from './../../helpers/parsers/requiredParams';
import ItemsTask        from '../../requests/ItemsTask';
import itemTransformer  from '../../transformers/itemTransformer';
import levelTransformer from './../../transformers/levelTransformer';
import errorParser      from './../requests/parsers/errorParser';
import LevelUserTask    from './../requests/LevelUserTask';
import levelUserParser  from './../requests/parsers/levelUserParser';
import levelUserFilterListParser  from './../requests/parsers/levelUserFilterListParser';
import levelValidation  from './validation/level';
import RequestManager   from './../../requests/RequestManager';
import utils            from './../../utils/helpers';

const router = Router(levelValidation);

const logHead = '[LEVEL-USER] -- ';

router.get('/', validate(levelValidation), (req, res) => {

    console.log('[LEVEL-USER] -- Llegue');

    res.header("Access-Control-Allow-Origin", "*");


    const configParams = confParser.requiredParams(req.query);
    let levelUserParams = levelUserParser.levelUserParams(req.query);
    const levelUserFilterList= levelUserFilterListParser.levelUserFilterListParams(req.query);
    if(levelUserFilterList) levelUserParams=Object.assign(levelUserFilterList,levelUserParams);

    const levelUserTask = new LevelUserTask(levelUserParams, configParams);

    console.log(logHead+'levelUserTask URL=',levelUserTask.getUrl());
    console.log(logHead+'levelUserTask levelUserParams=', JSON.stringify(levelUserParams));
    console.log(logHead+'levelUserTask configParams=', JSON.stringify(configParams));

    new RequestManager.addRequest(levelUserTask).then((result) => {
        const data = result.data;

        if (!data || data.errors) {
            console.log(logHead+'ERROR -- principal',data.errors);
            const requestError = errorParser.parseError(result);
            const statusCode = requestError.errors[0].status ? requestError.errors[0].status : 400;
            // res.header("Access-Control-Allow-Origin", "*");
            res.status(statusCode).json(requestError);
            return;
        }

        let ribbons = data.response.modules.module;
        let components = utils.levelParser(ribbons);
        const total = components.length;

        console.log('[LEVEL-USER] -- Procesando',total,' cintas.');
        components = levelTransformer.collection(components);
        const flags = utils.getFlags(components);

        delete configParams['api_version'];
        console.log(logHead+"BORRADO parametro api_version.")

        for (let i = 0; i < flags.length; i++) {
            if (!flags[i].recommender) { // -----------------LA SEEN !!!
              console.log(logHead,i," -- SI soy cinta seen.",flags[i].url)

              levelUserParams = levelUserParser.levelUserParams(req.query);
                if(levelUserFilterList && flags[i].url.indexOf("filterlist") === -1) {
                  levelUserParams = Object.assign(levelUserFilterList, levelUserParams);
                }
                const itemsTask = new ItemsTask(req, flags[i].url, configParams, levelUserParams);
                // console.log(logHead+"****************************************************")
                // console.log(logHead+"LAUNCH! >>\n",flags[i].url, "\n-------\n", configParams, "\n-------\n",levelUserParams)
                // console.log(logHead+"****************************************************")

                new RequestManager.addRequest(itemsTask).then((result) => {
                    const data = result.data;

                    if (!data || data.errors) {
                      console.log(logHead+'[LEVEL-USER] -- ERROR V -- !data || data.errors ', i, data.errors);
                      flags[i].flag = true;
                      // launchResponse(res, flags, {total, components});
                      noItems(res);
                      throw Error('CASO V Thrown')
                      // return;
                    }

                    const response = data.response.groups || data.response.highlight;
                    components[flags[i].index].items = itemTransformer.collection(response);

                    if (flags[i].url.indexOf('user/seen') !== -1) {
                        const seen = components[flags[i].index];

                        for (let i = 0; i < flags.length; i++) {
                          console.log(logHead+"Internal loop ",i, flags[i].url)

                            levelUserParams = levelUserParser.levelUserParams(req.query);

                            if(levelUserFilterList && flags[i].url.indexOf("filterlist") === -1) {
                              levelUserParams = Object.assign(levelUserFilterList, levelUserParams);
                            }

                            if (flags[i].recommender && flags[i].url.indexOf('seenrecommendations') >= 0) { // ---------------------
                              // console.log(logHead+"@@@@@ seenrecommendations >= 0", i, flags[i].url)
                              const itemsTask = new ItemsTask(req, flags[i].url, configParams, levelUserParams);
                              new RequestManager.addRequest(itemsTask).then((result) => {
                                  const data = result.data;

                                  if (!data || data.errors) {
                                    console.log(logHead+'[LEVEL-USER] -- ERROR -- flagB', i, flags[i].url, data.errors);
                                    return;
                                  }

                                  const items = data.response.items;
                                  const itemsParsed = [];

                                  for (let e = 0; e < items.length; e++) {
                                      if (items[e].recommendation && items[e].recommendation.length > 0) {
                                          let item = items[e].recommendation[0];
                                          item = itemTransformer.item(item);

                                          if (items[e].seen && items[e].seen.image_medium) {
                                              item.sibling= items[e].seen.image_medium;
                                          }

                                          itemsParsed.push(item);
                                      }
                                  }

                                  components[flags[i].index].title = 'Por que viste, te recomendamos';
                                  components[flags[i].index].items = itemsParsed;

                                  flags[i].flag = true;  // GOOSE!
                                  console.log(logHead+'CASO W !!!!!!!!!!!!')
                                  launchResponse(res, flags, {total, components})
                              }).catch((error)=>{
                                console.log(logHead+'NUEVO ERROR [A] !!!!! ', error)
                              });

                            } 
                            else if (flags[i].recommender && flags[i].url.indexOf('seenrecommendations') < 0)   // ---------------------
                            {
                              // console.log(logHead+"@@@@@ seenrecommendations < 0", i, flags[i].url)
                              if(!seen.items[components[i].offset])
                              {
                                flags[i].flag = true;
                                console.log(logHead+'CASO X !!!!!!!!!!!!')
                                launchResponse(res, flags, {total, components})
                              }
                              else
                              {
                                const group_id = seen.items[components[i].offset].group_id;
                                const siblingCover = seen.items[components[i].offset].portrait;
                                const siblingTitle = seen.items[components[i].offset].title;
                                console.log(logHead+'Recomendation -- group_id', group_id)
                                const newParams = Object.assign({}, levelUserParams, {group_id});
                                const itemsTask = new ItemsTask(req, flags[i].url, configParams, newParams);

                                new RequestManager.addRequest(itemsTask).then((result) => {
                                  flags[i].flag = true;

                                  const data = result.data;

                                  if (!data || data.errors) {
                                    console.error(logHead+'edc:error - 3', flags[i].url);
                                    //res.status(400).json(data);
                                    return;
                                  }

                                  const response = data.response.groups || data.response.highlight;

                                  // TODO where can i get the title?
                                  components[flags[i].index].title = `Por que você assistiu ${siblingTitle}, nós recomendamos você`;
                                  components[flags[i].index].sibling = {cover: siblingCover.replace('https:', 'https:'), title: siblingTitle};
                                  // console.log('[Cambio de en url http a https ]', components[flags[i].index].sibling)
                                  components[flags[i].index].items = itemTransformer.collection(response);

                                  flags[i].flag = true; // GOOSE!!
                                  console.log(logHead+'CASO Y !!!!!!!!!!!!')
                                  launchResponse(res, flags, {total, components})

                                }).catch((error)=>{
                                  console.log(logHead+'NUEVO ERROR [B] !!!!! ',error)
                                });
                              }

                            }
                        }
                    }

                    flags[i].flag = true; // GOOSE!!
                    console.log(logHead+'CASO Z !!!!!!!!!!!!')
                    launchResponse(res, flags, {total, components})

                }).catch((error) => {
                  try{
                    console.log(logHead+"NUEVO ERROR [C] !!!!! cinta=", i, "error=", error.name, error.message)
                    for(var f=0; f<flags.length;f++) flags[f].flag=true; // mando todo como esta
                    console.log(logHead+"NUEVO ERROR [C] !!!!! flags \n", flags)
                    // console.error("ERRORItem Error", requestError.errors[0])
                    noItems(res,components);
                    return;
                  }catch(error){
                    console.log(logHead+'ERROR IN ERROR --', error.name, error.message)
                  }
                });
            }else{
              console.log(logHead,i," -- no soy cinta seen.",flags[i].url)
              // console.log("**\n**\n**\n"); console.log(total, components); console.log("**\n**\n**\n")
            }
        }

        if (flags.length === 0) { //Excepcion para cuando no hay items en la level
          noItems(res,components)
          // res.header("Access-Control-Allow-Origin", "*");
          // res.status(200).json({ response: { byUser: true, components, gridType: "ribbons", total: 0, } });
        }

        console.log(logHead+'FIN Llamado a cintas.')

    })
    .catch(error=>{
      console.log(logHead+"API PANIC -- EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
      console.log(logHead,error);
      console.log(logHead+"API PANIC -- EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
    })

    console.log(logHead+'FIN sincronico.');

});


function launchResponse(res, flags, response) {
  try{
    const launchResponse = utils.launchResponse(flags);
    console.log(logHead+'*** launchResponse should run?', launchResponse)
    if (launchResponse) {
        console.log(logHead+"*** launchResponse launched!");
        res.status(200).json({ response });
        console.log(logHead+"*** launchResponse READY.||");
    }
  }catch(error){
    console.log(logHead+'*** ERROR EN EL launchResponse >> ',error);
  }
}

function noItems(res,components){ //Excepcion para cuando no hay items.
  res.status(200).json({ response: { byUser: true, components, gridType: "ribbons", total: 0 } });
}

// EXPORT
module.exports = router;
