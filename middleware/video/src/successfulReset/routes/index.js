import { Router } from 'express';
import validate from 'express-validation';
import paramsValidation from './validation/params';
const router = Router(paramsValidation);

router.get('/', validate(paramsValidation), (req, res) => {
  const response = { status:'ok'};
  res.header("Access-Control-Allow-Origin", "*");
  res.status(200).json({
    response: response
  });
});

module.exports = router;
