const Joi = require('joi');

module.exports = {
  query: {
    app_version: Joi.string().optional(),
  }
};
