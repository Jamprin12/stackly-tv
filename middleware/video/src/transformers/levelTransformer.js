﻿import Transformer from '../helpers/utils/Transformer';
import Helpers from '../utils/helpers';

class LevelTransformer extends Transformer {
    transform(model) {
        const data = this.getData(model);

        return {
            id: model.name,
            byUser: data.byUser,
            type: data.type,
            title: data.title,
            url: data.url,
            offset: data.offset,
            showFooter: data.showFooter ? data.showFooter : false,
            items: []
        };
    }

    getData(model) {
        const data = {};
        const components = model.components.component;

        components.map((component, index) => {
            const type = component.type;
            const name = component.name;

            if (type === 'SectionHeader') {
                data.title = component.properties.large;
            }

            if (type === 'carrouselfavorited') {
                data.title = component.title;
                data.showFooter = true;
            }

            if (name === 'carrousel'
                || name === 'carrouselizq'
                || name === 'carrouselder'
            ) {
                data.byUser = this.getByUser(component.properties.byuser);
                data.type = this.getCarouselType(component.type);
                data.url = '/' + component.properties.url;
                data.offset = (component.properties && Object.keys(component.properties).indexOf("offset") != -1) ? component.properties.offset : null;
            }
        });
                
        return data;
    }

    getByUser(byUser) {
        if (byUser === 'true') {
            byUser = true;
        } else if (byUser === 'false') {
            byUser = false;
        }

        return byUser
    }

    // 'highlight', 'portrait', 'landscape', 'cd', 'user-profile', 'square'
    getCarouselType(type) {
        switch (type) {
            case 'Highlight':
                type = 'highlight';
                break;
            case 'Carrouselhorizontal':
            case 'Carrouselvertical':
            case 'Carrouselrecommended':
            default:
                type = 'landscape';
                break;
        }

        return type;
    }
}

export default new LevelTransformer();
