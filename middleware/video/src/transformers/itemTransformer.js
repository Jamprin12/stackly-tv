import Transformer from '../helpers/utils/Transformer';
import channels    from '../level/data/channels';

class itemTransformer extends Transformer {
  resizeImage (src, width, height) {
    if (!src) {
      return false
    }
    // let result = src.includes("assetsnx") ? `${src}?size&w=${width}` : src.includes("clarovideocdn") ? width < 500 ? `${src}?size&imwidth=${width}` : `${src}?size=${width}x${height}` : src;
    let result="";

    if(src.indexOf('?')!==-1){
      src=src.substring(0,src.indexOf('?'));
    }

    if(src.includes("assetsnx")){
      result = `${src}?w=${width}`
    }else if(src.includes("clarovideocdn")){
      if(width < 500){
        result = `${src}?imwidth=${width}`
      }else{
        result =`${src}?size=${width}x${height}`
      }
    }else{
      result = src
    }

    return result;
  };

    transform(model, params = {}) {
        const group_id = model.id || model.group_id || '';
        const channel_url = channels[group_id] ? channels[group_id] : null;
        //   console.log("test_model", model)

        //
        // solo para la cinta de Continuar-Assistindo
        // calculo el vistime short
        let vistime_short = null;
        if (model.vistime) {
          try {
            if (model.vistime.last.minutes && model.vistime.duration.minutes) {
              vistime_short = {
                last: model.vistime.last.minutes,
                duration: model.vistime.duration.minutes,
                progess: parseInt((model.vistime.last.minutes / model.vistime.duration.minutes) * 100 )
              };
            }
          } catch (e) {
            vistime_short = null; // por las dudas, si falla algo no mando nada.
          }
        }

        return {
            // ORIGINAL: model,
            group_id,
            cover: model.image_large + "?size&w=290",
            cover_new: model.image_large || model.image_small,
            image_still: model.image_still || model.image_small,
            image_frames: (model.image_frames) ? model.image_frames : null,
            image_background: model.image_background || model.image_large || null,
            image_background_new: this.getImageBackgroundNew(model),
            image_highlight: (model.image_highlight) ? model.image_highlight : null,
            portrait: model.image_medium,

            cover: this.resizeImage(model.image_large, 290),
            image_background: this.resizeImage((model.image_background || model.image_large || null), 1280, 720),

            title: model.title || '',
            year: model.year || null,
            rating: this.getRating(model),
            description: this.getDescription(model, params.highlight),
            large_description: this.getDescriptionLarge(model, params.highlight),
            provider: this.getProvider(model.proveedor_name),
            type: this.getType(model),
            format_types: model.format_types ? model.format_types.split(',') : model.format_types,
            href: this.getHref(model),
            channel_url,
            adUrl: this.getAdUrl(model, params.highlight),
            ads: model.type === 'app_behaviour' ? 'app_behaviour': null,
            live_enabled: model.live_enabled,
            timeshift: model.timeshift ? Math.floor(model.timeshift/60) : 0,
            channel_number: model.channel_number ? model.channel_number : null,
            // vistime: model.vistime ? model.vistime : null,
            vistime_short: vistime_short,
            season_number: model.season_number ? model.season_number : null,
            episode_number: model.episode_number ? model.episode_number : null,
            title_episode: model.title_episode ? model.title_episode : null,
        };
    }

    getRating(model){
      let rating=null
      if(model.external){
        if(model.external.gracenote){
          rating=model.external.gracenote.rating_classind;
        }else{
          // console.log('getRating -- gracenote not found in',model.id || null)
        }
      }else{
        // console.log('getRating -- external not found in',model.id || null);
      }
      return rating;
    }

    getImageBackgroundNew(model){
      let result =  (model.image_background) ? model.image_background : null // el original
      if(model.type=='node') result = result || model.image_large || null;
      return result; 
    }

    getType(model){
      let result = model.live_enabled === '1' ? 'live' : 'vod' // el original
      if(model.type=='node') result='node';
      return result;
    }

    getProvider(provider) {
        return (provider === 'AMCO')
            ? 'default'
            : provider || ''
    }

    getHref(model) {
        let href = null;

        switch (model.type) {
            case 'group':
                href = `/vcard/${model.id || model.group_id}`;
                break;
            case 'node':
                href = `/node/${model.section}`;
                break;
            case 'special':
                href = `/special/${model.special}`;
                break;
            case 'live':
                href = `/player/${model.group_id || model.id}`;
                break;
            default:
                href = '';
                break;
        }

        if(model && model.type === 'app_behaviour') href = '#';

        return href;
    }

    getCover(model, highlight) {
        let cover = null;

        if (highlight) {
            cover = model.image_highlight || '';
        } else {
            cover = model.image_large || '';
        }

        return cover;
    }

    getDescription(model, highlight) {
        if (highlight) {
            return model.short_description || model.description || model.crest || null;
            // return model.crest || null;
        } else {
            return model.short_description || model.description || model.crest || null;
        }

    }
    getDescriptionLarge(model, highlight) {
        if (highlight) {
            // console.log('////////////////////////////////////', model);
            // return model.description_large || null;
            return model.crest || null;
        } else {
            // console.log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$', model.description_large);
            // return model.description || null;
            return model.description_large || null;
        }
    }



    getAppBehaviour(model) {
        if (typeof model.app_behaviour !== 'string' || model.app_behaviour === 'none') {
            return null;
        } else {
            try {
                return JSON.parse(model.app_behaviour);
            } catch (e) {
                return null;
            }
        }
    }

    getAdUrl(model, isHighlight) {
        if (isHighlight) {
            const dfpAdUrl = 'https://pubads.g.doubleclick.net/gampad/adx';
            const appBehaviour = this.getAppBehaviour(model);
            if (appBehaviour !== null && typeof appBehaviour === 'object') {
                const iu = appBehaviour.ad_id;
                const width = appBehaviour.ad_size[0];
                const height = appBehaviour.ad_size[1];
                const sz = `${width}x${height}`;
                return `${dfpAdUrl}?iu=${iu}&sz=${sz}`;
            }
        } else {
            return null;
        }
    }
}

export default new itemTransformer();
