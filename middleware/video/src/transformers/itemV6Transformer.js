import Transformer from "../helpers/utils/Transformer";
import channels from "../level/data/channels";

class itemV6Transformer extends Transformer {

  /**
   * Bucar el objeto real dentro del item
   * @param {object} item Objeto a convertir
   */
  getModel(item) {
    if (item.type && item.type !== null && item[item.type]) {
      // tiene un tipo, lo esperado
      item[item.type].model_type=item.type;
      return item[item.type];
    } else {
      // no sabemos que paso.. viejo?
      return item;
    }
  }

  /**
   *
   * @param {objeto} model
   * @param {parametros} params
   */
  transform(model, params = {}) {

    // console.log("########## itemV6Transformer\n\n", model, "\n\n", params, "\n\n");

    const group_id = model.id || model.group_id || "";
    const channel_url = channels[group_id] ? channels[group_id] : null;
    //   console.log("test_model", model)

    //
    // solo para la cinta de Continuar-Assistindo
    // calculo el vistime short
    let vistime_short = null;
    if (model.vistime) {
      try {
        if (model.vistime.last.minutes && model.vistime.duration.minutes) {
          vistime_short = {
            last: model.vistime.last.minutes,
            duration: model.vistime.duration.minutes,
            progess: parseInt(
              (model.vistime.last.minutes / model.vistime.duration.minutes) *
                100
            )
          };
        }
      } catch (e) {
        vistime_short = null; // por las dudas, si falla algo no mando nada.
      }
    }

    return {
      // ORIGINAL: model,


      group_id: model.id,
      cover_orig: model.image_large,
      cover: this.getCover(model), //model.image_large + "?size&w=290" || null,
      image_still: model.image_still || model.image_small,
      image_frames: model.image_frames || null,
      image_background: model.image_background || model.image_large || null,
      image_highlight: model.image_highlight || null,
      portrait: model.image_medium || null,

      title: model.title || "",
      year: model.year || null,
      rating: this.getRating(model),

      description: this.getDescription(model, params.highlight),
      large_description: this.getDescriptionLarge(model, params.highlight),
      provider: this.getProvider(model.proveedor_name),

      // EN VIVO
      type: this.getType(model),


      // ?? NO SABEMOS SI SE USA
      format_types: model.format_types ? model.format_types.split(",") : model.format_types,
      href: model.model_type === 'node' ? `/node/${model.node}` : null,
      // href: this.getHref(model), // se genera desde la APP
      // channel_url: model.channel_url, // no trae nada
      // adUrl: this.getAdUrl(model, params.highlight), no se usa
      // ads: model.type === "app_behaviour" ? "app_behaviour" : null,  // no trae nunca nada. Goose.
      live_enabled: model.live_enabled,
      timeshift: model.timeshift ? Math.floor(model.timeshift / 60) : 0,


      channel_number: model.channel_number ? model.channel_number : null,

      // para Ao Vivo
      vistime_short: vistime_short,

      // nuevos
      duration: model.duration,
      media: this.getMedia(model),
      genre: this.getGenres(model),

      // SERIES
      title_episode: model.title_episode ? model.title_episode : null,
      season_number: model.season_number ? model.season_number : null,
      episode_number: model.episode_number ? model.episode_number : null,
    };
  }

  getRating(model){
    let rating=null
    if(model.external){
      if(model.external.gracenote){
        rating=model.external.gracenote.rating_classind;
      }else{
        // console.log('getRating -- gracenote not found in',model.id || null)
      }
    }else{
      // console.log('getRating -- external not found in',model.id || null);
    }
    return rating;
  }

  getType(model){
    let result = model.live_enabled === '1' ? 'live' : 'vod' // el original
    if(model.type=='node') result='node';
    return result;
  }

  getProvider(provider) {
    return provider === "AMCO" ? "default" : provider || "";
  }

    
  resizeImage (src, width, height) {
    // let result = src.includes("assetsnx") ? `${src}?size&w=${width}` : src.includes("clarovideocdn") ? width < 500 ? `${src}?size&imwidth=${width}` : `${src}?size=${width}x${height}` : src;
    let result="";

    if(src.indexOf('?')!==-1){
      src=src.substring(0,src.indexOf('?'));
    }

    if(src.includes("assetsnx")){
      result = `${src}?w=${width}`
    }else if(src.includes("clarovideocdn")){
      if(width < 500){
        result = `${src}?imwidth=${width}`
      }else{
        result =`${src}?size=${width}x${height}`
      }
    }else{
      result = src
    }

    return result;
  };


  getBackground(model) {
    let back = model.image_large || "";

    if(back.length){
      // if(back.indexOf('?')===-1) cover+='?'; // si no tiene parametros
      // back+= "&w=290"; // solo si tiene algo
      back=this.resizeImage(back,1280,72)
    }

    return back;
  }

  getCover(model, highlight) {
    let cover = null;

    if (highlight) {
      cover = model.image_highlight || "";
    } else {
      cover = model.image_large || "";
    }

    if(cover.length){
      // if(cover.indexOf('?')===-1) cover+='?'; // si no tiene parametros
      // cover+= "&w=290"; // solo si tiene algo
      cover=this.resizeImage(cover, 290)
    }

    return cover;
  }

  getDescription(model, highlight) {
    if (highlight) {
      return model.short_description || model.description || null;
      // return model.crest || null;
    } else {
      return model.short_description || model.description || null;
    }
  }

  getDescriptionLarge(model, highlight) {
    if (highlight) {
      // console.log('////////////////////////////////////', model);
      return model.description_large || null;
      // return model.crest || null;
    } else {
      // console.log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$', model.description_large);
      // return model.description || null;
      return model.description_large || null;
    }
  }

  getAppBehaviour(model) {
    if (
      typeof model.app_behaviour !== "string" ||
      model.app_behaviour === "none"
    ) {
      return null;
    } else {
      try {
        return JSON.parse(model.app_behaviour);
      } catch (e) {
        return null;
      }
    }
  }

  getMedia(model){
    var result = model.media || null;
    return result;
  }

  getGenres(model){
    var result=model.genres || null;
    return result;
  }

  // getAdUrl(model, isHighlight) {
  //   if (isHighlight) {
  //     const dfpAdUrl = "https://pubads.g.doubleclick.net/gampad/adx";
  //     const appBehaviour = this.getAppBehaviour(model);
  //     if (appBehaviour !== null && typeof appBehaviour === "object") {
  //       const iu = appBehaviour.ad_id;
  //       const width = appBehaviour.ad_size[0];
  //       const height = appBehaviour.ad_size[1];
  //       const sz = `${width}x${height}`;
  //       return `${dfpAdUrl}?iu=${iu}&sz=${sz}`;
  //     }
  //   } else {
  //     return null;
  //   }
  // }
  
}

export default new itemV6Transformer();
