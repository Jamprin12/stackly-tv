import { Router } from "express";
import validate from "express-validation";

import utils from "./../../utils/helpers";
import RequestManager from "./../../requests/RequestManager";
import levelValidation from "./validation/level";
import LevelTask from "./../requests/LevelTask";
import ItemsTask from "../../requests/ItemsTask";
import levelParser from "./../requests/parsers/levelParser";
import confParser from "./../../helpers/parsers/requiredParams";
import levelTransformer from "../../transformers/levelTransformer";
import itemTransformer from "../../transformers/itemTransformer";
import itemV6Transformer from "../../transformers/itemV6Transformer";
import errorParser from "./../requests/parsers/errorParser";

import guideTask from "./../requests/guideTask";
import EpgChannels from "../../epg/requests/EpgChannels";

var moment = require("moment");
var momentDurationFormatSetup = require("moment-duration-format");

momentDurationFormatSetup(moment);

const router = Router(levelValidation);

function customTemplate() {
  return this.duration._data.minutes == 0 ? "hh[h]" : "hh[h]mm";
}

router.get("/", validate(levelValidation), (req, res) => {
  const configParams = confParser.requiredParams(req.query);
  const levelParams = levelParser.levelParams(req.query);

  console.log(new Date(), "[LEVEL] -- init. configParams >>\n", configParams);
  console.log(new Date(), "[LEVEL] -- init. levelparams >>\n", levelParams);

  if (levelParams.epg_version == "false") {
    delete levelParams.epg_version;
  }
  if (levelParams.soa_version == "false") {
    delete levelParams.soa_version;
  }
  const levelTask = new LevelTask(req, levelParams, configParams);

  new RequestManager.addRequest(levelTask)
    .then(result => {
      const data = result.data;
      if (!data || data.errors) {
        const requestError = errorParser.parseError(result);
        const statusCode = requestError.errors[0].status
          ? requestError.errors[0].status
          : 400;
        res.header("Access-Control-Allow-Origin", "*");
        res.status(statusCode).json(requestError);
        return;
      }
      let gridType = "ribbons";
      if (
        data.response.modules.module.length > 0 &&
        data.response.modules.module[0].type !== "carrousel" &&
        data.response.modules.module[0].type !== "carrouseldoble" &&
        data.response.modules.module[0].type !== "superhighlight_title"
      ) {
        gridType = "list";
      }
      const responseData = (guideTv, levelData) => {
        let ribbons = data.response.modules.module;
        //ribbons = (req.query.first_page) ? ribbons.splice(0, 5) : ribbons.splice(5, ribbons.length - 1);

        //
        // HACK GOOSE!
        // EXCLUIR PROVIDERS DE LA HOME
        //
        if(false){ // working
          for(var i=0;i<ribbons.length;i++){
            if(ribbons[i].name=='providers-netflex'){
              const borrado = ribbons.splice(i,1) // adios!!!!
              console.log('BORRADO!!!!!', borrado)
            }
          }
        }
        //
        //

        let components = utils.levelParser(ribbons);
        components = levelTransformer.collection(components);

        if (guideTv) {
          const guide = utils.parserGuideTv(guideTv);

          components.splice(0, 0, {
            items: guide,
            id: "guia-tv",
            title: "Guía TV",
            type: "landscape"
          });
        }

        const total = components.length;
        const byUser = utils.getByUserTotal(components);

        const flags = utils.getFlags(components);
        const isTvNode = levelParams.isCurrentNodoTV;
        // console.log(new Date(), "[LEVEL] -- isTvNode?", isTvNode);
        // console.log(new Date(), "[LEVEL] -- flags?", flags.length);

        for (let i = 0; i < flags.length; i++) {
          let itemsTask = null;

          if (isTvNode) {
            //
            // isTvNode
            //
            let date = new Date()
              .toJSON()
              .replace("-", "")
              .replace("-", "")
              .substring(0, 8);
            const epgParams = {
              date_from: date + "000000",
              date_to: date + "000000",
              from: "0",
              quantity: "1000",
              node_id: levelParams.node_id,
              metadata: "full" // Force common largo
            };

            if (levelParams.epg_version) {
              epgParams.epg_version = levelParams.epg_version;
            }

            if (levelParams.soa_version) {
              epgParams.soa_version = levelParams.soa_version;
            }

            // console.log(new Date(),"[LEVEL] -- EPG ",i,">>",JSON.stringify(epgParams));
            const channelsTask = new EpgChannels(req, epgParams, configParams);

            new RequestManager.addRequest(channelsTask)
              .then(resultEpg => {
                const epgData = resultEpg && resultEpg.data;
                // console.log(new Date(),"[LEVEL] -- EPG ",i,"<< ",epgData.response.channels.length);

                if (
                  epgData &&
                  epgData.status == 0 &&
                  epgData.response &&
                  epgData.response.channels
                ) {
                  const channels = epgData.response.channels;
                  const sortedItems = [];
                  channels.forEach(channel => {
                    const item = channel.group.common;
                    if (item) {
                      item.id = channel.number;
                      item.group_id = channel.group_id;
                      item.channel_number = channel.number;
                      item.proveedor =
                        channel.group &&
                        channel.group.common &&
                        channel.group.common.extendedcommon &&
                        channel.group.common.extendedcommon.media &&
                        channel.group.common.extendedcommon.media.proveedor;
                      item.cover = channel.group.common.image_small;
                      item.format_types = channel.group.common.extendedcommon.format.types.split(
                        ","
                      );
                      item.href = "";
                      item.image_still = channel.group.common.image_small;
                      item.portrait = channel.group.common.image_medium;
                      item.provider = "default";
                      item.type = "live";
                      item.year = null;
                      item.live_enabled = "1";
                      item.rating =
                        channel.group.common.extendedcommon.media.rating.code;
                      item.timeshift = Math.floor(
                        parseInt(
                          channel.group.common.extendedcommon.media.timeshift,
                          10
                        ) / 60
                      );
                      item.adUrl = null;
                      item.ads = null;
                      item.channel_url = null;

                      item.encodes = [];
                      item.fast_play = null;
                      if (
                        channel.group &&
                        channel.group.common &&
                        channel.group.common.extendedcommon &&
                        channel.group.common.extendedcommon.media &&
                        channel.group.common.extendedcommon.media.language &&
                        channel.group.common.extendedcommon.media.language
                          .options &&
                        channel.group.common.extendedcommon.media.language
                          .options.option &&
                        channel.group.common.extendedcommon.media.language
                          .options.option[0]
                      ) {
                        item.encodes =
                          channel.group.common.extendedcommon.media.language.options.option[0].encodes;
                        item.fast_play =
                          channel.group.common.extendedcommon.media.language.options.option[0].fast_play;
                        delete channel.group.common.extendedcommon;
                      }
                      item.live_url = null;
                      if (channel.epg_url) {
                        item.live_url = channel.epg_url;
                      }
                      item.background =
                        channel.group &&
                        channel.group.common &&
                        channel.group.common.image_background;

                      sortedItems.push(item);
                    }
                  }, this);
                  components[flags[i].index].items = sortedItems;
                  // console.log(new Date(),"[LEVEL] -- EPG ",i," * component..",sortedItems.length);
                }

                // console.log(new Date(),"[LEVEL] -- flag <<",i," -- sending!");
                res.header("Access-Control-Allow-Origin", "*");
                res.header("Max-Age", "3600");
                res.status(200).json({
                  response: { gridType, total, byUser, components }
                });
                // console.log(new Date(), "[LEVEL] -- flag <<", i, " -- sent!");
                // console.log(new Date(),"[LEVEL] -- ----------------------------------------------------------------");
              })
              .catch(error => {
                const requestError = errorParser.parseError(error);
                const statusCode = requestError.errors[0].status
                  ? requestError.errors[0].status
                  : 400;
                // console.error("Item Error -- [tv] flag=",i, requestError.errors[0]);
                res.header("Access-Control-Allow-Origin", "*");
                res.status(statusCode).json(requestError);
              });
          } else {
            //
            // NO ES TV
            //

            //
            // SOLO SI NO ES EPG
            //
            if (flags[i].url.indexOf("/epg/channel") == -1) {
              itemsTask = new ItemsTask(
                req,
                flags[i].url,
                configParams,
                levelParams
              );
              console.log(
                new Date(),
                "[LEVEL] -- flag >>",
                i,
                " URL=",
                itemsTask.getUrl()
              );

              new RequestManager.addRequest(itemsTask)
                .then(result => {
                  // console.log(new Date(),"[LEVEL] -- flag <<",i," -- init >> ",components[flags[i].index].type);

                  flags[i].flag = true;
                  const data = result.data;

                  if (!data || data.errors) {
                    const requestError = errorParser.parseError(result);
                    const statusCode = requestError.errors[0].status
                      ? requestError.errors[0].status
                      : 400;
                    res.header("Access-Control-Allow-Origin", "*");
                    res.status(statusCode).json(requestError);
                    return;
                  }

                  if (configParams.api_version == "v5.86") {
                    const response =
                      data.response.groups || data.response.highlight; // modo 5.87 normal // modo 5.87 hightlight
                    // console.log(new Date(), "[LEVEL] -- flag <<", i, " -- response.");

                    let params = {};
                    if (
                      components[flags[i].index] &&
                      (components[flags[i].index].type === "highlight" ||
                        components[flags[i].index].type ===
                          "superhighlight_title")
                    ) {
                      params.highlight =
                        components[flags[i].index].type === "highlight" ||
                        components[flags[i].index].type ===
                          "superhighlight_title";
                    }

                    components[
                      flags[i].index
                    ].items = itemTransformer.collection(response, params);
                  } else {

                    const response = data.response.items;
                    console.log("SOY V6 !!!!!!!!!!!!!!!!!!!!! >> components[flags[i].index].type",components[flags[i].index].type);
                    // console.log(new Date(), "[LEVEL] -- flag <<", i, " -- response.");

                    let params = {};
                    if ( components[flags[i].index] && (components[flags[i].index].type === "highlight" || components[flags[i].index].type === "superhighlight_title" ) ) {
                      params.highlight = components[flags[i].index].type === "highlight" || components[flags[i].index].type === "superhighlight_title";
                    }
                    components[ flags[i].index ].items = itemV6Transformer.collection(response, params);

                  }

                  const launchResponse = utils.launchResponse(flags);
                  // console.log(new Date(),"[LEVEL] -- flag <<",i," -- launch-response=", launchResponse);
                  if (launchResponse) {
                    // console.log(new Date(),"[LEVEL] -- flag <<",i," -- the last !! init");
                    for (let e = 0; e < components.length; e++) {
                      if (
                        levelParams.user_status === "anonymous" &&
                        components[e].byUser &&
                        components[e].items.length === 0
                      ) {
                        components.splice(e, 1);
                        e -= 1;
                      }
                    }
                    // console.log(new Date(),"[LEVEL] -- flag <<",i," -- the last !! some build.");
                    res.header("Access-Control-Allow-Origin", "*");
                    res.status(200).json({
                      response: { gridType, total, byUser, components }
                    });
                    // console.log(new Date(),"[LEVEL] -- flag <<",i," -- the last !! sent!");
                    // console.log(new Date(),"[LEVEL] -- ----------------------------------------------------------------");
                  }
                })
                .catch(error => {
                  const requestError = errorParser.parseError(error);
                  const statusCode = requestError.errors[0].status
                    ? requestError.errors[0].status
                    : 400;

                  console.error(
                    "Item Error -- [notv] flag=",
                    i,
                    requestError.errors[0]
                  );
                  res.header("Access-Control-Allow-Origin", "*");
                  res.status(statusCode).json(requestError);
                });

              //
              // SI ES EPG
              //
            } else {
              // console.log("[LEVEL] - EVENT! - /epg/channel (!)", flags[i]);
              // const launchResponse = utils.launchResponse(flags);

              // Parametros
              // let special_params={
              //   date_from : new Date().toJSON().replace(/-/g, "").replace(/:/g, "").replace("T", "").substring(0, 12) + "00",
              //   date_to : new Date().toJSON().replace(/-/g, "").replace(/:/g, "").replace("T", "").substring(0, 12) + "05"
              // };
              let special_params = {
                date_from: levelParams.app_now,
                date_to: levelParams.app_now.substring(0, 12) + "05"
              };

              // console.log("-----------------------------------------");
              Object.assign(special_params, levelParams);
              // console.log("[LEVEL] - EVENT! - params=",JSON.stringify(special_params));
              // console.log("-----------------------------------------");

              // Llamado
              itemsTask = new ItemsTask(
                req, 
                flags[i].url,
                configParams,
                special_params
              );
              console.log(
                new Date(),
                "[LEVEL] -- EVENT! >>",
                i,
                " URL=",
                itemsTask.getUrl()
              );
              new RequestManager.addRequest(itemsTask)
                .then(result => {
                  flags[i].flag = true;
                  const data = result.data;

                  if (!data || data.errors) {
                    const requestError = errorParser.parseError(result);
                    const statusCode = requestError.errors[0].status
                      ? requestError.errors[0].status
                      : 400;
                    res.header("Access-Control-Allow-Origin", "*");
                    res.status(statusCode).json(requestError);
                    return;
                  }

                  // console.log("-----------------------------------------");
                  // console.log(new Date(),"[LEVEL] -- flag <<",i," -- init >> ",components[flags[i].index].type);
                  // console.log("[LEVEL] -- EVENT! >> -- entry >> ",data.entry);
                  // console.log("[LEVEL] -- EVENT! >> -- response << ",data.response);

                  let items = data.response.channels.map(i => {
                    let c = i.group.common;
                    let e = i.events[0];
                    return {
                      image_channel: i.image,
                      cover: e.ext_eventimage_name_horizontal, // por ahora, del canal
                      image_background: c.image_background, // por ahora, del canal

                      title: e.name,
                      description: e.description,
                      year: e.ext_year,
                      duration: e.duration,
                      date_begin: e.date_begin,
                      date_end: e.date_end,
                      // date_end_new: moment.duration(advanceResult(e.date_begin, e.date_end, special_params.date_from).end_time, 'minutes').format('hh[h]m', { trim: false }),
                      //date_end_new: moment.duration(advanceResult(e.date_begin, e.date_end, special_params.date_from).end_time, 'minutes').format(customTemplate, { trim: false }),
                      date_end_new: moment(e.date_end)
                        .format("HH:mm")
                        .replace(":", "h")
                        .replace("h00", "h"),
                      date_advance: advance(
                        e.date_begin,
                        e.date_end,
                        special_params.date_from
                      ),

                      group_id: c.id,
                      genres: e.genres

                      //
                      // ------------------
                      //
                      // id: i.id,
                      // number: i.number,
                      // name: i.name,
                      // hd: i.hd,
                      // image: i.image,
                      // group_id: i.group_id,
                      // liveref: i.liveref,
                      // epg_url: i.epg_url,
                      // provider_metadata_id: i.provider_metadata_id,
                      // provider_metadata_name: i.provider_metadata_name,
                      // group:  {
                      //   id: c.id,
                      //   title: c.title,
                      //   title_episode: c.title_episode,
                      //   title_uri: c.title_uri,
                      //   title_original: c.title_original,
                      //   description: c.description,
                      //   description_large: c.description_large,
                      //   short_description: c.short_description,
                      //   image_large: c.image_large,
                      //   image_medium: c.image_medium,
                      //   image_small: c.image_small,
                      //   image_still: c.image_still,
                      //   image_background: c.image_background,
                      //   url_imagen_t1: c.url_imagen_t1,
                      //   url_imagen_t2: c.url_imagen_t2,
                      //   image_base_horizontal: c.image_base_horizontal,
                      //   image_base_vertical: c.image_base_vertical,
                      //   image_base_square: c.image_base_square,
                      //   image_clean_horizontal: c.image_clean_horizontal,
                      //   image_clean_vertical: c.image_clean_vertical,
                      //   image_clean_square: c.image_clean_square,
                      //   image_sprites: c.image_sprites,
                      //   image_frames: c.image_frames,
                      //   image_external: c.image_external,
                      //   duration: c.duration,
                      //   date: c.date,
                      //   year: c.year,
                      //   preview: c.preview,
                      //   season_number: c.season_number,
                      //   episode_number: c.episode_number,
                      //   format_types: c.format_types,
                      //   live_enabled: c.live_enabled,
                      //   live_type: c.live_type,
                      //   live_ref: c.live_ref,
                      //   channel_number: c.channel_number,
                      //   timeshift: c.timeshift,
                      //   votes_average: c.votes_average,
                      //   rating_code: c.rating_code,
                      //   proveedor_name: c.proveedor_name,
                      //   proveedor_code: c.proveedor_code,
                      //   encoder_tecnology: c.encoder_tecnology,
                      //   recorder_technology: c.recorder_technology,
                      //   resource_name: c.resource_name,
                      //   is_series: c.is_series
                      // }, //i.group.common,
                      // event: {
                      //   channel_id: e.channel_id,
                      //   id: e.id,
                      //   name: e.name,
                      //   description: e.description,
                      //   talent: e.talent,
                      //   date_begin: e.date_begin,
                      //   date_end: e.date_end,
                      //   duration: e.duration,
                      //   language: e.language,
                      //   type: e.type,
                      //   group_id: e.group_id,
                      //   confirmado: e.confirmado,
                      //   id_empleado: e.id_empleado,
                      //   tms_id: e.tms_id,
                      //   event_alf_id: e.event_alf_id,
                      //   ext_ncont_id: e.ext_ncont_id,
                      //   ext_nevt_id: e.ext_nevt_id,
                      //   ext_actors: e.ext_actors,
                      //   ext_director: e.ext_director,
                      //   ext_year: e.ext_year,
                      //   ext_country: e.ext_country,
                      //   ext_original_name: e.ext_original_name,
                      //   ext_ep_original_name: e.ext_ep_original_name,
                      //   ext_series_id: e.ext_series_id,
                      //   ext_season_id: e.ext_season_id,
                      //   ext_episode_id: e.ext_episode_id,
                      //   ext_language: e.ext_language,
                      //   ext_serie_short_desc: e.ext_serie_short_desc,
                      //   ext_serie_desc: e.ext_serie_desc,
                      //   image_base_horizontal: e.image_base_horizontal,
                      //   image_base_vertical: e.image_base_vertical,
                      //   image_base_square: e.image_base_square,
                      //   ext_eventimage_name: e.ext_eventimage_name,
                      //   ext_catchup: e.ext_catchup,
                      //   ext_startover: e.ext_startover,
                      //   ext_recordable: e.ext_recordable,
                      //   parental_rating: e.parental_rating,
                      //   aud_stereo: e.aud_stereo,
                      //   aud_dolby: e.aud_dolby,
                      //   vid_black_and_white: e.vid_black_and_white,
                      //   dvb_content: e.dvb_content,
                      //   user_content: e.user_content,
                      //   genres: e.genres,
                      //   group_rel: e.group_rel
                      // }
                    };
                  });

                  components[flags[i].index].type = "events";
                  components[flags[i].index].date_from =
                    special_params.date_from;
                  components[flags[i].index].date_to = special_params.date_to;
                  components[flags[i].index].items = items;
                  // console.log("-----------------------------------------");

                  const launchResponse = utils.launchResponse(flags);
                  // console.log(new Date(),"[LEVEL] -- flag <<",i," -- launch-response.");
                  if (launchResponse) {
                    // console.log(new Date(),"[LEVEL] -- flag <<",i," -- the last !! init");
                    for (let e = 0; e < components.length; e++) {
                      if (
                        levelParams.user_status === "anonymous" &&
                        components[e].byUser &&
                        components[e].items.length === 0
                      ) {
                        components.splice(e, 1);
                        e -= 1;
                      }
                    }
                    // console.log(new Date(),"[LEVEL] -- flag <<",i," -- the last !! some build.");
                    res.header("Access-Control-Allow-Origin", "*");
                    res.status(200).json({
                      response: { gridType, total, byUser, components }
                    });
                    // console.log(new Date(),"[LEVEL] -- flag <<",i," -- the last !! sent!");
                    // console.log(new Date(),"[LEVEL] -- ----------------------------------------------------------------");
                  }
                })
                .catch(error => {
                  console.log(
                    "2EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
                  );
                  console.error(error);
                  console.log(
                    "2EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
                  );
                  res.status(400).json({ salida: "ok" });
                });

              // console.log("-----------------------------------------");
            }
          }
        }

        if (flags.length === 0) {
          //Excepcion para cuando todos los ribbons son de usuario (miscontenidos)
          res.header("Access-Control-Allow-Origin", "*");
          res.status(200).json({
            response: {
              byUser: true,
              components,
              gridType: "ribbons",
              total: 0
            }
          });
        }
      };

      const asyncGuide = (res, levelData) => {
        let guideTv = null;

        if (res && res.data && res.data.response && res.data.response.nodes) {
          guideTv = res.data.response.nodes;
        }

        responseData(guideTv, levelData);
      };
      //TODO this is still needed until APA is loaded with showEpgRibbon key in all devices
      const isHomeUserNode =
        levelParams.node === "home" || levelParams.node === "nagrainicio"; // HACK -- GOOSE -- "homeuser" esto hay que pasarlo a configuracion
      if (
        isHomeUserNode &&
        configParams.device_type === "ptv" &&
        levelParams.show_epg_ribbon
      ) {
        const guide = new guideTask(req, levelParams, configParams);
        new RequestManager.addRequest(guide)
          .then(result => {
            asyncGuide(result, data);
          })
          .catch(err => {
            //console.log('error', err)
            asyncGuide(err, data);
          });
      } else {
        responseData(null, data);
      }
    })
    .catch(e => {
      console.log("ERROR 1 ----------------------------------");
      console.log(e);
      console.log("ERROR 1 ----------------------------------");
    });
});

function advance(since, to, now) {
  let result = {
    total: (strToDate(to).getTime() - strToDate(since).getTime()) / 1000 / 60,
    paso: (strToDate(now).getTime() - strToDate(since).getTime()) / 1000 / 60
  };
  result.porcent = parseInt((result.paso / result.total) * 100);
  // console.log("{ADV} - since,to,now", since, to, now, result);
  return result;
}

function advanceResult(since, to, now) {
  let result = {
    total: (strToDate(to).getTime() - strToDate(since).getTime()) / 1000 / 60,
    paso: (strToDate(now).getTime() - strToDate(since).getTime()) / 1000 / 60
  };
  result.end_time = parseInt(result.total - result.paso);
  return result;
}

function strToDate(string) {
  // string="2020/12/25 13:14:15"
  string = string
    .replace(/\//g, "")
    .replace(/:/g, "")
    .replace(" ", "");
  // console.log("strToDate >>",string, parseInt(string.substring(0,4)), parseInt(string.substring(4,6)), parseInt(string.substring(6,8)), parseInt(string.substring(8,10)), parseInt(string.substring(10,12)), parseInt(string.substring(12,14)) )
  let result = new Date(
    parseInt(string.substring(0, 4)),
    parseInt(string.substring(4, 6)) - 1, // los meses empiezan en 0
    parseInt(string.substring(6, 8)),
    parseInt(string.substring(8, 10)),
    parseInt(string.substring(10, 12)),
    parseInt(string.substring(12, 14))
  );
  // console.log("strToDate >>",string, result.toUTCString() )
  return result;
}

module.exports = router;

// NOTE requests
// highlight https://mfwktv2sony-api.clarovideo.net/services/cms/superhighlight?superhighlight=homeuser&region=mexico&HKS=(tiqlt3rsa2uja3v9qaiusjgjq4)&api_version=v5.5&authpn=amco&authpt=12e4i8l6a581a&device_category=tv&device_manufacturer=sony&device_model=sony&device_type=Workstation&device_id=c5b3da32-ec5e-4a65-8f98-4db945ef4cf9&device_name=sony&device_so=2.4.2&region=mexico&format=json
// level     https://mfwktv2sony-api.clarovideo.net/services/cms/level?HKS=(tiqlt3rsa2uja3v9qaiusjgjq4)&api_version=v5.5&authpn=amco&authpt=12e4i8l6a581a&device_category=tv&device_manufacturer=sony&device_model=sony&device_type=Workstation&device_id=c5b3da32-ec5e-4a65-8f98-4db945ef4cf9&device_name=sony&device_so=2.4.2&region=mexico&format=json&node=homeuser&user_status=anonymous
