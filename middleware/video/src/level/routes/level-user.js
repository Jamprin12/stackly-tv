import confParser from "./../../helpers/parsers/requiredParams";
import ItemsTask from "../../requests/ItemsTask";
import levelTransformer from "./../../transformers/levelTransformer";
import LevelUserTask from "../../leveluser/requests/LevelUserTask";
import levelUserParser from "../../leveluser/requests/parsers/levelUserParser";
import levelUserFilterListParser from "../../leveluser/requests/parsers/levelUserFilterListParser";
import RequestManager from "./../../requests/RequestManager";
import utils from "./../../utils/helpers";

import { getItemProperties } from "./index2";

const MaysPrimera = (siblingTitle) => {
  return siblingTitle.charAt(0).toUpperCase() + siblingTitle.slice(1);
};

const getComponents = async (
  req,
  { configParams, levelUserParams, carrouseles }
) => {
  let components = utils.levelParser(carrouseles);
  components = levelTransformer.collection(components);

  // filtro por si hay mas de uno, pero solo se va a utilizar el primero
  const noRecommenders = components.filter(
    (item) => item.id.toLowerCase().indexOf("recomendador") === -1
  );
  const itemsTask = new ItemsTask(
    req,
    noRecommenders[0].url,
    configParams,
    levelUserParams
  );
  const contAssistindo = await new RequestManager.addRequest(itemsTask);
 if (!contAssistindo || !contAssistindo.data || contAssistindo.data.errors) {
    console.log(`(WARN)[LEVEL-USER:getComponents]{NO-SEEN}`,contAssistindo.data)
    return [];
  }
  // ---

  const resultFinal = [];
  resultFinal.push({
    id: noRecommenders[0].id,
    title: noRecommenders[0].title,
    items: contAssistindo.data.response.groups.map((item) => {
      return getItemProperties({
        item,
        version: configParams.api_version,
        category: noRecommenders[0],
      });
    }),
  });

  // los recomendados, busco la info reccoriendo los items de assistindo
  const recommenders = components.filter(
    (item) => item.url.toLowerCase().indexOf("recommendations") > -1
  );
  await Promise.all(
    recommenders.map(async (category, i) => {
      try{
        const group_id =
          resultFinal[0].items[category.offset].group_id || resultFinal[0].items[category.offset].id;

        const siblingTitle = resultFinal[0].items[category.offset].title;

        const itemsTask = new ItemsTask(req, category.url, configParams, {
          ...levelUserParams,
          group_id,
        });

        const result = await new RequestManager.addRequest(itemsTask);
        if (!result || !result.data || result.data.errors) {
          return;
        }

        const response =
          result.data.response.groups || result.data.response.highlight;

        resultFinal.push({
          id: category.id,
          title: `Por que você assistiu ${MaysPrimera(siblingTitle)}`,
          items: response.map((item) => {
            return getItemProperties({
              item,
              version: configParams.api_version,
              category,
            });
          }),
        });
      }catch(error){
        console.log(`(ERROR)[LEVEL-USER:getComponents][Promise.Recomender.${i}]|offset=${category.offset}|${error.errno}|${error.code}|${error.message}`,)
      }
    })
  );

  return resultFinal;
};

const getData = async (
  req,
  { userHash, app_now, first_page, show_epg_ribbon, ...query }
) => {
  try {
    const configParams = confParser.requiredParams(query);
    let levelUserParams = levelUserParser.levelUserParams(query);

    const levelUserFilterList = levelUserFilterListParser.levelUserFilterListParams(
      query
    );

    const levelUserTask = new LevelUserTask(
      req,
      { ...levelUserParams, ...levelUserFilterList },
      configParams
    );
    const result = await RequestManager.addRequest(levelUserTask);

    const data = result && result.data;

    if (!data || data.errors) {
      return;
    }

    const components = await getComponents(req, {
      configParams,
      levelUserParams,
      carrouseles: data.response.modules.module,
    });

    return components;
  } catch (e) {
    console.log("Error level-user", e.message);
  }

  return [];
};

export default getData;
