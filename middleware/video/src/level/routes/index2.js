import { Router } from "express";
import validate from "express-validation";
import get from "lodash/get";

import utils from "./../../utils/helpers";
import RequestManager from "./../../requests/RequestManager";
import levelValidation from "./validation/level";
import LevelTask from "./../requests/LevelTask";
import ItemsTask from "../../requests/ItemsTask";
import levelParser from "./../requests/parsers/levelParser";
import confParser from "./../../helpers/parsers/requiredParams";
import levelTransformer from "../../transformers/levelTransformer";
import errorParser from "./../requests/parsers/errorParser";

import DataUser from "./level-user";
import DataMyContents from "./level-mycontents";

var moment = require("moment");

const router = Router(levelValidation);

const getItemPropertiesTv = ({
  item = {},
  common = {},
  event = {},
  dateBegin,
  dateEnd,
  dateFrom,
}) => {
  let isSerie = common.is_series;

  let imaFull = isSerie
    ? get(event, "ext_eventimage.cast_ensemble_16_9") !== "" &&
      get(event, "ext_eventimage.cast_ensemble_16_9")
    : get(event, "ext_eventimage.key_art_16_9") !== "" &&
      get(event, "ext_eventimage.key_art_16_9");

  if (!imaFull) {
    imaFull =
      (get(event, "ext_eventimage.cast_ensemble_16_9") !== "" &&
        get(event, "ext_eventimage.cast_ensemble_16_9")) ||
      (get(event, "ext_eventimage.key_art_16_9") !== "" &&
        get(event, "ext_eventimage.key_art_16_9")) ||
      (event.image_clean_horizontal !== "" && event.image_clean_horizontal) ||
      common.image_background;
  }

  //let imaCard = event.image_background || item.image_background;
  let imaCard = get(event, "ext_eventimage.iconic_16_9");
  const description = event.description || item.description;
  const duration = event.duration || item.duration;
  const name = event.name || item.name;

  let genres = [
    get(event, "genres.genre_name"),
    get(event, "genres.sub_genre_name"),
  ];
  genres = genres
    .filter((g) => {
      return g !== "";
    })
    .join(", "); // filtro si no viene nada.

  return {
    image_channel: item.image && resizeImage(item.image, 290),
    imageFull: imaFull && resizeImage(imaFull, 1280, 720),
    imageCard: imaCard && resizeImage(imaCard, 290),
    title: name,
    description: description,
    duration: duration,
    date_begin: dateBegin,
    date_end: dateEnd,
    date_end_new: moment(dateEnd.replace(/\//g, "-").replace(" ", "T"))
      .format("HH:mm")
      .replace(":", "h")
      .replace("h00", "h"),
    date_advance: advance(dateBegin, dateEnd, dateFrom),
    group_id: common.id,
    year: event.ext_year,
    category: genres,
    original: item, // para test
  };
};

const getItemProperties = ({ item, version = "v5.86", category }) => {
  let itemFinal = item;
  if (version === "v6") {
    itemFinal = get(item, item.type);
  }

  // talent
  if (get(itemFinal, "first_name")) {
    const imaCard = get(itemFinal, "image_highlight");

    const title = itemFinal.name
      ? `${itemFinal.name || ""} ${itemFinal.surname || ""}`
      : `${itemFinal.first_name || ""} ${itemFinal.last_name || ""}`;

    return {
      type: "talent",
      internal_ids: itemFinal.id || [],
      group_id: itemFinal.id,
      imageFull: null,
      imageCard: imaCard && resizeImage(imaCard, 110),
      title: title,
    };
  }

  const {
    id,
    group_id,
    type,
    section,
    title,
    description,
    short_description,
    image_large,
    image_background,
    year,
    crest,
    duration,
    node,
    external,
    description_large,
    image_clean_horizontal,
    image_base_horizontal,
    season_number,
    episode_number,
    is_series: isSerie,
  } = itemFinal;

  let genres = get(external, "gracenote.genres", []).slice(0, 2).join(", "); // solo los primeros dos!

  let rating = get(external, "gracenote.rating_classind") || null;

  let imaCard =
    get(external, "gracenote.assets.vod_art_16_9") ||
    get(external, "gracenote.assets.banner_l1_16_9") ||
    image_base_horizontal ||
    image_large;

  let imaFull =
    type == "node"
      ? image_background
      : get(external, "gracenote.assets.key_art_16_9") ||
        get(external, "gracenote.assets.cast_ensemble_16_9") ||
        imaCard ||
        image_background ||
        image_clean_horizontal;

  if (category && category.id === "nx-providers") {
    imaFull = image_background;
  }

  let href = type == "node" ? `/node/${section}` : null;
  if (version === "v6") {
    href = node ? `/node/${node}` : null;
  }

  return {
    group_id: id || group_id || "",
    href: href,
    provider: category && category.id === "nx-providers",
    imageFull: imaFull && resizeImage(imaFull, 1280, 720),
    imageCard: imaCard && resizeImage(imaCard, 290),
    title: title || "",
    year: year || null,
    rating: rating,
    description:
      short_description ||
      description ||
      crest ||
      get(external, "gracenote.description") ||
      description_large,
    duration,
    season: season_number,
    episode: episode_number,
    category: genres,
    original: itemFinal, // para test
  };
};

const resizeImage = (src, width, height) => {
  if (src.indexOf("?") > 0) {
    src = src.substring(0, src.indexOf("?"));
  }

  let result = src.includes("assetsnx")
    ? `${src}?w=${width}`
    : src.includes("clarovideocdn")
    ? width < 500
      ? `${src}?imwidth=${width}`
      : `${src}?size=${width}x${height}`
    : src;

  return result;
};

const responseData = async (
  req,
  ribbons,
  levelParams,
  configParams,
  epgParams
) => {
  let components = utils.levelParser(ribbons);
  components = levelTransformer.collection(components);

  let compoFinal = await Promise.all(
    components.map(async (car) => {
      try {
        if (car.byUser) {
          return {
            id: car.id,
            title: car.title,
            items: [],
          };
        } else {
          if (car.url.indexOf("/epg/channel") == -1) {
            let itemsTask = new ItemsTask(
              req,
              car.url,
              configParams,
              levelParams
            );
            let result = await new RequestManager.addRequest(itemsTask);

            const data = result.data;
            if (!data || data.errors) {
              const origen = `url=${car.url}|configParams=${JSON.stringify(
                configParams
              )}|levelParams=${JSON.stringify(levelParams)}`;
              if (data.errors)
                console.log(
                  `(ERROR)[LEVEL:responseData]{ERROR-DATA}|${origen}|${JSON.stringify(
                    data.errors
                  )}`
                );
              else
                console.log(
                  `(ERROR)[LEVEL:responseData]{ERROR-RESPONSE}|${origen}|${JSON.stringify(
                    result
                  )}`
                );
              return {};
            }

            let items = [];
            if (configParams.api_version !== "v6") {
              items = data.response.groups || data.response.highlight;
            } else {
              items = data.response.items;
            }

            return {
              id: car.id,
              title: car.title,
              type: car.id === "nx-talent" ? "talent" : "",
              items: items.map((item) => {
                return getItemProperties({
                  item,
                  version: configParams.api_version,
                  category: car,
                });
              }),
            };
          } else {
            let now = moment(levelParams.app_now, "YYYYMMDDHHmmss");
            let special_params = {
              ...levelParams,
              filter_id: epgParams.filter_id,
              date_from: now.format("YYYYMMDDHHmmss"),
              date_to: now.add(10, "minutes").format("YYYYMMDDHHmmss"),
            };
            let itemsTask = new ItemsTask(
              req,
              car.url,
              configParams,
              special_params
            );
            let result = await new RequestManager.addRequest(itemsTask);

            const data = result.data;
            if (!data || data.errors) {
              return {};
            }

            return {
              id: car.id,
              title: car.title,
              type: "ao-vivo",
              items: data.response.channels.map((item) => {
                return getItemPropertiesTv({
                  item,
                  common: item.group.common,
                  event: item.events[0],
                  dateBegin: get(item, "events.0.date_begin"),
                  dateEnd: get(item, "events.0.date_end"),
                  dateFrom: special_params.date_from,
                });
              }),
            };
          }
        }
      } catch (error) {
        console.log(
          `(ERROR)[LEVEL:responseData]{ERROR}|${error.errno}|${error.code}|${error.message}`
        );
        return {};
      }
    })
  );

  return (compoFinal && compoFinal.filter((item) => item)) || [];
};

const advance = (since, to, now) => {
  const strToDate = (string) => {
    string = string.replace(/\//g, "").replace(/:/g, "").replace(" ", "");
    let result = new Date(
      parseInt(string.substring(0, 4)),
      parseInt(string.substring(4, 6)) - 1, // los meses empiezan en 0
      parseInt(string.substring(6, 8)),
      parseInt(string.substring(8, 10)),
      parseInt(string.substring(10, 12)),
      parseInt(string.substring(12, 14))
    );
    return result;
  };

  if (strToDate(now).getTime() > strToDate(to).getTime()) {
    return {
      total: (strToDate(to).getTime() - strToDate(since).getTime()) / 1000 / 60,
      paso: (strToDate(to).getTime() - strToDate(since).getTime()) / 1000 / 60,
      porcent: 100,
    };
  }

  let result = {
    total: (strToDate(to).getTime() - strToDate(since).getTime()) / 1000 / 60,
    paso: (strToDate(now).getTime() - strToDate(since).getTime()) / 1000 / 60,
  };
  result.porcent = parseInt((result.paso / result.total) * 100);
  return result;
};

const getData = async ({ req, query }) => {
  try {
    const configParams = confParser.requiredParams(query);
    const levelParams = levelParser.levelParams(query);
    const epgParams = {
      date_from: levelParams.app_now,
      date_to: levelParams.app_now.substring(0, 12) + "05",
      filter_id: query["filter_id"] || "",
    };

    if (levelParams.node === "mycontents") {
      const gridType = "ribbons";
      let components = [];

      try {
        components = await DataMyContents(req, {
          ...query,
          api_version: "v5.83",
        });
      } catch (e) {
        return [];
      }
      return { components, gridType };
    }

    if (levelParams.epg_version == "false") {
      delete levelParams.epg_version;
    }
    if (levelParams.soa_version == "false") {
      delete levelParams.soa_version;
    }

    const levelTask = new LevelTask(req, levelParams, configParams);

    let result = await new RequestManager.addRequest(levelTask);

    const data = result && result.data;
    if (!data || data.errors) {
      return { components: [], gridType: "" };
    }

    let ribbons = data.response.modules.module;

    let gridType = "ribbons";
    if (
      ribbons.length > 0 &&
      ribbons[0].type !== "carrousel" &&
      ribbons[0].type !== "carrouseldoble" &&
      ribbons[0].type !== "superhighlight_title"
    ) {
      gridType = "list";
    }

    /// mezclo los ribbons entre si
    const [noUser, user] = await Promise.all([
      responseData(req, ribbons, levelParams, configParams, epgParams),
      DataUser(req, { ...query, api_version: "v5.86" }),
    ]);

    const components = noUser.map((item) => {
      if (user && user.length) {
        const dataUser = user.find((r) => r.id === item.id);
        if (dataUser) {
          if (item.title) {
            return { ...dataUser, title: item.title };
          } else {
            return dataUser;
          }
        }
      }
      return item;
    });
    // ---------------------------

    return { components, gridType };
  } catch (error) {
    console.log(
      `(ERROR)[LEVEL:getData]{ERROR}|${error.errno}|${error.code}|${error.message}`
    );
    return { components: [], gridType: "" };
  }
};

router.get("/", validate(levelValidation), async (req, res) => {
  const { components, gridType } = await getData({ req, query: req.query });

  const final = {
    gridType,
    total: components.length,
    byUser: utils.getByUserTotal(components || []),
    components,
  };

  res.header("Access-Control-Allow-Origin", "*");
  res.status(200).json({ response: final });
});

export { getItemProperties, getItemPropertiesTv, resizeImage };
export default router;
