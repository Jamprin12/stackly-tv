const Joi = require('joi');

/*
&device_name=sony
&device_so=2.4.2
*/

module.exports = {
    query: {
        api_version:         Joi.string().required(),
        authpn:              Joi.string().required(),
        authpt:              Joi.string().required(),
        format:              Joi.string().required(),
        region:              Joi.string().required(),
        device_id:           Joi.string().required(),
        device_category:     Joi.string().required(),
        device_model:        Joi.string().required(),
        device_type:         Joi.string().required(),
        device_manufacturer: Joi.string().required(),
        HKS:                 Joi.string().required(),

        node:                Joi.string().required(),
        user_status:         Joi.string().required(),

        first_page:          Joi.bool(),
        node_id:             Joi.empty('').optional(),
        show_epg_ribbon:     Joi.bool(),
        epg_version:         Joi.string().optional(),
        soa_version:         Joi.string().optional()
    }
};
