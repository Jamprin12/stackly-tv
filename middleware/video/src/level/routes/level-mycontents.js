import get from "lodash/get";
import _ from "lodash";
import moment from "moment";

import confParser from "./../../helpers/parsers/requiredParams";
import ItemsTask from "../../requests/ItemsTask";
import LevelUserTask from "../../leveluser/requests/LevelUserTask";
import LevelTask from "./../requests/LevelTask";
import levelUserParser from "../../leveluser/requests/parsers/levelUserParser";
import levelUserFilterListParser from "../../leveluser/requests/parsers/levelUserFilterListParser";
import RequestManager from "./../../requests/RequestManager";

import { getItemPropertiesTv, getItemProperties, resizeImage } from "./index2";

const DataMyContents = async (
  req,
  { userHash, first_page, show_epg_ribbon, ...query }
) => {
  try {
    const configParams = confParser.requiredParams(query);
    let levelUserParams = levelUserParser.levelUserParams(query);
    const levelUserFilterList = levelUserFilterListParser.levelUserFilterListParams(
      query
    );

    const levelUserTask = new LevelUserTask(
      req,
      { ...levelUserParams, ...levelUserFilterList },
      configParams
    );
    const levelTask = new LevelTask(
      req,
      { ...levelUserParams, ...levelUserFilterList },
      configParams
    );

    const [levelUser, level] = await Promise.all([
      RequestManager.addRequest(levelUserTask),
      RequestManager.addRequest(levelTask),
    ]);

    const data = get(levelUser, "data");
    if (!data || data.errors) {
      return [];
    }
    const modules = get(data, "response.modules.module", []);

    const llamados = await Promise.all(
      modules.map((m) => shootQuerie(req, m.name, m, query, levelUserParams))
    );

    const levelComponents = get(level, "data.response.modules.module", []);
    const components = levelComponents
      .map((item) => {
        const dataUser = llamados.find((r) => r.id === item.name);
        if (dataUser) {
          if (get(item, "components.component.1.properties.large")) {
            return {
              ...dataUser,
              title: get(item, "components.component.1.properties.large"),
            };
          } else {
            return dataUser;
          }
        }
        return item;
      })
      .filter((i) => i !== null);

    return components;
  } catch (e) {
    return [];
  }
};

const shootQuerie = async (req, type, data, configParams, levelUserParams) => {
  const getItems = async (type) => {
    switch (type) {
      case "nx-user-purchase":
        let items = await getDataPurchase(
          get(data, "components.component[0].properties.url", null),
          req,
          configParams,
          levelUserParams
        );

        return items.map((item) => {
          return getItemProperties({
            item,
          });
        });

      case "nx-user-channels":
        return await getDataChannels(
          get(data, "components.component[0].properties.url", null),
          req,
          configParams,
          levelUserParams
        );

      case "nx-user-provider":
        return await getDataProvider(
          get(data, "components.component[0].properties.url", null),
          req,
          configParams,
          levelUserParams
        );

      default:
        return null;
    }
  };

  return {
    id: data.name,
    type: data.name === "nx-user-channels" ? "ao-vivo" : null,
    title:
      get(data, "components.component.0.title") ||
      get(data, "components.component.0.properties.title") !== "false"
        ? get(data, "components.component.0.properties.title")
        : data.name,
    items: await getItems(type),
  };
};

const getDataPurchase = async (url, req, configParams, levelUserParams) => {
  try {
    const itemsTask = new ItemsTask(req, url, configParams, levelUserParams);
    const result = await RequestManager.addRequest(itemsTask);

    if (result.data.errors) {
      return [];
    }

    return get(result, "data.response.groups", []);
  } catch (e) {
    return [];
  }
};

const getDataChannels = async (url, req, configParams, levelUserParams) => {
  try {
    const itemsTask = new ItemsTask(req, url, configParams, {
      ...configParams,
      api_version: "v5.90",
      epg_version: configParams.filter_id,
      filter_id: null,
      event_date: get(
        configParams,
        "app_now",
        moment().format("YYYYMMDDHHmmss")
      ),
      format: "json",
    });
    const result = await RequestManager.addRequest(itemsTask);

    if (result.data.errors) {
      return [];
    }

    let channels = get(result, "data.response.channels", []);

    // a veces viene un object :(
    if (typeof channels === "object") {
      channels = _.map(channels, (val, id) => {
        return { ...val };
      });
    }

    const final = channels.map((item) => {
      return getItemPropertiesTv({
        item,
        common: item.group.common,
        event: item.events[0],
        dateBegin: get(
          item,
          "events.0.date_begin",
          moment().format("YYYY/MM/DD 00:00:00")
        ),
        dateEnd: get(
          item,
          "events.0.date_end",
          moment().format("YYYY/MM/DD 00:00:00")
        ),
        dateFrom: moment().format("YYYYMMDDHHmmss"),
      });
    });

    return final;
  } catch (e) {
    return [];
  }
};

const getDataProvider = async (url, req, configParams, levelUserParams) => {
  try {
    const itemsTask = new ItemsTask(req, url, configParams, {
      ...levelUserParams,
      api_version: "v1.0",
    });

    const itemsTaskApa = new ItemsTask(
      req,
      "/services/apa/metadata",
      configParams,
      {
        ...levelUserParams,
        sessionKey: configParams.sessionKey,
        // sessionKey: "4dde21173bf1a21b301b1e9f7f1af979-brasil",
      }
    );

    const [result, apa] = await Promise.all([
      RequestManager.addRequest(itemsTask),
      RequestManager.addRequest(itemsTaskApa),
    ]);

    if (result.data.errors || apa.data.errors) {
      return [];
    }

    const response = get(
      result,
      "data.response.result.entitlements",
      []
    ).filter((item) => item.type === "SVOD");
    const responseApa = JSON.parse(get(apa, "data.mi_proveedor", []));

    // filtrado y union de datos
    const dateNow = Date.now();
    const aResponse = [];
    response.map((item) => {
      aResponse[item.key] =
        !item.expiration_date || !item.expiration_date < dateNow;
    });

    const final = responseApa
      .filter((item) => aResponse[item.bss_code])
      .map((item) => {
        return {
          title: item.title,
          href: item.node_name,
          provider: true,
          imageFull:
            item.image_background &&
            resizeImage(item.image_background, 1280, 720),
          imageCard: item.image && resizeImage(item.image, 290),
        };
      });

    return final;
  } catch (e) {
    return [];
  }
};

export default DataMyContents;
