import AbstractRequestTask from '../../requests/AbstractRequestTask';
import config from '../../config';

class guiaTask extends AbstractRequestTask {
    constructor(req, params, config) {
        super(req);

        this.config = config;
        this.params = params;
    }

    getUrl() {
        var url=`${this.microfwk}/services/epg/menu`;
        console.log('[REQUEST] -- guideTask --',url)
        return url        
        // const domain = config.endPoints.microfwk;
        // return `${domain}/services/epg/menu`
    }

    getParams() {
        let params = super.getParams(this.config);
        this.params = Object.assign(params, this.params);

        return params;
    }

    success(data, b) {
        this.resolve(data);
    }
}

export default guiaTask;
