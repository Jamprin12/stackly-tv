function levelParams(p) {
  const params = {
    node: p.node
  };
  if (p.user_status) {
    params["user_status"] = p.user_status;
  }
  if (p.node_id) {
    params["node_id"] = p.node_id;
  }
  if (p.show_epg_ribbon) {
    params["show_epg_ribbon"] = p.show_epg_ribbon;
  }
  if (p.isCurrentNodoTV) {
    params["isCurrentNodoTV"] = p.isCurrentNodoTV;
  }
  if (p.epg_version) {
    params["epg_version"] = p.epg_version;
  }
  if (p.soa_version) {
    params["soa_version"] = p.soa_version;
  }
  if (p.tenant_code) {
    params["tenant_code"] = p.tenant_code;
  }
  if (p.app_now) {
    params["app_now"] = p.app_now;
  }
  // if (p.filter_id) {
  //   params["filter_id"] = p.filter_id;
  // }
  //
  // GOOSE [NX]
  // si es un nodo loco de nx, hay que agregar
  // el parametro para abajo.
  if(p.node.indexOf('nx_')!==-1){
    params["type"]="providers"
  }

  return params;
}

export default {
  levelParams
};
