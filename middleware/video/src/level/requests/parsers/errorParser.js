import utils from '../../../utils/helpers';

function parseError(error) {
  try {
    let status = null;
    let statusText = null;
    let url = null;
    let completeUrl = null;
    let params = {};
    let errorItem = {};
    
    if (error.response) {
      status = error.response.status;
      statusText = error.response.statusText;
    }

    if (error.response && error.response.data && error.response.data.errors) {
        errorItem = error.response.data.errors[0];
    }

    if (utils.isEmpty(errorItem) && error.data && error.data.errors) {
        errorItem = error.data.errors;
    }

    url = error.config.url;
    params = error.config.params;
    completeUrl =  utils.addParamsUrl(url, params);

    return {
      entry: params,
      errors: [
        {
          error: error.message,
          code: error.code,
          status,
          statusText,
          url,
          completeUrl,
          errorItem
        }],
    };

  } catch (e) {
    const errorData = {
      errors: [
        {
          error: e.message,
          status: 400
        }],
    };
    return errorData;
  }
}

export default {
  parseError
}
