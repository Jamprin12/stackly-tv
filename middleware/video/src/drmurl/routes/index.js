import { Router }             from 'express';
import validate               from 'express-validation';
import axios                  from 'axios';
import ManifestUtils          from '../../utils/ManifestUtils';
import Minimize               from 'minimize';

import DRMUrlValidationRules from './validation/drm-url';

const router = new Router();

router.get("/", validate(DRMUrlValidationRules), (req, res)  => {
  const {manifest} = req.query;
  
  axios.get(manifest).then((result) => {
    const xml = result.data;
    const object = ManifestUtils.xmlToObject(xml);
    const protectionObject = ManifestUtils.getProtectionData(object);
    let la_url = protectionObject.DATA.LA_URL;
    res = ManifestUtils.setCacheHeader(res);
    res.set('Content-Type', 'text/json');
    res.set('Access-Control-Allow-Origin', '*');
    res.json({server_url: la_url});
  });
  
});


export default router;
